% Chapter Template
% cSpell:words parencite onfwhitepaper includegraphics resizebox sdncomponents
% linewidth comparqui redireccionar enrutamiento subfigure toposdn Nicira toposinsdn
\chapter{Diseño de la arquitectura del hardware} % Main chapter title

\label{Chapter5} % Change X to a consecutive number; for referencing this
                     % chapter elsewhere, use \ref{ChapterX}

En este capítulo se explicará cómo se  diseñó la arquitectura de una capa MAC en
hardware para FPGA.

En primer lugar  se exponen los motivos que llevaron  al desarrollo del hardware
en esta etapa y de qué forma se  planeó el diseño. En segundo lugar, se enumeran
los requerimientos que el hardware debía cumplir. En tercer lugar se desarrollan
sus  principios  de  funcionamiento.  Por  último  se  ofrecen  detalles  de  la
implementación del diseño de la arquitectura de la MAC.

\section{Motivación}

En la etapa del diseño de la  arquitectura del hardware es importante tener bien
en claro las funcionalidades que debe cumplir  el diseño en sí, como también los
requerimientos de hardware  que se tienen a  la hora de realizar  la síntesis en
FPGA. Para esto  se realizó el estudio  del estándar IEEE 802.3  - Ethernet como
así también se tuvieron  en cuenta las características de la FPGA  en la cual se
implementa el diseño que  se detallan en \parencite{de0_nano_soc_specs}. Cabe
aclarar que este  diseño debe a su vez  dar soporte a FPGA similares  ya sea del
mismo o de otro fabricante.

Esta etapa es crítica  debido a que este diseño debe ser una  guía a seguir a la
hora de la codificación en HDL.

\section{Requerimientos}

A continuación se enumeran los requerimientos funcionales del diseño de hardware:

\begin{itemize}
\item \textbf{RF05:}  El diseño debe cumplir  las funciones básicas de  una capa
  MAC ethernet según el estándar.
\item \textbf{RF06:}  El diseño debe  respetar las  interfaces con el  cliente y
  capas inferiores según se establece en el estándar.
\item \textbf{RF07:} El diseño debe dar soporte a múltiples capas del tipo MII.
\end{itemize}

\section{Principios de funcionamiento}

En esta sección  se presentan los principios de funcionamiento  del diseño de la
arquitectura  de la  capa  MAC. El  flujo  de datos  se divide  en  dos vías  de
transmisión. La via  de salida de datos o transmisión  (en inglés \textit{Egress
Datapath}) y la  vía de ingreso de datos o  recepción (en inglés \textit{Ingress
Datapath}). En la  Figura \ref{fig:egress_and_ingress} se puede  ver un diagrama
de los bloques que  se implementaron y más adelante en  este capítulo se explica
en detalle qué función cumple cada uno y cómo se relacionan entre ellos.

\begin{figure}[H]
	\centering
	\includegraphics[max width=\textwidth]{egress_and_ingress.pdf}
	\caption[Diagrama  del datapath  de egress  e ingress  de la  capa MAC  con RS
sublayer e MII.]{Diagrama del datapath de egress e ingress de la capa MAC con RS
sublayer e MII.}
	\label{fig:egress_and_ingress}
\end{figure}

\subsection{Encapsulado y transmisión de tramas. Egress Datapath}

\begin{itemize}
\item  Según  el  estándar, a  la  hora  de  realizar  una transmisión  en  modo
  full-duplex (único modo  soportado para 100 o 400 Gb/s)  el cliente indicará que
  desea transmitir  información mediante la primitiva  MA\_DATA.request. Esta señal
  será implementada según \textbf{RF06}.
\item La transmisión de datos una vez  recibida la petición se divide en etapas.
  En primer  lugar la  MAC se  encarga de formar  los campos  DESTINATION ADDRESS,
  SOURCE  ADDRESS,  LENGTH/TYPE,  CLIENT  DATA   del  frame  según  su  estado  de
  configuración y los datos que le entrega el cliente.
\item En segundo lugar, en base a la trama ensamblada, se genera el campo FCS en
  función de los campos previamente construidos según indica el estándar y como se
  detalla en \ref{sec:fcs}.
\item En  tercer lugar se agrega  el PREAMBLE y  el START OF FRAME  DELIMITER al
  inicio de la trama.
\item En cuarto lugar, la capa MAC da inicio a la etapa de transmisión y entrega
  los datos a la Reconciliation Sublayer en el orden que se establece en la Figura
  \ref{fig:frame_ethernet}. La misma realiza  la conversión de la trama entregada de  forma serial a un
  formato paralelo  y entrega los datos  a la MII ya  sea GMII, CGMII u  otra. Con
  esto se satisface \textbf{RF05}.
\item Finalmente  la capa MII  transmite a capas  inferiores los datos  según se
  establece en \ref{sec:gmii_rs} o \ref{sec:xlgmii_rs} en función de la MII que se
  implemente como se establece en \textbf{RF07}.
\end{itemize}

\subsection{Recepción y filtrado de tramas. Ingress Datapath}

Según  el estándar,  cuando se  reciba una  trama la  capa MAC  indicará que  ha
arribado una  trama mediante  la primitiva  MA\_DATA.indication. Esta  señal será
implementada según \textbf{RF06}.

La recepción de tramas  se divide en etapas. En primer lugar  la interfaz MII ya
sea GMII o CGMII, implementadas en este trabajo, detecta una trama entrante.

En segundo lugar la interfaz MII, cualquiera  sea según \textbf{RF07}, pasa los datos con
formato paralelo a la Reconciliation Sublayer  para que luego ésta los serialice
y se los entregue a la capa MAC.

En tercer lugar  la capa MAC busca y  remueve el PREAMBLE y el START  OF FRAME y
comienza a capturar los campos del frame entrante.

En cuarto lugar, en base a la trama entrante, se genera un CRC para comparar con
el campo FCS de dicha trama.

En  quinto lugar  se  genera un  código  de estado  de  recepción indicando  los
posibles  errores que  se pueden  producir en  la recepción  de una  trama según
\ref{sec:trama_invalida}.

Finalmente  se  entregan  los  contenidos  del  frame  al  cliente.  Esto  viene
acompañado de  la primitiva  MA\_DATA.indication como así  también el  código de
recepción calculado en la etapa anterior. Con esto se satisface \textbf{RF05}.

\section{Detalles de la implementación}

Las funcionalidades de la capa MAC  se encapsularon en distintos bloques de RTL.
En esta  sección se exponen las  funcionalidades de cada bloque  que en conjunto
realizan las tareas que requiere una capa MAC según el estándar.

En primer  lugar se tratan  los bloques de  la capa MAC  por si misma,  tanto de
Egress (salida de datos) como Ingress  (ingreso de datos). Luego se muestran los
bloques  de la  capa Reconciliation  Sublayer y  la capa  MII haciendo  la misma
distinción de Ingress y Egress.

\subsection{Capa MAC: Egress}

En esta  sección se expondrán en  primer lugar los bloques  independientes de la
capa MAC en Egress y finalmente se  explica la funcionalidad de la capa MAC del
lado del transmisor con las interacciones de los bloques.

\subsubsection{Transmit Data Encapsulation}

Este bloque es el  encargado de convertir la petición del  cliente para el envío
de una trama  en una trama lista  para ser enviada con formato  IEEE 802.3. Este
bloque se encuentra controlado por una máquina de estados finitos y un timer que
le indican qué  parte de la trama debe construir  en cada instante. Inicialmente
el cliente  MAC le  indica que quiere  enviar una trama  y qué  contenidos deben
tener los distintos campos mediante los puertos de entrada.

\begin{figure}[H]
	\centering
	\includegraphics[max width=\textwidth]{transmit_data_encapsulation.pdf}
	\caption[Diagrama de bloques de transmit data encapsulation.]{Diagrama de
    bloques de transmit data encapsulation.}
    \label{fig:transmit_data_encapsulation}
\end{figure}

\paragraph{Puertos de entrada}
\begin{itemize}
\item \underline{Puertos de datos:}
  \begin{itemize}
  \item \textbf{i\_data\_valid:}  Indica a la capa  MAC que los  datos a la entrada  de los
    otros puertos  son válidos. En función  a esta señal se  construyen, delimitan y
    transmiten tramas
  \item  \textbf{i\_destination\_address:} Indica  la dirección  MAC destino  que se  desea
    insertar en la trama a enviar.
  \item \textbf{i\_source\_address:} Indica  la dirección MAC fuente que  se desea insertar
    en la trama a enviar.
  \item  \textbf{i\_configured\_mac\_src\_addr:}  En  caso  de  configurarse  que  se  debe
    sobreescribir el  campo SRC  ADDR con  el establecido en  la MAC  localmente, se
    utiliza el valor de este puerto.
  \item \textbf{i\_lentyp:} Este  puerto indica el campo LENGTH/TYPE que  se desea insertar
    en la trama  a enviar. En función del  valor de este campo, se  interpreta si se
    desea indicar el tipo o el largo de la trama.
  \item \textbf{i\_client\_data:} Datos que se deben insertar en el campo DATA de la trama.
  \item \textbf{i\_client\_fcs:} En caso de configurarse la capa MAC para que no calcule el
    CRC32, el cliente será  el responsable de hacerlo y este  puerto indica el valor
    que irá en la trama.
  \item  \textbf{i\_calculated\_fcs:} En  caso de  que la  capa MAC  sea la  responsable de
    calcular el CRC32 para insertar en el  campo FCS, el valor calculado se presenta
    en este puerto.
\end{itemize}
\item \underline{Puertos de configuración:}
  \begin{itemize}
  \item  \textbf{i\_override\_src\_addr:} Indica  al  bloque que  ignore  el valor  del
    puerto i\_source\_address e inserte el valor configurado localmente en su lugar.
  \item \textbf{i\_fcs\_present:} Indica  al bloque si el cliente será  el encargado de
    proveer el campo FCS.
  \end{itemize}
\end{itemize}

\paragraph{Puertos de salida}
\begin{itemize}
\item \underline{Puertos de datos:}
  \begin{itemize}
    \item \textbf{o\_data:} Los datos del frame ensamblado listo para ser enviado.
    \item \textbf{o\_data\_valid:} Indica validez de los datos del puerto o\_data.
  \end{itemize}
\end{itemize}

\paragraph{Resumen de funcionamiento}\\~\\
Inicialmente el bloque monitorea la señal i\_data\_valid esperando que el
cliente desee  construir una trama.  Cuando se detecta  un flanco de  subida, el
bloque comienza  a tomar los  datos de los distintos  puertos y asignarlos  a un
buffer en el cual se va conformando  la trama. Primero inserta el preámbulo y el
SFD, luego  la dirección destino, dirección  fuente, campo de LEN/TYP,  campo de
datos y finalmente FCS.  Esto lo coordina la máquina de  estados en conjunto con
el timer que le indica qué campo debe insertar en el buffer en cada instante. En
el caso en el cual el dato a enviar sea menor al tamaño mínimo establecido en el
estándar, se hace un  padding según la norma. Mientras se forma  la trama, se le
indica al bloque  Frame Transmitter que ya  se pueden ir enviando datos  y se le
indica  al bloque  CRC32  Calculator,  si es  que  corresponde,  que comience  a
calcular el campo FCS para insertarlo al final de la trama. Cuando se termina de
construir la  trama, se  pone el puerto  de salida o\_data\_valid  en bajo  y se
vuelve a esperar por una petición del cliente.

\subsubsection{CRC32 Calculator}

Este bloque es el encargado de calcular  el campo FCS para ser encapsulado en la
trama Ethernet según el estándar. El bloque implementa el algoritmo de CRC32 con
el polinomio que se le indica, en este caso, CRC32 según el estándar.

\begin{figure}[H]
	\centering
	\includegraphics[max width=\textwidth]{crc32_calculator.pdf}
	\caption[Diagrama de bloques de CRC32 Calculator]{Diagrama de bloques de CRC32 Calculator.}
    \label{fig:crc32_calculator}
\end{figure}

\paragraph{Puertos de entrada}
\begin{itemize}
\item \underline{Puertos de datos:}
  \begin{itemize}
  \item \textbf{i\_data:} Datos sobre los cuales el bloque calcula el CRC32.
  \item \textbf{i\_data\_valid:} Indica la validez de los datos a la entrada del bloque, en el puerto i\_data.
  \end{itemize}
\end{itemize}

\paragraph{Puertos de salida}
\begin{itemize}
\item \underline{Puertos de datos:}
  \begin{itemize}
  \item \textbf{o\_crc\_32:} El CRC32 obtenido como resultado.
  \item \textbf{o\_crc\_ready:} Indica validez de los datos del puerto o\_crc\_32.
  \end{itemize}
\end{itemize}

\paragraph{Resumen de funcionamiento}\\~\\
El bloque realiza el cálculo del CRC32 según se establece matemáticamente. Toma
un dato sobre el cual se va a trabajar y le hace XOR con el polinomio del CRC32.
En esta implementación, los datos sobre los cuales se desea calcular el CRC no
se encuentran presentes en el mismo instante, por lo tanto se debe construir un
buffer. Inicialmente se calcula la XOR sobre los 32 bits más significativos del
dato. Luego, sobre el resultado obtenido, más un bit más del dato inicial, se
repite la operación. Se hacen NB\_DATA-1 operaciones, en donde NB\_DATA es el
paralelismo del bus de entrada. En el siguiente clock, se concatena el resultado
del clock anterior con el nuevo dato y se repite la misma operación. Esto se
realiza tantas veces como el monitor de valid lo indique. Dicho monitor es
controlado por el puerto de entrada i\_data\_valid que, cuando los datos a la
entrada no sean válidos, se encargará de dejar que el bloque termine de procesar
los datos y cuando se entregue un CRC válido en el puerto de salida o\_crc\_32, se
levanta la señal o\_crc\_ready durante un pulso de clock.

\subsubsection{Client Length Calculator}

Este bloque es  el encargado de calcular  el tamaño de los datos  que el cliente
pretende enviar  en una trama  Ethernet. Este bloque  se encarga de  calcular el
tamaño de la trama que quiere enviar el cliente.

\begin{figure}[H]
	\centering
	\includegraphics[max width=\textwidth]{client_length_calculator.pdf}
	\caption[Diagrama de bloques del Client Length Calculator]{Diagrama de bloques del Client Length Calculator.}
  \label{fig:client_length_calculator}
\end{figure}

\paragraph{Puertos de entrada}
\begin{itemize}
\item \underline{Puertos de datos:}
  \begin{itemize}
  \item \textbf{i\_data\_valid:} Indica que los datos sobre los cuales se va a calcular el largo son válidos.
  \item \textbf{i\_tail\_size:} Indica cuántos bytes de los recibidos en el último clock son válidos.
  \end{itemize}
\end{itemize}

\paragraph{Puertos de salida}
\begin{itemize}
\item \underline{Puertos de datos:}
  \begin{itemize}
  \item \textbf{o\_frame\_size:} El largo del frame.
  \item \textbf{o\_frame\_size\_found:} Indica validez de los datos del puerto o\_frame\_size.
  \end{itemize}
\end{itemize}
\end{itemize}

\paragraph{Resumen de funcionamiento}\\~\\

  Cuando se detecta un flanco de subida  en el puerto i\_data\_valid, se inicia un
contador y cuando  se detecta un flanco  de bajada, se lo detiene.  El valor del
contador luego se  multiplica por el paralelismo  de datos que se  utiliza en la
capa MAC, se  lo afecta por el factor  de desplazamiento o shift que  pueda o no
aplicarse sobre  los datos  respecto al  paralelismo que  se utiliza.  Además se
tiene en cuenta el i\_tail\_size para finalmente calcular el tamaño de los datos.

\subsubsection{Transmition Status Generator}

\paragraph{Resumen de funcionamiento}\\~\\

Este bloque  es el encargado  de indicarle al cliente  si la transmisión  que él
inició, se pudo llevar a cabo exitosamente o  si se produjo un error y cuál fue.
En egress se considera que se puede  dar solamente un tipo de error. Esto ocurre
cuando se  detecta que el  largo de  los datos entregados  por el cliente  no es
consistente con  el campo de LEN/TYP  que indica el  mismo. En dicho caso  se le
indica al cliente que ocurrió un error  en la transmisión y no se transmiten los
datos.

\subsubsection{Deferring Full Duplex}

Este bloque  es el encargado  de controlar el throughput  con el cual  se envían
datos a capas  inferiores para asegurar que  se cumpla el tiempo  entre tramas o
interpacket gap que establece el estándar.

\begin{figure}[H]
	\centering
	\includegraphics[max width=\textwidth]{deferring_full_duplex.pdf}
	\caption[Diagrama de bloques de Deferring Full Duplex]{Diagrama de bloques de
    Deferring Full Duplex.}
  \label{fig:deferring_full_duplex}
\end{figure}

\paragraph{Resumen de funcionamiento}\\~\\

Cuando se  desea realizar una transmisión  en full duplex, se  debe asegurar que
entre tramas se respete el Interpacket Gap  que establece la norma. Para esto se
debe esperar cierto  tiempo entre trama y trama. Este  bloque se implementó como
una máquina de estados con tres estados.

\begin{itemize}
\item \textbf{Wait Transmitting:} En este estado se espera a que se le indique a
  la máquina  de estados  que se está  transmitiendo una trama.  Aquí la  señal de
  deferring o espera se encuentra en cero.
\item  \textbf{Wait End  of Transmition:}  En  este estado  se espera  a que  la
  transmisión de la trama finalice.
\item \textbf{Wait IPG:} En  este estado se indica que se está  en estado de IPG
  por medio de la señal deferring y una  vez que el timer de espera lo indique, se
  vuelve  al estado  inicial. El  tiempo de  espera es  parametrizable para  darle
  flexibilidad a la capa MAC.
\end{itemize}

\subsubsection{Frame Transmitter}

Este bloque  es el encargado  de enviar la trama  a las capas  inferiores cuando
corresponda.

\begin{figure}[H]
	\centering
	\includegraphics[max width=\textwidth]{frame_transmitter.pdf}
	\caption[Diagrama de bloques de Frame Transmitter]{Diagrama de bloques de
    Frame Transmitter.}
  \label{fig:frame_transmitter}
\end{figure}

\paragraph{Puertos de entrada}
\begin{itemize}
\item \underline{Puertos de datos:}
  \begin{itemize}
  \item  \textbf{i\_frame\_ready:} Indica  que hay  una trama  lista para  ser
    enviada.
  \item \textbf{i\_deferring:} Indica  que la capa MAC se  encuentra en estado
    de deferring.
  \item \textbf{i\_data:} Los datos a enviar.
  \end{itemize}
\item \underline{Puertos de configuración:}
  \begin{itemize}
    \item \textbf{i\_enable\_deferring:} Habilita o  deshabilita la espera de IPG
      entre tramas.
  \end{itemize}
\end{itemize}

\paragraph{Puertos de salida}
\begin{itemize}
\item \underline{Puertos de datos:}
  \begin{itemize}
    \item \textbf{o\_transmitting:} Indica si se encuentra transmitiendo datos en este instante.
    \item \textbf{o\_data:} Los datos a transmitir.
  \end{itemize}
\end{itemize}
\end{itemize}

\paragraph{Resumen de funcionamiento}\\~\\

Cuando  se desea  realizar una  transmisión, el  frame transmitter  monitorea el
estado  de  la  señal  de  i\_frame\_ready  que le  indicará  si  hay  datos  para
transmitir. Si  el protocolo de deferring  se encuentra habilitado, la  señal de
i\_deferring indicará si se debe esperar  para transmitir los datos. Cuando no se
está transmitiendo, se transmiten ceros.

\subsubsection{interacción de los bloques de Egress}

Cuando  el   cliente  desea   enviar  una  trama,   indica  mediante   la  señal
i\_data\_valid que los datos que se presentan en los puertos del bloque Transmit
Data  Encapsulation  son  válidos.  En  este momento  el  bloque  Transmit  Data
Encapsulation le indica  al bloque CRC32 Calculator que comience  con el cálculo
del  CRC  para ser  insertado  en  el  campo FCS.  Una  vez  que se  tienen  los
suficientes datos como para iniciar la transmisión, se le indica al bloque Frame
Transmitter que se  desea transmitir. Si el bloque Deferring  Full Duplex indica
que  no se  debe esperar  para  transmitir la  trama,  los datos  se empiezan  a
transmitir a las capas inferiores. Cuando  la señal de i\_data\_valid se cae, el
bloque Client  Length Calculator  se encuentra en  condiciones de  determinar el
tamaño de  los datos que han  sido enviados. En  función a esto, será  el bloque
Transmition Status  Generator quien se encargará  de indicarle al cliente  si ha
ocurrido  un  error,  en caso  contrario  si  la  transmisión  se lleva  a  cabo
exitosamente, se le indica esto al cliente.

\subsection{Capa MAC: Ingress}

En esta  sección se expondrán en  primer lugar los bloques  independientes de la
capa MAC en Igress  y finalmente se explica la funcionalidad de  la capa MAC del
lado del receptor con las interacciones de los bloques.

\subsubsection{Preamble and SFD Finder and Aligner}

Este bloque es  el encargado de encontrar  en la entrada de  datos, el PREAMBLE
seguido del START OF  FRAME DELIMITER para dar inicio a la  captura de una nueva
trama de datos entrante. Además alineará los datos al paralelismo que se utiliza
en el bloque.

\begin{figure}[H]
	\centering
	\includegraphics[max width=\textwidth]{preamble_finder_1.pdf}
	\caption[Diagrama de bloques del Preamble and SFD Finder and Aligner (1/2)]{Diagrama del bloques de Preamble and SFD Finder and Aligner (1/2)}
  \label{fig:preamble_finder_1}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[max width=\textwidth]{preamble_finder_2.pdf}
	\caption[Diagrama de bloques del Preamble and SFD Finder and Aligner (2/2)]{Diagrama de bloques del Preamble and SFD Finder and Aligner (2/2)}
  \label{fig:preamble_finder_2}
\end{figure}

\paragraph{Puertos de entrada}
\begin{itemize}
\item \underline{Puertos de datos:}
  \begin{itemize}
    \item \textbf{i\_data:} Los datos recibidos de capas inferiores.
    \item \textbf{i\_data\_valid:} Indica la validez de los datos en el puerto i\_data.
  \end{itemize}
\end{itemize}

\paragraph{Puertos de salida}
\begin{itemize}
\item \underline{Puertos de datos:}
  \begin{itemize}
    \item \textbf{o\_sof:} Indica el inicio de una trama.
    \item \textbf{o\_data:} Los datos a recibidos alineados al paralelismo.
  \end{itemize}
\end{itemize}

\paragraph{Resumen de funcionamiento}\\~\\

Cuando se  presentan datos  válidos en  i\_data según  el puerto  i\_data\_valid lo
indique, el bloque  comienza a buscar el preámbulo seguido  del SFD en cualquier
posición del  paralelismo, esté  o no  alineado. Una vez  que se  encuentra esta
secuencia, el bloque alinea los datos a la salida de o\_data e indica el comienzo
de los datos válidos mediante el puerto o\_sof.

\subsubsection{Frame Splitter}

Este bloque es  el encargado de capturar  los campos de la cabecera  de la trama
ingresante.

\begin{figure}[H]
	\centering
	\includegraphics[max width=\textwidth]{frame_splitter.pdf}
	\caption[Diagrama de bloques del Frame Splitter]{Diagrama de bloques del Frame Splitter.}
  \label{fig:frame_splitter}
\end{figure}

\paragraph{Puertos de entrada}
\begin{itemize}
\item \underline{Puertos de datos:}
  \begin{itemize}
  \item \textbf{i\_data:} Los datos recibidos de capas inferiores.
  \item \textbf{i\_data\_valid:} Indica la validez de los datos en el puerto i\_data.
  \end{itemize}
\end{itemize}

\paragraph{Puertos de salida}
\begin{itemize}
\item \underline{Puertos de datos:}
  \begin{itemize}
    \item \textbf{o\_dest\_address:} El campo DST ADDR de la trama recibida
    \item \textbf{o\_source\_address:} El campo SRC ADDR de la trama recibida.
    \item \textbf{o\_fcs:} El campo FCS de la trama recibida.
    \item \textbf{o\_length\_type:} El campo LENGTH/TYPE de la trama recibida.
    \item \textbf{o\_data:} El campo DATA de la trama recibida.
    \item \textbf{o\_data\_valid:} Indica la validez del puerto o\_data.
  \end{itemize}
\end{itemize}

\paragraph{Resumen de funcionamiento}\\~\\

Cuando se  presentan datos  válidos en  i\_data según  el puerto  i\_data\_valid lo
indique,  el bloque  comienza a  capturar los  distintos campos  de la  trama en
función del timer  del bloque. Esto lo hace  en el orden en el  que se encuentra
conformada la trama  que será el mismo  orden en el que se  transmiten y reciben
los datos.

\subsubsection{Layer Management Recognize Address}

Este bloque es el encargado de identificar  si la trama se encuentra destinada a
la estación que ha realizado la recepción.

\begin{figure}[H]
	\centering
	\includegraphics[max width=\textwidth]{layer_management_recognize_address.pdf}
	\caption[Diagrama de bloques del Layer Management Recognize Address]{Diagrama de bloques del Layer Management Recognize Address.}
  \label{fig:layer_management_recognize_address}
\end{figure}

\paragraph{Puertos de entrada}
\begin{itemize}
\item \underline{Puertos de datos:}
  \begin{itemize}
    \item \textbf{i\_rx\_mac\_address:} La dirección MAC destino de la trama recibida a comparar.
  \end{itemize}
\item \underline{Puertos de configuración:}
  \begin{itemize}
  \item \textbf{i\_mac\_station\_addr:}  Puerto que  configura el valor del registro
    mac station addr.
  \item \textbf{i\_broadcast\_addr:}  Puerto que  configura el valor del registro
    broadcast addr.
  \item \textbf{i\_multicast\_addr\_1:}  Puerto que  configura el valor del registro
    multicast addr 1.
  \item \textbf{i\_multicast\_addr\_n:}  Puerto que  configura el valor del registro
    multicast addr n.
  \item \textbf{i\_enable\_multicast\_addr\_n:}  Puerto que  habilita a  la capa
    MAC a aceptar tramas del grupo multicast n.
    \item \textbf{i\_promiscuous\_enabled:} Modo promiscuo que indica si se debe
      aceptar una trama destinada a cualquier estación.
  \end{itemize}
\end{itemize}

\paragraph{Puertos de salida}
\begin{itemize}
\item \underline{Puertos de datos:}
  \begin{itemize}
    \item \textbf{o\_macaddress\_match:} Indica si la trama debe ser aceptada por la estación
    debido a que se encuentra destinada a la misma.
  \end{itemize}
\end{itemize}

\paragraph{Resumen de funcionamiento}\\~\\

Este bloque compara  la dirección que se presenta en  el puerto i\_rx\_mac\_address
con  el  conjunto de  direcciones  almacenadas  y  configuradas  en la  MAC.  La
condición para que se genere la señal de o\_macaddress\_match es que alguna de las
direcciones configuradas coincida  con la dirección en i\_rx\_mac\_address  o en su
defecto   que   se   encuentre    configurado   el   modo   promiscuo   mediante
i\_promiscuous\_enabled. Los puertos de i\_enable\_multicast\_addr\_n configuran si se
debe  tener  en  cuenta   la  dirección  multicast\_addr\_n  correspondiente  como
candidato.

\subsubsection{CRC32 Checker}

Este bloque  es el  encargado de  chequear el  campo de  FCS generando  un nuevo
código CRC32 y validándolo con el contenido en dicho campo.

\begin{figure}[H]
	\centering
	\includegraphics[max width=\textwidth]{crc32_checker.pdf}
	\caption[Diagrama de bloques del CRC32 Checker]{Diagrama de bloques del CRC32 Checker.}
  \label{fig:crc32_checker}
\end{figure}

\paragraph{Puertos de entrada}
\begin{itemize}
\item \underline{Puertos de datos:}
  \begin{itemize}
    \item \textbf{i\_data:} Datos sobre los cuales el bloque calcula el CRC32.
    \item \textbf{i\_data\_valid:} Indica la validez de los datos a la entrada del bloque, en el puerto i\_data.
    \item \textbf{i\_crc32:} El valor del CRC a comparar.
  \end{itemize}
\end{itemize}

\paragraph{Puertos de salida}
\begin{itemize}
\item \underline{Puertos de datos:}
  \begin{itemize}
    \item \textbf{o\_crc\_valid:} Indica si los valores de los CRC a comparar coinciden.
    \item \textbf{o\_crc\_ready:} Indica validez de los datos del puerto o\_crc\_valid.
  \end{itemize}
\end{itemize}

\paragraph{Resumen de funcionamiento}\\~\\

El  funcionamiento es  el  mismo que  el  del bloque  CRC32  Calculator, con  la
diferencia de que su salida será si se  ha dado o no una coincidencia (en inglés
\textit{match}) con el FCS de la trama entrante.

\subsubsection{Frame Length Calculator}

Este bloque es el encargado de calcular el tamaño del frame recibido.

\paragraph{Resumen de funcionamiento}\\~\\

Es  una instancia  idéntica a  la  del bloque  Client Length  Calculator con  la
diferencia de  que calcula  la longitud  de los  datos de  la trama  entrante al
receptor.

\subsubsection{Reception Status Generator}

Este bloque  es el  encargado de generar  un estado de  recepción de  trama para
informar al cliente en qué condición se realizó la recepción de la trama.

\paragraph{Resumen de funcionamiento}\\~\\

En función de la información que se recopila de los distintos bloques del camino de recepción, se informa al cliente el estado de la trama recibida. Los posibles estados son:

\begin{itemize}
\item \textbf{receiveOK:} Indica que no han  ocurrido errores en la recepción de
  la trama.
\item \textbf{frameCheckError:}  Indica que el  campo FCS del frame  no coincide
  con el CRC calculado en base a la trama recibida.
\item \textbf{lengthError:} Indica  que el largo de los datos  del campo DATA no
  se corresponde con lo indicado en el campo LENGTH/TYPE.
\item \textbf{alignmentError:} Indica que no se  ha recibido un número entero de
  Bytes (esto no puede  ocurrir en la implementación de este  trabajo debido a que
  se trabaja con paralelismos de datos múltiplos de 8).
\item \textbf{addressError:} Le  indica al cliente que la trama  no se encuentra
  destinada  a esta  estación. En  esta etapa  se puede  tomar la  decisión de  no
  entregar la trama al cliente.
\end{itemize}

\subsubsection{Interacción de los bloques de Ingress}

Cuando las  capas inferiores  le indican a  la capa MAC  que hay  datos válidos,
comienza el intento de alineación al preámbulo y SFD. El bloque Preamble and SFD
Finder and Aligner comienza a buscar la secuencia correspondiente para alinearse
y una vez que la encuentra, acomoda  los datos al incio del paralelismo para que
los siguientes  bloques puedan trabajar con  los datos alineados. En  esta etapa
comienza la  descomposición del frame en  la cual el Frame  Splitter captura los
distintos campos de la cabecera de la  trama incluido el campo de datos. Esto es
pasado  a los  demás bloques  y  luego al  cliente. El  bloque Layer  Management
Recognize Address será el encargado de identificar si la dirección destino de la
trama coincide con alguna de las aceptadas en la estación MAC. Esto se lo indica
al bloque Reception Status Generator. A  su vez, el bloque CRC32 Checker realiza
el cálculo del CRC para verificar la  integridad de los datos recibidos. Una vez
que  se tiene  esta información,  se lo  informa de  la misma  manera al  bloque
Reception Status Generator. El bloque  Frame Length Calculator, obtiene el largo
de la trama  completa y el largo del  campo de datos y se los  entrega al bloque
Reception  Status Generator.  Éste último  será  el encargado  de entregarle  al
cliente un  código de estado  de la recepción en  función de la  información que
obtiene de los demás bloques.

\subsection{Reconciliation Sublayer y GMII}\label{sec:rs_gmii}

En esta  sección se  expondrán las funcionalidades  implementadas de  la subcapa
Reconciliation Sublayer y de la interfaz GMII. En la Figura \ref{fig:rs_gmii} se
muestra  un  diagrama  a  nivel   de  sistemas  del  hardware  implementado.  La
implementación se realizó en conjunto para ambas funcionalidades.

\begin{figure}[H]
	\centering
	\includegraphics[max width=\textwidth]{rs_gmii.pdf}
	\caption[Diagrama de  bloques de  Reconciliation Sublayer y  GMII]{Diagrama de
bloques de Reconciliation Sublayer y GMII.}
  \label{fig:rs_gmii}
\end{figure}

\subsubsection{Egress GMII Reconciliation Sublayer}

Este bloque es el encargado de convertir las señales provenientes de la capa MAC
a un  formato estandarizado para la  capa GMII según establece  el estándar IEEE
802.3. Para poder  adaptar la implementación de  la capa MAC que  trabaja con un
paralelismo de  64 bits  o más a  la GMII  que opera con  64 bits,  es necesario
introducir un  buffer de conversión  de paralelismo,  en donde se  almacenan los
datos a la entrada de a 64 bits por flanco de clock y se los lee de a 8 bits por
flanco de clock.

\subsubsection{Egress GMII Interface}

Este bloque  es el encargado de  generar las señales  de datos, de control  y de
error según la información que recibe de la capa RS para ser transmitidos a PCS.
Para esta implementación, los datos se envían  de a 8 bits por clock por o\_TXD,
se codifica simplemente  la señal de o\_TX\_EN  en función del nivel  de la FIFO
que contiene datos. Siempre que haya  datos para ser transmitidos, esta señal se
encuentra en alto. Debido a que el  propósito del desarrollo de esta interfaz es
la comunicación  con la MAC TSE  de Altera para  validar el diseño dentro  de un
banco de prueba  controlado, la señal de error o\_TX\_ER  se mantendrá constante
en bajo.

\subsubsection{Ingress GMII Reconciliation Sublayer}

Este bloque es el encargado de convertir las señales provenientes de la interfaz
GMII al  formato de señales implementadas  para la MAC del  presente trabajo. El
funcionamiento es exactamente  análogo al camino de egress de  datos pero con un
funcionamiento en el  sentido opuesto. Se realiza una  conversión de paralelismo
de 8 a 64 bits luego de pasar por una FIFO de entrada para que luego la interfaz
GMII pueda regenerar las señales para la capa MAC.

\subsubsection{Ingress GMII Interface}

Este bloque  es el  encargado de  codificar los datos  que recibe  de la  PCS en
bloques    estandarizados     para    entregarlos     a    la     subcapa    RS.
o\_PLS\_DATA\_indication     contiene    la     información,    mientras     que
o\_PLS\_DATA\_VALID\_indication   indicará    la   validez   de    los   mismos.
o\_tail\_size\_bits indica cuántos  bits de los 64 de paralelismo  son válidos y
o\_RS\_error indica a la capa MAC que se ha recibido un error en i\_RX\_ER.

\subsection{Reconciliation Sublayer y CGMII}

En esta  sección se  expondrán las funcionalidades  implementadas de  la subcapa
Reconciliation Sublayer y  de la interfaz CGMII. En  la Figura \ref{fig:rs_gmii}
se  muestra un  diagrama  a  nivel de  sistemas  del  hardware implementado.  La
implementación se realizó en conjunto para ambas funcionalidades.

\begin{figure}[H]
	\centering
	\includegraphics[max width=\textwidth]{rs_cgmii.pdf}
	\caption[Diagrama de  bloques de  Reconciliation Sublayer y  CGMII]{Diagrama de
bloques de Reconciliation Sublayer y CGMII.}
  \label{fig:rs_cgmii}
\end{figure}

\subsubsection{Egress CGMII Reconciliation Sublayer}

Este bloque es el encargado de convertir las señales provenientes de la capa MAC
a un  formato que pueda ser  comprendido por la  interfaz CGMII de 802.3  ba. En
esta implementación  debido a la naturaleza  de la implementación de  la MAC, el
funcionamiento es  simplemente indicar a la  CGMII cuántos datos son  válidos en
función   de  las   señales  i\_PLS\_DATA\_request,   i\_data\_request\_valid  e
i\_tail\_size\_bits. La  primera contiene  los datos  para ser  transmitidos. La
segunda indica  la validez de  los mismos y la  tercera indica cuántos  bits del
paralelismo de datos son válidos. Esto se le indica a la máquina de estados GMII
TX FSM para que pueda controlar la salida de datos.

\subsubsection{Egress CGMII Interface}

Este bloque es el  encargado de generar las señales de datos  y de control según
la información que recibe  de la capa RS para ser transmitidos  a PCS. Para esta
implementación, se implementa una máquina de estado que controla cuándo se deben
codificar caracteres  especiales para  indicar inicio  y finalización  de trama,
ausencia de datos válidos o algún  error. En una transmisión típica sin errores,
se reemplaza el primer  byte del preámbulo por un caracter de  START y el último
por  uno de  TERMINATE.  Luego se  indica  que  no hay  más  datos válidos  para
transmisión codificando  caracteres IDLE. Si se  produce algún error, ya  sea un
error de estado  del enlace (en Inglés \textit{Linkfault}) u  otro, se codifican
los caracteres correspondientes. Este flujo de  datos viaja por la señal o\_TXD.
Además de  esto, se  deberá codificar la  señal o\_TXC para  indicar si  el byte
correspondiente codificado en o\_TXD es de datos o de control.

\subsubsection{Ingress CGMII Reconciliation Sublayer}

Este bloque es el encargado de convertir las señales provenientes de la interfaz
CGMII al formato  de señales implementadas para la MAC  del presente trabajo. De
la misma forma que para la subcapa  RS de GMII, el funcionamiento es exactamente
análogo al camino  de egress de datos  pero con un funcionamiento  en el sentido
opuesto. Se  realiza la  conversión del  formato de señales  según lo  indica la
máquina  de estados  finita  del  bloque para  que  el  funcionamiento de  capas
inferiores sea completamente transparente.

\subsubsection{Ingress CGMII Interface}

Este bloque  es el  encargado de  codificar los datos  que recibe  de la  PCS en
bloques  estandarizados para  entregarlos a  la subcapa  RS. Realiza  el proceso
inverso  que en  el  camino  de Egress,  decodifica  los  caracteres de  control
provenientes de capas inferiores y genera el flujo de datos para que la capa MAC
pueda trabajar.