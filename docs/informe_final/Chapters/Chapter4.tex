% Chapter Template
% cSpell:words parencite onfwhitepaper includegraphics resizebox sdncomponents
% linewidth comparqui redireccionar enrutamiento subfigure toposdn Nicira toposinsdn
\chapter{Diseño de un simulador de MAC Ethernet} % Main chapter title

\label{Chapter4} % Change X to a consecutive number; for referencing this
                     % chapter elsewhere, use \ref{ChapterX}

En este  capítulo se  explicará la  forma en la  que se  diseñó e  implementó un
simulador de  MAC Ethernet  hecho en  Python con  el fin  de generar  señales de
estímulo para ser usadas en el banco de prueba del diseño de la MAC en hardware.

En  primer lugar  se exponen  los  motivos que  llevaron al  desarrollo de  este
simulador. En segundo  lugar, se enumeran los requerimientos  que este simulador
debía cumplir. En tercer lugar  se desarrollan sus principios de funcionamiento.
Por último se ofrecen los detalles de la implementación.

\section{Motivación}

En los diseños en  hardware es muy importante tener un  banco de pruebas robusto
en  etapa  de  simulación  para  evitar  problemas  de  funcionamiento  una  vez
sintetizado el  proyecto. Muchas  veces la  tarea de  verificación por  medio de
testbench es  tediosa y compleja y  no cubre en  gran medida el universo  de las
pruebas que  se desean  realizar. Por  esta razón se  utiliza una  estrategia de
verificación  llamada  vector matching  que  constituye  una  de las  etapas  de
verificación más importantes previas a la  etapa de síntesis. Para esto, se debe
construir un  simulador que realice  las mismas funcionalidades que  los bloques
que se desean verificar y comparar los resultados que se obtienen.

\subsection{Vector Matching}

El vector matching es un proceso de  verificación que se realiza para validar el
funcionamiento  del  RTL  en  etapa   de  simulación.  Para  esto  es  necesario
desarrollar un simulador,  en general codificado en un lenguaje  de alto o medio
nivel, que cumpla las mismas funcionalidades  que el RTL que se desea verificar.
Se generan señales  de estímulo (en general por medio  del mismo simulador) para
tanto el  RTL en etapa de  simulación como para  el simulador y se  comparan las
señales. Este  proceso permite  verificar que el  hardware simulado  se comporte
correctamente y facilita en gran medida  la verificación exhaustiva debido a las
facilidades que este método brinda.

\section{Requerimientos}\label{sec:req_simulador}

A continuación se enumeran los requerimientos funcionales del simulador:

\begin{itemize}
\item \textbf{RF01:} El  simulador debe generar una cantidad de  tramas según se
  le indique con campos parametrizables.
\item \textbf{RF02:}  El simulador debe implementar  las funcionalidades mínimas
  de un transmisor y un receptor MAC conectados en configuración de loopback.
\item \textbf{RF03:} El  simulador debe generar archivos de  texto con contenido
  binario con el flujo de bits de un transmisor.
\item \textbf{RF04:} El  simulador debe generar archivos de  texto con contenido
  binario con el flujo  de bits de un receptor en base al  archivo con el flujo de
  bits del transmisor.
\end{itemize}

\section{Principios de funcionamiento}

A continuación se desarrollan los principios de funcionamiento del simulador. De
forma abreviada, el simulador se comporta como se puede ver en el diagrama de la
Figura \ref{fig:simulador_actividades}.

\begin{figure}[H]
	\centering
	\includegraphics[max width=\textwidth]{simulador_actividades.png}
	\caption[Diagrama  de  actividades  del simulador  MAC  Ethernet]{Diagrama  de
    actividades del simulador MAC Ethernet.}
	\label{fig:simulador_actividades}
\end{figure}

\subsection{Generación y transmisión de tramas}\label{sec:gen_tramas}

El simulador debe  implementar las funcionalidades básicas de  un transmisor MAC
según  \textbf{RF02}. Para  esto, debe  ser capaz  de construir  la cantidad  de
tramas indicadas por el usuario  (\textbf{RF01}) con sus campos correspondientes
según se encuentren configurados y luego generar  un flujo de bits de salida con
los frames construidos como se indica en \textbf{RF03}.

Esto se logra por etapas. En  primer lugar se generan condiciones aleatorias que
determinan en qué condición se generará el frame. Esto puede significar un frame
válido, o uno que genere algún tipo de error.

En  segundo lugar  se construyen  los  campos del  frame. El  usuario indica  el
DESTINATION  ADDRESS y  el SOURCE  ADDRESS. Se  genera un  LENGTH/TYPE aleatorio
dentro de los permitidos y en base  a este campo se generan bits aleatorios para
luego  introducirse en  el CLIENT  DATA. Finalmente,  utilizando la  librería de
CRC32 \parencite{crc_check} se  computa el campo FCS. En base  a las condiciones
generadas en la  primera etapa, estos campos pueden ser  corruptos o modificados
si es que así lo indican las condiciones aleatorias.

La  primera y  segunda etapa  se  repiten tantas  veces como  tramas el  usuario
indique que desea generar para transmitir.

En  tercer  lugar se  genera  un  flujo de  bits  con  las tramas  concatenadas,
intercalando bits de Interpacket Gap (bits que no corresponden a ninguna trama y
son necesarios para evitar agobiar al receptor). Finalmente se genera un archivo
de texto con el flujo de bits generado en la tercer etapa.

\subsection{Recepción de tramas}\label{sec:rec_tramas}

El simulador  debe implementar  las funcionalidades básicas  de un  receptor MAC
según \textbf{RF02}. Esto, al  igual que en el caso de  la generación de tramas,
se logra en  varias etapas. En primer  lugar el simulador debe  abrir el archivo
generado previamente en la etapa de generación de tramas para ser utilizado como
entrada del receptor.

En segundo lugar  debe encontrar una secuencia de bits  con el preámbulo seguido
de un start of frame.

En tercer lugar  el simulador comienza a  recorrer el flujo de bits  a partir de
los start  of frame e ir  capturando los datos correspondientes  a los distintos
campos.  Estos  campos  son:  DESTINATION  ADDRESS,  SOURCE  ADDRESS,
LENGTH/TYPE,  CLIENT DATA  y FCS.  Además se  genera en  base a  estos campos  y
utilizando nuevamente  la misma librería que  en el simulador del  transmisor el
CRC32.

En cuarto  lugar, en  base a  los campos capturados,  el simulador  del receptor
chequea las siguientes condiciones en busca  de posibles errores según el estándar:

\begin{itemize}
\item La longitud  en bits del frame  supera el tamaño máximo  indicado por el
  estándar.
\item El CRC que genera el receptor en  base al frame recibido, no es igual al
  contenido del campo FCS de la trama recibida.
\item  El largo  en bytes  del  campo CLIENT  DATA  no se  corresponde con  el
  contenido del campo LENGTH/TYPE.
\item El largo en bits del frame no es un número entero de octetos.
\item El campo  DESTINATION ADDRESS del frame entrante no  es igual al ADDRESS
  configurado en el receptor.
\end{itemize}

Finalmente, se genera un archivo de  texto con el flujo de bits correspondientes
al receptor en base a las etapas anteriores como se indica en \textbf{RF04}.

\section{Detalles de la implementación}

En esta  sección se hace  foco en  los detalles de  la implementación y  cómo se
satisfacen   los   requerimientos   funcionales   enumerados   en   la   sección
\ref{sec:req_simulador}.

\subsection{Frame generator: generación y transmisión de tramas}

Las funcionalidades  de generación y transmisión  de tramas la realiza  el Frame
Generator. Implementado en Python, en la Figura \ref{fig:frame_gen_secuencia} se
ofrece una visión de sus funcionalidades.

\begin{figure}[H]
	\centering
	\includegraphics[max width=\textwidth]{frame_gen_secuencia.png}
	\caption[Diagrama de secuencia del Frame generator.]{Diagrama de secuencia del
    Frame generator.}
	\label{fig:frame_gen_secuencia}
\end{figure}


Como se introdujo en la sección \ref{sec:gen_tramas}, la generación de tramas se
realiza por etapas.  Inicialmente se realiza una configuración de  los campos de
las  tramas   que  se  desean  generar.   Esto  se  hace  mediante   la  función
\textit{configure\_frame\_fields()} (\textbf{RF01}).  A continuación  el usuario
le    indica   al    simulador   cuántas    tramas   desea    generar   mediante
\textit{generate\_n\_frames()}  (\textbf{RF01}). Aquí  comienzan las  tareas del
simulador para  generar los campos del  frame, ensamblar la trama  y realizar la
transmisión (\textbf{RF02}), esto se realiza en la siguiente secuencia:

\begin{enumerate}
\item \textit{generateRandomFrameConditions()} genera condiciones aleatorias que
  definen qué  error (si lo tiene)  deberá incluir dicha trama.  Estas condiciones
  aleatorias pueden ser configuradas indicando con  qué frecuencia se desea que se
  genera una trama con un determinado error.  Estos errores pueden ser un error de
  CRC,  un  destination  address  incorrecto,  un tamaño  de  payload  que  no  se
  corresponda  con el  campo  LENGTH/TYPE o  un frame  demasiado  grande según  se
  establece en el estándar.
\item Se generan  listas vacías data\_list y  control\_data\_list que contendrán
  el flujo de datos y de control que serán exportados posteriormente.
\item  Se generan  los  campos de  la trama  según  se configuraron  previamente
  modificados por las condiciones aleatorias que se obtienen de la etapa 1.
\item Se  calcula un CRC en  base a la  trama ensamblada en  la etapa 3 y  se la
  modifica  o no  según se  haya establecido  en la  etapa 1.  Para esta  etapa se
  utiliza la librería de Python de CRC32.
\item Se  genera el flujo  de datos y  de control en  base a las  trama generada
  anteponiendo un preámbulo y un start of frame.
\end{enumerate}

Esta secuencia  se repite  tantas veces  como frames hayan  sido pedidos  por el
usuario. A partir de  aquí se exportan los archivos en  formato de texto binario
(\textbf{RF03}).


\subsection{Frame parser: Recepción de tramas}

Las funcionalidades de recepción y control  de errores en la recepción de tramas
la   realiza  el   Frame  Parser.   Implementado   en  Python,   en  la   Figura
\ref{fig:frame_parser_secuencia} se ofrece una visión de sus funcionalidades.

\begin{figure}[H]
	\centering
	\includegraphics[max width=\textwidth]{frame_parser_secuencia.png}
	\caption[Diagrama de secuencia del Frame parser.]{Diagrama de secuencia del
    Frame parser.}
	\label{fig:frame_parser_secuencia}
\end{figure}

Como se introdujo en la sección  \ref{sec:rec_tramas}, la recepción de tramas se
realiza por etapas. Inicialmente el usuario  invoca al programa y le realiza una
petición mediante parseFrames()  indicando los archivos de flujos de  datos y de
control  que  debe  utilizar  el  simulador del  receptor.  Estos  archivos  son
generados por  el Frame  Generator. A  partir de aquí  el simulador  comienza la
tarea de encontrar el inicio y final de las tramas, capturar los campos, generar
un CRC, chequear errores  de recepción e indicar al cliente  MAC que ha arribado
una nueva trama adjuntando a su  vez los campos capturados (\textbf{RF02}). Esto
ocurre en la siguiente secuencia de pasos:

\begin{enumerate}
\item El simulador debe buscar el inicio de tramas dentro del archivo de datos
  basándose  en  el  flujo  de  control.  De esta  parte  se  encarga  la  función
  findSofPreamble().
\item En función de  estas posiciones en las cuales se  encuentra el inicio de
  nuevos frames, el frame parser procede a  capturar los campos del frame según el
  orden que se indica en el Estándar.
\item A partir de los campos capturados  a excepción del FCS se calcula el CRC
  con la trama entrante.
\item  Se genera  un código  de estado  de recepción  en función  de la  trama
  recibida. Se chequea que  el largo del frame no exceda  el máximo permitido, que
  el CRC generado a la hora de la  recepción sea idéntico al contenido en el campo
  FCS, que el largo en bytes del campo de datos sea igual al número indicado en el
  campo LENGTH/TYPE,  que el  número de  bits del  frame sea  un número  entero de
  octetos y que la trama esté destinada al receptor en cuestión.
\end{enumerate}

Esta secuencia  se repite tantas veces  como frames hayan sido  encontrados. A
partir de  aquí se  exportan los  archivos en  formato de  texto binario  con el
contenido de los campos del frame y  su código de estado de recepción para todas
las tramas recibidas (\textbf{RF04}).
