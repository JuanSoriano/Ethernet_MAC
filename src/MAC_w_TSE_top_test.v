module MAC_w_TSE_top_test ();
   //************************
   //For TSE
   //************************
   localparam NB_DATA_TSE               = 32 ;
   localparam LOG2_NB_DATA_TSE          = 5 ;
   localparam MIN_FRAME_SIZE_BYTES      = 0 ;
   localparam MAX_FRAME_SIZE_BYTES      = 1500 ;
   localparam LOG2_MAX_FRAME_SIZE_BYTES = 11;
   localparam LOG2_MAX_FRAME_SIZE_BITS  = 14;
   localparam NB_DST_ADDR               = 48;
   localparam NB_SRC_ADDR               = 48;
   localparam NB_LENTYP                 = 16;
   localparam NB_CRC32                  = 32;
   localparam CRC32_POLYNOMIAL          = 33'h104C11DB7;
   localparam NB_DA_TYPE                = 2;
   localparam NB_SA_TYPE                = 2;
   localparam NB_LENT_TYPE              = 2;
   localparam NB_DATA_TSE_TYPE          = 2;
   localparam NB_CRC_TYPE               = 1;
   localparam N_ADDRESSES               = 10;
   localparam LOG2_N_ADDRESSES          = 4;
   localparam LOG2_NBDATA_DIV4          = 2;  //log2(NB_DATA_TSE/4)
   localparam NB_GMII                   = 8;

   localparam NB_TIMER                  = 10;
   localparam NB_GPIO                   = 8;

   //***********************
   //For MAC
   //***********************
   localparam NB_DATA                   = 64 ;
   localparam LOG2_NB_DATA              = 6 ;
   localparam LOG2_N_RX_STATUS          = 3 ;
   localparam NB_ADDRESS                = 48 ;
   localparam N_MULTICAST_ADDRESS       = 3 ;
   localparam LOG2_N_MULTICAST_ADDRESS  = 2 ;

   //TX
   localparam LOG2_NB_LENTYP            = 4 ;
   localparam MIN_LENGTH_FRAME_BYTES    = 64;
   localparam MAX_LENGTH_FRAME_BYTES    = 1982;
   localparam PREAMBLE                  = 56'h55555555555555;
   localparam NB_PREAMBLE               = 56;
   localparam SFD                       = 8'hD5;
   localparam NB_SFD                    = 8;
   localparam MIN_FRAME_SIZE_BITS       = 512;
   localparam IPG_BITS                  = 96;
   localparam IPG_STRETCH_BITS          = 0;

   //GMII
   localparam NB_XC                     = 8;
   localparam N_LANES                   = 8;

   //Framegen/checker
   localparam NB_DATA_PRBS              = 64 ;
   localparam NB_DATA_TYPE              = 2;

   //Stat counter
   localparam NB_COUNTER                = 10;

   //****************************************************************************************
   //                            SIGNALS AND VARIABLES
   //****************************************************************************************

   //************************
   //For TSE
   //************************

   wire [NB_GMII-1:0]                       TSE_eth0_gmii_d_MAC   ;
   wire                                     TSE_eth0_gmii_dv_MAC  ;
   wire                                     TSE_eth0_gmii_err_MAC ;

   //Avalon-ST TX signals
   wire [NB_DATA_TSE-1:0]                   TSE_o_ff_tx_data; //LSB is TSE_newer
   wire                                     TSE_o_ff_sop; //Start of TSE_packet
   wire                                     TSE_o_ff_tx_eop; //End of TSE_packet
   wire                                     TSE_o_tx_rdy;
   wire                                     TSE_o_ff_tx_wren; //Acts like data TSE_valid
   wire                                     TSE_o_ff_tx_crc_fwd;
   wire                                     TSE_o_tx_err; //Asserted in last byte, indicates that the transmit frame is invalid. MAC forwards the invalid frame to GMII with TSE_error
   reg [LOG2_NBDATA_DIV4-1:0]               TSE_o_ff_tx_mod; //11: [23:0] not valid; 10: [15:0] not valid; 01: [7:0] not valid; 00: All TSE_valid
   wire                                     TSE_o_ff_tx_septy; //To indicate to stop writing to the FIFO buffer and initiate TSE_backpressure
   wire                                     TSE_tx_ff_uflow; //Underflow in FIFO TSE_buffer
   //Frame generator signals
   wire [NB_DATA_TSE-1:0]                   TSE_i_data;
   wire                                     TSE_i_fcs_included;
   wire                                     TSE_i_data_valid;
   wire                                     TSE_i_frame_transmitted;
   wire [LOG2_MAX_FRAME_SIZE_BITS-1:0]      TSE_i_frame_size_bits;
   wire [LOG2_NB_DATA_TSE-1:0]              TSE_i_tail_size_bits;
   //Ingress
   //Frame checker signals
   wire [NB_DATA_TSE-1:0]                   TSE_o_data;
   wire                                     TSE_o_data_valid;
   reg [LOG2_NB_DATA_TSE-1:0]               TSE_o_tail_size_bits;

   //Frame Gen/Checker
   wire [NB_DATA_TSE-1:0]                   TSE_data_o;
   wire [NB_DST_ADDR-1:0]                   TSE_destination_addres_o;
   wire [NB_SRC_ADDR-1:0]                   TSE_source_address_o;
   wire [NB_LENTYP-1:0]                     TSE_lentype_o;
   wire [NB_CRC32-1:0]                      TSE_fcs_o;
   wire                                     TSE_fcs_included_o;
   wire                                     TSE_data_valid_o;
   wire                                     TSE_frame_transmitted_o;
   wire                                     TSE_llc_frame_format_i;
   wire                                     TSE_request_frame_i;

   reg                                      TSE_llc_frame_format;
   reg [NB_DA_TYPE-1:0]                     TSE_da_type;
   reg [NB_DST_ADDR-1:0]                    TSE_da_set;
   reg [LOG2_N_ADDRESSES-1:0]               TSE_da_list_selection;
   reg [NB_SA_TYPE-1:0]                     TSE_sa_type;
   reg [NB_SRC_ADDR-1:0]                    TSE_sa_set;
   reg [LOG2_N_ADDRESSES-1:0]               TSE_sa_list_selection;
   reg [NB_LENT_TYPE-1:0]                   TSE_lent_type;
   reg [NB_LENTYP-1:0]                      TSE_lentyp_set;
   reg [NB_DATA_TSE_TYPE-1:0]               TSE_data_type;
   reg [NB_DATA_TSE-1:0]                    TSE_data_pattern;
   reg [NB_CRC_TYPE-1:0]                    TSE_crc_type;
   reg                                      TSE_generate_crc;
   reg [NB_CRC32-1:0]                       TSE_set_crc;

   wire [NB_DA_TYPE-1:0]                    TSE_da_type_i;
   wire [NB_DST_ADDR-1:0]                   TSE_da_set_i;
   wire [LOG2_N_ADDRESSES-1:0]              TSE_da_list_selection_i;
   wire [NB_SA_TYPE-1:0]                    TSE_sa_type_i;
   wire [NB_SRC_ADDR-1:0]                   TSE_sa_set_i;
   wire [LOG2_N_ADDRESSES-1:0]              TSE_sa_list_selection_i;
   wire [NB_LENT_TYPE-1:0]                  TSE_lent_type_i;
   wire [NB_LENTYP-1:0]                     TSE_lentyp_set_i;
   wire [NB_DATA_TSE_TYPE-1:0]              TSE_data_type_i;
   wire [NB_DATA_TSE-1:0]                   TSE_data_pattern_i;
   wire [NB_CRC_TYPE-1:0]                   TSE_crc_type_i;
   wire                                     TSE_generate_crc_i;
   wire [NB_CRC32-1:0]                      TSE_set_crc_i;

   wire                                     TSE_missmatch_o;

   //Frame generator
   wire [NB_DATA_TSE-1:0]                   TSE_fg_data_o;
   wire [NB_DST_ADDR-1:0]                   TSE_fg_destination_address_o;
   wire [NB_SRC_ADDR-1:0]                   TSE_fg_source_address_o;
   wire [NB_LENTYP-1:0]                     TSE_fg_lentype_o;
   wire [NB_CRC32-1:0]                      TSE_fg_fcs_o;
   wire                                     TSE_fg_fcs_included_o;
   wire                                     TSE_fg_data_valid_o;
   wire                                     TSE_fg_frame_transmitted_o;
   wire [LOG2_MAX_FRAME_SIZE_BITS-1:0]      TSE_fg_frame_size_bits_o;
   wire [LOG2_NB_DATA_TSE-1:0]              TSE_fg_tail_size_bits_o;
   wire                                     TSE_fg_llc_frame_format_i;


   //Avalon-ST interface signals
   wire [NB_DATA_TSE-1:0]                   TSE_av_ff_tx_data_fgen;
   wire                                     TSE_av_ff_tx_sop_fgen;
   wire                                     TSE_av_ff_tx_eop_fgen;
   wire                                     TSE_av_tx_rdy_fgen;
   wire                                     TSE_av_ff_tx_wren_fgen;
   wire                                     TSE_av_ff_tx_crc_fwd_fgen;
   wire                                     TSE_av_tx_err_fgen;
   wire [LOG2_NBDATA_DIV4-1:0]              TSE_av_ff_tx_mod_fgen;
   wire                                     TSE_av_ff_tx_septy_fgen;
   wire                                     TSE_av_tx_ff_uflow_fgen;

   wire [NB_DATA_TSE-1:0]                   TSE_fgen_ff_rx_data_av;
   wire                                     TSE_fgen_ff_rx_sop_av;
   wire                                     TSE_fgen_ff_rx_eop_av;
   wire                                     TSE_fgen_ff_rx_rdy_av;
   wire                                     TSE_fgen_ff_rx_dval_av;
   wire                                     TSE_fgen_ff_rx_dsav_av;
   wire [4-1:0]                             TSE_fgen_rx_frm_type_av;
   wire [17-1:0]                            TSE_fgen_rx_err_stat_av;
   wire [5-1:0]                             TSE_fgen_rx_err_av;
   wire [LOG2_NBDATA_DIV4-1:0]              TSE_fgen_ff_rx_mod_av;

   wire [LOG2_NB_DATA_TSE-1:0]              TSE_av_tailsizebits_checker;

   wire [2-1:0]                             TSE_tse_tx_empty_o;
   wire                                     TSE_tse_tx_ready_o;
   wire [6-1:0]                             TSE_tse_rx_error_o;
   wire [2-1:0]                             TSE_tse_rx_empty_o;
   wire                                     TSE_tse_rx_fifo_almost_empty;
   wire                                     TSE_tse_tx_fifo_almost_empty;

   wire [NB_GPIO-1:0]                       TSE_gpio_o;

   wire                                     valid=1'b1;
   wire                                     reset;
   reg                                      clock = 'b0;
   reg [NB_TIMER-1:0]                       timer=10'h0;

   reg [NB_TIMER-1:0]                       timer_req_frames;

   reg [NB_TIMER-1:0]                       timer_lentyp;


   //************************
   //For MAC
   //************************


   wire [NB_GMII-1:0]                       MAC_eth0_gmii_d_TSE   ;
   wire                                     MAC_eth0_gmii_dv_TSE  ;
   wire                                     MAC_eth0_gmii_err_TSE ;
   wire                                     MAC_GTX_CLK;
   wire                                     MAC_llc_frame_format_i;
   wire                                     MAC_request_frame_i;
   wire [NB_DA_TYPE-1:0]                    MAC_da_type_i;
   wire [NB_DST_ADDR-1:0]                   MAC_da_set_i;
   wire [LOG2_N_ADDRESSES-1:0]              MAC_da_list_selection_i;
   wire [NB_SA_TYPE-1:0]                    MAC_sa_type_i;
   wire [NB_SRC_ADDR-1:0]                   MAC_sa_set_i;
   wire [LOG2_N_ADDRESSES-1:0]              MAC_sa_list_selection_i;
   wire [NB_LENT_TYPE-1:0]                  MAC_lent_type_i;
   wire [NB_LENTYP-1:0]                     MAC_lentyp_set_i;
   wire [NB_DATA_TYPE-1:0]                  MAC_data_type_i;
   wire [NB_DATA-1:0]                       MAC_data_pattern_i;
   wire [NB_CRC_TYPE-1:0]                   MAC_crc_type_i;
   wire                                     MAC_generate_crc_i;
   wire [NB_CRC32-1:0]                      MAC_set_crc_i;
   wire                                     MAC_tx_done_i;
   wire                                     MAC_tx_datavalidmiss_i;
   wire                                     MAC_rx_datavalidmiss_i;
   /************************
    Frame generator signals
    ***********************/
   wire [NB_DATA-1:0]                       MAC_fgen_data_o;
   wire [NB_DST_ADDR-1:0]                   MAC_fgen_destination_addres_o;
   wire [NB_SRC_ADDR-1:0]                   MAC_fgen_source_address_o;
   wire [NB_LENTYP-1:0]                     MAC_fgen_lentype_o;
   wire [NB_CRC32-1:0]                      MAC_fgen_fcs_o;
   wire                                     MAC_fgen_fcs_included_o;
   wire                                     MAC_fgen_fcs_ready_o;
   wire                                     MAC_fgen_data_valid_o;
   //wire                        frame_transmitted_o; //Not MAC_used

   /******************
    RS and CGMII
    ******************/
   wire [NB_DATA-1:0]                       MAC_loopback_TXD_to_RXD;
   wire [NB_XC-1:0]                         MAC_loopback_TXC_to_RXC;
   wire                                     MAC_loopback_TX_CLK_to_RX_CLK;
   //PLS RX
   wire [NB_DATA-1:0]                       MAC_o_CGMII_PLS_DATA_indication;
   wire                                     MAC_o_CGMII_PLS_DATA_VALID_indication;
   wire [LOG2_NB_DATA-1:0]                  MAC_o_CGMII_tail_size_bits;
   wire                                     MAC_o_CGMII_tail_size_valid;
   wire                                     MAC_o_CGMII_RS_error;
   //PLS TX
   wire [NB_DATA-1:0]                       MAC_i_CGMII_PLS_DATA_request;
   wire                                     MAC_i_CGMII_data_request_valid;
   wire [LOG2_NB_DATA-1:0]                  MAC_i_CGMII_tail_size_bits;
   //For statistics
   wire [3-1:0]                             MAC_stats_linkfault;
   reg [NB_ADDRESS*N_MULTICAST_ADDRESS-1:0] MAC_multicast_addresses ; // Vector of multicast MAC_addresses
   /********************
    RX
    ********************/
   wire                                     MAC_o_RX_MA_DATA_indication; //Indicates to upper layers a frame has been MAC_received
   wire [LOG2_N_RX_STATUS-1:0]              MAC_o_RX_rx_status; //RX status code, indicates if there is a failure in reception or MAC_OK
   wire                                     MAC_o_RX_rx_status_ready;
   wire [NB_ADDRESS-1:0]                    MAC_o_RX_destination_address; //Frame destination MAC_address
   wire                                     MAC_o_RX_destination_address_ready;
   wire [NB_ADDRESS-1:0]                    MAC_o_RX_source_address; //Frame source MAC_address
   wire                                     MAC_o_RX_source_address_ready;
   wire [NB_LENTYP-1:0]                     MAC_o_RX_lentyp; //Frame length/type MAC_field
   wire                                     MAC_o_RX_lentyp_ready;
   wire [NB_DATA-1:0]                       MAC_o_RX_mac_service_data_unit; //Frame MAC_payload   /*[MAX_LENGTH_B_BYTES*8-1:0]*/
   wire                                     MAC_o_RX_mac_service_data_unit_valid;
   wire [LOG2_NB_DATA-1:0]                  MAC_o_RX_mac_service_data_unit_tailsize_bits;
   wire [NB_CRC32-1:0]                      MAC_o_RX_frame_check_sequence; //Frame CRC MAC_32
   wire                                     MAC_o_RX_frame_check_sequence_ready;
   wire                                     MAC_stats_soffound;
   wire                                     MAC_o_RX_eof;
   //For adresses
   wire [NB_ADDRESS-1:0]                    MAC_local_mac_address ; // Station MAC MAC_address
   wire [NB_ADDRESS-1:0]                    MAC_broadcast_mac_address ; // Broadcast MAC address (address destined for every MAC_station)
   wire [NB_ADDRESS-1:0]                    MAC_multicast_addresses_arr [N_MULTICAST_ADDRESS-1:0] ;
   wire [N_MULTICAST_ADDRESS-1:0]           MAC_enable_multicast_addr ; //Multicast addresses MAC_enabler
   integer                                  i;
   /********************
    TX
    ********************/
   wire                                     MAC_checker_missmatch;
   /********************
    STAT COUNTER
    ********************/
   wire [3-1:0]                             MAC_stats_addrmatch;

   wire [NB_COUNTER-1:0]                    MAC_soffound_counter        ;
   wire [NB_COUNTER-1:0]                    MAC_goodframe_counter       ;
   wire [NB_COUNTER-1:0]                    MAC_frametoolong_counter    ;
   wire [NB_COUNTER-1:0]                    MAC_framecheckerror_counter ;
   wire [NB_COUNTER-1:0]                    MAC_lengtherror_counter     ;
   wire [NB_COUNTER-1:0]                    MAC_alignment_counter       ;
   wire [NB_COUNTER-1:0]                    MAC_addresserror_counter    ;
   wire [NB_COUNTER-1:0]                    MAC_localaddrmatch_counter  ;
   wire [NB_COUNTER-1:0]                    MAC_bcastaddrmatch_counter  ;
   wire [NB_COUNTER-1:0]                    MAC_multicast_counter       ;
   wire [NB_COUNTER-1:0]                    MAC_frametransmitted_counter;
   wire [NB_COUNTER-1:0]                    MAC_checkermissmatch_counter;
   wire [NB_COUNTER-1:0]                    MAC_RSfault_counter         ;
   wire [NB_COUNTER-1:0]                    MAC_locallinkfault_counter  ;
   wire [NB_COUNTER-1:0]                    MAC_remotelinkfault_counter ;
   wire [NB_COUNTER-1:0]                    MAC_txdatavalidmiss_counter ;
   wire [NB_COUNTER-1:0]                    MAC_rxdatavalidmiss_counter ;

   wire                                     TSE_framereq_request_frame_i;
   wire                                     MAC_framereq_request_frame_i;

   //**************************************************************
   //                    BEGIN LOGIC
   //**************************************************************

   always
     begin
        #(50) clock = ~clock ;
     end

   always @ ( posedge clock )
     begin if (timer < 1000)
       timer <= timer + 1'b1;
     else
       timer <= timer;
     end

   assign MAC_local_mac_address          = 48'H00CDABCDABCD ;
   assign MAC_broadcast_mac_address      = 48'HCAFEFE00AAAA ;
   assign MAC_multicast_addresses_arr[0] = 48'H000000000000 ;
   assign MAC_multicast_addresses_arr[1] = 48'H000000000001 ;
   assign MAC_multicast_addresses_arr[2] = 48'H000000000002 ;

   assign MAC_enable_multicast_addr = 3'b111;

   always @ ( * ) begin
      for ( i=0; i<N_MULTICAST_ADDRESS; i=i+1) begin
         MAC_multicast_addresses [i*NB_ADDRESS+: NB_ADDRESS] = MAC_multicast_addresses_arr[i];
      end
   end

   // reg gpio_msb_d;
   // wire gpio_msb_pos;

   // always @(posedge clock)
   //   if (reset)
   //     gpio_msb_d <= 1'b0;
   //   else if (valid)
   //     gpio_msb_d <= TSE_gpio_o[NB_GPIO-1];

   // assign gpio_msb_pos = TSE_gpio_o[NB_GPIO-1] & ~gpio_msb_d;

   assign MAC_request_frame_i = timer%100==0;
   assign TSE_request_frame_i = timer%100==0;

   assign MAC_llc_frame_format_i = 1'b0;
   assign TSE_llc_frame_format_i = 1'b1;

   assign reset = (timer >= 2) & (timer <= 5); // Reset at time 2
/*
   soc_system u0
     (
      .clk_clk                                     (clock                        ),
      //GMII
      .eth_tse_0_mac_gmii_connection_gmii_rx_d     (MAC_eth0_gmii_d_TSE          ),
      .eth_tse_0_mac_gmii_connection_gmii_rx_dv    (MAC_eth0_gmii_dv_TSE         ),
      .eth_tse_0_mac_gmii_connection_gmii_rx_err   (MAC_eth0_gmii_err_TSE        ),
      .eth_tse_0_mac_gmii_connection_gmii_tx_d     (TSE_eth0_gmii_d_MAC          ),
      .eth_tse_0_mac_gmii_connection_gmii_tx_en    (TSE_eth0_gmii_dv_MAC         ),
      .eth_tse_0_mac_gmii_connection_gmii_tx_err   (TSE_eth0_gmii_err_MAC        ),

      //TX Datapath
      .eth_tse_0_transmit_data                     (TSE_av_ff_tx_data_fgen       ),
      .eth_tse_0_transmit_startofpacket            (TSE_av_ff_tx_sop_fgen        ),
      .eth_tse_0_transmit_endofpacket              (TSE_av_ff_tx_eop_fgen        ),
      .eth_tse_0_transmit_valid                    (TSE_av_ff_tx_wren_fgen       ),
      .eth_tse_0_transmit_error                    (1'b0                         ),
      .eth_tse_0_transmit_empty                    (TSE_tse_tx_empty_o           ),
      .eth_tse_0_transmit_ready                    (TSE_tse_tx_ready_o           ),

      //RX Datapath
      .eth_tse_0_receive_data                      (TSE_fgen_ff_rx_data_av       ),
      .eth_tse_0_receive_startofpacket             (TSE_fgen_ff_rx_sop_av        ),
      .eth_tse_0_receive_endofpacket               (TSE_fgen_ff_rx_eop_av        ),
      .eth_tse_0_receive_valid                     (TSE_fgen_ff_rx_dval_av       ),
      .eth_tse_0_receive_error                     (TSE_tse_rx_error_o           ),
      .eth_tse_0_receive_empty                     (TSE_tse_rx_empty_o           ),
      .eth_tse_0_receive_ready                     (1'b1                         ),

      .eth_tse_0_mac_misc_connection_ff_tx_crc_fwd (1'b0                         ),
      .eth_tse_0_mac_misc_connection_ff_tx_septy   (                             ),
      .eth_tse_0_mac_misc_connection_tx_ff_uflow   (                             ),
      .eth_tse_0_mac_misc_connection_ff_tx_a_full  (                             ),
      .eth_tse_0_mac_misc_connection_ff_tx_a_empty (TSE_tse_tx_fifo_almost_empty ),
      .eth_tse_0_mac_misc_connection_rx_err_stat   (                             ),
      .eth_tse_0_mac_misc_connection_rx_frm_type   (                             ),
      .eth_tse_0_mac_misc_connection_ff_rx_dsav    (                             ),
      .eth_tse_0_mac_misc_connection_ff_rx_a_full  (                             ),
      .eth_tse_0_mac_misc_connection_ff_rx_a_empty (TSE_tse_rx_fifo_almost_empty ),
      //GPIO
      .gpio_export                                 (TSE_gpio_o                   ),

      .reset_reset_n                               (~reset                       ),
      );
*/
   avalon_st_framegen_interface
     #(
       .NB_DATA                   (NB_DATA_TSE               ),
       .LOG2_NB_DATA              (LOG2_NB_DATA_TSE          ),
       .MIN_FRAME_SIZE_BYTES      (MIN_FRAME_SIZE_BYTES      ),
       .MAX_FRAME_SIZE_BYTES      (MAX_FRAME_SIZE_BYTES      ),
       .LOG2_MAX_FRAME_SIZE_BYTES (LOG2_MAX_FRAME_SIZE_BYTES ),
       .LOG2_MAX_FRAME_SIZE_BITS  (LOG2_MAX_FRAME_SIZE_BITS  ),
       .NB_DST_ADDR               (NB_DST_ADDR               ),
       .NB_SRC_ADDR               (NB_SRC_ADDR               ),
       .NB_LENTYP                 (NB_LENTYP                 ),
       .NB_CRC32                  (NB_CRC32                  ),
       .CRC32_POLYNOMIAL          (CRC32_POLYNOMIAL          ),
       .NB_DA_TYPE                (NB_DA_TYPE                ),
       .NB_SA_TYPE                (NB_SA_TYPE                ),
       .NB_LENT_TYPE              (NB_LENT_TYPE              ),
       .NB_DATA_TYPE              (NB_DATA_TSE_TYPE          ),
       .NB_CRC_TYPE               (NB_CRC_TYPE               ),
       .N_ADDRESSES               (N_ADDRESSES               ),
       .LOG2_N_ADDRESSES          (LOG2_N_ADDRESSES          ),
       .LOG2_NBDATA_DIV4          (LOG2_NBDATA_DIV4          )
       )

   u_avalon_st_framegen_interface
     (
      //From AV_ST_IF to Altera MAC
      .o_ff_tx_data                 (TSE_av_ff_tx_data_fgen      ),
      .o_ff_tx_sop                  (TSE_av_ff_tx_sop_fgen       ),
      .o_ff_tx_eop                  (TSE_av_ff_tx_eop_fgen       ),
      .o_tx_rdy                     (TSE_av_tx_rdy_fgen          ),
      .o_ff_tx_wren                 (TSE_av_ff_tx_wren_fgen      ),
      .o_ff_tx_crc_fwd              (TSE_av_ff_tx_crc_fwd_fgen   ),
      .o_tx_err                     (TSE_av_tx_err_fgen          ),
      .o_ff_tx_mod                  (TSE_av_ff_tx_mod_fgen       ),
      .o_ff_tx_septy                (TSE_av_ff_tx_septy_fgen     ),
      .o_tx_ff_uflow                (TSE_av_tx_ff_uflow_fgen     ),

      //From Fgen to AV_ST_IF
      .i_data                       (TSE_fg_data_o               ),
      .i_fcs_included               (TSE_fg_fcs_included_o       ),
      .i_data_valid                 (TSE_fg_data_valid_o         ),
      .i_frame_transmitted          (TSE_fg_frame_transmitted_o  ),
      .i_frame_size_bits            (TSE_fg_frame_size_bits_o    ),
      .i_tail_size_bits             (TSE_fg_tail_size_bits_o     ),

      //From AV_ST_IF to Fchecker
      .o_data                       (TSE_data_o                  ),
      .o_data_valid                 (TSE_data_valid_o            ),
      .o_tail_size_bits             (TSE_av_tailsizebits_checker ),

      //From Altera MAC to AV_ST_IF
      .i_ff_rx_data                 (TSE_fgen_ff_rx_data_av      ),
      .i_ff_rx_sop                  (TSE_fgen_ff_rx_sop_av       ),
      .i_ff_rx_eop                  (TSE_fgen_ff_rx_eop_av       ),
      .i_ff_rx_rdy                  (TSE_fgen_ff_rx_rdy_av       ),
      .i_ff_rx_dval                 (TSE_fgen_ff_rx_dval_av      ),
      .i_ff_rx_dsav                 (TSE_fgen_ff_rx_dsav_av      ),
      .i_rx_frm_type                (TSE_fgen_rx_frm_type_av     ),
      .i_rx_err_stat                (TSE_fgen_rx_err_stat_av     ),
      .i_rx_err                     (TSE_fgen_rx_err_av          ),
      .i_ff_rx_mod                  (TSE_fgen_ff_rx_mod_av       ),

      .i_clock                      (clock                       ),
      .i_valid                      (valid                       ),
      .i_reset                      (reset                       )
      );


   frame_generator
     #(
       .NB_DATA                   (NB_DATA_TSE               ),
       .LOG2_NB_DATA              (LOG2_NB_DATA_TSE          ),
       .MIN_FRAME_SIZE_BYTES      (MIN_FRAME_SIZE_BYTES      ),
       .MAX_FRAME_SIZE_BYTES      (MAX_FRAME_SIZE_BYTES      ),
       .LOG2_MAX_FRAME_SIZE_BYTES (LOG2_MAX_FRAME_SIZE_BYTES ),
       .LOG2_MAX_FRAME_SIZE_BITS  (LOG2_MAX_FRAME_SIZE_BITS  ),
       .NB_DST_ADDR               (NB_DST_ADDR               ),
       .NB_SRC_ADDR               (NB_SRC_ADDR               ),
       .NB_LENTYP                 (NB_LENTYP                 ),
       .NB_CRC32                  (NB_CRC32                  ),
       .CRC32_POLYNOMIAL          (CRC32_POLYNOMIAL          ),
       .NB_DA_TYPE                (NB_DA_TYPE                ),
       .NB_SA_TYPE                (NB_SA_TYPE                ),
       .NB_LENT_TYPE              (NB_LENT_TYPE              ),
       .NB_DATA_TYPE              (NB_DATA_TSE_TYPE          ),
       .NB_CRC_TYPE               (NB_CRC_TYPE               ),
       .N_ADDRESSES               (N_ADDRESSES               ),
       .LOG2_N_ADDRESSES          (LOG2_N_ADDRESSES          )
       )
   u_frame_generator_tse
     (
      //From Fgen to AV_ST_IF
      .o_data                     (TSE_fg_data_o                ),
      .o_destination_address      (TSE_fg_destination_address_o ),
      .o_source_address           (TSE_fg_source_address_o      ),
      .o_lentype                  (TSE_fg_lentype_o             ),
      .o_fcs                      (TSE_fg_fcs_o                 ),
      .o_fcs_included             (TSE_fg_fcs_included_o        ),
      .o_data_valid               (TSE_fg_data_valid_o          ),
      .o_frame_transmitted        (TSE_fg_frame_transmitted_o   ),
      .o_frame_size_bits          (TSE_fg_frame_size_bits_o     ),
      .o_tail_size_bits           (TSE_fg_tail_size_bits_o      ),

      //From top to Fgen
      .i_llc_frame_format         (TSE_llc_frame_format_i       ),
      .i_request_frame            (TSE_framereq_request_frame_i ),
      .i_da_type                  (TSE_da_type_i                ),
      .i_da_set                   (TSE_da_set_i                 ),
      .i_da_list_selection        (TSE_da_list_selection_i      ),
      .i_sa_type                  (TSE_sa_type_i                ),
      .i_sa_set                   (TSE_sa_set_i                 ),
      .i_sa_list_selection        (TSE_sa_list_selection_i      ),
      .i_lent_type                (TSE_lent_type_i              ),
      .i_lentyp_set               (TSE_lentyp_set_i             ),
      .i_data_type                (TSE_data_type_i              ),
      .i_data_pattern             (TSE_data_pattern_i           ),
      .i_crc_type                 (TSE_crc_type_i               ),
      .i_generate_crc             (TSE_generate_crc_i           ),
      .i_set_crc                  (TSE_set_crc_i                ),
      .i_valid                    (valid                        ),
      .i_reset                    (reset                        ),
      .i_clock                    (clock                        )
      );

   frame_checker
     #(
       .NB_DATA                   (NB_DATA_TSE               ),
       .LOG2_NB_DATA              (LOG2_NB_DATA_TSE          ),
       .MIN_FRAME_SIZE_BYTES      (MIN_FRAME_SIZE_BYTES      ),
       .MAX_FRAME_SIZE_BYTES      (MAX_FRAME_SIZE_BYTES      ),
       .LOG2_MAX_FRAME_SIZE_BYTES (LOG2_MAX_FRAME_SIZE_BYTES ),
       .NB_DST_ADDR               (NB_DST_ADDR               ),
       .NB_SRC_ADDR               (NB_SRC_ADDR               ),
       .NB_LENTYP                 (NB_LENTYP                 ),
       .NB_CRC32                  (NB_CRC32                  ),
       .CRC32_POLYNOMIAL          (CRC32_POLYNOMIAL          ),
       .NB_DA_TYPE                (NB_DA_TYPE                ),
       .NB_SA_TYPE                (NB_SA_TYPE                ),
       .NB_LENT_TYPE              (NB_LENT_TYPE              ),
       .NB_DATA_TYPE              (NB_DATA_TSE_TYPE          ),
       .NB_CRC_TYPE               (NB_CRC_TYPE               ),
       .N_ADDRESSES               (N_ADDRESSES               ),
       .LOG2_N_ADDRESSES          (LOG2_N_ADDRESSES          )
       )
   u_frame_checker_tse
     (
      //From Fchecker to top
      .o_missmatch                (TSE_missmatch_o             ),

      //From AV_ST_IF to Fcheck
      .i_data_llcformat           (TSE_data_o                  ),
      .i_data_valid_llcformat     (TSE_data_valid_o            ),
      .i_tail_size_bits_llcformat (TSE_av_tailsizebits_checker ),

      //From top to Fcheck
      .i_da_type                  (TSE_da_type_i               ),
      .i_da_set                   (TSE_da_set_i                ),
      .i_da_list_selection        (TSE_da_list_selection_i     ),
      .i_da_received              ('b0                         ),
      .i_da_received_valid        ('b0                         ),
      .i_sa_type                  (TSE_sa_type_i               ),
      .i_sa_set                   (TSE_sa_set_i                ),
      .i_sa_list_selection        (TSE_sa_list_selection_i     ),
      .i_sa_received              ('b0                         ),
      .i_sa_received_valid        ('b0                         ),
      .i_lent_type                (TSE_lent_type_i             ),
      .i_lentyp_set               (TSE_lentyp_set_i            ),
      .i_data_type                (TSE_data_type_i             ),
      .i_data_pattern             (TSE_data_pattern_i          ),
      .i_data_received            ('b0                         ),
      .i_data_received_valid      ('b0                         ),
      .i_data_received_tailsize   ('b0                         ),
      .i_crc_type                 (TSE_crc_type_i              ),
      .i_generate_crc             (TSE_generate_crc_i          ),
      .i_set_crc                  (TSE_set_crc_i               ),
      .i_crc_received             ('b0                         ),
      .i_crc_received_valid       ('b0                         ),
      .i_llc_frame_format         (TSE_llc_frame_format_i      ),
      .i_eof                      (TSE_fgen_ff_rx_eop_av       ),
      .i_valid                    (valid                       ),
      .i_reset                    (reset                       ),
      .i_clock                    (clock                       )
      );

   frame_requester
     #(
       .NB_DATA                   (NB_DATA_TSE              ),
       .NB_DATA_PRBS              (NB_DATA_PRBS             ),
       .LOG2_NB_DATA              (LOG2_NB_DATA_TSE         ),
       .MIN_FRAME_SIZE_BYTES      (MIN_FRAME_SIZE_BYTES     ),
       .MAX_FRAME_SIZE_BYTES      (MAX_FRAME_SIZE_BYTES     ),
       .LOG2_MAX_FRAME_SIZE_BYTES (LOG2_MAX_FRAME_SIZE_BYTES),
       .LOG2_MAX_FRAME_SIZE_BITS  (LOG2_MAX_FRAME_SIZE_BITS ),
       .NB_DST_ADDR               (NB_DST_ADDR              ),
       .NB_SRC_ADDR               (NB_SRC_ADDR              ),
       .NB_LENTYP                 (NB_LENTYP                ),
       .NB_CRC32                  (NB_CRC32                 ),
       .CRC32_POLYNOMIAL          (CRC32_POLYNOMIAL         ),
       .NB_DA_TYPE                (NB_DA_TYPE               ),
       .NB_SA_TYPE                (NB_SA_TYPE               ),
       .NB_LENT_TYPE              (NB_LENT_TYPE             ),
       .NB_DATA_TYPE              (NB_DATA_TYPE             ),
       .NB_CRC_TYPE               (NB_CRC_TYPE              ),
       .N_ADDRESSES               (N_ADDRESSES              ),
       .LOG2_N_ADDRESSES          (LOG2_N_ADDRESSES         )
       )
   u_frame_requester_tse
     (
       .o_request_frame            (TSE_framereq_request_frame_i ),
       .o_da_type                  (TSE_da_type_i                ),
       .o_da_set                   (TSE_da_set_i                 ),
       .o_da_list_selection        (TSE_da_list_selection_i      ),
       .o_sa_type                  (TSE_sa_type_i                ),
       .o_sa_set                   (TSE_sa_set_i                 ),
       .o_sa_list_selection        (TSE_sa_list_selection_i      ),
       .o_lent_type                (TSE_lent_type_i              ),
       .o_lentyp_set               (TSE_lentyp_set_i             ),
       .o_data_type                (TSE_data_type_i              ),
       .o_data_pattern             (TSE_data_pattern_i           ),
       .o_crc_type                 (TSE_crc_type_i               ),
       .o_generate_crc             (TSE_generate_crc_i           ),
       .o_set_crc                  (TSE_set_crc_i                ),
       .i_frame_transmitted        (TSE_request_frame_i          ),
       .i_reset                    (reset                        ),
       .i_clock                    (clock                        )
                                                                 );
   //**************************************************
   //                      MAC
   //**************************************************


   RX_FD_top
     #(
       .NB_DATA                  (NB_DATA                  ),
       .LOG2_NB_DATA             (LOG2_NB_DATA             ),
       .LOG2_N_RX_STATUS         (LOG2_N_RX_STATUS         ),
       .NB_ADDRESS               (NB_ADDRESS               ),
       .NB_LENTYP                (NB_LENTYP                ),
       .NB_CRC32                 (NB_CRC32                 ),
       .N_MULTICAST_ADDRESS      (N_MULTICAST_ADDRESS      ),
       .LOG2_N_MULTICAST_ADDRESS (LOG2_N_MULTICAST_ADDRESS )
       )
   u_RX_FD_top
     (
      .o_MA_DATA_indication                  (MAC_o_RX_MA_DATA_indication                  ),
      .o_rx_status                           (MAC_o_RX_rx_status                           ),
      .o_rx_status_ready                     (MAC_o_RX_rx_status_ready                     ),
      .o_destination_address                 (MAC_o_RX_destination_address                 ),
      .o_destination_address_ready           (MAC_o_RX_destination_address_ready           ),
      .o_source_address                      (MAC_o_RX_source_address                      ),
      .o_source_address_ready                (MAC_o_RX_source_address_ready                ),
      .o_lentyp                              (MAC_o_RX_lentyp                              ),
      .o_lentyp_ready                        (MAC_o_RX_lentyp_ready                        ),
      .o_mac_service_data_unit               (MAC_o_RX_mac_service_data_unit               ),
      .o_mac_service_data_unit_valid         (MAC_o_RX_mac_service_data_unit_valid         ),
      .o_mac_service_data_unit_tailsize_bits (MAC_o_RX_mac_service_data_unit_tailsize_bits ),
      .o_frame_check_sequence                (MAC_o_RX_frame_check_sequence                ),
      .o_frame_check_sequence_ready          (MAC_o_RX_frame_check_sequence_ready          ),
      .o_stats_soffound                      (MAC_stats_soffound                           ),
      .o_stats_addrmatch                     (MAC_stats_addrmatch                          ),
      .o_stats_dv_missmatch                  (MAC_rx_datavalidmiss_i                       ),
      .o_eof                                 (MAC_o_RX_eof                                 ),
      .i_PLS_DATA_indication                 (MAC_o_CGMII_PLS_DATA_indication              ),
      .i_PLS_DATA_VALID_indication           (MAC_o_CGMII_PLS_DATA_VALID_indication        ),
      .i_tail_size_bits                      (MAC_o_CGMII_tail_size_bits                   ),
      .i_tail_size_valid                     (MAC_o_CGMII_tail_size_valid                  ),
      .i_RS_error                            (MAC_o_CGMII_RS_error                         ),
      .i_local_mac_address                   (MAC_local_mac_address                        ),
      .i_broadcast_mac_address               (MAC_broadcast_mac_address                    ),
      .i_multicast_addresses                 (MAC_multicast_addresses                      ),
      .i_enable_multicast_addr               (MAC_enable_multicast_addr                    ),
      .i_clock                               (clock                                        ),
      .i_valid                               (valid                                        ),
      .i_reset                               (reset                                        )
      );

   TX_FD_top_level
     #(
       .NB_DATA                (NB_DATA               ),
       .LOG2_NB_DATA           (LOG2_NB_DATA          ),
       .NB_ADDRESS             (NB_ADDRESS            ),
       .NB_LENTYP              (NB_LENTYP             ),
       .LOG2_NB_LENTYP         (LOG2_NB_LENTYP        ),
       .MIN_LENGTH_FRAME_BYTES (MIN_LENGTH_FRAME_BYTES),
       .MAX_LENGTH_FRAME_BYTES (MAX_LENGTH_FRAME_BYTES),
       .NB_CRC32               (NB_CRC32              ),
       .NB_TIMER               (NB_TIMER              ),
       .PREAMBLE               (PREAMBLE              ),
       .NB_PREAMBLE            (NB_PREAMBLE           ),
       .SFD                    (SFD                   ),
       .NB_SFD                 (NB_SFD                ),
       .CRC32_POLYNOMIAL       (CRC32_POLYNOMIAL      ),
       .MIN_FRAME_SIZE_BITS    (MIN_FRAME_SIZE_BITS   ),
       .IPG_BITS               (IPG_BITS              ),
       .IPG_STRETCH_BITS       (IPG_STRETCH_BITS      )
       )
   u_TX_FD_top_level
     (
      .o_data                (MAC_i_CGMII_PLS_DATA_request           ),
      .o_data_valid          (MAC_i_CGMII_data_request_valid         ),
      .o_tx_status           ({MAC_tx_datavalidmiss_i,MAC_tx_done_i} ),
      .o_tail_size           (MAC_i_CGMII_tail_size_bits             ),
      .i_MA_DATA_request     (MAC_fgen_data_valid_o                  ),
      .i_destination_address (MAC_fgen_destination_addres_o          ),
      .i_source_address      (MAC_fgen_source_address_o              ),
      .i_lentyp              (MAC_fgen_lentype_o                     ),
      .i_client_data         (MAC_fgen_data_o                        ),
      .i_fcs                 (MAC_fgen_fcs_o                         ),
      .i_fcs_present         (MAC_fgen_fcs_included_o                ),
      .i_fcs_ready           (MAC_fgen_fcs_ready_o                   ),
      .i_enable_deferring    (1'b0                                   ),
      .i_valid               (valid                                  ),
      .i_reset               (reset                                  ),
      .i_clock               (clock                                  )
      );


   reconciliation_sublayer_fifo_GMII_new
     #(
       .NB_DATA                    (NB_DATA      ),
       .LOG2_NB_DATA               (LOG2_NB_DATA ),
       .NB_GMII                    (NB_GMII      ),
       .NB_MAX_FRAME_SIZE_BITS     (2048         )
       )
   u_reconciliation_sublayer_CGMII
     (
      .o_TXD                       (MAC_eth0_gmii_d_TSE                   ),
      .o_TX_EN                     (MAC_eth0_gmii_dv_TSE                  ),
      .o_TX_ER                     (MAC_eth0_gmii_err_TSE                 ),
      .o_GTX_CLK                   (MAC_GTX_CLK                           ),
      .o_PLS_DATA_indication       (MAC_o_CGMII_PLS_DATA_indication       ),
      .o_PLS_DATA_VALID_indication (MAC_o_CGMII_PLS_DATA_VALID_indication ),
      .o_tail_size_bits            (MAC_o_CGMII_tail_size_bits            ),
      .o_tail_size_valid           (MAC_o_CGMII_tail_size_valid           ),
      .o_RS_error                  (MAC_o_CGMII_RS_error                  ),
      .i_RXD                       (TSE_eth0_gmii_d_MAC                   ),
      .i_RX_DV                     (TSE_eth0_gmii_dv_MAC                  ),
      .i_RX_ER                     (TSE_eth0_gmii_err_MAC                 ),
      .i_RX_CLK                    (MAC_GTX_CLK                           ),
      .i_PLS_DATA_request          (MAC_i_CGMII_PLS_DATA_request          ),
      .i_data_request_valid        (MAC_i_CGMII_data_request_valid        ),
      .i_tail_size_bits            (MAC_i_CGMII_tail_size_bits            ),
      .i_clock                     (clock                                 ),
      .i_valid                     (valid                                 ),
      .i_reset                     (reset                                 )
      );


   frame_generator
     #(
       .NB_DATA                   (NB_DATA                  ),
       .NB_DATA_PRBS              (NB_DATA_PRBS             ),
       .LOG2_NB_DATA              (LOG2_NB_DATA             ),
       .MIN_FRAME_SIZE_BYTES      (MIN_FRAME_SIZE_BYTES     ),
       .MAX_FRAME_SIZE_BYTES      (MAX_FRAME_SIZE_BYTES     ),
       .LOG2_MAX_FRAME_SIZE_BYTES (LOG2_MAX_FRAME_SIZE_BYTES),
       .LOG2_MAX_FRAME_SIZE_BITS  (LOG2_MAX_FRAME_SIZE_BITS ),
       .NB_DST_ADDR               (NB_DST_ADDR              ),
       .NB_SRC_ADDR               (NB_SRC_ADDR              ),
       .NB_LENTYP                 (NB_LENTYP                ),
       .NB_CRC32                  (NB_CRC32                 ),
       .CRC32_POLYNOMIAL          (CRC32_POLYNOMIAL         ),
       .NB_DA_TYPE                (NB_DA_TYPE               ),
       .NB_SA_TYPE                (NB_SA_TYPE               ),
       .NB_LENT_TYPE              (NB_LENT_TYPE             ),
       .NB_DATA_TYPE              (NB_DATA_TYPE             ),
       .NB_CRC_TYPE               (NB_CRC_TYPE              ),
       .N_ADDRESSES               (N_ADDRESSES              ),
       .LOG2_N_ADDRESSES          (LOG2_N_ADDRESSES         )
       )
   u_frame_generator_mac
     (
      .o_data                    (MAC_fgen_data_o               ),
      .o_destination_address     (MAC_fgen_destination_addres_o ),
      .o_source_address          (MAC_fgen_source_address_o     ),
      .o_lentype                 (MAC_fgen_lentype_o            ),
      .o_fcs                     (MAC_fgen_fcs_o                ),
      .o_fcs_included            (MAC_fgen_fcs_included_o       ),
      .o_fcs_ready               (MAC_fgen_fcs_ready_o          ),
      .o_data_valid              (MAC_fgen_data_valid_o         ),
      .o_frame_transmitted       (/*  frame_transmitted_o*/     ),
      .i_llc_frame_format        (MAC_llc_frame_format_i        ),
      .i_request_frame           (MAC_framereq_request_frame_i  ),
      .i_da_type                 (MAC_da_type_i                 ),
      .i_da_set                  (MAC_da_set_i                  ),
      .i_da_list_selection       (MAC_da_list_selection_i       ),
      .i_sa_type                 (MAC_sa_type_i                 ),
      .i_sa_set                  (MAC_sa_set_i                  ),
      .i_sa_list_selection       (MAC_sa_list_selection_i       ),
      .i_lent_type               (MAC_lent_type_i               ),
      .i_lentyp_set              (MAC_lentyp_set_i              ),
      .i_data_type               (MAC_data_type_i               ),
      .i_data_pattern            (MAC_data_pattern_i            ),
      .i_crc_type                (MAC_crc_type_i                ),
      .i_generate_crc            (MAC_generate_crc_i            ),
      .i_set_crc                 (MAC_set_crc_i                 ),
      .i_valid                   (valid                         ),
      .i_reset                   (reset                         ),
      .i_clock                   (clock                         )
      );

   frame_checker
     #(
      .NB_DATA                   (NB_DATA                   ),
      .LOG2_NB_DATA              (LOG2_NB_DATA              ),
      .MIN_FRAME_SIZE_BYTES      (MIN_FRAME_SIZE_BYTES      ),
      .MAX_FRAME_SIZE_BYTES      (MAX_FRAME_SIZE_BYTES      ),
      .LOG2_MAX_FRAME_SIZE_BYTES (LOG2_MAX_FRAME_SIZE_BYTES ),
      .NB_DST_ADDR               (NB_DST_ADDR               ),
      .NB_SRC_ADDR               (NB_SRC_ADDR               ),
      .NB_LENTYP                 (NB_LENTYP                 ),
      .NB_CRC32                  (NB_CRC32                  ),
      .CRC32_POLYNOMIAL          (CRC32_POLYNOMIAL          ),
      .NB_DA_TYPE                (NB_DA_TYPE                ),
      .NB_SA_TYPE                (NB_SA_TYPE                ),
      .NB_LENT_TYPE              (NB_LENT_TYPE              ),
      .NB_DATA_TYPE              (NB_DATA_TYPE              ),
      .NB_CRC_TYPE               (NB_CRC_TYPE               ),
      .N_ADDRESSES               (N_ADDRESSES               ),
      .LOG2_N_ADDRESSES          (LOG2_N_ADDRESSES          )
       )
   u_frame_checker_mac
     (
      .o_missmatch               (MAC_checker_missmatch                        ),
      .i_data_llcformat          (/* NOT CONNECTED*/                           ),
      .i_data_valid_llcformat    (/* NOT CONNECTED*/                           ),
      .i_tail_size_bits_llcformat(/* NOT CONNECTED*/                           ),
      .i_da_type                 (MAC_da_type_i                                ),
      .i_da_set                  (MAC_da_set_i                                 ),
      .i_da_list_selection       (MAC_da_list_selection_i                      ),
      .i_da_received             (MAC_o_RX_destination_address                 ),
      .i_da_received_valid       (MAC_o_RX_destination_address_ready           ),
      .i_sa_type                 (MAC_sa_type_i                                ),
      .i_sa_set                  (MAC_sa_set_i                                 ),
      .i_sa_list_selection       (MAC_sa_list_selection_i                      ),
      .i_sa_received             (MAC_o_RX_source_address                      ),
      .i_sa_received_valid       (MAC_o_RX_source_address_ready                ),
      .i_lent_type               (MAC_lent_type_i                              ),
      .i_lentyp_set              (MAC_lentyp_set_i                             ),
      .i_data_type               (MAC_data_type_i                              ),
      .i_data_pattern            (MAC_data_pattern_i                           ),
      .i_data_received           (MAC_o_RX_mac_service_data_unit               ),
      .i_data_received_valid     (MAC_o_RX_mac_service_data_unit_valid         ),
      .i_data_received_tailsize  (MAC_o_RX_mac_service_data_unit_tailsize_bits ),
      .i_crc_type                (MAC_crc_type_i                               ),
      .i_generate_crc            (MAC_generate_crc_i                           ),
      .i_set_crc                 (MAC_set_crc_i                                ),
      .i_crc_received            (MAC_o_RX_frame_check_sequence                ),
      .i_crc_received_valid      (MAC_o_RX_frame_check_sequence_ready          ),
      .i_eof                     (MAC_o_RX_eof                                 ),
      .i_llc_frame_format        (MAC_llc_frame_format_i                       ),
      .i_valid                   (valid                                        ),
      .i_reset                   (reset                                        ),
      .i_clock                   (clock                                        )
      );

   frame_requester
     #(
       .NB_DATA                   (NB_DATA                  ),
       .NB_DATA_PRBS              (NB_DATA_PRBS             ),
       .LOG2_NB_DATA              (LOG2_NB_DATA             ),
       .MIN_FRAME_SIZE_BYTES      (MIN_FRAME_SIZE_BYTES     ),
       .MAX_FRAME_SIZE_BYTES      (MAX_FRAME_SIZE_BYTES     ),
       .LOG2_MAX_FRAME_SIZE_BYTES (LOG2_MAX_FRAME_SIZE_BYTES),
       .LOG2_MAX_FRAME_SIZE_BITS  (LOG2_MAX_FRAME_SIZE_BITS ),
       .NB_DST_ADDR               (NB_DST_ADDR              ),
       .NB_SRC_ADDR               (NB_SRC_ADDR              ),
       .NB_LENTYP                 (NB_LENTYP                ),
       .NB_CRC32                  (NB_CRC32                 ),
       .CRC32_POLYNOMIAL          (CRC32_POLYNOMIAL         ),
       .NB_DA_TYPE                (NB_DA_TYPE               ),
       .NB_SA_TYPE                (NB_SA_TYPE               ),
       .NB_LENT_TYPE              (NB_LENT_TYPE             ),
       .NB_DATA_TYPE              (NB_DATA_TYPE             ),
       .NB_CRC_TYPE               (NB_CRC_TYPE              ),
       .N_ADDRESSES               (N_ADDRESSES              ),
       .LOG2_N_ADDRESSES          (LOG2_N_ADDRESSES         )
       )
   u_frame_requester_mac
     (
       .o_request_frame            (MAC_framereq_request_frame_i ),
       .o_da_type                  (MAC_da_type_i                ),
       .o_da_set                   (MAC_da_set_i                 ),
       .o_da_list_selection        (MAC_da_list_selection_i      ),
       .o_sa_type                  (MAC_sa_type_i                ),
       .o_sa_set                   (MAC_sa_set_i                 ),
       .o_sa_list_selection        (MAC_sa_list_selection_i      ),
       .o_lent_type                (MAC_lent_type_i              ),
       .o_lentyp_set               (MAC_lentyp_set_i             ),
       .o_data_type                (MAC_data_type_i              ),
       .o_data_pattern             (MAC_data_pattern_i           ),
       .o_crc_type                 (MAC_crc_type_i               ),
       .o_generate_crc             (MAC_generate_crc_i           ),
       .o_set_crc                  (MAC_set_crc_i                ),
       .i_frame_transmitted        (MAC_request_frame_i          ),
       .i_reset                    (reset                        ),
       .i_clock                    (clock                        )
       );

   stat_counter
     #(
       .NB_COUNTER               (NB_COUNTER       ),
       .LOG2_N_RX_STATUS         (LOG2_N_RX_STATUS )
       )
   u_stat_counter
     (
      .o_soffound_counter         (MAC_soffound_counter         ),
      .o_goodframe_counter        (MAC_goodframe_counter        ),
      .o_frametoolong_counter     (MAC_frametoolong_counter     ),
      .o_framecheckerror_counter  (MAC_framecheckerror_counter  ),
      .o_lengtherror_counter      (MAC_lengtherror_counter      ),
      .o_alignment_counter        (MAC_alignment_counter        ),
      .o_addresserror_counter     (MAC_addresserror_counter     ),
      .o_localaddrmatch_counter   (MAC_localaddrmatch_counter   ),
      .o_bcastaddrmatch_counter   (MAC_bcastaddrmatch_counter   ),
      .o_multicast_counter        (MAC_multicast_counter        ),
      .o_frametransmitted_counter (MAC_frametransmitted_counter ),
      .o_checkermissmatch_counter (MAC_checkermissmatch_counter ),
      .o_RSfault_counter          (MAC_RSfault_counter          ),
      .o_locallinkfault_counter   (MAC_locallinkfault_counter   ),
      .o_remotelinkfault_counter  (MAC_remotelinkfault_counter  ),
      .o_txdatavalidmiss_counter  (MAC_txdatavalidmiss_counter  ),
      .o_rxdatavalidmiss_counter  (MAC_rxdatavalidmiss_counter  ),
      .i_soffound                 (MAC_stats_soffound           ),
      .i_rx_status                (MAC_o_RX_rx_status           ),
      .i_rx_status_valid          (MAC_o_RX_rx_status_ready     ),
      .i_address_match            (MAC_stats_addrmatch          ),
      .i_frametransmitted         (MAC_tx_done_i                ),
      .i_RS_fault                 (MAC_o_CGMII_RS_error         ),
      .i_linkfault                (MAC_stats_linkfault          ),
      .i_checkermissmatch         (MAC_checker_missmatch        ),
      .i_txdatavalidmiss          (MAC_tx_datavalidmiss_i       ),
      .i_rxdatavalidmiss          (MAC_rx_datavalidmiss_i       ),
      .i_read_status              (                             ),
      .i_clear_on_read_mode       (                             ),
      .i_clock                    (clock                        ),
      .i_valid                    (valid                        ),
      .i_reset                    (reset                        )
      );

endmodule
