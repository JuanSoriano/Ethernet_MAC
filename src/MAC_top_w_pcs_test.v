module MAC_top_w_pcs_test();
   //RX
   localparam NB_DATA                   = 64 ;
   localparam LOG2_NB_DATA              = 6 ;
   localparam LOG2_N_RX_STATUS          = 3 ;
   localparam NB_ADDRESS                = 48 ;
   localparam NB_LENTYP                 = 16 ;
   localparam NB_CRC32                  = 32 ;
   localparam N_MULTICAST_ADDRESS       = 3 ;
   localparam LOG2_N_MULTICAST_ADDRESS   = 2 ;

   //TX
   localparam LOG2_NB_LENTYP             = 4 ;
   localparam MIN_LENGTH_FRAME_BYTES     = 64;
   localparam MAX_LENGTH_FRAME_BYTES     = 1982;
   localparam NB_TIMER                   = 10; // Nr of bits from frame
   localparam PREAMBLE                   = 56'h55555555555555;
   localparam NB_PREAMBLE                = 56;
   localparam SFD                        = 8'hD5;
   localparam NB_SFD                     = 8;
   localparam CRC32_POLYNOMIAL           = 33'h104C11DB7;
   localparam MIN_FRAME_SIZE_BITS        = 512;
   localparam IPG_BITS                   = 96;
   localparam IPG_STRETCH_BITS           = 0;

   //GMII
   localparam NB_XC                      = 8;
   localparam N_LANES                    = 8;

   //Framegen/checker
   localparam NB_DATA_PRBS              = 64 ;
   localparam MIN_FRAME_SIZE_BYTES      = 0 ;
   localparam MAX_FRAME_SIZE_BYTES      = 1500 ;
   localparam LOG2_MAX_FRAME_SIZE_BYTES = 11;
   localparam LOG2_MAX_FRAME_SIZE_BITS  = 14;
   localparam NB_DST_ADDR               = 48;
   localparam NB_SRC_ADDR               = 48;
   localparam NB_DA_TYPE                = 2;
   localparam NB_SA_TYPE                = 2;
   localparam NB_LENT_TYPE              = 2;
   localparam NB_DATA_TYPE              = 2;
   localparam NB_CRC_TYPE               = 1;
   localparam N_ADDRESSES               = 10;
   localparam LOG2_N_ADDRESSES          = 4;

   //Stat counter
   localparam NB_COUNTER                = 32;

   //PCS
	 parameter							LEN_DATA_BLOCK 	= 64;
	 parameter							LEN_CTRL_BLOCK 	= 8;
	 parameter							LEN_CODED_BLOCK	= 66;
	 parameter							TX_NMODULES		= 2;
	 parameter							RX_NMODULES 	= 2;

   wire                        llc_frame_format_i;
   wire                        request_frame_i;
   wire [NB_DA_TYPE-1:0]       da_type_i;
   wire [NB_DST_ADDR-1:0]      da_set_i;
   wire [LOG2_N_ADDRESSES-1:0] da_list_selection_i;
   wire [NB_SA_TYPE-1:0]       sa_type_i;
   wire [NB_SRC_ADDR-1:0]      sa_set_i;
   wire [LOG2_N_ADDRESSES-1:0] sa_list_selection_i;
   wire [NB_LENT_TYPE-1:0]     lent_type_i;
   wire [NB_LENTYP-1:0]        lentyp_set_i;
   wire [NB_DATA_TYPE-1:0]     data_type_i;
   wire [NB_DATA-1:0]          data_pattern_i;
   wire [NB_CRC_TYPE-1:0]      crc_type_i;
   wire                        generate_crc_i;
   wire [NB_CRC32-1:0]         set_crc_i;
   wire                        tx_done_i;
   wire                        tx_datavalidmiss_i;
   wire                        rx_datavalidmiss_i;
   /************************
    Frame generator signals
    ***********************/
    wire [NB_DATA-1:0]          fgen_data_o;
    wire [NB_DST_ADDR-1:0]      fgen_destination_addres_o;
    wire [NB_SRC_ADDR-1:0]      fgen_source_address_o;
    wire [NB_LENTYP-1:0]        fgen_lentype_o;
    wire [NB_CRC32-1:0]         fgen_fcs_o;
    wire                        fgen_fcs_included_o;
    wire                        fgen_fcs_ready_o;
    wire                        fgen_data_valid_o;
    //wire                        frame_transmitted_o; //Not used

   /******************
    RS and CGMII
    ******************/
   wire [NB_DATA-1:0]           mac_TXD_to_pcs;
   wire [NB_XC-1:0]             mac_TXC_to_pcs;
   wire                         mac_TX_CLK_to_pcs;

   wire [NB_DATA-1:0]           pcs_RXD_to_mac;
   wire [NB_XC-1:0]             pcs_RXC_to_mac;
   //PLS RX
   wire [NB_DATA-1:0]           o_CGMII_PLS_DATA_indication;
   wire                         o_CGMII_PLS_DATA_VALID_indication;
   wire [LOG2_NB_DATA-1:0]      o_CGMII_tail_size_bits;
   wire                         o_CGMII_tail_size_valid;
   wire                         o_CGMII_RS_error;
   //PLS TX
   wire [NB_DATA-1:0]           i_CGMII_PLS_DATA_request;
   wire                         i_CGMII_data_request_valid;
   wire [LOG2_NB_DATA-1:0]      i_CGMII_tail_size_bits;
   //For statistics
   wire [3-1:0]                 stats_linkfault;

   /********************
           RX
   ********************/
   wire                        o_RX_MA_DATA_indication; //Indicates to upper layers a frame has been received
   wire [LOG2_N_RX_STATUS-1:0] o_RX_rx_status; //RX status code, indicates if there is a failure in reception or OK
   wire                        o_RX_rx_status_ready;
   wire [NB_ADDRESS-1:0]       o_RX_destination_address; //Frame destination address
   wire                        o_RX_destination_address_ready;
   wire [NB_ADDRESS-1:0]       o_RX_source_address; //Frame source address
   wire                        o_RX_source_address_ready;
   wire [NB_LENTYP-1:0]        o_RX_lentyp; //Frame length/type field
   wire                        o_RX_lentyp_ready;
   wire [NB_DATA-1:0]          o_RX_mac_service_data_unit; //Frame payload   /*[MAX_LENGTH_B_BYTES*8-1:0]*/
   wire                        o_RX_mac_service_data_unit_valid;
   wire [LOG2_NB_DATA-1:0]     o_RX_mac_service_data_unit_tailsize_bits;
   wire [NB_CRC32-1:0]         o_RX_frame_check_sequence; //Frame CRC 32
   wire                        o_RX_frame_check_sequence_ready;
   wire                        stats_soffound;
   wire                        o_RX_eof;
   //For adresses
   wire [NB_ADDRESS-1:0]       local_mac_address ; // Station MAC address
   wire [NB_ADDRESS-1:0]       broadcast_mac_address ; // Broadcast MAC address (address destined for every station)
   reg [NB_ADDRESS*N_MULTICAST_ADDRESS-1:0] multicast_addresses ; // Vector of multicast addresses
   wire [NB_ADDRESS-1:0]                    multicast_addresses_arr [N_MULTICAST_ADDRESS-1:0] ;
   wire [N_MULTICAST_ADDRESS-1:0]           enable_multicast_addr ; //Multicast addresses enabler
   integer                                  i;
   /********************
           TX
   ********************/
   wire                        checker_missmatch;
   /********************
       STAT COUNTER
    ********************/
   wire [3-1:0]                stats_addrmatch;

   wire [NB_COUNTER-1:0]        soffound_counter        ;
   wire [NB_COUNTER-1:0]        goodframe_counter       ;
   wire [NB_COUNTER-1:0]        frametoolong_counter    ;
   wire [NB_COUNTER-1:0]        framecheckerror_counter ;
   wire [NB_COUNTER-1:0]        lengtherror_counter     ;
   wire [NB_COUNTER-1:0]        alignment_counter       ;
   wire [NB_COUNTER-1:0]        addresserror_counter    ;
   wire [NB_COUNTER-1:0]        localaddrmatch_counter  ;
   wire [NB_COUNTER-1:0]        bcastaddrmatch_counter  ;
   wire [NB_COUNTER-1:0]        multicast_counter       ;
   wire [NB_COUNTER-1:0]        frametransmitted_counter;
   wire [NB_COUNTER-1:0]        checkermissmatch_counter;
   wire [NB_COUNTER-1:0]        RSfault_counter         ;
   wire [NB_COUNTER-1:0]        locallinkfault_counter  ;
   wire [NB_COUNTER-1:0]        remotelinkfault_counter ;
   wire [NB_COUNTER-1:0]        txdatavalidmiss_counter ;
   wire [NB_COUNTER-1:0]        rxdatavalidmiss_counter ;

   wire                         promiscuous_enable = 1'b0;

   /**********************
            PCS
    *********************/
	 wire [LEN_CODED_BLOCK-1 : 0] scrambled_data;



   reg  clock = 1'b0 ;
   always
     begin
        #(50) clock = ~clock ;
     end

   assign llc_frame_format_i = 1'b0;

   wire valid;
   wire reset;
   // wire clock;
   reg [5-1:0] timer = 0;
   wire        stop_timer;

   always @ ( posedge clock )
     begin
        if (~stop_timer)
          timer <= timer + 1;
     end

   // assign clock = i_clock;
   assign reset = (timer == 4) ;
   assign stop_timer = (timer == 10);
   assign valid = 1'b1;

   //Adresses
   assign local_mac_address          = 48'H000000000009 ;
   assign broadcast_mac_address      = 48'HCAFEFE00AAAA ;
   assign multicast_addresses_arr[0] = 48'H000000000000 ;
   assign multicast_addresses_arr[1] = 48'H000000000001 ;
   assign multicast_addresses_arr[2] = 48'H000000000002 ;

   assign enable_multicast_addr = 3'b111;

   always @ ( * ) begin
      for ( i=0; i<N_MULTICAST_ADDRESS; i=i+1) begin
         multicast_addresses [i*NB_ADDRESS+: NB_ADDRESS] = multicast_addresses_arr[i];
      end
   end

   RX_FD_top
     #(
       .NB_DATA                  (NB_DATA                  ),
       .LOG2_NB_DATA             (LOG2_NB_DATA             ),
       .LOG2_N_RX_STATUS         (LOG2_N_RX_STATUS         ),
       .NB_ADDRESS               (NB_ADDRESS               ),
       .NB_LENTYP                (NB_LENTYP                ),
       .NB_CRC32                 (NB_CRC32                 ),
       .N_MULTICAST_ADDRESS      (N_MULTICAST_ADDRESS      ),
       .LOG2_N_MULTICAST_ADDRESS (LOG2_N_MULTICAST_ADDRESS )
       )
   u_RX_FD_top
     (
      .o_MA_DATA_indication                  (o_RX_MA_DATA_indication                  ),
      .o_rx_status                           (o_RX_rx_status                           ),
      .o_rx_status_ready                     (o_RX_rx_status_ready                     ),
      .o_destination_address                 (o_RX_destination_address                 ),
      .o_destination_address_ready           (o_RX_destination_address_ready           ),
      .o_source_address                      (o_RX_source_address                      ),
      .o_source_address_ready                (o_RX_source_address_ready                ),
      .o_lentyp                              (o_RX_lentyp                              ),
      .o_lentyp_ready                        (o_RX_lentyp_ready                        ),
      .o_mac_service_data_unit               (o_RX_mac_service_data_unit               ),
      .o_mac_service_data_unit_valid         (o_RX_mac_service_data_unit_valid         ),
      .o_mac_service_data_unit_tailsize_bits (o_RX_mac_service_data_unit_tailsize_bits ),
      .o_frame_check_sequence                (o_RX_frame_check_sequence                ),
      .o_frame_check_sequence_ready          (o_RX_frame_check_sequence_ready          ),
      .o_stats_soffound                      (stats_soffound                           ),
      .o_stats_addrmatch                     (stats_addrmatch                          ),
      .o_stats_dv_missmatch                  (rx_datavalidmiss_i                       ),
      .o_eof                                 (o_RX_eof                                 ),
      .i_PLS_DATA_indication                 (o_CGMII_PLS_DATA_indication              ),
      .i_PLS_DATA_VALID_indication           (o_CGMII_PLS_DATA_VALID_indication        ),
      .i_tail_size_bits                      (o_CGMII_tail_size_bits                   ),
      .i_tail_size_valid                     (o_CGMII_tail_size_valid                  ),
      .i_RS_error                            (o_CGMII_RS_error                         ),
      .i_local_mac_address                   (local_mac_address                        ),
      .i_broadcast_mac_address               (broadcast_mac_address                    ),
      .i_multicast_addresses                 (multicast_addresses                      ),
      .i_enable_multicast_addr               (enable_multicast_addr                    ),
      .i_promiscuous_enable                  (promiscuous_enable                       ),
      .i_clock                               (clock                                    ),
      .i_valid                               (valid                                    ),
      .i_reset                               (reset                                    )
      );

   TX_FD_top_level
     #(
       .NB_DATA                (NB_DATA               ),
       .LOG2_NB_DATA           (LOG2_NB_DATA          ),
       .NB_ADDRESS             (NB_ADDRESS            ),
       .NB_LENTYP              (NB_LENTYP             ),
       .LOG2_NB_LENTYP         (LOG2_NB_LENTYP        ),
       .MIN_LENGTH_FRAME_BYTES (MIN_LENGTH_FRAME_BYTES),
       .MAX_LENGTH_FRAME_BYTES (MAX_LENGTH_FRAME_BYTES),
       .NB_CRC32               (NB_CRC32              ),
       .NB_TIMER               (NB_TIMER              ),
       .PREAMBLE               (PREAMBLE              ),
       .NB_PREAMBLE            (NB_PREAMBLE           ),
       .SFD                    (SFD                   ),
       .NB_SFD                 (NB_SFD                ),
       .CRC32_POLYNOMIAL       (CRC32_POLYNOMIAL      ),
       .MIN_FRAME_SIZE_BITS    (MIN_FRAME_SIZE_BITS   ),
       .IPG_BITS               (IPG_BITS              ),
       .IPG_STRETCH_BITS       (IPG_STRETCH_BITS      )
       )
   u_TX_FD_top_level
     (
      .o_data                (i_CGMII_PLS_DATA_request       ),
      .o_data_valid          (i_CGMII_data_request_valid     ),
      .o_tx_status           ({tx_datavalidmiss_i,tx_done_i} ),
      .o_tail_size           (i_CGMII_tail_size_bits         ),
      .i_MA_DATA_request     (fgen_data_valid_o              ),
      .i_destination_address (fgen_destination_addres_o      ),
      .i_source_address      (fgen_source_address_o          ),
      .i_lentyp              (fgen_lentype_o                 ),
      .i_client_data         (fgen_data_o                    ),
      .i_fcs                 (fgen_fcs_o                     ),
      .i_fcs_present         (fgen_fcs_included_o            ),
      .i_fcs_ready           (fgen_fcs_ready_o               ),
      .i_enable_deferring    (1'b0                           ),
      .i_valid               (valid                          ),
      .i_reset               (reset                          ),
      .i_clock               (clock                          )
      );


   reconciliation_sublayer_CGMII
     #(
       .NB_DATA                    (NB_DATA     ),
       .LOG2_NB_DATA               (LOG2_NB_DATA),
       .NB_XC                      (NB_XC       ),
       .N_LANES                    (N_LANES     )
       )
   u_reconciliation_sublayer_CGMII
     (
      .o_TXD                       (mac_TXD_to_pcs                    ),
      .o_TXC                       (mac_TXC_to_pcs                    ),
      .o_TX_CLK                    (mac_TX_CLK_to_pcs                 ),
      .o_PLS_DATA_indication       (o_CGMII_PLS_DATA_indication       ),
      .o_PLS_DATA_VALID_indication (o_CGMII_PLS_DATA_VALID_indication ),
      .o_tail_size_bits            (o_CGMII_tail_size_bits            ),
      .o_tail_size_valid           (o_CGMII_tail_size_valid           ),
      .o_RS_error                  (o_CGMII_RS_error                  ),
      .o_stats_linkfault           (stats_linkfault                   ),
      .i_RXD                       (pcs_RXD_to_mac                    ),
      .i_RXC                       (pcs_RXC_to_mac                    ),
      .i_RX_CLK                    (mac_TX_CLK_to_pcs                 ),
      .i_PLS_DATA_request          (i_CGMII_PLS_DATA_request          ),
      .i_data_request_valid        (i_CGMII_data_request_valid        ),
      .i_tail_size_bits            (i_CGMII_tail_size_bits            ),
      .i_clock                     (clock                             ),
      .i_valid                     (valid                             ),
      .i_reset                     (reset                             )
      );

   frame_generator
     #(
       .NB_DATA                   (NB_DATA                  ),
       .NB_DATA_PRBS              (NB_DATA_PRBS             ),
       .LOG2_NB_DATA              (LOG2_NB_DATA             ),
       .MIN_FRAME_SIZE_BYTES      (MIN_FRAME_SIZE_BYTES     ),
       .MAX_FRAME_SIZE_BYTES      (MAX_FRAME_SIZE_BYTES     ),
       .LOG2_MAX_FRAME_SIZE_BYTES (LOG2_MAX_FRAME_SIZE_BYTES),
       .LOG2_MAX_FRAME_SIZE_BITS  (LOG2_MAX_FRAME_SIZE_BITS ),
       .NB_DST_ADDR               (NB_DST_ADDR              ),
       .NB_SRC_ADDR               (NB_SRC_ADDR              ),
       .NB_LENTYP                 (NB_LENTYP                ),
       .NB_CRC32                  (NB_CRC32                 ),
       .CRC32_POLYNOMIAL          (CRC32_POLYNOMIAL         ),
       .NB_DA_TYPE                (NB_DA_TYPE               ),
       .NB_SA_TYPE                (NB_SA_TYPE               ),
       .NB_LENT_TYPE              (NB_LENT_TYPE             ),
       .NB_DATA_TYPE              (NB_DATA_TYPE             ),
       .NB_CRC_TYPE               (NB_CRC_TYPE              ),
       .N_ADDRESSES               (N_ADDRESSES              ),
       .LOG2_N_ADDRESSES          (LOG2_N_ADDRESSES         )
       )
   u_frame_generator
     (
      .o_data                    (fgen_data_o              ),
      .o_destination_address     (fgen_destination_addres_o),
      .o_source_address          (fgen_source_address_o    ),
      .o_lentype                 (fgen_lentype_o           ),
      .o_fcs                     (fgen_fcs_o               ),
      .o_fcs_included            (fgen_fcs_included_o      ),
      .o_fcs_ready               (fgen_fcs_ready_o         ),
      .o_data_valid              (fgen_data_valid_o        ),
      .o_frame_transmitted       (/*  frame_transmitted_o*/),
      .i_llc_frame_format        (llc_frame_format_i       ),
      .i_request_frame           (request_frame_i          ),
      .i_da_type                 (da_type_i                ),
      .i_da_set                  (da_set_i                 ),
      .i_da_list_selection       (da_list_selection_i      ),
      .i_sa_type                 (sa_type_i                ),
      .i_sa_set                  (sa_set_i                 ),
      .i_sa_list_selection       (sa_list_selection_i      ),
      .i_lent_type               (lent_type_i              ),
      .i_lentyp_set              (lentyp_set_i             ),
      .i_data_type               (data_type_i              ),
      .i_data_pattern            (data_pattern_i           ),
      .i_crc_type                (crc_type_i               ),
      .i_generate_crc            (generate_crc_i           ),
      .i_set_crc                 (set_crc_i                ),
      .i_valid                   (valid),
      .i_reset                   (reset),
      .i_clock                   (clock)
      );

   frame_checker
     #(
      .NB_DATA                   (NB_DATA                   ),
      .LOG2_NB_DATA              (LOG2_NB_DATA              ),
      .MIN_FRAME_SIZE_BYTES      (MIN_FRAME_SIZE_BYTES      ),
      .MAX_FRAME_SIZE_BYTES      (MAX_FRAME_SIZE_BYTES      ),
      .LOG2_MAX_FRAME_SIZE_BYTES (LOG2_MAX_FRAME_SIZE_BYTES ),
      .NB_DST_ADDR               (NB_DST_ADDR               ),
      .NB_SRC_ADDR               (NB_SRC_ADDR               ),
      .NB_LENTYP                 (NB_LENTYP                 ),
      .NB_CRC32                  (NB_CRC32                  ),
      .CRC32_POLYNOMIAL          (CRC32_POLYNOMIAL          ),
      .NB_DA_TYPE                (NB_DA_TYPE                ),
      .NB_SA_TYPE                (NB_SA_TYPE                ),
      .NB_LENT_TYPE              (NB_LENT_TYPE              ),
      .NB_DATA_TYPE              (NB_DATA_TYPE              ),
      .NB_CRC_TYPE               (NB_CRC_TYPE               ),
      .N_ADDRESSES               (N_ADDRESSES               ),
      .LOG2_N_ADDRESSES          (LOG2_N_ADDRESSES          )
       )
   u_frame_checker
     (
      .o_missmatch               (checker_missmatch                        ),
      .i_data_llcformat          (/* NOT CONNECTED*/                       ),
      .i_data_valid_llcformat    (/* NOT CONNECTED*/                       ),
      .i_tail_size_bits_llcformat(/* NOT CONNECTED*/                       ),
      .i_da_type                 (da_type_i                                ),
      .i_da_set                  (da_set_i                                 ),
      .i_da_list_selection       (da_list_selection_i                      ),
      .i_da_received             (o_RX_destination_address                 ),
      .i_da_received_valid       (o_RX_destination_address_ready           ),
      .i_sa_type                 (sa_type_i                                ),
      .i_sa_set                  (sa_set_i                                 ),
      .i_sa_list_selection       (sa_list_selection_i                      ),
      .i_sa_received             (o_RX_source_address                      ),
      .i_sa_received_valid       (o_RX_source_address_ready                ),
      .i_lent_type               (lent_type_i                              ),
      .i_lentyp_set              (lentyp_set_i                             ),
      .i_data_type               (data_type_i                              ),
      .i_data_pattern            (data_pattern_i                           ),
      .i_data_received           (o_RX_mac_service_data_unit               ),
      .i_data_received_valid     (o_RX_mac_service_data_unit_valid         ),
      .i_data_received_tailsize  (o_RX_mac_service_data_unit_tailsize_bits ),
      .i_crc_type                (crc_type_i                               ),
      .i_generate_crc            (generate_crc_i                           ),
      .i_set_crc                 (set_crc_i                                ),
      .i_crc_received            (o_RX_frame_check_sequence                ),
      .i_crc_received_valid      (o_RX_frame_check_sequence_ready          ),
      .i_eof                     (o_RX_eof                                 ),
      .i_llc_frame_format        (llc_frame_format_i                       ),
      .i_valid                   (valid                                    ),
      .i_reset                   (reset                                    ),
      .i_clock                   (clock                                    )
      );

   frame_requester
     #(
       .NB_DATA                   (NB_DATA                  ),
       .NB_DATA_PRBS              (NB_DATA_PRBS             ),
       .LOG2_NB_DATA              (LOG2_NB_DATA             ),
       .MIN_FRAME_SIZE_BYTES      (MIN_FRAME_SIZE_BYTES     ),
       .MAX_FRAME_SIZE_BYTES      (MAX_FRAME_SIZE_BYTES     ),
       .LOG2_MAX_FRAME_SIZE_BYTES (LOG2_MAX_FRAME_SIZE_BYTES),
       .LOG2_MAX_FRAME_SIZE_BITS  (LOG2_MAX_FRAME_SIZE_BITS ),
       .NB_DST_ADDR               (NB_DST_ADDR              ),
       .NB_SRC_ADDR               (NB_SRC_ADDR              ),
       .NB_LENTYP                 (NB_LENTYP                ),
       .NB_CRC32                  (NB_CRC32                 ),
       .CRC32_POLYNOMIAL          (CRC32_POLYNOMIAL         ),
       .NB_DA_TYPE                (NB_DA_TYPE               ),
       .NB_SA_TYPE                (NB_SA_TYPE               ),
       .NB_LENT_TYPE              (NB_LENT_TYPE             ),
       .NB_DATA_TYPE              (NB_DATA_TYPE             ),
       .NB_CRC_TYPE               (NB_CRC_TYPE              ),
       .N_ADDRESSES               (N_ADDRESSES              ),
       .LOG2_N_ADDRESSES          (LOG2_N_ADDRESSES         )
       )
   u_frame_requester
     (
       .o_request_frame            (request_frame_i    ),
       .o_da_type                  (da_type_i          ),
       .o_da_set                   (da_set_i           ),
       .o_da_list_selection        (da_list_selection_i),
       .o_sa_type                  (sa_type_i          ),
       .o_sa_set                   (sa_set_i           ),
       .o_sa_list_selection        (sa_list_selection_i),
       .o_lent_type                (lent_type_i        ),
       .o_lentyp_set               (lentyp_set_i       ),
       .o_data_type                (data_type_i        ),
       .o_data_pattern             (data_pattern_i     ),
       .o_crc_type                 (crc_type_i         ),
       .o_generate_crc             (generate_crc_i     ),
       .o_set_crc                  (set_crc_i          ),
       .i_frame_transmitted        (tx_done_i          ),
       .i_reset                    (reset              ),
       .i_clock                    (clock              )
       );

   stat_counter
     #(
       .NB_COUNTER               (NB_COUNTER),
       .LOG2_N_RX_STATUS         (LOG2_N_RX_STATUS)
       )
   u_stat_counter
     (
      .o_soffound_counter         (soffound_counter        ),
      .o_goodframe_counter        (goodframe_counter       ),
      .o_frametoolong_counter     (frametoolong_counter    ),
      .o_framecheckerror_counter  (framecheckerror_counter ),
      .o_lengtherror_counter      (lengtherror_counter     ),
      .o_alignment_counter        (alignment_counter       ),
      .o_addresserror_counter     (addresserror_counter    ),
      .o_localaddrmatch_counter   (localaddrmatch_counter  ),
      .o_bcastaddrmatch_counter   (bcastaddrmatch_counter  ),
      .o_multicast_counter        (multicast_counter       ),
      .o_frametransmitted_counter (frametransmitted_counter),
      .o_checkermissmatch_counter (checkermissmatch_counter),
      .o_RSfault_counter          (RSfault_counter         ),
      .o_locallinkfault_counter   (locallinkfault_counter  ),
      .o_remotelinkfault_counter  (remotelinkfault_counter ),
      .o_txdatavalidmiss_counter  (txdatavalidmiss_counter ),
      .o_rxdatavalidmiss_counter  (rxdatavalidmiss_counter ),
      .i_soffound                 (stats_soffound          ),
      .i_rx_status                (o_RX_rx_status          ),
      .i_rx_status_valid          (o_RX_rx_status_ready    ),
      .i_address_match            (stats_addrmatch         ),
      .i_frametransmitted         (tx_done_i               ),
      .i_RS_fault                 (o_CGMII_RS_error        ),
      .i_linkfault                (stats_linkfault         ),
      .i_checkermissmatch         (checker_missmatch       ),
      .i_txdatavalidmiss          (tx_datavalidmiss_i      ),
      .i_rxdatavalidmiss          (rx_datavalidmiss_i      ),
      .i_read_status              (1'b0                    ),
      .i_clear_on_read_mode       (1'b0                    ),
      .i_clock                    (clock                   ),
      .i_valid                    (valid                   ),
      .i_reset                    (reset                   )
      );

   //PCS
   tx_modules
	   #(
	     )
   u_tx_modules
	   (
	    .i_clock(clock),
	    .i_reset(reset),
	    .i_tx_data(mac_TXD_to_pcs),
	    .i_tx_ctrl(mac_TXC_to_pcs),
	    .i_enable(2'b11),
	    .o_scrambled_data(scrambled_data)
	    );

   rx_modules
	   #(
	     )
   u_rx_modules
	   (
	    .i_clock(clock),
	    .i_reset(reset),
	    .i_enable(2'b11),
	    .i_scrambled_data(scrambled_data),
	    .o_rx_raw_data(pcs_RXD_to_mac),
	    .o_rx_raw_ctrl(pcs_RXC_to_mac)
	    );

endmodule
