module rx_vector_matching ();

   localparam                                       DATA_FILE                 = "sim_data.mem" ;
   localparam                                       CONTROL_FILE              = "sim_control.mem";
   localparam                                       DATAVALID_FILE            = "sim_datavalid.mem";
   localparam                                       DATA_INDICATION_FILE      = "sim_data_indication.mem";
   localparam                                       LOG2_FRAME_SIZE           = 14 ;
   localparam                                       NB_DST_ADDR               = 48 ;
   localparam                                       NB_SRC_ADDR               = 48 ;
   localparam                                       NB_CRC                    = 32 ;

   localparam                                       NB_DATA                   = 64 ;
   localparam                                       LOG2_NB_DATA              = 6 ;
   localparam                                       RX_FIFO_DEPTH             = 4 ;
   localparam                                       RX_FIFO_ADDR              = 4 ;
   localparam                                       NB_ADDRESS                = 48 ;
   localparam                                       LOG2_NB_ADDRESS           = 6 ;
   localparam                                       NB_LENTYP                 = 16 ;
   localparam                                       LOG2_NB_LENTYP            = 4 ;
   localparam                                       N_MULTICAST_ADDRESS       = 3 ;
   localparam                                       LOG2_N_MULTICAST_ADDRESS  = 4 ;
   localparam                                       MAX_LENGTH_B_BYTES        = 1982 ;
   localparam                                       LOG2_FRAME_SIZE_BITS      = 14 ;
   localparam                                       NB_CRC32                  = 32 ;
   localparam                                       LOG2_NB_CRC32             = 5 ;
   localparam                                       N_RX_STATUS               = 5 ;
   localparam                                       LOG2_N_RX_STATUS          = 3 ;
   localparam                                       NBYTES_MAX_FRAME_SIZE     = 2026 ;
   localparam                                       NBYTES_MAX_DATA_SIZE      = 1982 ;
   localparam                                       LOG2_NBYTES_MAX_DATA_SIZE = 11 ;

   localparam                                       max_frame_size_bits       = NBYTES_MAX_FRAME_SIZE * 8;
   localparam                                       log2_max_frame_size_bits  = clogb2(max_frame_size_bits);

   localparam                                       LEN_NB_DATA               = 64;
   localparam                                       MAX_N_FRAMES              = 1024;



   //preamble finder params
   localparam                                       NB_STAT_CNT               = 16 ;
   localparam                                       NB_PREAMBLE               = 56 ;
   localparam                                       NB_SFD                    = 8 ;
   localparam                                       NB_TIMER                  = 16 ;
   localparam                                       PREAMBLE                  = 56'hAAAAAAAAAAAAAA;
   localparam                                       SFD                       = 8'hAB;

   //==========================================================================
   // INTERNAL SIGNALS.
   //==========================================================================
   //----------------------------------
   //Top level
   //----------------------------------
   // Outputs.
   //To upper layers

   wire                                             MA_DATA_indication_o; //Indicates to upper layers a frame has been received
   wire [LOG2_N_RX_STATUS-1:0]                      rx_status_o; //RX status code, indicates if there is a failure in reception or OK
   wire                                             rx_status_ready_o;
   wire [NB_ADDRESS-1:0]                            destination_address_o; //Frame destination address
   wire                                             destination_address_ready_o;
   wire [NB_ADDRESS-1:0]                            source_address_o; //Frame source address
   wire                                             source_address_ready_o;
   wire [NB_LENTYP-1:0]                             lentyp_o;
   wire                                             lentyp_ready_o;
   wire [NB_DATA-1:0]                               mac_service_data_unit_o; //Frame payload            [MAX_LENGTH_B_BYTES*8-1:0]?
   wire                                             mac_service_data_unit_valid_o;
   wire [NB_CRC32-1:0]                              frame_check_sequence_o; //Frame CRC 32
   wire                                             frame_check_sequence_ready_o;

   //From lower layers
   wire [LOG2_FRAME_SIZE_BITS-1:0]                  framesize_bits_i;
   wire [LOG2_NBYTES_MAX_DATA_SIZE-1:0]             data_size_bytes_i;
   wire [LOG2_NB_DATA-1:0]                          tail_size_bits_i;
   wire                                             data_valid_i;
   wire [NB_DATA-1:0]                               data_i;

   //For adresses
   wire [NB_ADDRESS-1:0]                            tb_local_mac_address_i ; // Station MAC address
   wire [NB_ADDRESS-1:0]                            tb_broadcast_mac_address_i ; // Broadcast MAC address (address destined for every station)
   reg [NB_ADDRESS*N_MULTICAST_ADDRESS-1:0]         tb_multicast_addresses_i ; // Vector of multicast addresses
   wire [NB_ADDRESS-1:0]                            tb_multicast_addresses_arr [N_MULTICAST_ADDRESS-1:0] ;
   wire [N_MULTICAST_ADDRESS-1:0]                   tb_enable_multicast_addr_i ; //Multicast addresses enabler

   // Other
   reg                                              clock_i = 1'b0;
   wire                                             tb_valid_i;
   wire                                             tb_reset_i ;
   integer                                          timer = 0 ;
   integer                                          i ;

   // File related variables
   integer                                          fd_data_input = 0;
   integer                                          fd_control_input = 0;
   integer                                          fd_datavalid_input = 0;
   integer                                          fd_dataindication_input = 0;
   integer                                          scan_file ;

   //Registers to parse data
   reg [NB_DATA-1:0]                                data_reg;
   reg [LOG2_FRAME_SIZE_BITS-1:0]                   framesize_b_reg;
   reg [LOG2_NBYTES_MAX_DATA_SIZE-1:0]              data_size_reg;
   reg [LOG2_NB_DATA-1:0]                           tail_size_reg;
   reg                                              data_valid_reg;


   reg [NB_DATA-1:0]                                data_reg_d;
   reg [LOG2_FRAME_SIZE_BITS-1:0]                   framesize_b_reg_d;
   reg [LOG2_NBYTES_MAX_DATA_SIZE-1:0]              data_size_reg_d;
   reg [LOG2_NB_DATA-1:0]                           tail_size_reg_d;

   reg                                              data_valid_d;
   reg                                              data_valid_dd;
   wire                                             data_valid_pos;

   //Registers to parse simulator output
   reg [NB_ADDRESS-1:0]                             dst_addr_reg;
   reg [NB_ADDRESS-1:0]                             src_addr_reg;
   reg [NB_LENTYP-1:0]                              lentyp_reg;
   reg [NB_DATA-1:0]                                payload_reg;
   reg [NB_CRC32-1:0]                               captured_fcs_reg;
   reg [NB_CRC32-1:0]                               calculated_crc_reg;
   reg [LOG2_N_RX_STATUS-1:0]                       rx_status_reg;


   reg [NB_ADDRESS-1:0]                             dst_addr_reg_d;
   reg [NB_ADDRESS-1:0]                             src_addr_reg_d;
   reg [NB_LENTYP-1:0]                              lentyp_reg_d;
   reg [NB_DATA-1:0]                                payload_reg_d;
   reg [NB_CRC32-1:0]                               captured_fcs_reg_d;
   reg [NB_CRC32-1:0]                               calculated_crc_reg_d;
   reg [LOG2_N_RX_STATUS-1:0]                       rx_status_reg_d;

   reg [NB_ADDRESS-1:0]                             dst_addr_reg_dd;
   reg [NB_ADDRESS-1:0]                             src_addr_reg_dd;
   reg [NB_LENTYP-1:0]                              lentyp_reg_dd;
   reg [NB_DATA-1:0]                                payload_reg_dd;
   reg [NB_CRC32-1:0]                               captured_fcs_reg_dd;
   reg [NB_CRC32-1:0]                               calculated_crc_reg_dd;
   reg [LOG2_N_RX_STATUS-1:0]                       rx_status_reg_dd;
   //For preamble finder
   // Outputs
   wire                                             lock_tb_o ;
   wire                                             time_out_tb_o ;
   wire                                             bad_rdv_tb_o ;
   wire [NB_STAT_CNT-1:0]                           time_out_cnt_tb_o ;
   wire [NB_STAT_CNT-1:0]                           bad_rdv_cnt_tb_o ;
   wire [LOG2_NB_DATA-1:0]                          index_tb_o ;
   wire                                             sof_tb_o ;

   //Connection from preamble finder top top
   wire [NB_DATA-1:0]                               preafi_data_top ; //Shifted data from PF is now the data input for toplevel
   wire                                             preafi_valid ; //Take the valid output from PF as data_valid for toplevel
   wire                                             preafi_valid_top;
   reg                                              preafi_valid_top_d;
   wire                                             preafi_valid_top_pos;

   // Vector match
   wire                                             vmatch_rx_status;
   wire                                             vmatch_dst_addr;
   wire                                             vmatch_src_addr;
   wire                                             vmatch_lentyp;
   wire                                             vmatch_data;
   wire                                             vmatch_crc32calc;

   reg [clogb2(MAX_N_FRAMES)-1:0]                   vmatch_status_error_counter;
   reg [clogb2(MAX_N_FRAMES)-1:0]                   vmatch_dstaddr_error_counter;
   reg [clogb2(MAX_N_FRAMES)-1:0]                   vmatch_srcaddr_error_counter;
   reg [clogb2(MAX_N_FRAMES)-1:0]                   vmatch_lentyp_error_counter;
   reg [clogb2(MAX_N_FRAMES)-1:0]                   vmatch_data_error_counter;
   reg [clogb2(MAX_N_FRAMES)-1:0]                   vmatch_crc32_error_counter;

   assign vmatch_rx_status = (rx_status_o != rx_status_reg) & rx_status_ready_o;
   assign vmatch_dst_addr = (destination_address_o != dst_addr_reg_d) & destination_address_ready_o;
   assign vmatch_src_addr = (source_address_o != src_addr_reg_d) & source_address_ready_o;
   assign vmatch_lentyp = (lentyp_o != lentyp_reg_d) & lentyp_ready_o;
   assign vmatch_crc32calc = (frame_check_sequence_o != captured_fcs_reg_d) & frame_check_sequence_ready_o;


   always @(posedge clock_i)
     begin
        if (tb_reset_i) begin
           vmatch_status_error_counter <= 'b0;
           vmatch_dstaddr_error_counter <= 'b0;
           vmatch_srcaddr_error_counter <= 'b0;
           vmatch_lentyp_error_counter <= 'b0;
           vmatch_data_error_counter <= 'b0;
           vmatch_crc32_error_counter <= 'b0;
        end else if (tb_valid_i) begin
           if (vmatch_rx_status)
             vmatch_status_error_counter <= vmatch_status_error_counter + 1'b1;
           if (vmatch_dst_addr)
             vmatch_dstaddr_error_counter <= vmatch_dstaddr_error_counter + 1'b1;
           if (vmatch_src_addr)
          vmatch_srcaddr_error_counter <= vmatch_srcaddr_error_counter + 1'b1;
           if (vmatch_lentyp)
             vmatch_lentyp_error_counter <= vmatch_lentyp_error_counter + 1'b1;
           if (vmatch_crc32calc)
             vmatch_crc32_error_counter <= vmatch_crc32_error_counter + 1'b1;
        end // if (tb_valid_i)
     end

   RX_top_level
     #(
       .NB_DATA                      (NB_DATA                    ),
       .LOG2_NB_DATA                 (LOG2_NB_DATA               ),
       .RX_FIFO_DEPTH                (RX_FIFO_DEPTH              ),
       .RX_FIFO_ADDR                 (RX_FIFO_ADDR               ),
       .NB_ADDRESS                   (NB_ADDRESS                 ),
       .LOG2_NB_ADDRESS              (LOG2_NB_ADDRESS            ),
       .NB_LENTYP                    (NB_LENTYP                  ),
       .LOG2_NB_LENTYP               (LOG2_NB_LENTYP             ),
       .N_MULTICAST_ADDRESS          (N_MULTICAST_ADDRESS        ),
       .LOG2_N_MULTICAST_ADDRESS     (LOG2_N_MULTICAST_ADDRESS   ),
       .MAX_LENGTH_B_BYTES           (MAX_LENGTH_B_BYTES         ),
       .LOG2_FRAME_SIZE_BITS         (LOG2_FRAME_SIZE_BITS       ),
       .NB_CRC32                     (NB_CRC32                   ),
       .LOG2_NB_CRC32                (LOG2_NB_CRC32              ),
       .N_RX_STATUS                  (N_RX_STATUS                ),
       .LOG2_N_RX_STATUS             (LOG2_N_RX_STATUS           ),
       .NBYTES_MAX_FRAME_SIZE        (NBYTES_MAX_FRAME_SIZE      ),
       .NBYTES_MAX_DATA_SIZE         (NBYTES_MAX_DATA_SIZE       ),
       .LOG2_NBYTES_MAX_DATA_SIZE    (LOG2_NBYTES_MAX_DATA_SIZE  )
       )
   u_RX_top_level
     (
      .o_MA_DATA_indication          (MA_DATA_indication_o       ),
      .o_rx_status                   (rx_status_o                ),
      .o_rx_status_ready             (rx_status_ready_o          ),
      .o_destination_address         (destination_address_o      ),
      .o_destination_address_ready   (destination_address_ready_o),
      .o_source_address              (source_address_o           ),
      .o_source_address_ready        (source_address_ready_o     ),
      .o_lentyp                      (lentyp_o                   ),
      .o_lentyp_ready                (lentyp_ready_o             ),
      .o_mac_service_data_unit       (mac_service_data_unit_o    ),
      .o_mac_service_data_unit_valid (mac_service_data_unit_valid_o),
      .o_frame_check_sequence        (frame_check_sequence_o     ),
      .o_frame_check_sequence_ready  (frame_check_sequence_ready_o),

      .i_receive_data_valid          (preafi_valid_top           ),
      .i_data                        (preafi_data_top            ),
      .i_local_mac_address           (tb_local_mac_address_i     ),
      .i_broadcast_mac_address       (tb_broadcast_mac_address_i ),
      .i_multicast_addresses         (tb_multicast_addresses_i   ),
      .i_enable_multicast_addr       (tb_enable_multicast_addr_i ),

      .i_frame_size_bits             (framesize_bits_i           ),  //Size of the complete frame
      .i_data_size_bytes             (data_size_bytes_i          ), //Size of the payload field
      .i_tail_size_bits              (tail_size_bits_i           ),

      .i_clock                       (clock_i                    ),
      .i_valid                       (tb_valid_i                 ),
      .i_reset                       (tb_reset_i                 )
     ) ;

    preamble_finder
      #(
        .NB_DATA                     (NB_DATA                    ),
        .LOG2_NB_DATA                (LOG2_NB_DATA               ),
        .NB_PREAMBLE                 (NB_PREAMBLE                ),
        .NB_SFD                      (NB_SFD                     ),
        .NB_TIMER                    (NB_TIMER                   ),
        .NB_STAT_CNT                 (NB_STAT_CNT                )
      )
    u_preamble_finder
      (
        // Outputs
        .o_lock                      (lock_tb_o                  ),//Can be omitted
        .o_time_out                  (time_out_tb_o              ),//Can be omitted
        .o_bad_rdv                   (bad_rdv_tb_o               ),//Can be omitted
        .o_time_out_cnt              (time_out_cnt_tb_o          ),//Can be omitted
        .o_bad_rdv_cnt               (bad_rdv_cnt_tb_o           ),//Can be omitted
        .o_index                     (index_tb_o                 ), //Can be omitted
        .o_data                      (preafi_data_top            ),
        .o_sof                       (sof_tb_o                   ), //Can be omitted
        .o_valid                     (preafi_valid_top           ),
        // Inputs
        .i_data                      (data_i                     ),
        .i_valid                     (tb_valid_i                 ),
        .i_rdv                       (data_valid_i               ),
        .i_preamble                  (PREAMBLE                   ), //preamble_tb_i
        .i_sfd                       (SFD                        ), //sfd_tb_i
        .i_time_out_limit            (16'hFFFF                   ), //time_out_limit_tb_i
        .i_check_time_out            (1'b0                       ), //check_time_out_tb_i

        .i_read_status               (1'b0                       ), //read_status_tb_i
        .i_clear_on_read_mode        (1'b0                       ), //clear_on_read_mode_tb_i

        .i_reset                     (tb_reset_i                 ),
        .i_clock                     (clock_i                    )
    ) ;

   initial
     begin
        data_valid_reg = 0 ;
        fd_data_input = $fopen(DATA_FILE,"r");
        if (fd_data_input==0)
          begin
             $display("Error de lectura de archivo DATA_FILE");
             $stop;
          end
        fd_control_input = $fopen(CONTROL_FILE,"r");
        if (fd_control_input==0)
          begin
             $display("Error de lectura de archivo CONTROL_FILE");
             $stop;
          end
        fd_datavalid_input = $fopen(DATAVALID_FILE,"r");
        if (fd_datavalid_input==0)
          begin
             $display("Error de lectura de archivo DATAVALID_FILE");
             $stop;
          end
        fd_dataindication_input = $fopen(DATA_INDICATION_FILE, "r");
        if (fd_dataindication_input==0)
          begin
             $display("Error de lectura de archivo DATA_INDICATION");
             $stop;
          end
     end // initial begin

   //File read
   always @ (posedge clock_i)
     begin
        if (tb_valid_i) begin
           //Capture data
           scan_file = $fscanf(fd_data_input, "%b", data_reg) ;
           if ($feof(fd_data_input))begin
             $display ("EOF data");
             $fclose(fd_data_input);
           end

           //Capture datavalid
           scan_file = $fscanf(fd_datavalid_input, "%1b", data_valid_reg);
           if($feof(fd_datavalid_input)) begin
             $display("EOF datavalid");
             $fclose(fd_datavalid_input);
           end
           if (data_valid_pos)
             begin
                //Capture control fields
                scan_file = $fscanf(fd_control_input, "%b", framesize_b_reg);
                if($feof(fd_control_input))begin
                  $display("EOF control");
                  $fclose(fd_control_input);
                end
                scan_file = $fscanf(fd_control_input, "%b", data_size_reg);
                scan_file = $fscanf(fd_control_input, "%b", tail_size_reg);

                //Capture data_indication fields
                scan_file = $fscanf(fd_dataindication_input, "%b", dst_addr_reg);
                if($feof(fd_dataindication_input))begin
                  $display("EOF_dataindication");
                  $fclose(fd_dataindication_input);
                end
                scan_file = $fscanf(fd_dataindication_input, "%b", src_addr_reg);
                scan_file = $fscanf(fd_dataindication_input, "%b", lentyp_reg);
                scan_file = $fscanf(fd_dataindication_input, "%b", payload_reg);
                scan_file = $fscanf(fd_dataindication_input, "%b", captured_fcs_reg);
                scan_file = $fscanf(fd_dataindication_input, "%b", calculated_crc_reg);
                scan_file = $fscanf(fd_dataindication_input, "%b", rx_status_reg);
             end
        end
     end

   assign data_i = data_reg_d;
   assign framesize_bits_i = framesize_b_reg_d;
   assign data_size_bytes_i = data_size_reg_d;
   assign tail_size_bits_i = tail_size_reg_d;
   assign data_valid_i = data_valid_d;

   //Registered signals
   always @ (posedge clock_i)
     begin
        if (tb_reset_i) begin
           data_valid_d <= 'b0;
           data_valid_dd <= 'b0;
           preafi_valid_top_d <= 'b0;

           data_reg_d <= 'b0;
           framesize_b_reg_d <= 'b0;
           data_size_reg_d <= 'b0;
           tail_size_reg_d <= 'b0;

           //Registered signals
           dst_addr_reg_d <= 'b0;
           src_addr_reg_d <= 'b0;
           lentyp_reg_d <= 'b0;
           payload_reg_d <= 'b0;
           captured_fcs_reg_d <= 'b0;
           calculated_crc_reg_d <= 'b0;
           rx_status_reg_d <= 'b0;

           //Delayed regs
           dst_addr_reg_dd <= 'b0;
           src_addr_reg_dd <= 'b0;
           lentyp_reg_dd <= 'b0;
           payload_reg_dd <= 'b0;
           captured_fcs_reg_dd <= 'b0;
           calculated_crc_reg_dd <= 'b0;
           rx_status_reg_dd <= 'b0;
        end else begin
           data_valid_d <= data_valid_reg;
           data_valid_dd <= data_valid_d;
           data_reg_d <= data_reg;
           framesize_b_reg_d <= framesize_b_reg;
           data_size_reg_d <= data_size_reg;
           tail_size_reg_d <= tail_size_reg;
           preafi_valid_top_d <= preafi_valid_top;

           //Registered signals
           if (preafi_valid_top_pos) begin
              dst_addr_reg_d <= dst_addr_reg;
              src_addr_reg_d <= src_addr_reg;
              lentyp_reg_d <= lentyp_reg;
              payload_reg_d <= payload_reg;
              captured_fcs_reg_d <= captured_fcs_reg;
              calculated_crc_reg_d <= calculated_crc_reg;
              rx_status_reg_d <= rx_status_reg;

              dst_addr_reg_dd <= dst_addr_reg_d;
              src_addr_reg_dd <= src_addr_reg_d;
              lentyp_reg_dd <= lentyp_reg_d;
              payload_reg_dd <= payload_reg_d;
              captured_fcs_reg_dd <= captured_fcs_reg_d;
              calculated_crc_reg_dd <= calculated_crc_reg_d;
              rx_status_reg_dd <= rx_status_reg_d;
           end
        end
     end
   assign data_valid_pos = ~data_valid_dd & data_valid_i ;
   assign preafi_valid_top_pos = ~preafi_valid_top_d & preafi_valid_top;

   always @(posedge clock_i)
     payload_reg_d <= payload_reg_d << NB_DATA;

   always #50 clock_i = ~clock_i ;

   always @ ( posedge clock_i )
     begin
        timer   <= timer + 1;
     end

   assign tb_reset_i = (timer == 2) ; // Reset at time 2
   assign tb_valid_i = timer>3 ;

   //Adresses
   assign tb_local_mac_address_i        = 48'H000000000009 ;
   assign tb_broadcast_mac_address_i    = 48'H00000000A2B2 ;
   assign tb_multicast_addresses_arr[0] = 48'H000000000000 ;
   assign tb_multicast_addresses_arr[1] = 48'H000000000001 ;
   assign tb_multicast_addresses_arr[2] = 48'H000000000002 ;


   always @ ( * ) begin
      for ( i=0; i<N_MULTICAST_ADDRESS; i=i+1) begin
         tb_multicast_addresses_i [i*NB_ADDRESS+: NB_ADDRESS] = tb_multicast_addresses_arr[i];
      end
   end

   assign tb_enable_multicast_addr_i = 3'b111 ;

   function integer clogb2;
      input integer depth;
      for (clogb2=0; depth>0; clogb2=clogb2+1)
        depth = depth >> 1;
   endfunction // clogb2

endmodule
