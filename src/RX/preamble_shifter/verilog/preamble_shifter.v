module preamble_shifter
#(
    // Parameters.
    parameter                                       NB_DATA                = 64 ,
    parameter                                       LOG2_NB_DATA           = 6 ,
    parameter 										PREAMBLE_SIZE 		   = 56 ,   	//7*8 bits
    parameter 										NB_TIMER 		       = 14,		//LOG2 Nr of bytes in a frame
    parameter 										MAX_TIMEOUT 		   = 1500 
)
(
    // Outputs.
    output  wire              					    o_receive_data_valid , // Signals a data valid before a timeout
    output  wire             [NB_DATA-1:0]     		o_data ,    		   // Output a 1 to signal a match in addresses

    // Inputs.
    input   wire                                    i_receive_data_valid , // Data valid input signal
    input   wire             [NB_DATA-1:0]     		i_data,

    input   wire                                    i_valid ,   		   // Throughput control.

    input   wire                                    i_reset ,
    input   wire                                    i_clock

) ;

    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================

    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
    reg                                             pos_edge ;
    wire                                            pe ;
    wire 											time_out ;
    reg 											timer ;

    reg                                             data_d ;
    wire              [2*NB_DATA-1:0]               data_concat ;

    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    //o_receive_data_valid

    always @ (posedge i_clock)
    begin

        pos_edge <= pe ;
    end

    assign pe = i_receive_data_valid & ~ pos_edge ; 	//Timer input

    always @ (posedge i_clock)
    begin
        if (i_reset || pe)
        	timer <= {NB_TIMER{1'b0}} ;
        else if (i_valid && !time_out)
        	timer <= timer + 1'b1 ;
    end

    assign time_out = (timer >= MAX_TIMEOUT) ; //spyglass disabel A124 -- Carry drop and operand size mismatch is intended.

    assign o_receive_data_valid = ~ time_out & i_receive_data_valid ; 	//o_receive_data_valid output

    //o_data
    always @ (posedge i_clock)
    begin
        data_d <= i_data ;
    end

    assign data_concat = {data_d, i_data} ;

    assign o_data = (data_concat >> (PREAMBLE_SIZE % NB_DATA)) ;

endmodule
