module preamble_shifter_tb ();

    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================

    // Parameters.
    localparam                                      NB_DATA                = 64 ;
    localparam                                      LOG2_NB_DATA           = 6 ;
    localparam 										PREAMBLE_SIZE 		   = 56 ;   	//7*8 bits
    localparam 										NB_TIMER 		       = 14 ;		//LOG2 Nr of bytes in a frame
    localparam 										MAX_TIMEOUT 		   = 1500 ;

    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
    // Outputs.
    reg              					    receive_data_valid_tb_o ; // Signals a data valid before a timeout
    reg              [NB_DATA-1:0]     		data_tb_o ;    		   // Output a 1 to signal a match in addresses

    // Inputs.
    wire                                    receive_data_valid_tb_i ; // Data valid input signal
    wire             [NB_DATA-1:0]     		data_tb_i ;

    wire                                    valid_tb_i ;   		   // Throughput control.

    wire                                            reset ;
    reg                                             clock = 1'b0 ;
    integer                                         timer = 0 ;
            
    reg                                             test_passed = 1'b0 ;

    //==========================================================================
    // CONNECTION TO DUT
    //==========================================================================
    preamble_finder
    #(
        .NB_DATA                        (NB_DATA      ),
        .LOG2_NB_DATA                   (LOG2_NB_DATA ),
        .PREAMBLE_SIZE                  (PREAMBLE_SIZE),
        .NB_TIMER                       (NB_TIMER     ),
        .MAX_TIMEOUT                    (MAX_TIMEOUT  )
    )
    u_preamble_finder
    (
        // Outputs.
        o_receive_data_valid            (receive_data_valid_tb_o),
        o_data                          (data_tb_o),              
        // Inputs.
        i_receive_data_valid            (receive_data_valid_tb_i), 
        i_data                          (data_tb_i),
        i_valid                         (valid_tb_i),             
        i_reset                         (reset),
        i_clock                         (clock)
    ) ;

    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    always
    begin
        #(50) clock = ~clock ;
    end

    always @ ( posedge clock )
    begin
        timer   <= timer + 1;
    end

    assign reset = (timer == 2) ; // Reset at time 2
    assign valid_tb_i = 1'b1 ;


endmodule
