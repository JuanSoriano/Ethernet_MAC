/*
 Invalid MAC Frames shall be passed with the corresponding status error code according to
 page 58 of 802.3 Ethernet standard
 */
module RX_MAC
  #(
    // Parameters.
    parameter                                        NB_DATA                   = 64 , //Parallelism
    parameter                                        LOG2_NB_DATA              = 6 ,
    parameter                                        RX_FIFO_DEPTH             = 4 ,
    parameter                                        RX_FIFO_ADDR              = 4 ,
    parameter                                        NB_ADDRESS                = 48 , //Number of bits in an address
    parameter                                        LOG2_NB_ADDRESS           = 6 ,
    parameter                                        NB_LENTYP                 = 16 ,
    parameter                                        LOG2_NB_LENTYP            = 4 ,
    parameter                                        N_MULTICAST_ADDRESS       = 16 , // Number of supported multicast addresses
    parameter                                        LOG2_N_MULTICAST_ADDRESS  = 4 ,    // Number of bits needed for the multicast addr vector
    parameter                                        MAX_LENGTH_B_BYTES        = 1982 ,
    parameter                                        LOG2_FRAME_SIZE_BITS      = 14 ,
    parameter                                        NB_CRC32                  = 32 ,
    parameter 												               LOG2_NB_CRC32             = 5 ,
    parameter 												               N_RX_STATUS               = 5 ,
    parameter                                        CRC32_POLYNOMIAL          = 33'h104C11DB7,
    // For reception status
    parameter                                        LOG2_N_RX_STATUS          = 3 ,
    parameter                                        NBYTES_MAX_FRAME_SIZE     = 2026 ,
    parameter                                        NBYTES_MAX_DATA_SIZE      = 1982 ,
    parameter                                        LOG2_NBYTES_MAX_DATA_SIZE = 11
    )
   (
    // Outputs.
    //To upper layers
    output wire                                     o_MA_DATA_indication, //Indicates to upper layers a frame has been received
    output wire [LOG2_N_RX_STATUS-1:0]              o_rx_status, //RX status code, indicates if there is a failure in reception or OK
    output wire                                     o_rx_status_ready,
    output wire [NB_ADDRESS-1:0]                    o_destination_address, //Frame destination address
    output wire                                     o_destination_address_ready,
    output wire [NB_ADDRESS-1:0]                    o_source_address, //Frame source address
    output wire                                     o_source_address_ready,
    output wire [NB_LENTYP-1:0]                     o_lentyp, //Frame length/type field
    output wire                                     o_lentyp_ready,
    output wire [NB_DATA-1:0]                       o_mac_service_data_unit, //Frame payload   /*[MAX_LENGTH_B_BYTES*8-1:0]*/
    output reg                                      o_mac_service_data_unit_valid,
    output wire [LOG2_NB_DATA-1:0]                  o_mac_service_data_unit_tailsize_bits,
    output wire [NB_CRC32-1:0]                      o_frame_check_sequence, //Frame CRC 32
    output wire                                     o_frame_check_sequence_ready,

    output wire [3-1:0]                             o_stats_addrmatch,
    output wire                                     o_stats_dv_missmatch,
    output wire                                     o_eof,


    //Inputs.
    //From lower layers
    input wire                                      i_receive_data_valid , //From lower layers
    input wire [NB_DATA-1:0]                        i_data ,
    input wire [LOG2_NB_DATA-1:0]                   i_tail_size_bits ,
    input wire                                      i_tail_size_valid ,
    input wire                                      i_RS_error,

    //For addresses in LMRA
    input wire [NB_ADDRESS-1:0]                     i_local_mac_address , // Station MAC address
    input wire [NB_ADDRESS-1:0]                     i_broadcast_mac_address , // Broadcast MAC address (address destined for every station)
    input wire [NB_ADDRESS*N_MULTICAST_ADDRESS-1:0] i_multicast_addresses , // Vector of multicast addresses
    input wire [N_MULTICAST_ADDRESS-1:0]            i_enable_multicast_addr , // Enabled multicast addresses
    input wire                                      i_promiscuous_enable,
    input wire                                      i_fcs_forward,

    // Other
    input wire                                      i_clock ,
    input wire                                      i_valid ,
    input wire                                      i_reset

    ) ;
   //==========================================================================
   // LOCAL PARAMETERS.
   //==========================================================================
   localparam NB_TIMER = 10;
   //==========================================================================
   // INTERNAL SIGNALS.
   //==========================================================================

   //Connection signals
   //Signals coming from splitter
   wire [NB_DATA-1:0]                               splitter_mac_service_data_unit_output ;
   wire                                             splitter_data_valid ; //Destination address checked and it's a match
   wire [NB_ADDRESS-1:0]                            splitter_dst_addr_lmra ;
   wire [NB_ADDRESS-1:0]                            splitter_src_addr_output ;
   wire [NB_LENTYP-1:0]                             splitter_len_typ ;
   wire [NB_CRC32-1:0]                              splitter_crc_crc32 ; //CRC 32 data to be checked by CRC32
   wire                                             splitter_crc_valid_crc32 ; //Indicates the new CRC received from the frame is ready
   wire                                             splitter_enable_lmra ;

   wire                                             splitter_dstready_output;
   wire                                             splitter_srcaddr_output;
   wire                                             splitter_lentypready_output;
   //Signals coming from Layer Management Recognize Address
   wire                                             lmra_macaddress_match_splitter ;

   wire                                             splitter_len_error;
   reg [NB_TIMER-1:0]                               timer;

   //Signals coming from CRC32
   wire                                             crc32_crc_valid ;
   wire                                             crc32_crc_ready ;

   //Signals coming from reception_status
   wire [LOG2_N_RX_STATUS-1:0]                      receptionstatus_status_output ;
   wire                                             receptionstatus_statusvalid_output ;

   reg                                              rdvalid_d;
   wire                                             rdvalid_pos;
   wire                                             rdvalid_neg;

   reg [NB_DATA+NB_LENTYP-1:0]                      extended_mac_service_data_unit ;
   reg                                              splitter_data_valid_d;
   wire                                             splitter_data_valid_pos;
   reg                                              extra_client_clock;
   reg [NB_DATA-1:0]                                delayed_data_to_crc;
   wire                                             datavalid_to_crc;
   wire                                             remove_clock_datavalid_crc;
   wire [NB_CRC32-1:0]                              crc32_o;
   wire [LOG2_NB_DATA-1:0]                          crc_tail_size_bits;

   wire                                             lencalc_framesize_bits_ready_status;
   wire [LOG2_FRAME_SIZE_BITS-1:0]                  lencalc_framesize_bits_status;
   wire                                             lencalc_frame_too_long_status;

   wire                                             calculate_state;
   reg                                              calculate_frame_state;

   reg [LOG2_NB_DATA-1:0]                           tailsize_reg;
   wire                                             ignore_length_check; //Eth II

   //==========================================================================
   // ALGORITHM.
   //==========================================================================
   assign o_MA_DATA_indication    = i_receive_data_valid;
   assign o_mac_service_data_unit = extended_mac_service_data_unit[NB_DATA+NB_LENTYP-1-:NB_DATA];//splitter_mac_service_data_unit_output;
   assign o_destination_address   = splitter_dst_addr_lmra ;
   assign o_source_address        = splitter_src_addr_output ;
   assign o_lentyp                = splitter_len_typ ;
   assign o_frame_check_sequence  = splitter_crc_crc32 ;
   assign o_rx_status             = receptionstatus_status_output ;

   assign o_destination_address_ready = splitter_dstready_output;
   assign o_source_address_ready = splitter_srcaddr_output;
   assign o_lentyp_ready = splitter_lentypready_output;
   always @(posedge i_clock)
     o_mac_service_data_unit_valid <= splitter_data_valid | extra_client_clock;
   assign o_mac_service_data_unit_tailsize_bits = (NB_LENTYP+splitter_len_typ*8)%NB_DATA;
   assign crc_tail_size_bits = (tailsize_reg+NB_CRC32)%NB_DATA;

   assign o_eof = rdvalid_neg;

   always @(posedge i_clock)
     if (i_reset)
       tailsize_reg <= 'b0;
     else if (i_valid & i_tail_size_valid)
       tailsize_reg <= i_tail_size_bits;

   assign ignore_length_check = 1'b0;

   splitter
     #(
       .NB_DATA                    (NB_DATA                                             ),
       .LOG2_NB_DATA               (LOG2_NB_DATA                                        ),
       .NB_DST_ADDR                (NB_ADDRESS                                          ),
       .NB_SRC_ADDR                (NB_ADDRESS                                          ),
       .NB_LENTYP                  (NB_LENTYP                                           ),
       .NB_CRC                     (NB_CRC32                                            ),
       .LOG2_FRAME_SIZE_BITS       (LOG2_FRAME_SIZE_BITS                                )
       )
   u_splitter
     (
      //Outputs
      .o_data                      (splitter_mac_service_data_unit_output               ),
      .o_data_valid                (splitter_data_valid                                 ),
      .o_dst_addr                  (splitter_dst_addr_lmra                              ),
      .o_dst_addr_ready            (splitter_dstready_output                            ),
      .o_enable_lmra               (splitter_enable_lmra                                ),
      .o_src_addr                  (splitter_src_addr_output                            ),
      .o_src_addr_ready            (splitter_srcaddr_output                             ),
      .o_len_typ                   (splitter_len_typ                                    ),
      .o_len_typ_ready             (splitter_lentypready_output                         ),
      .o_crc                       (splitter_crc_crc32                                  ),
      .o_crc_ready                 (o_frame_check_sequence_ready                        ),
      .o_crc_valid                 (splitter_crc_valid_crc32                            ),
      //Inputs
      .i_data                      (i_data                                              ),
      .i_valid                     (i_valid                                             ),
      .i_frame_ready               (i_receive_data_valid                                ),
      .i_dest_address_ok           (lmra_macaddress_match_splitter                      ), //Coming from Layer Mgmt Rec Address
      .i_rx_error                  (o_stats_dv_missmatch                                ),
      .i_reset                     (i_reset                                             ),
      .i_clock                     (i_clock                                             )
      );

   layer_management_recognize_address
     #(
       .NB_ADDRESS                 (NB_ADDRESS                                          ),
       .LOG2_NB_ADDRESS            (LOG2_NB_ADDRESS                                     ),
       .N_MULTICAST_ADDRESS        (N_MULTICAST_ADDRESS                                 ),
       .LOG2_N_MULTICAST_ADDRESS   (LOG2_N_MULTICAST_ADDRESS                            )
       )
   u_layer_management_recognize_address
     (
      // Outputs.
      .o_macaddress_match          (lmra_macaddress_match_splitter                      ),
      .o_stats_addrmatch           (o_stats_addrmatch                                   ),
      // Inputs.
      .i_rx_mac_address            (splitter_dst_addr_lmra                              ),
      .i_local_mac_address         (i_local_mac_address                                 ),
      .i_broadcast_mac_address     (i_broadcast_mac_address                             ),
      .i_multicast_addresses       (i_multicast_addresses                               ),
      .i_enable_multicast_addr     (i_enable_multicast_addr                             ),
      .i_promiscuous_enable        (i_promiscuous_enable                                ),
      .i_valid                     (i_valid                                             ),
      .i_addr_valid                (splitter_enable_lmra                                ),
      .i_reset                     (i_reset                                             ),
      .i_clock                     (i_clock                                             )
      ) ;

   CRC32
     #(
       .NB_DATA                    (NB_DATA                                             ),
       .LOG2_NB_DATA               (LOG2_NB_DATA                                        ),
       .NB_CRC32                   (NB_CRC32                                            ),
       .CRC32_POLYNOMIAL           (CRC32_POLYNOMIAL                                    ),
       .REF_IN                     (1                                                   ),
       .REF_OUT                    (1                                                   )
       )
   u_CRC32
     (
      // Outputs.
    	.o_crc32_valid               (crc32_crc_valid                                     ),
      .o_crc_ready                 (crc32_crc_ready                                     ),
      .o_crc32                     (crc32_o                                             ),
   	  // Inputs.
 		  .i_data                      (delayed_data_to_crc                                 ),
 		  .i_data_valid                (datavalid_to_crc                                    ),
 		  .i_crc32                     (splitter_crc_crc32                                  ),
		  .i_crc32_valid               (o_frame_check_sequence_ready                        ),
      .i_tail_size_bits            (crc_tail_size_bits                                  ),
 		  .i_valid                     (i_valid                                             ),
 		  .i_reset                     (i_reset                                             ),
 		  .i_clock                     (i_clock                                             )
      ) ;

   reception_status
     #(
       .N_RX_STATUS                (N_RX_STATUS                                         ),
       .LOG2_N_RX_STATUS           (LOG2_N_RX_STATUS                                    ),
       .NBYTES_MAX_FRAME_SIZE      (NBYTES_MAX_FRAME_SIZE                               ),
       .NBYTES_MAX_DATA_SIZE       (NBYTES_MAX_DATA_SIZE                                ),
       .LOG2_NBYTES_MAX_DATA_SIZE  (LOG2_NBYTES_MAX_DATA_SIZE                           ),
       .NB_LENTYP                  (NB_LENTYP                                           ),
       .LOG2_NB_LENTYP             (LOG2_NB_LENTYP                                      )
       )
   u_reception_status
     (
      .o_status                    (receptionstatus_status_output                       ),
      .o_status_valid              (o_rx_status_ready                                   ),

      .i_frame_size_bits           (lencalc_framesize_bits_status                       ), //Size of the complete frame
      .i_frame_size_bits_ready     (lencalc_framesize_bits_ready_status                 ),
      //.i_missmatch_dv_len          (o_stats_dv_missmatch                                ),
      .i_ignore_length_check       (ignore_length_check                                 ), //Ethernet II frame
      .i_frame_too_long            (lencalc_frame_too_long_status                       ),
      .i_lentyp                    (splitter_len_typ                                    ),
      .i_fcs_valid                 (crc32_crc_valid                                     ),
      .i_fcs_forward               (i_fcs_forward                                       ),
      .i_destination_address_match (lmra_macaddress_match_splitter                      ),
      .i_RS_error                  (i_RS_error                                          ),
      .i_calculate_state           (calculate_state                                     ),
      .i_valid                     (i_valid                                             ),
      .i_reset                     (i_reset                                             ),
      .i_clock                     (i_clock                                             )
      ) ;

   length_calculator
     #(
       .NB_DATA                    (NB_DATA                                             ),
       .LOG2_NB_DATA               (LOG2_NB_DATA                                        ),
       .NB_TIMER                   (NB_TIMER                                            ),
       .NBYTES_MAX_FRAME_SIZE      (NBYTES_MAX_FRAME_SIZE                               ),
       .NB_LENTYP                  (NB_LENTYP                                           ),
       .LOG2_MAX_FRAME_SIZE        (LOG2_FRAME_SIZE_BITS                                ),
       .FULL_FRAME_COMPARISON      (1                                                   )
        )
   u_length_calculator
     (
      .o_frame_size_found          (lencalc_framesize_bits_ready_status                 ),
      .o_frame_size                (lencalc_framesize_bits_status                       ),
      .o_frame_too_long            (lencalc_frame_too_long_status                       ),
      .o_dv_missmatch              (o_stats_dv_missmatch                                ),
      .i_rdv                       (i_receive_data_valid                                ),
      .i_shift                     ('b0                                                 ),
      .i_tail_size                 (i_tail_size_bits                                    ),
      .i_lentyp                    (splitter_len_typ ),
      .i_lentyp_valid              (splitter_lentypready_output),
      .i_reset                     (i_reset                                             ),
      .i_valid                     (i_valid                                             ),
      .i_clock                     (i_clock                                             )
      );


   always @(posedge i_clock) begin
     if (i_reset | calculate_state)
       calculate_frame_state <= 1'b0;
     else if (rdvalid_pos)
       calculate_frame_state <= 1'b1;
   end

   assign calculate_state = ((crc32_crc_ready | lencalc_frame_too_long_status | o_stats_dv_missmatch) & calculate_frame_state) | i_RS_error;
   //Remove last clock when the only data in the last NB_DATA only has CRC32 data
   assign remove_clock_datavalid_crc = (tailsize_reg == 0) ? 1'b0 : tailsize_reg <= NB_CRC32;
   //assign remove_clock_datavalid_crc = (i_tail_size_bits == NB_CRC32);

   assign datavalid_to_crc = (rdvalid_neg) ? !remove_clock_datavalid_crc : rdvalid_d;

   always @(posedge i_clock)
     begin
        if (i_reset) begin
           rdvalid_d <= 1'b0;
           delayed_data_to_crc <= {NB_DATA{1'b0}};
        end else if (i_valid) begin
           rdvalid_d <= i_receive_data_valid;
           delayed_data_to_crc <= i_data;
        end
     end

   assign rdvalid_pos = i_receive_data_valid & ~rdvalid_d;
   assign rdvalid_neg = ~i_receive_data_valid & rdvalid_d;

   //Add extra clock if needed
   always @(posedge i_clock)
     extra_client_clock <= (((NB_LENTYP+splitter_len_typ*8) % NB_DATA) == 0) ? 1'b0 : ((NB_LENTYP+splitter_len_typ*8) %NB_DATA <= NB_LENTYP) & splitter_data_valid;

   always @(posedge i_clock)
     begin
        if (i_reset)
          splitter_data_valid_d <= 1'b0;
        else if (i_valid)
          splitter_data_valid_d <= splitter_data_valid;
     end

   assign splitter_data_valid_pos = splitter_data_valid & ~splitter_data_valid_d;

   always @( posedge i_clock ) begin
      if (i_valid)
        casez ({splitter_lentypready_output, splitter_data_valid_pos, splitter_data_valid})
          3'b10?: extended_mac_service_data_unit[NB_DATA+NB_LENTYP-1-:NB_LENTYP] = splitter_len_typ; //Add lentyp to concatenation
          3'b11?: extended_mac_service_data_unit = {splitter_len_typ, splitter_mac_service_data_unit_output}; //Add both if they happen in the same clock
          3'b011: extended_mac_service_data_unit[NB_DATA-1:0] = splitter_mac_service_data_unit_output; //Append first data
          3'b001: extended_mac_service_data_unit = {extended_mac_service_data_unit[NB_LENTYP-1:0], splitter_mac_service_data_unit_output}; //Shift data
        endcase // casez ({splitter_lentypready_output, splitter_data_valid_pos, (splitter_data_valid | extra_client_clock)})
   end

   function integer clogb2;
      input integer                                     depth;
      for (clogb2=0; depth>0; clogb2=clogb2+1)
        depth = depth >> 1;
   endfunction // clogb2

endmodule
