/*PREAMBLE = 0x55 0x55 0x55 0x55 0x55 0x55 0x55
*SFD = 0xD5
*/
module preamble_finder_tb ();

    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================
    // Parameters.
    localparam                                   NB_DATA         = 64 ;
    localparam                                   LOG2_NB_DATA    = 6 ;
    localparam                                   NB_PREAMBLE     = 56 ;
    localparam                                   NB_SFD          = 8 ;
    localparam                                   NB_TIMER        = 16 ;
    localparam                                   NB_STAT_CNT     = 16 ;

    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================


    // Outputs
    wire                                lock_tb_o ;
    wire                                time_out_tb_o ;
    wire                                bad_rdv_tb_o ;
    wire    [NB_STAT_CNT-1:0]           time_out_cnt_tb_o ;
    wire    [NB_STAT_CNT-1:0]           bad_rdv_cnt_tb_o ;
    wire    [LOG2_NB_DATA-1:0]          index_tb_o ;
    wire    [NB_DATA-1:0]               data_tb_o ;
    wire                                 sof_tb_o ;
    wire                                valid_tb_o ;
    // Inputs
    wire    [NB_DATA-1:0]               data_tb_i ;
    wire                                valid_tb_i ;
    wire                                rdv_tb_i ;
//  wire    [NB_PREAMBLE-1:0]           preamble_tb_i ;
//  wire    [NB_SFD-1:0]                sfd_tb_i ;
//  wire    [NB_TIMER-1:0]              time_out_limit_tb_i ;
//  wire                                check_time_out_tb_i ;
//  wire                                read_status_tb_i ;
//  wire                                clear_on_read_mode_tb_i ;

    wire                                reset ;
    reg                                 clock = 1'b0;
    integer                             timer = 0;

    reg                                 test_passed = 1'b0;

    //==========================================================================
    // CONNECTION TO DUT
    //==========================================================================
    preamble_finder
    #(
        .NB_DATA         (NB_DATA     ),
        .LOG2_NB_DATA    (LOG2_NB_DATA),
        .NB_PREAMBLE     (NB_PREAMBLE ),
        .NB_SFD          (NB_SFD      ),
        .NB_TIMER        (NB_TIMER    ),
        .NB_STAT_CNT     (NB_STAT_CNT )
    )
    u_preamble_finder
    (
        // Outputs
        .o_lock                         (lock_tb_o),
        .o_time_out                     (time_out_tb_o),
        .o_bad_rdv                      (bad_rdv_tb_o),
                                        
        .o_time_out_cnt                 (time_out_cnt_tb_o),
        .o_bad_rdv_cnt                  (bad_rdv_cnt_tb_o),
                                        
        .o_index                        (index_tb_o),
        .o_data                         (data_tb_o),
        .o_sof                          (sof_tb_o),
        .o_valid                        (valid_tb_o),
        // Inputs
        .i_data                         (data_tb_i),
        .i_valid                        (valid_tb_i),
        .i_rdv                          (rdv_tb_i),
        .i_preamble                     (56'h55555555555555), //preamble_tb_i),
        .i_sfd                          (8'hD5),              //sfd_tb_i
        .i_time_out_limit               (8'b11111111),        //time_out_limit_tb_i  
        .i_check_time_out               (1'b0),               //check_time_out_tb_i),

        .i_read_status                  (1'b0),               //read_status_tb_i),
        .i_clear_on_read_mode           (1'b0),               //clear_on_read_mode_tb_i),

        .i_reset                        (reset),
        .i_clock                        (clock)
    ) ;

    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    always
    begin
        #(50) clock = ~clock ;
    end

    always @ ( posedge clock )
    begin
        timer   <= timer + 1;
    end

    assign reset = (timer == 2) ; // Reset at time 2
    assign valid_tb_i = 1'b1 ;
    assign rdv_tb_i = 1'b1 ;
    assign data_tb_i = 64'h55555555555555D5 ;


    always @( posedge clock )
    begin
        if ( lock_tb_o == 1'b0 )
            test_passed <= 1'b0 ;
        else if ( lock_tb_o == 1'b1 )
            test_passed <= 1'b1 ;
        if ( test_passed )
            $display( "TEST PASSED!" ) ;
        else
            $display( "TEST FAILED..." ) ;
    end

endmodule