module preamble_finder
  #(
    // Parameters.
    parameter                                   NB_DATA         = 64 ,
    parameter                                   LOG2_NB_DATA    = 6 ,
    parameter                                   NB_PREAMBLE     = 56 ,
    parameter                                   NB_SFD          = 8 ,
    parameter                                   NB_TIMER        = 16 ,
    parameter                                   NB_STAT_CNT     = 16
    )
   (
    // Outputs.
    output wire                   o_lock , //Signal that remains high during valid and SOF match => can be used as MAC valid because no timeout is implemented
    output wire                   o_time_out ,
    output wire                   o_bad_rdv ,

    output reg [NB_STAT_CNT-1:0]  o_time_out_cnt ,
    output reg [NB_STAT_CNT-1:0]  o_bad_rdv_cnt ,

    output reg [LOG2_NB_DATA-1:0] o_index , //Index from left to right. Ex: <lastbitsofSOF>000|END OF PARALLELISM => o_index=3
    output wire [NB_DATA-1:0]     o_data , //Shifted data according to o_index => can be used as data input for MAC
    output reg                    o_sof , // o_sof: Start Of Frame.
    output wire                   o_valid , //Valid signal for MAC
    // Inputs.
    input wire [NB_DATA-1:0]      i_data ,
    input wire                    i_valid , // Throughput control.
    input wire                    i_rdv , // i_rdv: Receive Data Valid.
    input wire [NB_PREAMBLE-1:0]  i_preamble ,
    input wire [NB_SFD-1:0]       i_sfd , // i_sfd: Start of Frame Delimiter.
    input wire [NB_TIMER-1:0]     i_time_out_limit ,
    input wire                    i_check_time_out ,

    input wire                    i_read_status ,
    input wire                    i_clear_on_read_mode ,

    input wire                    i_reset ,
    input wire                    i_clock
    ) ;

   //==========================================================================
   // LOCAL PARAMETERS.
   //==========================================================================
   localparam                                  NB_FAS          = NB_PREAMBLE + NB_SFD ;
   // localparam                                  N_SAVED_DATA    = ( ( NB_FAS%NB_DATA )==0 )? ( NB_FAS / NB_DATA ) : (( NB_FAS / NB_DATA )+1) ;
   localparam                                  NB_SHIFTER      = NB_DATA + NB_FAS ;
   localparam                                  NB_STATE        = 3 ;
   localparam      [NB_STATE-1:0]              ST_0_INIT       = 0 ;
   localparam      [NB_STATE-1:0]              ST_1_WAIT       = 1 ;
   localparam      [NB_STATE-1:0]              ST_2_LOCK       = 2 ;
   // localparam      [NB_STATE-1:0]              ST_3_TIME_OUT   = 3 ;
   // localparam      [NB_STATE-1:0]              ST_4_BAD_RDV    = 4 ;   // Bad Recive Data Valid Pos edge.

   //==========================================================================
   // INTERNAL SIGNALS.
   //==========================================================================
   wire [NB_FAS-1:0]              fas ;
   wire [NB_SHIFTER-1:0]          shifter ;
   reg [NB_SHIFTER-NB_DATA-1:0]   shifter_mem ;
   genvar                         ii ;
   wire [NB_DATA-1:0]             match_bus ;
   wire                           match ;

   reg                            rdv_i_d ;
   wire                           rdv_pos ;
   wire                           rdv_neg;
   reg [NB_TIMER-1:0]             timer ;

   reg [NB_STATE-1:0]             state ;
   reg [NB_STATE-1:0]             state_next ;

   reg                            fsmo_reset_timer ;
   reg                            fsmo_get_index   ;
   reg                            fsmo_in_lock     ;
   reg                            fsmo_sof         ;
   reg                            fsmo_time_out    ;
   reg                            fsmo_bad_rdv     ;

   reg [LOG2_NB_DATA-1:0]         index ;

   reg                            read_status_ms ;
   reg                            read_status_dm ;
   reg                            read_status_d ; //Added
   wire                           read_status_pos ;

   wire                           time_out ;

   reg [NB_STAT_CNT-1:0]          cnt_time_out ;
   reg [NB_STAT_CNT-1:0]          cnt_bad_rdv ;

   integer                        ia ;


   //==========================================================================
   // ALGORITHM.
   //==========================================================================

   // Merge preamble and SFD in one search pattern (FAS).
   assign  fas = {i_preamble, i_sfd} ;

   // FAS search correlator.
   //--------------------------------------------------------------------------

   // Concatenate data with data from previous clock to allow searching for FAS
   // in any possition of the data bus.
   always @( posedge i_clock )
     if ( i_valid )
       shifter_mem <= {shifter_mem, i_data} ;  //spyglass disable w172 -- [NOTE]: MSB drop is intentional to simplify coding.
   assign  shifter = {shifter_mem, i_data} ;

   // Correlators for searching FAS.
   generate
      for ( ii=0; ii<NB_DATA; ii=ii+1 )
        begin : genfor_comparators
           wire    [NB_FAS-1:0]                possible_fas ;

           assign  possible_fas = shifter[ ii +: NB_FAS ] ;
           assign  match_bus[ ii ] = ( possible_fas == fas ) ;
        end // genfor_comparators
   endgenerate

   // Match signal (indicating FAS is present in side the sifter.
   assign  match = | match_bus ;


   // Control FSM.
   //--------------------------------------------------------------------------

   // Generate trigger signal for FSM based on posedge of receive data valid.
   always @( posedge i_clock )
     if ( i_reset )
       rdv_i_d <= 1'b0 ;
     else if ( i_valid )
       rdv_i_d <= i_rdv ;
   assign  rdv_pos = i_rdv & ~rdv_i_d ;
   assign rdv_neg = ~i_rdv & rdv_i_d ;

   // State update.
   always @( posedge i_clock )
     begin
        if ( i_reset )
          state <= ST_0_INIT ;
        else if ( i_valid )
          state <= state_next ;
     end


   // Calculate next state and FSM outputs.
   always @( * )
     begin

        state_next          = ST_0_INIT ;
        fsmo_reset_timer    = 1'b1 ;
        fsmo_get_index      = 1'b0 ;
        fsmo_in_lock        = 1'b0 ;
        fsmo_sof            = 1'b0 ;
        fsmo_time_out       = 1'b0 ;
        fsmo_bad_rdv        = 1'b0 ;

        case ( state )
          ST_0_INIT :
            begin
               casez ( {rdv_pos, match, time_out} )
                 3'b11?  : state_next    = ST_2_LOCK ;
                 3'b10?  : state_next    = ST_1_WAIT ;
                 default : state_next    = ST_0_INIT ;
               endcase
               fsmo_reset_timer    = ~rdv_pos ;
               fsmo_get_index      = rdv_pos & match ;
               fsmo_in_lock        = 1'b0 ;
               fsmo_sof            = rdv_pos & match ;
               fsmo_time_out       = 1'b0 ;
               fsmo_bad_rdv        = 1'b0 ;
            end

          ST_1_WAIT :
            begin
               casez ( {rdv_pos, match, time_out} )
                 // 3'b1??  : state_next    = ST_4_BAD_RDV ; ST_0_INIT
                 3'b1??  : state_next    = ST_0_INIT ;
                 3'b01?  : state_next    = ST_2_LOCK ;
                 // 3'b001  : state_next    = ST_3_TIME_OUT ;
                 3'b001  : state_next    = ST_0_INIT ;
                 default : state_next    = ST_1_WAIT ;
               endcase
               fsmo_reset_timer    = 1'b0 ;
               fsmo_get_index      = match ;
               fsmo_in_lock        = 1'b0 ;
               fsmo_sof            = match ;
               fsmo_time_out       = time_out ;
               fsmo_bad_rdv        = rdv_pos ;
            end

          ST_2_LOCK :
            begin
               casez ( {rdv_pos, match, time_out} )
                 3'b11?  : state_next    = ST_2_LOCK ;
                 3'b10?  : state_next    = ST_1_WAIT ;
                 3'b001  : state_next    = ST_0_INIT ;
                 default : state_next    = ST_2_LOCK ;
               endcase
               fsmo_reset_timer    = ~rdv_pos ;
               fsmo_get_index      = rdv_pos & match ;
               fsmo_in_lock        = i_rdv ;
               fsmo_sof            = rdv_pos & match ;
               fsmo_time_out       = 1'b0 ;
               fsmo_bad_rdv        = 1'b0 ;
            end

        endcase
     end


   // Time out monitor.
   always @( posedge i_clock )
     begin
        if ( i_reset || i_valid && fsmo_reset_timer )
          timer <= {NB_TIMER{1'b0}} ;
        else if ( i_valid && !time_out )
          timer <= timer + 1'b1 ;   //spyglass disabel A124 -- Carry drop and operand size mismatch is intended.
     end
   //assign  time_out
   //            = ( timer >= i_time_out_limit ) & i_check_time_out ;
   assign time_out = rdv_neg; //There is no timeout for MAC Ethernet, instead, use rdv_neg

   // Capture frame alignment index.
   always @( posedge i_clock )
     begin
        if ( i_reset )
          o_index <= {LOG2_NB_DATA{1'b0}} ;
        else if ( i_valid && fsmo_get_index )
          o_index <= index ;
     end
   always @( * )
     begin
        index = {LOG2_NB_DATA{1'b0}} ;
        for ( ia=0; ia<NB_DATA; ia=ia+1 )
          if ( match_bus[ ia ] )
            index = ia[ LOG2_NB_DATA-1:0 ] ;  //spyglass disable ASD3 -- Integer chopping intentional.
     end


   assign  o_data = shifter[ NB_DATA-1 + o_index -: NB_DATA ] ;
   always @( posedge i_clock )
     if ( i_reset )
       o_sof <= 1'b0 ;
     else if ( i_valid )
       o_sof <= fsmo_sof ;
   assign  o_valid = fsmo_in_lock & i_valid & i_rdv;


   // Main output assignment.
   //--------------------------------------------------------------------------
   assign  o_lock      = fsmo_in_lock ;
   assign  o_time_out  = time_out ;
   assign  o_bad_rdv   = fsmo_bad_rdv ;



   //--------------------------------------------------------------------------

   // Double Flop Sync. for read control
   always @( posedge i_clock )
     begin
        read_status_ms <= i_read_status ;
        read_status_dm <= read_status_ms ;
        read_status_d <= read_status_dm ;
     end
   assign  read_status_pos = read_status_dm & ~read_status_d ;


   // Read status from block
   //--------------------------------------------------------------------------

   always @( posedge i_clock )
     begin
        if ( read_status_pos && i_clear_on_read_mode )
          cnt_time_out <= {NB_STAT_CNT{1'b0}} ;
        else if ( fsmo_time_out && i_valid )
          cnt_time_out <= cnt_time_out + 1'b1 ;
     end

   always @( posedge i_clock )
     begin
        if ( read_status_pos )
          o_time_out_cnt <= cnt_time_out ;
     end

   always @( posedge i_clock )
     begin
        if ( read_status_pos && i_clear_on_read_mode )
          cnt_bad_rdv <= {NB_STAT_CNT{1'b0}} ;
        else if ( fsmo_time_out && i_valid )
          cnt_bad_rdv <= cnt_bad_rdv + 1'b1 ;
     end

   always @( posedge i_clock )
     begin
        if ( read_status_pos )
          o_bad_rdv_cnt <= cnt_bad_rdv ;
     end


endmodule
