module preamble_finder_test ();

   localparam                                       DATA_FILE                 = "/home/jsoriano/Ethernet_MAC/RX/simulador_python/data" ;
   localparam                                       DATAVALID_FILE            = "/home/jsoriano/Ethernet_MAC/RX/simulador_python/datavalid";
   localparam                                       LOG2_FRAME_SIZE           = 14 ;
   localparam                                       NB_DST_ADDR               = 48 ;
   localparam                                       NB_SRC_ADDR               = 48 ;
   localparam                                       NB_CRC                    = 32 ;

   localparam                                       NB_DATA                   = 64 ;
   localparam                                       LOG2_NB_DATA              = 6 ;
   localparam                                       RX_FIFO_DEPTH             = 4 ;
   localparam                                       RX_FIFO_ADDR              = 4 ;
   localparam                                       NB_ADDRESS                = 48 ;
   localparam                                       LOG2_NB_ADDRESS           = 6 ;
   localparam                                       NB_LENTYP                 = 16 ;
   localparam                                       LOG2_NB_LENTYP            = 4 ;
   localparam                                       N_MULTICAST_ADDRESS       = 3 ;
   localparam                                       LOG2_N_MULTICAST_ADDRESS  = 4 ;
   localparam                                       MAX_LENGTH_B_BYTES        = 1982 ;
   localparam                                       LOG2_FRAME_SIZE_BITS      = 14 ;
   localparam                                       NB_CRC32                  = 32 ;
   localparam                                       LOG2_NB_CRC32             = 5 ;
   localparam                                       N_RX_STATUS               = 5 ;
   localparam                                       LOG2_N_RX_STATUS          = 3 ;
   localparam                                       NBYTES_MAX_FRAME_SIZE     = 2026 ;
   localparam                                       NBYTES_MAX_DATA_SIZE      = 1982 ;
   localparam                                       LOG2_NBYTES_MAX_DATA_SIZE = 11 ;

   localparam                                       max_frame_size_bits       = NBYTES_MAX_FRAME_SIZE * 8;
   localparam                                       log2_max_frame_size_bits  = clogb2(max_frame_size_bits);

   localparam                                       LEN_NB_DATA               = 64;

   //preamble finder params
   localparam                                       NB_STAT_CNT               = 16 ;
   localparam                                   NB_PREAMBLE     = 56 ;
   localparam                                   NB_SFD          = 8 ;
   localparam                                   NB_TIMER        = 16 ;
   //==========================================================================
   // INTERNAL SIGNALS.
   //==========================================================================
   wire                                             data_valid_i;
   wire [NB_DATA-1:0]                               data_i;

   // Other
   reg                                              clock_i = 1'b0;
   wire                                             tb_valid_i;
   wire                                             tb_reset_i ;
   integer                                          timer = 0 ;
   // File related variables
   integer                                          fd_data_input = 0;
   integer                                          fd_datavalid_input = 0;
   integer                                          scan_file ;

   //Registers to parse data
   reg [NB_DATA-1:0]                                data_reg;
   reg                                              data_valid_reg;

   reg                                              data_valid_d;
   reg                                              data_valid_dd;
   wire                                             data_valid_pos;

   //For preamble finder
   // Outputs
   wire                                             lock_tb_o ;
   wire                                             time_out_tb_o ;
   wire                                             bad_rdv_tb_o ;
   wire [NB_STAT_CNT-1:0]                           time_out_cnt_tb_o ;
   wire [NB_STAT_CNT-1:0]                           bad_rdv_cnt_tb_o ;
   wire [LOG2_NB_DATA-1:0]                          index_tb_o ;
   wire [NB_DATA-1:0]                               data_tb_o ;
   wire                                             sof_tb_o ;
   wire                                             valid_tb_o ;
   initial
     begin
        data_valid_reg = 0 ;
        fd_data_input = $fopen(DATA_FILE,"r");
        if (fd_data_input==0)
          begin
             $display("Error de lectura de archivo DATA_FILE");
             $stop;
          end
        fd_datavalid_input = $fopen(DATAVALID_FILE,"r");
        if (fd_datavalid_input==0)
          begin
             $display("Error de lectura de archivo DATAVALID_FILE");
             $stop;
          end
     end // initial begin

   //File read
   always @ (posedge clock_i)
     begin
        if (tb_valid_i) begin
           //Capture data
           scan_file = $fscanf(fd_data_input, "%b", data_reg) ;
           if ($feof(fd_data_input))begin
             $display ("EOF data");
             $fclose(fd_data_input);
           end

           //Capture datavalid
           scan_file = $fscanf(fd_datavalid_input, "%1b", data_valid_reg);
           if($feof(fd_datavalid_input)) begin
             $display("EOF datavalid");
             $fclose(fd_datavalid_input);
           end
        end
     end


    //==========================================================================
    // CONNECTION TO DUT
    //==========================================================================
    preamble_finder
    #(
        .NB_DATA         (NB_DATA     ),
        .LOG2_NB_DATA    (LOG2_NB_DATA),
        .NB_PREAMBLE     (NB_PREAMBLE ),
        .NB_SFD          (NB_SFD      ),
        .NB_TIMER        (NB_TIMER    ),
        .NB_STAT_CNT     (NB_STAT_CNT )
    )
    u_preamble_finder
    (
        // Outputs
        .o_lock                         (lock_tb_o),
        .o_time_out                     (time_out_tb_o),
        .o_bad_rdv                      (bad_rdv_tb_o),

        .o_time_out_cnt                 (time_out_cnt_tb_o),
        .o_bad_rdv_cnt                  (bad_rdv_cnt_tb_o),

        .o_index                        (index_tb_o),
        .o_data                         (data_tb_o),
        .o_sof                          (sof_tb_o),
        .o_valid                        (valid_tb_o),
        // Inputs
        .i_data                         (data_i),
        .i_valid                        (tb_valid_i),
        .i_rdv                          (data_valid_i),
        .i_preamble                     (56'hAAAAAAAAAAAAAA), //preamble_tb_i),
        .i_sfd                          (8'hAB),              //sfd_tb_i
        .i_time_out_limit               (16'hFFFF),        //time_out_limit_tb_i
        .i_check_time_out               (1'b0),               //check_time_out_tb_i),

        .i_read_status                  (1'b0),               //read_status_tb_i),
        .i_clear_on_read_mode           (1'b0),               //clear_on_read_mode_tb_i),

        .i_reset                        (tb_reset_i),
        .i_clock                        (clock_i)
    ) ;



   assign data_i = data_reg;
   assign data_valid_i = data_valid_reg;

   //Posedge datavalid
   always @ (posedge clock_i)
     begin
        if (tb_reset_i) begin
           data_valid_d <= 'b0;
           data_valid_dd <= 'b0;
        end else begin
           data_valid_d <= data_valid_reg;
           data_valid_dd <= data_valid_d;
        end
     end
        assign data_valid_pos = ~data_valid_dd & data_valid_i ;



   always #50 clock_i = ~clock_i ;

   always @ ( posedge clock_i )
     begin
        timer   <= timer + 1;
     end

   assign tb_reset_i = (timer == 2) ; // Reset at time 2
   assign tb_valid_i = timer>4 ;

   function integer clogb2;
      input integer depth;
      for (clogb2=0; depth>0; clogb2=clogb2+1)
        depth = depth >> 1;
   endfunction // clogb2

endmodule
