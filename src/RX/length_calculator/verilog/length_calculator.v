module length_calculator
  #(
    // Parameters.
    parameter                                   NB_DATA               = 64 ,
    parameter                                   LOG2_NB_DATA          = 6 ,
    parameter                                   NB_TIMER              = 16 ,
    parameter                                   NBYTES_MAX_FRAME_SIZE = 2026 ,
    parameter                                   NB_LENTYP             = 16 ,
    parameter                                   LOG2_MAX_FRAME_SIZE   = 14,
    parameter                                   FULL_FRAME_COMPARISON = 0
    )
   (
    // Outputs.
    output wire                          o_frame_size_found ,
    output reg [LOG2_MAX_FRAME_SIZE-1:0] o_frame_size ,
    output wire                          o_frame_too_long ,
    output reg                           o_dv_missmatch,

    // Inputs.
    input wire                           i_rdv , // i_rdv: Receive Data Valid.
    input wire [LOG2_NB_DATA-1:0]        i_shift ,
    input wire [LOG2_NB_DATA-1:0]        i_tail_size ,
    input wire [NB_LENTYP-1:0]           i_lentyp,
    input wire                           i_lentyp_valid,

    input wire                           i_reset ,
    input wire                           i_valid ,
    input wire                           i_clock
) ;

    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================

    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
   reg                                   rdv_i_d ;
   wire                                  rdv_pos ;
   wire                                  rdv_neg ;
   reg [NB_TIMER-1:0]                    timer ;

   wire                                  timer_start ;
   wire                                  timer_stop ;

   reg                                   timer_status ;
   reg [NB_LENTYP-1:0]                   lentyp_reg;

   localparam                            NB_HEADER_AND_CRC = 2*48+16+32;
   localparam                            MIN_LENTYP = 46;



    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    // Generate trigger signal based on posedge of receive data valid.
    always @( posedge i_clock )
    begin
        if ( i_reset )
            rdv_i_d <= 1'b0 ;
        else if ( i_valid )
            rdv_i_d <= i_rdv ;
    end

    assign rdv_pos = i_rdv & ~rdv_i_d ;

    // Generate trigger signal based on negedge of receive data valid.

    assign rdv_neg = ~i_rdv & rdv_i_d ;

    // Timer Start and Stop
    assign timer_start = rdv_pos ;
    assign timer_stop = rdv_neg ;

    always @ ( posedge i_clock )
    begin
        if ( (i_reset | timer_stop) & i_valid )
            timer_status <= 1'b0 ;
        else if ( timer_start & i_valid )
            timer_status <= 1'b1 ;
    end

    always @( posedge i_clock )
    begin
        if ( i_reset | timer_stop)
            timer <= {NB_TIMER{1'b0}} ;
        else if ( i_valid & (timer_status | timer_start))
            timer <= timer + 1'b1 ;
     end

   always @( posedge i_clock ) begin
     if ( i_reset | ~i_rdv)
       o_frame_size <= 'b0;
     else if ( i_valid )
       o_frame_size <= (NB_DATA*(timer+1)) - i_shift - ((NB_DATA-i_tail_size)%NB_DATA) ;
   end

   always @(posedge i_clock) begin
      if (i_reset)
        lentyp_reg <= 'b0;
      else if (i_valid & i_lentyp_valid)
        lentyp_reg <= (i_lentyp<MIN_LENTYP & FULL_FRAME_COMPARISON) ? MIN_LENTYP : i_lentyp;
   end

   always @(posedge i_clock) begin
      if (o_frame_size_found)
        o_dv_missmatch <= (lentyp_reg*8 != ({{(NB_LENTYP-LOG2_MAX_FRAME_SIZE){1'b0}},o_frame_size} - (FULL_FRAME_COMPARISON*NB_HEADER_AND_CRC) ));
      else
        o_dv_missmatch <= 1'b0;
   end

   assign o_frame_too_long = o_frame_size > NBYTES_MAX_FRAME_SIZE*8;
   assign o_frame_size_found = timer_stop | o_frame_too_long;

endmodule
