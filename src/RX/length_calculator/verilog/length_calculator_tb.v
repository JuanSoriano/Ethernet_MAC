module length_calculator_tb ();

    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================
    // Parameters.
	localparam                          NB_DATA             = 64 ;
	localparam                          LOG2_NB_DATA        = 6  ;
	localparam                          NB_TIMER            = 16 ;
	localparam                          LOG2_MAX_FRAME_SIZE = 16 ;

    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
    reg                                 frame_size_found_tb_o ;
    reg   [LOG2_MAX_FRAME_SIZE-1:0]     frame_size_tb_o ;
    wire                                timer_overflow_tb_o ;
    // Inputs.
    wire                                rdv_tb_i ;     // i_rdv: Receive Data Valid.
    wire      [NB_DATA-1:0]             shift_tb_i ;
    wire      [NB_DATA-1:0]             tail_size_tb_i ;
    wire                                valid_tb_i ;

    wire                                reset ;
    reg                                 clock = 1'b0;
    integer                             timer = 0;



    //==========================================================================
    // CONNECTION TO DUT
    //==========================================================================
    length_calculator
    #(
		.NB_DATA            			(NB_DATA            ),
		.LOG2_NB_DATA       			(LOG2_NB_DATA       ),
		.NB_TIMER           			(NB_TIMER           ),
		.LOG2_MAX_FRAME_SIZE			(LOG2_MAX_FRAME_SIZE)
    )
    u_length_calculator
    (
		.o_frame_size_found 			(frame_size_found_tb_o),				
		.o_frame_size 					(frame_size_tb_o),		
		.o_timer_overflow 				(timer_overflow_tb_o),			
		.i_rdv 							(rdv_tb_i),
		.i_shift 						(shift_tb_i),	
		.i_tail_size 					(tail_size_tb_i),		
		.i_reset 						(reset),
		.i_clock						(clock)	
    ) ;

    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    always
    begin
        #(50) clock = ~clock ;
    end

    always @ ( posedge clock )
    begin
        timer   <= timer + 1;
    end

    assign reset = (timer == 2) ; // Reset at time 2
    assign valid_tb_i = (timer >= 4) ;

endmodule