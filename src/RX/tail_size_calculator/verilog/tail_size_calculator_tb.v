module tail_size_calculator_tb ();

    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================
    // Parameters.
    localparam                                       NB_DATA                = 64 ;
    localparam                                       LOG2_NB_DATA           = 6 ;

    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
    // Outputs.
    reg              [LOG2_NB_DATA-1:0]             tail_size_tb_o ;    // Output a 1 to signal a match in addresses

    // Inputs.
    wire                                            receive_data_valid_tb_i ; // Data valid input signal
    wire             [LOG2_NB_DATA-1:0]             tail_size_tb_i ;

    wire                                            valid_tb_i ;   // Throughput control.

    wire                                            reset ;
    reg                                             clock = 1'b0 ;
    integer                                         timer = 0 ;

    reg                                             test_passed = 1'b0 ;

    //==========================================================================
    // CONNECTION TO DUT
    //==========================================================================

    tail_size_calculator
    #(
        .NB_DATA                (NB_DATA      ),
        .LOG2_NB_DATA           (LOG2_NB_DATA )
    )
    u_tail_size_calculator
    (
        //Outputs
        o_tail_size             (tail_size_tb_o),   
        //Inputs
        i_receive_data_valid    (receive_data_valid_tb_i),
        i_tail_size             (tail_size_tb_i),
        i_valid                 (valid_tb_i),
        i_reset                 (reset),
        i_clock                 (clock)
    ) ;

    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    always
    begin
        #(50) clock = ~clock ;
    end

    always @ ( posedge clock )
    begin
        timer   <= timer + 1;
    end

    assign reset = (timer == 2) ; // Reset at time 2
    assign valid_tb_i = 1'b1 ;
    assign receive_data_valid_tb_i = 1'b1 ;

/*    always @( posedge clock )
    begin
        if (  )
            test_passed <= 1'b0 ;
        else if (  )
            test_passed <= 1'b1 ;
        if ( test_passed )
            $display( "TEST PASSED!" ) ;
        else
            $display( "TEST FAILED..." ) ;
    end
*/

endmodule
