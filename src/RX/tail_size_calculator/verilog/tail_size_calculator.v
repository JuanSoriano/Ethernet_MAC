module tail_size_calculator
#(
    // Parameters.
    parameter                                       NB_DATA                = 64 ,
    parameter                                       LOG2_NB_DATA           = 6
)
(
    // Outputs.
    output  reg              [LOG2_NB_DATA-1:0]     o_tail_size ,    // Output a 1 to signal a match in addresses

    // Inputs.
    input   wire                                    i_receive_data_valid , // Data valid input signal
    input   wire             [LOG2_NB_DATA-1:0]     i_tail_size,

    input   wire                                    i_valid ,   // Throughput control.

    input   wire                                    i_reset ,
    input   wire                                    i_clock

) ;

    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================

    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
    reg                                             pos_edge ;
    wire                                            pe ;
    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    always @ (posedge i_clock)
    begin

        pos_edge <= pe ;
    end

    assign pe = ~ i_receive_data_valid & ~ pos_edge ;

    always @ (posedge i_clock)
    begin
        if (pe==1'b1)
        o_tail_size <= i_tail_size;
    end

    always @ (posedge i_clock)
    begin

        if (i_reset)
        begin
            o_tail_size <= 1'b0 ;
            pos_edge <= 1'b0 ;
        end
    end

endmodule
