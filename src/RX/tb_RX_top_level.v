module RX_top_level_tb () ;

    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================
    // Parameters.

    localparam                                       LOG2_FRAME_SIZE             = 14 ;
    localparam                                       NB_DST_ADDR                 = 48 ;
    localparam                                       NB_SRC_ADDR                 = 48 ;
    localparam                                       NB_CRC                      = 32 ;

    localparam                                       NB_DATA                     = 64 ;
    localparam                                       LOG2_NB_DATA                = 6 ;
    localparam                                       RX_FIFO_DEPTH               = 4 ;
    localparam                                       RX_FIFO_ADDR                = 4 ;
    localparam                                       NB_ADDRESS                  = 48 ;
    localparam                                       LOG2_NB_ADDRESS             = 6 ;
    localparam                                       NB_LENTYP                   = 16 ;
    localparam                                       LOG2_NB_LENTYP              = 4 ;
    localparam                                       N_MULTICAST_ADDRESS         = 4 ;
    localparam                                       LOG2_N_MULTICAST_ADDRESS    = 4 ;
    localparam                                       MAX_LENGTH_B_BYTES          = 1982 ;
    localparam                                       LOG2_FRAME_SIZE_BITS        = 14 ;
    localparam                                       NB_CRC32                    = 32 ;
    localparam                                       LOG2_NB_CRC32               = 5 ;
    localparam                                       N_RX_STATUS                 = 5 ;
    localparam                                       LOG2_N_RX_STATUS            = 3 ;
    localparam                                       NBYTES_MAX_FRAME_SIZE       = 2026 ;
    localparam                                       NBYTES_MAX_DATA_SIZE        = 1982 ;
    localparam                                       LOG2_NBYTES_MAX_DATA_SIZE   = 11 ;



    localparam                                      max_frame_size_bits = NBYTES_MAX_FRAME_SIZE * 8;
    localparam                                      log2_max_frame_size_bits = clogb2(max_frame_size_bits);
    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
//----------------------------------
//Top level
//----------------------------------
    // Outputs.
    //To upper layers
    wire                                             tb_MA_DATA_indication_o; //Indicates to upper layers a frame has been received
    wire    [LOG2_N_RX_STATUS-1:0]                   tb_rx_status_o; //RX status code, indicates if there is a failure in reception or OK
    wire    [NB_ADDRESS-1:0]                         tb_destination_address_o; //Frame destination address
    wire    [NB_ADDRESS-1:0]                         tb_source_address_o; //Frame source address
    wire    [NB_DATA-1:0]                            tb_mac_service_data_unit_o; //Frame payload            [MAX_LENGTH_B_BYTES*8-1:0]?
    wire    [NB_CRC32-1:0]                           tb_frame_check_sequence_o; //Frame CRC 32

    wire                                             tb_receive_data_valid_i; //From lower layers
    wire                                             tb_tail_size_bytes ; //Size of the tail in bytes received from lower layers

    //For adresses
    wire    [NB_ADDRESS-1:0]                         tb_local_mac_address_i ; // Station MAC address
    wire    [NB_ADDRESS-1:0]                         tb_broadcast_mac_address_i ; // Broadcast MAC address (address destined for every station)
    reg     [NB_ADDRESS*N_MULTICAST_ADDRESS-1:0]     tb_multicast_addresses_i ; // Vector of multicast addresses
    wire    [NB_ADDRESS-1:0]                         tb_multicast_addresses_arr [N_MULTICAST_ADDRESS-1:0] ;
    wire    [N_MULTICAST_ADDRESS-1:0]                tb_enable_multicast_addr_i ; //Multicast addresses enabler

//----------------------------------
//Other
//----------------------------------
    //Connections
    wire                [NB_DATA-1:0]                framegen_data_toplevel ;
    wire                                             framegen_datavalid_toplevel ;
    wire                                             trigger_frame_generation ;

    //
    wire    [clogb2(max_frame_size_bits)-1:0]                framegen_frame_size_bits ;
    wire    [log2_max_frame_size_bits-1:0]           freamegen_data_size_bytes ;

    // Other
    reg                                              tb_clock_i = 1'b0;
    wire                                             tb_valid_i;
    wire                                             tb_reset_i ;
    integer                                          timer = 0 ;
    integer                                          i ;

   frame_generator_b
    #(
        .NB_DATA                        ( NB_DATA                  ),
        .LOG2_NB_DATA                   ( LOG2_NB_DATA             ),
        .LOG2_FRAME_SIZE                ( LOG2_FRAME_SIZE          ),
        .NB_DST_ADDR                    ( NB_DST_ADDR              ),
        .NB_SRC_ADDR                    ( NB_SRC_ADDR              ),
        .NB_LENTYP                      ( NB_LENTYP                ),
        .NB_CRC                         ( NB_CRC                   )
    )
    u_frame_generator_b
    (
        .o_data                         ( framegen_data_toplevel     ),
        .o_data_valid                   ( framegen_datavalid_toplevel),
        .o_datasize_bytes               (freamegen_data_size_bytes),
        .o_framesize_bits               (framegen_frame_size_bits),
        .i_valid                        ( tb_valid_i               ),
        .i_frame_ready                  ( trigger_frame_generation ),
        .i_reset                        ( tb_reset_i                    ),
        .i_clock                        ( tb_clock_i                    )
    ) ;

    RX_top_level
    #(
        .NB_DATA                        (NB_DATA                     ),
        .LOG2_NB_DATA                   (LOG2_NB_DATA                ),
        .RX_FIFO_DEPTH                  (RX_FIFO_DEPTH               ),
        .RX_FIFO_ADDR                   (RX_FIFO_ADDR                ),
        .NB_ADDRESS                     (NB_ADDRESS                  ),
        .LOG2_NB_ADDRESS                (LOG2_NB_ADDRESS             ),
        .NB_LENTYP                      (NB_LENTYP                   ),
        .LOG2_NB_LENTYP                 (LOG2_NB_LENTYP              ),
        .N_MULTICAST_ADDRESS            (N_MULTICAST_ADDRESS         ),
        .LOG2_N_MULTICAST_ADDRESS       (LOG2_N_MULTICAST_ADDRESS    ),
        .MAX_LENGTH_B_BYTES             (MAX_LENGTH_B_BYTES          ),
        .LOG2_FRAME_SIZE_BITS           (LOG2_FRAME_SIZE_BITS        ),
        .NB_CRC32                       (NB_CRC32                    ),
        .LOG2_NB_CRC32                  (LOG2_NB_CRC32               ),
        .N_RX_STATUS                    (N_RX_STATUS                 ),
        .LOG2_N_RX_STATUS               (LOG2_N_RX_STATUS            ),
        .NBYTES_MAX_FRAME_SIZE          (NBYTES_MAX_FRAME_SIZE       ),
        .NBYTES_MAX_DATA_SIZE           (NBYTES_MAX_DATA_SIZE        ),
        .LOG2_NBYTES_MAX_DATA_SIZE      (LOG2_NBYTES_MAX_DATA_SIZE   )
    )
    u_RX_top_level
    (
        .o_MA_DATA_indication           (tb_MA_DATA_indication_o     ),
        .o_rx_status                    (tb_rx_status_o              ),
        .o_destination_address          (tb_destination_address_o    ),
        .o_source_address               (tb_source_address_o         ),
        .o_mac_service_data_unit        (tb_mac_service_data_unit_o  ),
        .o_frame_check_sequence         (tb_frame_check_sequence_o   ),

        .i_receive_data_valid           (tb_receive_data_valid_i     ),
        .i_data                         (framegen_data_toplevel      ),
        .i_local_mac_address            (tb_local_mac_address_i      ),
        .i_broadcast_mac_address        (tb_broadcast_mac_address_i  ),
        .i_multicast_addresses          (tb_multicast_addresses_i    ),
        .i_enable_multicast_addr        (tb_enable_multicast_addr_i  ),

        .i_frame_size_bits              (framegen_frame_size_bits),  //Size of the complete frame
        .i_data_size_bytes              (freamegen_data_size_bytes), //Size of the payload field

        .i_clock                        (tb_clock_i                  ),
        .i_valid                        (tb_valid_i                  ),
        .i_reset                        (tb_reset_i                  )
    ) ;

    always
    begin
        #(50) tb_clock_i = ~tb_clock_i ;
    end

    always @ ( posedge tb_clock_i )
    begin
        timer   <= timer + 1;
    end

    assign tb_receive_data_valid_i = framegen_datavalid_toplevel ; //Taking frame generator as lower layers, this signal is the same
    assign tb_MA_DATA_indication_o = framegen_datavalid_toplevel ; //Same as above
    assign tb_reset_i = (timer == 2) ; // Reset at time 2
    assign tb_valid_i = 1'b1 ;
    assign tb_tail_size_bytes = 0;

    //Frame generation
    assign trigger_frame_generation = (timer%(2500/4) == 0) ;


    //Adresses
    assign tb_local_mac_address_i        = 48'H000000000008 ;
    assign tb_broadcast_mac_address_i    = 48'HAAAAAAAAAAAB ;
    assign tb_multicast_addresses_arr[0] = 48'HAAAAAAAAAAAC ;
    assign tb_multicast_addresses_arr[1] = 48'HAAAAAAAAAAAF ;
    assign tb_multicast_addresses_arr[2] = 48'HAAAAAAAAAAAE ;
    assign tb_multicast_addresses_arr[3] = 48'HAAAAAAAAAAAF ;

    always @ ( * ) begin
        for ( i=0; i<N_MULTICAST_ADDRESS; i=i+1) begin
            tb_multicast_addresses_i [i*NB_ADDRESS+: NB_ADDRESS] = tb_multicast_addresses_arr[i];
        end
    end

    assign tb_enable_multicast_addr_i = 4'b1111 ;

   function integer clogb2;
      input integer depth;
      for (clogb2=0; depth>0; clogb2=clogb2+1)
        depth = depth >> 1;
   endfunction // clogb2

endmodule
