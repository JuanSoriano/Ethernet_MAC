module RX_FD_top
  #(
    parameter                          NB_DATA                   = 64 ,
    parameter                          LOG2_NB_DATA              = 6 ,
    parameter                          LOG2_N_RX_STATUS          = 3 ,
    parameter                          NB_ADDRESS                = 48 ,
    parameter                          NB_LENTYP                 = 16 ,
    parameter                          NB_CRC32                  = 32 ,
    parameter                          N_MULTICAST_ADDRESS       = 16 , // Number of supported multicast addresses
    parameter                          LOG2_N_MULTICAST_ADDRESS  = 4     // Number of bits needed for the multicast addr vector
    )
   (
    output wire                                     o_MA_DATA_indication, //Indicates to upper layers a frame has been received
    output wire [LOG2_N_RX_STATUS-1:0]              o_rx_status, //RX status code, indicates if there is a failure in reception or OK
    output wire                                     o_rx_status_ready,
    output wire [NB_ADDRESS-1:0]                    o_destination_address, //Frame destination address
    output wire                                     o_destination_address_ready,
    output wire [NB_ADDRESS-1:0]                    o_source_address, //Frame source address
    output wire                                     o_source_address_ready,
    output wire [NB_LENTYP-1:0]                     o_lentyp, //Frame length/type field
    output wire                                     o_lentyp_ready,
    output wire [NB_DATA-1:0]                       o_mac_service_data_unit, //Frame payload   /*[MAX_LENGTH_B_BYTES*8-1:0]*/
    output wire                                     o_mac_service_data_unit_valid,
    output wire [LOG2_NB_DATA-1:0]                  o_mac_service_data_unit_tailsize_bits,
    output wire [NB_CRC32-1:0]                      o_frame_check_sequence, //Frame CRC 32
    output wire                                     o_frame_check_sequence_ready,

    output wire                                     o_stats_soffound,
    output wire [3-1:0]                             o_stats_addrmatch,
    output wire                                     o_stats_dv_missmatch,
    output wire                                     o_eof,

    input wire [NB_DATA-1:0]                        i_PLS_DATA_indication,
    input wire                                      i_PLS_DATA_VALID_indication,
    input wire [LOG2_NB_DATA-1:0]                   i_tail_size_bits,
    input wire                                      i_tail_size_valid,
    input wire                                      i_RS_error,


    input wire [NB_ADDRESS-1:0]                     i_local_mac_address , // Station MAC address
    input wire [NB_ADDRESS-1:0]                     i_broadcast_mac_address , // Broadcast MAC address (address destined for every station)
    input wire [NB_ADDRESS*N_MULTICAST_ADDRESS-1:0] i_multicast_addresses , // Vector of multicast addresses
    input wire [N_MULTICAST_ADDRESS-1:0]            i_enable_multicast_addr , //Multicast addresses enabler
    input wire                                      i_promiscuous_enable,
    input wire                                      i_fcs_forward,

    input wire                                      i_clock,
    input wire                                      i_valid,
    input wire                                      i_reset
    );

   localparam                                       LOG2_FRAME_SIZE           = 14 ;
   localparam                                       NB_DST_ADDR               = 48 ;
   localparam                                       NB_SRC_ADDR               = 48 ;
   localparam                                       NB_CRC                    = 32 ;

   localparam                                       RX_FIFO_DEPTH             = 4 ;
   localparam                                       RX_FIFO_ADDR              = 4 ;
   localparam                                       LOG2_NB_ADDRESS           = 6 ;
   localparam                                       LOG2_NB_LENTYP            = 4 ;
   localparam                                       MAX_LENGTH_B_BYTES        = 1982 ;
   localparam                                       LOG2_FRAME_SIZE_BITS      = 14 ;
   localparam                                       LOG2_NB_CRC32             = 5 ;
   localparam                                       N_RX_STATUS               = 5 ;
   localparam                                       NBYTES_MAX_FRAME_SIZE     = 2026 ;
   localparam                                       NBYTES_MAX_DATA_SIZE      = 1982 ;
   localparam                                       LOG2_NBYTES_MAX_DATA_SIZE = 11 ;

   localparam                                       max_frame_size_bits       = NBYTES_MAX_FRAME_SIZE * 8;
   localparam                                       log2_max_frame_size_bits  = clogb2(max_frame_size_bits);

   localparam                                       LEN_NB_DATA               = 64;
   localparam                                       MAX_N_FRAMES              = 1024;

   //preamble finder params
   localparam                                       NB_STAT_CNT               = 16 ;
   localparam                                       NB_PREAMBLE               = 56 ;
   localparam                                       NB_SFD                    = 8 ;
   localparam                                       NB_TIMER                  = 16 ;
   localparam                                       PREAMBLE                  = 56'h55555555555555;
   localparam                                       SFD                       = 8'hD5;

   //==========================================================================
   // INTERNAL SIGNALS.
   //==========================================================================
   //----------------------------------
   //Top level
   //----------------------------------
   // Outputs.

   // Other
   //reg                                      clock_i = 1'b0;
   integer                                  i ;

   //For preamble finder
   // Outputs
   wire                                     lock_tb_o ;
   wire                                     time_out_tb_o ;
   wire                                     bad_rdv_tb_o ;
   wire [NB_STAT_CNT-1:0]                   time_out_cnt_tb_o ;
   wire [NB_STAT_CNT-1:0]                   bad_rdv_cnt_tb_o ;
   wire [LOG2_NB_DATA-1:0]                  index_tb_o ;
   wire                                     sof_tb_o ;

   //Connection from preamble finder top top
   wire [NB_DATA-1:0]                       preafi_data_top ; //Shifted data from PF is now the data input for toplevel
   wire                                     preafi_valid_top;
   wire                                     valid_to_rxmac;

   reg                                      pls_datavalid_reg;
   //wire                                     pls_datavalid_pos;
   wire                                     pls_datavalid_neg;

   wire                                     rx_error;
   reg                                      rx_error_reg;

   always @(posedge i_clock)
     if (i_reset)
       pls_datavalid_reg <= 1'b0;
     else if (i_valid)
       pls_datavalid_reg <= i_PLS_DATA_VALID_indication;

   //assign pls_datavalid_pos = i_PLS_DATA_VALID_indication & ~pls_datavalid_reg;
   assign pls_datavalid_neg = ~i_PLS_DATA_VALID_indication & pls_datavalid_reg;

   assign rx_error = ((o_rx_status == 1) & o_rx_status_ready) | o_stats_dv_missmatch;

   always @(posedge i_clock)
     if (i_reset | pls_datavalid_neg)
       rx_error_reg <= 1'b0;
     else if (i_PLS_DATA_VALID_indication & rx_error)
       rx_error_reg <= 1'b1;

   assign valid_to_rxmac = preafi_valid_top & ~rx_error_reg;

   RX_MAC
     #(
       .NB_DATA                              (NB_DATA                               ),
       .LOG2_NB_DATA                         (LOG2_NB_DATA                          ),
       .RX_FIFO_DEPTH                        (RX_FIFO_DEPTH                         ),
       .RX_FIFO_ADDR                         (RX_FIFO_ADDR                          ),
       .NB_ADDRESS                           (NB_ADDRESS                            ),
       .LOG2_NB_ADDRESS                      (LOG2_NB_ADDRESS                       ),
       .NB_LENTYP                            (NB_LENTYP                             ),
       .LOG2_NB_LENTYP                       (LOG2_NB_LENTYP                        ),
       .N_MULTICAST_ADDRESS                  (N_MULTICAST_ADDRESS                   ),
       .LOG2_N_MULTICAST_ADDRESS             (LOG2_N_MULTICAST_ADDRESS              ),
       .MAX_LENGTH_B_BYTES                   (MAX_LENGTH_B_BYTES                    ),
       .LOG2_FRAME_SIZE_BITS                 (LOG2_FRAME_SIZE_BITS                  ),
       .NB_CRC32                             (NB_CRC32                              ),
       .LOG2_NB_CRC32                        (LOG2_NB_CRC32                         ),
       .N_RX_STATUS                          (N_RX_STATUS                           ),
       .LOG2_N_RX_STATUS                     (LOG2_N_RX_STATUS                      ),
       .NBYTES_MAX_FRAME_SIZE                (NBYTES_MAX_FRAME_SIZE                 ),
       .NBYTES_MAX_DATA_SIZE                 (NBYTES_MAX_DATA_SIZE                  ),
       .LOG2_NBYTES_MAX_DATA_SIZE            (LOG2_NBYTES_MAX_DATA_SIZE             )
       )
   u_RX_MAC
     (
      //To top level
      .o_MA_DATA_indication                  (o_MA_DATA_indication                  ),
      .o_rx_status                           (o_rx_status                           ),
      .o_rx_status_ready                     (o_rx_status_ready                     ),
      .o_destination_address                 (o_destination_address                 ),
      .o_destination_address_ready           (o_destination_address_ready           ),
      .o_source_address                      (o_source_address                      ),
      .o_source_address_ready                (o_source_address_ready                ),
      .o_lentyp                              (o_lentyp                              ),
      .o_lentyp_ready                        (o_lentyp_ready                        ),
      .o_mac_service_data_unit               (o_mac_service_data_unit               ),
      .o_mac_service_data_unit_valid         (o_mac_service_data_unit_valid         ),
      .o_mac_service_data_unit_tailsize_bits (o_mac_service_data_unit_tailsize_bits ),
      .o_frame_check_sequence                (o_frame_check_sequence                ),
      .o_frame_check_sequence_ready          (o_frame_check_sequence_ready          ),
      .o_stats_addrmatch                     (o_stats_addrmatch                     ),
      .o_stats_dv_missmatch                  (o_stats_dv_missmatch                  ),
      .o_eof                                 (o_eof                                 ),

      //From preamble finder
      .i_receive_data_valid                  (valid_to_rxmac                        ),
      .i_data                                (preafi_data_top                       ),
      //From CGMII
      .i_tail_size_bits                      (i_tail_size_bits                      ),
      .i_tail_size_valid                     (i_tail_size_valid                     ),
      .i_RS_error                            (i_RS_error                            ),

      //For configuration
      .i_local_mac_address                   (i_local_mac_address                   ),
      .i_broadcast_mac_address               (i_broadcast_mac_address               ),
      .i_multicast_addresses                 (i_multicast_addresses                 ),
      .i_enable_multicast_addr               (i_enable_multicast_addr               ),
      .i_promiscuous_enable                  (i_promiscuous_enable                  ),
      .i_fcs_forward                         (i_fcs_forward                         ),

      .i_clock                               (i_clock                               ),
      .i_valid                               (i_valid                               ),
      .i_reset                               (i_reset                               )
      ) ;

   preamble_finder
     #(
       .NB_DATA                              (NB_DATA                               ),
       .LOG2_NB_DATA                         (LOG2_NB_DATA                          ),
       .NB_PREAMBLE                          (NB_PREAMBLE                           ),
       .NB_SFD                               (NB_SFD                                ),
       .NB_TIMER                             (NB_TIMER                              ),
       .NB_STAT_CNT                          (NB_STAT_CNT                           )
       )
   u_preamble_finder
     (
      // Outputs
      .o_lock                                (lock_tb_o                             ),//Can be omitted
      .o_time_out                            (time_out_tb_o                         ),//Can be omitted
      .o_bad_rdv                             (bad_rdv_tb_o                          ),//Can be omitted
      .o_time_out_cnt                        (time_out_cnt_tb_o                     ),//Can be omitted
      .o_bad_rdv_cnt                         (bad_rdv_cnt_tb_o                      ),//Can be omitted
      .o_index                               (index_tb_o                            ), //Can be omitted
      .o_data                                (preafi_data_top                       ),
      .o_sof                                 (o_stats_soffound                      ), //Can be omitted
      .o_valid                               (preafi_valid_top                      ),
      // Inputs
      .i_data                                (i_PLS_DATA_indication                 ),
      .i_valid                               (i_valid                               ),
      .i_rdv                                 (i_PLS_DATA_VALID_indication           ),
      .i_preamble                            (PREAMBLE                              ), //preamble_tb_i
      .i_sfd                                 (SFD                                   ), //sfd_tb_i
      .i_time_out_limit                      (16'hFFFF                              ), //time_out_limit_tb_i
      .i_check_time_out                      (1'b0                                  ), //check_time_out_tb_i

      .i_read_status                         (1'b0                                  ), //read_status_tb_i
      .i_clear_on_read_mode                  (1'b0                                  ), //clear_on_read_mode_tb_i

      .i_reset                               (i_reset                               ),
      .i_clock                               (i_clock                               )
      ) ;

   function integer clogb2;
      input integer depth;
      for (clogb2=0; depth>0; clogb2=clogb2+1)
        depth = depth >> 1;
   endfunction // clogb2

endmodule
