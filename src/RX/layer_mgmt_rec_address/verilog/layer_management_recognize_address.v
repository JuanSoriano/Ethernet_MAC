module layer_management_recognize_address
#(
    // Parameters.
    parameter                                               NB_ADDRESS                  = 48 ,
    parameter                                               LOG2_NB_ADDRESS             = 6 ,
    parameter                                               N_MULTICAST_ADDRESS         = 16 , // Number of supported multicast addresses
    parameter                                               LOG2_N_MULTICAST_ADDRESS    = 4    // Number of bits needed for the multicast addr vector
)
(
    // Outputs.
    output wire                                     o_macaddress_match , // Output a 1 to signal a match in addresses

    output wire [3-1:0]                             o_stats_addrmatch ,
    // Inputs.
    input wire [NB_ADDRESS-1:0]                     i_rx_mac_address , // Received MAC address to compare
    input wire [NB_ADDRESS-1:0]                     i_local_mac_address , // Station MAC address
    input wire [NB_ADDRESS-1:0]                     i_broadcast_mac_address , // Broadcast MAC address (address destined for every station)
    input wire [NB_ADDRESS*N_MULTICAST_ADDRESS-1:0] i_multicast_addresses , // Vector of multicast addresses
    input wire [N_MULTICAST_ADDRESS-1:0]            i_enable_multicast_addr , // Enabled multicast addresses
    input wire                                      i_addr_valid ,
    input wire                                      i_promiscuous_enable,

    input wire                                      i_valid , // Throughput control.

    input wire                                      i_reset ,
    input wire                                      i_clock
) ;
    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================
    localparam                                              N_ADDR = N_MULTICAST_ADDRESS+2; //local+broadcast+N_MULTICAST_ADDRESSES

    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
    wire            [NB_ADDRESS*N_ADDR-1:0]                 addresses ; //Vector containing addresses
    wire            [N_MULTICAST_ADDRESS-1:0]               multicast_match ; //Vector containing multicast matches

    wire            [N_ADDR-1:0]                            match_bus ;
    reg                                                     match ;

    genvar                                                  ii ;
    integer                                                 i ;
    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    assign addresses = {i_local_mac_address, i_broadcast_mac_address, i_multicast_addresses};

    // Address search
    //--------------------------------------------------------------------------

    // Correlators for searching for a match in address.
    generate
        for ( ii=0; ii<N_ADDR; ii=ii+1 ) //One comparison for each multicast + local + broadcast addresses
        begin : genfor_comparators
            wire    [NB_ADDRESS-1:0]                possible_address_match ;

            assign  possible_address_match = addresses[ ii*NB_ADDRESS +: NB_ADDRESS ] ;

            assign  match_bus[ ii ] = ( possible_address_match == i_rx_mac_address ) ;

        end // genfor_comparators
    endgenerate


    assign multicast_match = match_bus[N_ADDR-2-1:0] & i_enable_multicast_addr ;

    // Match signal indicates if there is a match in the addresses
   always @( posedge i_clock) begin
      if (i_reset)
        match <= 1'b0;
      else if (i_valid && i_addr_valid)
         match <= | {match_bus[N_ADDR-1-:2],multicast_match} ;
   end

   assign  o_macaddress_match = match | |{match_bus[N_ADDR-1-:2],multicast_match} | i_promiscuous_enable;
   assign o_stats_addrmatch = {match_bus[N_ADDR-1-:2],|multicast_match} | i_promiscuous_enable;

endmodule
