module layer_management_recognize_address_tb ();

    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================
    // Parameters.
    localparam                                       NB_ADDRESS                  = 48 ;
    localparam                                       LOG2_NB_ADDRESS             = 6 ;
    localparam                                       N_MULTICAST_ADDRESS         = 3 ; // Number of supported multicast addresses
    localparam                                       LOG2_N_MULTICAST_ADDRESS    = 1 ;  // Number of bits needed for the multicast addr vector


    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
    // Outputs.
    wire                                            macaddress_match_tb_o ;    // Output a 1 to signal a match in addresses

    // Inputs.
   reg [NB_ADDRESS-1:0]                             rx_mac_address_tb_i ; // Received MAC address to compare
    wire    [NB_ADDRESS-1:0]                        local_mac_address_tb_i ; // Station MAC address
    wire    [NB_ADDRESS-1:0]                        broadcast_mac_address_tb_i ; // Broadcast MAC address (address destined for every station)
    reg     [NB_ADDRESS*N_MULTICAST_ADDRESS-1:0]    multicast_addresses_tb_i ; // Vector of multicast addresses
    wire    [NB_ADDRESS-1:0]                        multicast_addresses_arr [N_MULTICAST_ADDRESS-1:0] ;
    wire    [N_MULTICAST_ADDRESS-1:0]               enable_multicast_addr_tb_i ; // Enabled multicast addresses

    wire                                            valid_tb_i ;   // Throughput control.

    wire                                            reset ;
    reg                                             clock = 1'b0 ;
    integer                                         timer = 0 ;
    integer                                         i = 0 ;

    reg                                             test_passed = 1'b0 ;

    //==========================================================================
    // CONNECTION TO DUT
    //==========================================================================

    layer_management_recognize_address
    #(
        .NB_ADDRESS                      (NB_ADDRESS               ),
        .LOG2_NB_ADDRESS                 (LOG2_NB_ADDRESS          ),
        .N_MULTICAST_ADDRESS             (N_MULTICAST_ADDRESS      ),
        .LOG2_N_MULTICAST_ADDRESS        (LOG2_N_MULTICAST_ADDRESS )
    )
    u_layer_management_recognize_address
    (
        // Inputs.
        .o_macaddress_match              (macaddress_match_tb_o),
        // Outputs.
        .i_rx_mac_address                (rx_mac_address_tb_i),
        .i_addr_valid                    (1'b1                ),
        .i_local_mac_address             (local_mac_address_tb_i),
        .i_broadcast_mac_address         (broadcast_mac_address_tb_i ),
        .i_multicast_addresses           (multicast_addresses_tb_i),
        .i_enable_multicast_addr         (enable_multicast_addr_tb_i),
        .i_valid                         (valid_tb_i),
        .i_reset                         (reset),
        .i_clock                         (clock)
    ) ;

    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    always
    begin
        #(50) clock = ~clock ;
    end

    always @ ( posedge clock )
    begin
        timer   <= timer + 1;
    end

    assign reset = (timer == 2) ; // Reset at time 2
    assign valid_tb_i = (timer >= 3 ) ;
    assign local_mac_address_tb_i = 48'HAAAAAAAAAAA1 ;
    assign broadcast_mac_address_tb_i = 48'HAAAAAAAAAAAB ;
    assign multicast_addresses_arr[0] = 48'HAAAAAAAAAAAC ;
    assign multicast_addresses_arr[1] = 48'HAAAAAAAAAAAF ;
    assign multicast_addresses_arr[2] = 48'HAAAAAAAAAAAE ;

    always @ ( * ) begin
        for ( i=0; i<N_MULTICAST_ADDRESS; i=i+1) begin
            multicast_addresses_tb_i [i*NB_ADDRESS+: NB_ADDRESS] = multicast_addresses_arr[i];
        end
    end

    assign enable_multicast_addr_tb_i = 3'b111 ;

   always @(*)
     case (timer)
       4 : rx_mac_address_tb_i = 48'HAAAAAAAAAAA1 ;
       5 : rx_mac_address_tb_i = 48'HAAAAAAAAAAAB ;
       6 : rx_mac_address_tb_i = 48'HAAAAAAAAAAAC ;
       7 : rx_mac_address_tb_i = 48'HAAAAAAAAAAAF ;
       8 : rx_mac_address_tb_i = 48'HAAAAAAAAAAAE ;
       9 : rx_mac_address_tb_i = 48'HAAAAAAAAAAAE ;
       10: rx_mac_address_tb_i = 48'HCAAAAAAAAAA1 ;
       11: rx_mac_address_tb_i = 48'HBAAAAAAAAAAE ;
       12: rx_mac_address_tb_i = 48'HEAAAAAAAAAAE ;
       13: rx_mac_address_tb_i = 48'HFAAAAAAAAAAF ;
       default: rx_mac_address_tb_i = 'b0;
     endcase

   always @ (posedge clock)
    begin
        if (timer >= 1000)
            $stop ();
    end



endmodule
