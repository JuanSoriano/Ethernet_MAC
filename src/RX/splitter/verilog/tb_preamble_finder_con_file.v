 module preamble_finder_test ();

   localparam                                       DATA_FILE                 = "/home/jsoriano/Ethernet_MAC/RX/simulador_python/data_sinsof" ;
   localparam                                       CONTROL_FILE              = "/home/jsoriano/Ethernet_MAC/RX/simulador_python/control";
   localparam                                       DATAVALID_FILE            = "/home/jsoriano/Ethernet_MAC/RX/simulador_python/datavalid";
   localparam                                       DATA_INDICATION_FILE      = "/home/jsoriano/Ethernet_MAC/RX/simulador_python/data_indication";
   localparam                                       LOG2_FRAME_SIZE           = 14 ;
   localparam                                       NB_DST_ADDR               = 48 ;
   localparam                                       NB_SRC_ADDR               = 48 ;
   localparam                                       NB_CRC                    = 32 ;

   localparam                                       NB_DATA                   = 64 ;
   localparam                                       LOG2_NB_DATA              = 6 ;
   localparam                                       RX_FIFO_DEPTH             = 4 ;
   localparam                                       RX_FIFO_ADDR              = 4 ;
   localparam                                       NB_ADDRESS                = 48 ;
   localparam                                       LOG2_NB_ADDRESS           = 6 ;
   localparam                                       NB_LENTYP                 = 16 ;
   localparam                                       LOG2_NB_LENTYP            = 4 ;
   localparam                                       N_MULTICAST_ADDRESS       = 3 ;
   localparam                                       LOG2_N_MULTICAST_ADDRESS  = 4 ;
   localparam                                       MAX_LENGTH_B_BYTES        = 1982 ;
   localparam                                       LOG2_FRAME_SIZE_BITS      = 14 ;
   localparam                                       NB_CRC32                  = 32 ;
   localparam                                       LOG2_NB_CRC32             = 5 ;
   localparam                                       N_RX_STATUS               = 5 ;
   localparam                                       LOG2_N_RX_STATUS          = 3 ;
   localparam                                       NBYTES_MAX_FRAME_SIZE     = 2026 ;
   localparam                                       NBYTES_MAX_DATA_SIZE      = 1982 ;
   localparam                                       LOG2_NBYTES_MAX_DATA_SIZE = 11 ;

   localparam                                       max_frame_size_bits       = NBYTES_MAX_FRAME_SIZE * 8;
   localparam                                       log2_max_frame_size_bits  = clogb2(max_frame_size_bits);

   localparam                                       LEN_NB_DATA               = 64;
   //==========================================================================
   // INTERNAL SIGNALS.
   //==========================================================================
   //----------------------------------
   //Top level
   //----------------------------------
   // Outputs.
   //To upper layers

   wire                                             MA_DATA_indication_o; //Indicates to upper layers a frame has been received
   wire [LOG2_N_RX_STATUS-1:0]                      rx_status_o; //RX status code, indicates if there is a failure in reception or OK
   wire [NB_ADDRESS-1:0]                            destination_address_o; //Frame destination address
   wire [NB_ADDRESS-1:0]                            source_address_o; //Frame source address
   wire [NB_DATA-1:0]                               mac_service_data_unit_o; //Frame payload            [MAX_LENGTH_B_BYTES*8-1:0]?
   wire [NB_CRC32-1:0]                              frame_check_sequence_o; //Frame CRC 32

   //From lower layers
   wire [LOG2_FRAME_SIZE_BITS-1:0]                  framesize_bits_i;
   wire [LOG2_NBYTES_MAX_DATA_SIZE-1:0]             data_size_bytes_i;
   wire [LOG2_NB_DATA-1:0]                          tail_size_bits_i;
   wire                                             data_valid_i;
   wire [NB_DATA-1:0]                               data_i;

   //For adresses
   wire [NB_ADDRESS-1:0]                            tb_local_mac_address_i ; // Station MAC address
   wire [NB_ADDRESS-1:0]                            tb_broadcast_mac_address_i ; // Broadcast MAC address (address destined for every station)
   reg [NB_ADDRESS*N_MULTICAST_ADDRESS-1:0]         tb_multicast_addresses_i ; // Vector of multicast addresses
   wire [NB_ADDRESS-1:0]                            tb_multicast_addresses_arr [N_MULTICAST_ADDRESS-1:0] ;
   wire [N_MULTICAST_ADDRESS-1:0]                   tb_enable_multicast_addr_i ; //Multicast addresses enabler

   // Other
   reg                                              clock_i = 1'b0;
   wire                                             tb_valid_i;
   wire                                             tb_reset_i ;
   integer                                          timer = 0 ;
   integer                                          i ;

   // File related variables
   integer                                          fd_data_input = 0;
   integer                                          fd_control_input = 0;
   integer                                          fd_datavalid_input = 0;
   integer                                          fd_dataindication_input = 0;
   integer                                          scan_file ;

   //Registers to parse data
   reg [NB_DATA-1:0]                                data_reg;
   reg [LOG2_FRAME_SIZE_BITS-1:0]                   framesize_b_reg;
   reg [LOG2_NBYTES_MAX_DATA_SIZE-1:0]              data_size_reg;
   reg [LOG2_NB_DATA-1:0]                           tail_size_reg;
   reg                                              data_valid_reg;

   reg                                              data_valid_d;
   reg                                              data_valid_dd;
   wire                                             data_valid_pos;

   //Registers to parse simulator output
   reg [NB_ADDRESS-1:0]                             dst_addr_reg;
   reg [NB_ADDRESS-1:0]                             src_addr_reg;
   reg [NB_LENTYP-1:0]                              lentyp_reg;
   reg [max_frame_size_bits-1:0]                    payload_reg;
   reg [NB_CRC32-1:0]                               captured_fcs_reg;
   reg [NB_CRC32-1:0]                               calculated_crc_reg;
   reg [LOG2_N_RX_STATUS-1:0]                       rx_status_reg;

   initial
     begin
        data_valid_reg = 0 ;
        fd_data_input = $fopen(DATA_FILE,"r");
        if (fd_data_input==0)
          begin
             $display("Error de lectura de archivo DATA_FILE");
             $stop;
          end
        fd_control_input = $fopen(CONTROL_FILE,"r");
        if (fd_control_input==0)
          begin
             $display("Error de lectura de archivo CONTROL_FILE");
             $stop;
          end
        fd_datavalid_input = $fopen(DATAVALID_FILE,"r");
        if (fd_datavalid_input==0)
          begin
             $display("Error de lectura de archivo DATAVALID_FILE");
             $stop;
          end
        fd_dataindication_input = $fopen(DATA_INDICATION_FILE, "r");
        if (fd_dataindication_input==0)
          begin
             $display("Error de lectura de archivo DATA_INDICATION");
             $stop;
          end
     end // initial begin

   //File read
   always @ (posedge clock_i)
     begin
        if (tb_valid_i) begin
           //Capture data
           scan_file = $fscanf(fd_data_input, "%b", data_reg) ;
           if ($feof(fd_data_input))begin
             $display ("EOF data");
             $fclose(fd_data_input);
           end

           //Capture datavalid
           scan_file = $fscanf(fd_datavalid_input, "%1b", data_valid_reg);
           if($feof(fd_datavalid_input)) begin
             $display("EOF datavalid");
             $fclose(fd_datavalid_input);
           end
           if (data_valid_pos)
             begin
                //Capture control fields
                scan_file = $fscanf(fd_control_input, "%b", framesize_b_reg);
                if($feof(fd_control_input))begin
                  $display("EOF control");
                  $fclose(fd_control_input);
                end
                // scan_file = $fscanf(fd_control_input, "%b", data_size_reg);
                // if($feof(fd_control_input))
                //   $display("EOF control");
                // scan_file = $fscanf(fd_control_input, "%b", tail_size_reg);
                // if($feof(fd_control_input))
                //   $display("EOF control");

                //Capture data_indication fields
                scan_file = $fscanf(fd_dataindication_input, "%b", dst_addr_reg);
                if($feof(fd_dataindication_input))begin
                  $display("EOF_dataindication");
                  $fclose(fd_dataindication_input);
                end
                // scan_file = $fscanf(fd_dataindication_input, "%b", src_addr_reg);
                // if($feof(fd_dataindication_input))
                //   $display("EOF_dataindication");
                // scan_file = $fscanf(fd_dataindication_input, "%b", lentyp_reg);
                // if($feof(fd_dataindication_input))
                //   $display("EOF_dataindication");
                // scan_file = $fscanf(fd_dataindication_input, "%b", payload_reg);
                // if($feof(fd_dataindication_input))
                //   $display("EOF_dataindication");
                // scan_file = $fscanf(fd_dataindication_input, "%b", captured_fcs_reg);
                // if($feof(fd_dataindication_input))
                //   $display("EOF_dataindication");
                // scan_file = $fscanf(fd_dataindication_input, "%b", calculated_crc_reg);
                // if($feof(fd_dataindication_input))
                //   $display("EOF_dataindication");
                // scan_file = $fscanf(fd_dataindication_input, "%b", rx_status_reg);
                // if($feof(fd_dataindication_input))
                //   $display("EOF_dataindication");
             end
        end
     end

   assign data_i = data_reg;
   assign framesize_bits_i = framesize_b_reg;
   assign data_size_bytes_i = data_size_reg;
   assign tail_size_bits_i = tail_size_reg;
   assign data_valid_i = data_valid_reg;

   //Posedge datavalid
   always @ (posedge clock_i)
     begin
        if (tb_reset_i) begin
           data_valid_d <= 'b0;
           data_valid_dd <= 'b0;
        end else begin
           data_valid_d <= data_valid_reg;
           data_valid_dd <= data_valid_d;
        end
     end
        assign data_valid_pos = ~data_valid_dd & data_valid_i ;



   always #50 clock_i = ~clock_i ;

   always @ ( posedge clock_i )
     begin
        timer   <= timer + 1;
     end

   assign tb_reset_i = (timer == 2) ; // Reset at time 2
   assign tb_valid_i = timer>4 ;

   //Adresses
   assign tb_local_mac_address_i        = 48'H000000000009 ;
   assign tb_broadcast_mac_address_i    = 48'H00000000A2B2 ;
   assign tb_multicast_addresses_arr[0] = 48'H000000000000 ;
   assign tb_multicast_addresses_arr[1] = 48'H000000000001 ;
   assign tb_multicast_addresses_arr[2] = 48'H000000000002 ;


   always @ ( * ) begin
      for ( i=0; i<N_MULTICAST_ADDRESS; i=i+1) begin
         tb_multicast_addresses_i [i*NB_ADDRESS+: NB_ADDRESS] = tb_multicast_addresses_arr[i];
      end
   end

   assign tb_enable_multicast_addr_i = 3'b111 ;

   function integer clogb2;
      input integer depth;
      for (clogb2=0; depth>0; clogb2=clogb2+1)
        depth = depth >> 1;
   endfunction // clogb2

endmodule
