module splitter_tb ();
    
/* preamble:7*8, sofd:8, dst-addr:6*8,  src-addr:6*8, length/dt:2*8, data:46*8--1500*8/1504*8--1982*8, crc:4*8, ext:??? */
//7+1+6+6+2=22 --> +46

/* dst-addr:6*8,  src-addr:6*8, length/dt:2*8, data:46*8--1500*8/1504*8--1982*8, crc:4*8, ext:??? */


    

    
    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================
    // Parameters.
    localparam                                  NB_DATA         = 64 ;
    localparam                                  LOG2_NB_DATA    = 6  ;
    localparam                                  LOG2_FRAME_SIZE = 14  ;
    localparam                                  NB_DST_ADDR     = 48 ;
    localparam                                  NB_SRC_ADDR     = 48 ;
    localparam                                  NB_LENTYP       = 16 ;
    localparam                                  NB_CRC          = 32 ;

    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
    // Outputs.
    wire	[NB_DATA-1:0]				data_tb_o ;
    wire 								data_valid_tb_o ;
    // FSM Control Outputs
    wire 								enable_crc_tb_o ;		// Only enable CRC checking if required by client
    wire 								crc_valid_tb_o ;		// Indicate to CRC block when there's valid data to be processed
    wire								get_crc_tb_o ;			

    // Inputs.
    wire    [NB_DATA-1:0]               data_tb_i ;
    wire                                valid_tb_i ;
    wire 								frame_ready_tb_i ;	// Indicates that a frame has arrived to the RX FIFO
    wire 								dest_address_ok_tb_i ; // Output from LayerMgmtRecAddr

    wire                                reset ;
    reg                                 clock = 1'b0 ;
    integer                             timer = 0 ;

    reg                                 test_passed = 1'b0 ;

    wire                                trigger_frame_generation ;

    //==========================================================================
    // CONNECTION TO DUT
    //==========================================================================

    splitter
    #(
        .NB_DATA                ( NB_DATA ),
        .LOG2_NB_DATA           ( LOG2_NB_DATA )
    )
    u_splitter
    (
        // Outputs.
        .o_data                 ( data_tb_o ),
        .o_data_valid           ( data_valid_tb_o ),
        // FSM Control Outputs
        .o_crc_valid            ( crc_valid_tb_o ),
        // Inputs.
        .i_data                 ( data_tb_i ),
        .i_valid                ( valid_tb_i ),
        .i_frame_ready          ( frame_ready_tb_i ),
        .i_dest_address_ok      ( dest_address_ok_tb_i ), 
        .i_reset                ( reset ),
        .i_clock                ( clock )
    ) ;

    frame_generator_b
    #(
        .NB_DATA                ( NB_DATA        ),
        .LOG2_NB_DATA           ( LOG2_NB_DATA   ),
        .LOG2_FRAME_SIZE        ( LOG2_FRAME_SIZE),
        .NB_DST_ADDR            ( NB_DST_ADDR    ),
        .NB_SRC_ADDR            ( NB_SRC_ADDR    ),
        .NB_LENTYP              ( NB_LENTYP      ),
        .NB_CRC                 ( NB_CRC         ) 
    )
    u_frame_generator_b
    (
        .o_data                  ( data_tb_i ),   
        .o_data_valid            ( frame_ready_tb_i ),           
                            
        .i_valid                 ( valid_tb_i ),       
        .i_frame_ready           ( trigger_frame_generation ),           
        .i_reset                 ( reset ),       
        .i_clock                 ( clock )    
    ) ;

    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    always
    begin
        #(50) clock = ~clock ;
    end

    always @ ( posedge clock )
    begin
        timer   <= timer + 1;
    end

    assign reset = (timer == 2) ; // Reset at time 2
    assign valid_tb_i = 1'b1 ;

    assign trigger_frame_generation = (timer%(2500/4) == 0) ;

    assign dest_address_ok_tb_i = 1'b1 ; // Promiscuo

endmodule