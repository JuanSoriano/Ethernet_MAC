/* preamble:7*8, sofd:8, dst-addr:6*8,  src-addr:6*8, length/dt:2*8, data:46*8--1500*8/1504*8--1982*8, crc:4*8, ext:??? */
//7+1+6+6+2=22 --> +46

/* dst-addr:6*8,  src-addr:6*8, length/dt:2*8, data:46*8--1500*8/1504*8--1982*8, crc:4*8, ext:??? */


/*
 P=64
 DDDDDDSS
 SSSSLLAA
 AAAAAAAA
 ....
 AAAAAAAC
 CCCEEEEE
 EEEEEEEE
 ....
 */


/*
 P=32
 DDDD
 DDSS
 SSSS
 LLAA
 AAAA
 AAAA
 ....
 AAAA
 AAAC
 CCCE
 EEEE
 EEEE
 EEEE
 ....
 */


/*
 P=16
 DD
 DD
 DD
 SS
 SS
 SS
 LL
 AA
 AA
 AA
 AA
 AA
 ....
 AA
 AA
 AA
 AC
 CC
 CE
 EE
 EE
 ....
 */


// [NOTE] The block is designed to work with a parallesim multiple of 8bits, and no greater than 512bits.
// [NOTE] We assume that extension bits are not required for the above layer and are thus dropped.

module splitter
  #(
    // Parameters.
    parameter                                   NB_DATA              = 64 ,
    parameter                                   LOG2_NB_DATA         = 6 ,
    parameter                                   NB_DST_ADDR          = 48 ,    // HINT: This is the only legal value.
    parameter                                   NB_SRC_ADDR          = 48 ,    // HINT: This is the only legal value.
    parameter                                   NB_LENTYP            = 16 ,    // HINT: This is the only legal value.
    parameter                                   NB_CRC               = 32 ,     // HINT: This is the only legal value.
    parameter                                   LOG2_FRAME_SIZE_BITS = 14
    )
   (
    // Outputs.
    output reg [NB_DATA-1:0]      o_data ,
    output reg                    o_data_valid ,
    output wire [NB_DST_ADDR-1:0] o_dst_addr ,
    output reg                    o_dst_addr_ready,
    output wire                   o_enable_lmra ,
    output wire [NB_SRC_ADDR-1:0] o_src_addr ,
    output reg                    o_src_addr_ready,
    output wire [NB_LENTYP-1:0]   o_len_typ ,
    output reg                    o_len_typ_ready,
    output wire [NB_CRC-1:0]      o_crc ,
    output wire                   o_crc_ready,

    // output  wire                                o_enable_crc ,          // Only enable CRC checking if required by client. NOTE: THis must be connected directly to the CRC calculator.
    output wire                   o_crc_valid , // Indicate to CRC block when there's valid data to be processed
    //   output  wire                                o_received_crc_ready ,  // Indicates the new CRC received from the frame is ready

    // Inputs.
    input wire [NB_DATA-1:0]      i_data ,
    input wire                    i_valid ,
    input wire                    i_frame_ready , // It is a pulse that indicates that a frame has arrived to the RX FIFO
    input wire                    i_dest_address_ok , // Output from LayerMgmtRecAddr
    input wire                    i_rx_error ,

    input wire                    i_reset ,
    input wire                    i_clock
    ) ;

   //==========================================================================
   // LOCAL PARAMETERS.
   //==========================================================================
   localparam                                  NB_TIMER                = LOG2_FRAME_SIZE_BITS - LOG2_NB_DATA +1 ;
   localparam                                  MIN_LENTYP_BYTES        = 16'd46;

   // FSM States
   localparam                                  NB_STATE                = 3 ;
   localparam      [NB_STATE-1:0]              ST_0_INIT               = 0 ;
   localparam      [NB_STATE-1:0]              ST_1_PROCESSING         = 1 ;


   localparam                                  O_LEN_TYP_MAX           = 1982*8 ;
   localparam                                  O_LEN_TYP_MIN           = 46*8 ;

   localparam                                  SIZEB_HEADER            = NB_DST_ADDR + NB_SRC_ADDR + NB_LENTYP /* + O_LEN_TYP_MIN */ + NB_CRC ;

   localparam                                  OFFSET_DATA             = (NB_DST_ADDR + NB_DST_ADDR + NB_LENTYP) % NB_DATA ;
   localparam                                  NB_DATA_EXT             = 2*NB_DATA ;

   //==========================================================================
   // INTERNAL SIGNALS.
   //==========================================================================
   reg [NB_STATE-1:0]             state ;
   reg                            fsmo_reset_timer ;
   //reg                            fsmo_frame_end   ;
   reg [NB_STATE-1:0]             state_next ;

   // Pos edge registers
   reg                            pos_edge_dst ;
   reg                            pos_edge_src ;
   reg                            pos_edge_lentyp ;
   // reg                            pos_edge_stop_o_data ;
   reg                            pos_edge_get_crc ;

   // Pos edge wires
   wire                           get_dst_addr ;
   wire                           get_src_addr ;
   wire                           get_len_typ ;
   //wire                           stop_o_data_pos_e ;

   wire [LOG2_NB_DATA-1:0]        crc_index ;

   wire                           timer_done ;
   reg [NB_TIMER-1:0]             timer ;

   reg [NB_DATA-1:0]              data_i_d ;
   wire                           data_valid_o ;
   wire [NB_DATA_EXT-1:0]         data_i_ext ;

   wire [LOG2_FRAME_SIZE_BITS-1:0] sizeb_frame ;
   wire                            get_dst_addr_level ;
   wire                            get_src_addr_level ;
   wire                            get_len_typ_level ;
   wire                            enable_o_data ;
   wire                            stop_o_data ;
   wire                            get_crc ;
   wire                            get_crc_level ;
   reg                             get_src_addr_d;
   reg [NB_LENTYP-1:0]             min_lentyp;

   //==========================================================================
   // ALGORITHM.
   //==========================================================================

   always @(posedge i_clock)
     begin
        if (i_reset)
          min_lentyp <= MIN_LENTYP_BYTES;
        else if (i_valid)
          min_lentyp <= (o_len_typ >= MIN_LENTYP_BYTES) ? o_len_typ : MIN_LENTYP_BYTES; //Used for padded frames
     end

   always @(posedge i_clock)
     begin
        if (i_reset) begin
          o_dst_addr_ready <= 1'b0;
        o_src_addr_ready <= 1'b0;
        o_len_typ_ready <= 1'b0;
           get_src_addr_d <= 1'b0;
        end else if (i_valid) begin
           o_dst_addr_ready <= get_dst_addr;
           o_src_addr_ready <= get_src_addr_d;
           o_len_typ_ready <= get_len_typ;
           get_src_addr_d <= get_src_addr;
        end
     end // always @ (posedge i_clock)

   // State update.
   always @( posedge i_clock )
     begin
        if ( i_reset )
          state <= ST_0_INIT ;
        else if ( i_valid )
          state <= state_next ;
     end


   always @( * )
     begin
        fsmo_reset_timer    = ~i_frame_ready ;
        //fsmo_frame_end      = 1'b0 ;
        state_next          = ST_0_INIT ;
        case ( state )
          ST_0_INIT :
            begin
               if ( i_frame_ready )
                 state_next  = ST_1_PROCESSING ;
               else
                 state_next  = ST_0_INIT ;
               fsmo_reset_timer    = ~i_frame_ready ;
               //fsmo_frame_end      = 1'b0 ;
            end
          ST_1_PROCESSING :
            begin
               if ( timer_done | i_rx_error )
                 state_next  = ST_0_INIT ;
               else
                 state_next  = ST_1_PROCESSING ;
               fsmo_reset_timer    = timer_done ;//| !i_dest_address_ok ;
               //fsmo_frame_end      = timer_done ;//| !i_dest_address_ok ;
            end
        endcase
     end


   assign  sizeb_frame = ( get_len_typ_level )? SIZEB_HEADER + min_lentyp*8 : SIZEB_HEADER + O_LEN_TYP_MIN;

   always @( posedge i_clock )
     begin
        if ( i_reset || i_valid && fsmo_reset_timer )
          timer <= {NB_TIMER{1'b0}} ;
        else if ( i_valid && !timer_done )
          timer <= timer + 1'b1 ;
     end
   assign  timer_done = ((timer*NB_DATA) >= sizeb_frame) | i_rx_error;


   assign  get_dst_addr_level = i_frame_ready ;
   assign  get_src_addr_level = ( ((timer+1) * NB_DATA ) >= NB_DST_ADDR) & i_frame_ready ;
   assign  get_len_typ_level = (( (timer+1) * NB_DATA ) >= ( NB_DST_ADDR + NB_SRC_ADDR )) & i_frame_ready;
   assign  enable_o_data = (( timer * NB_DATA ) >= ( NB_DST_ADDR + NB_SRC_ADDR + NB_LENTYP )) & i_frame_ready;
   assign  stop_o_data = (( timer * NB_DATA ) >= ( NB_DST_ADDR + NB_SRC_ADDR + NB_LENTYP + min_lentyp*8  )) & i_frame_ready;
   assign get_crc_level = (( (timer + 1) * NB_DATA ) > ( NB_DST_ADDR + NB_SRC_ADDR + NB_LENTYP + min_lentyp*8  )) & i_frame_ready ;

   assign   data_valid_o = enable_o_data & ~stop_o_data ; //& i_dest_address_ok ;

   always @( posedge i_clock )
     if ( i_valid )
       o_data_valid <= data_valid_o ;

   always @( posedge i_clock )
     begin
        if ( i_valid )
          data_i_d <= i_data ;
        if ( i_valid && data_valid_o )
          o_data <= data_i_ext[ NB_DATA_EXT - OFFSET_DATA - 1 -: NB_DATA  ] ;
     end

   assign  data_i_ext = { data_i_d, i_data } ;

   assign  o_crc_valid = get_dst_addr_level & ~stop_o_data ;

   data_locker_w_trigger
     #(
       .NB_DATA    ( NB_DATA               ),
       .NB_BUFFER  ( NB_DST_ADDR           ),
       .OFFSET     ( 0                     ),
       .RESET_VAL  ( 0                     )
       )
   u_data_locker_w_trigger__dst
     (
      .o_data     ( o_dst_addr            ),
      .o_ready    ( /*unused*/            ),
      .i_data     ( i_data                ),
      .i_valid    ( i_valid               ),
      .i_trigger  ( get_dst_addr          ),
      .i_reset    ( i_reset               ),
      .i_clock    ( i_clock               )
      ) ;

   data_locker_w_trigger
     #(
       .NB_DATA    ( NB_DATA               ),
       .NB_BUFFER  ( NB_SRC_ADDR           ),
       .OFFSET     ( NB_DST_ADDR % NB_DATA ),
       .RESET_VAL  ( 0                     )
       )
   u_data_locker_w_trigger__src
     (
      .o_data     ( o_src_addr            ),
      .o_ready    ( o_enable_lmra         ),
      .i_data     ( i_data                ),
      .i_valid    ( i_valid               ),
      .i_trigger  ( get_src_addr          ),
      .i_reset    ( i_reset               ),
      .i_clock    ( i_clock               )
      ) ;


   data_locker_w_trigger
     #(
       .NB_DATA    ( NB_DATA               ),
       .NB_BUFFER  ( NB_LENTYP             ),
       .OFFSET     ( (NB_DST_ADDR + NB_SRC_ADDR) % NB_DATA ),
       .RESET_VAL  ( O_LEN_TYP_MIN         )
       )
   u_data_locker_w_trigger__typ
     (
      .o_data     ( o_len_typ             ),
      .o_ready    ( /*unused*/            ),
      .i_data     ( i_data                ),
      .i_valid    ( i_valid               ),
      .i_trigger  ( get_len_typ           ),
      .i_reset    ( i_reset               ),
      .i_clock    ( i_clock               )
      ) ;


   data_locker_w_trigger_b
     #(
       .NB_DATA      ( NB_DATA               ),
       .LOG2_NB_DATA ( LOG2_NB_DATA          ),
       .NB_BUFFER    ( NB_CRC                ),
       .OFFSET       ( NB_DATA               ),
       .RESET_VAL    ( 0                     )
       )
   u_data_locker_w_trigger__crc
     (
      // .o_data     ( last_data_and_crc     ),
      .o_data     ( o_crc                 ),
      .o_ready    ( o_crc_ready           ),
      .i_data     ( i_data                ),
      .i_offset   ( crc_index             ),
      .i_valid    ( i_valid               ),
      .i_trigger  ( get_crc               ),
      .i_reset    ( i_reset               ),
      .i_clock    ( i_clock               )
      ) ;

   assign  crc_index = ( SIZEB_HEADER - NB_CRC + min_lentyp  *8 ) % NB_DATA ;

   // Posedge generation
   // DST ADDR
   always @ (posedge i_clock)
     begin
        pos_edge_dst <= get_dst_addr_level ;
     end

   assign get_dst_addr = get_dst_addr_level & ~ pos_edge_dst ;

   // SRC ADDR
   always @ (posedge i_clock)
     begin
        pos_edge_src <= get_dst_addr_level ;
     end

   assign get_src_addr = get_src_addr_level & ~ pos_edge_src ;

   // LENTYP ADDR
   always @ (posedge i_clock)
     begin
        pos_edge_lentyp <= get_len_typ_level ;
     end

   assign get_len_typ = get_len_typ_level & ~ pos_edge_lentyp ;

   // STOP O_DATA

   // always @ (posedge i_clock)
   //   begin
   //      pos_edge_stop_o_data <= stop_o_data ;
   //   end

   //assign stop_o_data_pos_e = stop_o_data & ~ pos_edge_stop_o_data ;

   // GET CRC
   always @ (posedge i_clock)
     begin
        pos_edge_get_crc <= get_crc_level ;
     end

   assign get_crc = get_crc_level & ~ pos_edge_get_crc ;

endmodule
