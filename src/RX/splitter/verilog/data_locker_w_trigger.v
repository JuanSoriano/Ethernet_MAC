module data_locker_w_trigger
#(
    // Parameters.
    parameter                                   NB_DATA     = 64 ,
    parameter                                   NB_BUFFER   = 16 ,
    parameter                                   NB_TIMER    = 16 ,
    parameter                                   OFFSET      = 48 ,
    parameter                                   RESET_VAL   = 0
)
(
    // Outputs.
    output  wire    [NB_BUFFER-1:0]             o_data ,
    output  wire                                o_ready ,

    // Inputs.
    input   wire    [NB_DATA-1:0]               i_data ,
    input   wire                                i_valid ,
    input 	wire 								i_trigger ,
    input   wire                                i_reset ,
    input   wire                                i_clock
) ;


    /* // BEGIN: Quick Instance.
    data_locker_w_trigger
    #(
        .NB_DATA    (   ),
        .NB_BUFFER  (   ),
        .OFFSET     (   ),
        .RESET_VAL  (   )
    )
    u_data_locker_w_trigger
    (
        .o_data     (   ),
        .o_ready    (   ),
        .i_data     (   ),
        .i_valid    (   ),
        .i_trigger  (   ),
        .i_reset    (   ),
        .i_clock    (   )
    ) ;
    */ // END: of Quick Instance.


    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================
    // FSM States
    localparam                                  N_TICKS         = ((OFFSET+NB_BUFFER) % NB_DATA==0)? ((OFFSET+NB_BUFFER) / NB_DATA) : ((OFFSET+NB_BUFFER) / NB_DATA + 1) ;



    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
    reg             [N_TICKS*NB_DATA-1:0]       buffer_ext ;
    wire                                         enable_capture;
    reg                                         enable_capture_r ;
    reg             [NB_TIMER-1:0]              timer ;
    wire                                        capture_done ;
    reg                                         capture_done_d ;

    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    // Control FSM.
    //--------------------------------------------------------------------------

    always @( posedge i_clock )
        if ( i_reset /*|| !i_trigger*/ )
        begin
            buffer_ext
                <= {N_TICKS*NB_DATA{1'b0}} ;
            buffer_ext[ N_TICKS*NB_DATA-1-OFFSET -: NB_BUFFER ]
                <= RESET_VAL ;
        end
        else if ( i_valid && enable_capture )
            buffer_ext
                <= { buffer_ext, i_data } ;


    always @( posedge i_clock )
        if ( i_reset || i_valid && capture_done )
            enable_capture_r
                <= 1'b0 ;
        else if ( i_valid && i_trigger )
            enable_capture_r
                <= 1'b1 ;
    assign  enable_capture
                = enable_capture_r | i_trigger ;


    always @( posedge i_clock )
    begin
        if ( i_reset || i_valid && capture_done )
            timer
                <= 0 ;
        else if ( i_valid && enable_capture )
            timer
                <= timer + 1'b1 ;
    end
    assign  capture_done
                = ( timer == N_TICKS-1 ) ;


    assign  o_data
                = buffer_ext[ N_TICKS*NB_DATA-1-OFFSET -: NB_BUFFER ] ;

    always @( posedge i_clock )
    begin
        if ( i_reset )
            capture_done_d
                <= 1'b0 ;
        else if ( i_valid )
            capture_done_d
                <= capture_done ;
    end
    assign  o_ready
                = capture_done & ~capture_done_d ;

endmodule
