module data_locker_w_trigger_b
  #(
    // Parameters.
    parameter                                   NB_DATA      = 64 ,
    parameter                                   LOG2_NB_DATA = 7 ,
    parameter                                   NB_BUFFER    = 16 ,
    parameter                                   NB_TIMER     = 16 ,
    parameter                                   OFFSET       = 48 ,
    parameter                                   RESET_VAL    = 0
    )
   (
    // Outputs.
    output wire [NB_BUFFER-1:0]   o_data ,
    output reg                    o_ready ,

    // Inputs.
    input wire [NB_DATA-1:0]      i_data ,
    input wire [LOG2_NB_DATA-1:0] i_offset ,
    input wire                    i_valid ,
    input wire                    i_trigger ,
    input wire                    i_reset ,
    input wire                    i_clock
    ) ;


   /* // BEGIN: Quick Instance.
    data_locker_w_trigger
    #(
    .NB_DATA    (   ),
    .NB_BUFFER  (   ),
    .OFFSET     (   ),
    .RESET_VAL  (   )
    )
    u_data_locker_w_trigger
    (
    .o_data     (   ),
    .o_ready    (   ),
    .i_data     (   ),
    .i_valid    (   ),
    .i_trigger  (   ),
    .i_reset    (   ),
    .i_clock    (   )
    ) ;
    */ // END: of Quick Instance.


   //==========================================================================
   // LOCAL PARAMETERS.
   //==========================================================================
   // FSM States
   localparam                                  N_TICKS         = ((OFFSET+NB_BUFFER) % NB_DATA==0)? ((OFFSET+NB_BUFFER) / NB_DATA) : ((OFFSET+NB_BUFFER) / NB_DATA + 1) ;



   //==========================================================================
   // INTERNAL SIGNALS.
   //==========================================================================
   reg [N_TICKS*NB_DATA-1:0]      buffer_ext ;
   wire                           enable_capture;
   wire                           capture_done;
   reg                            enable_capture_r ;
   reg [NB_TIMER-1:0]             timer ;
   reg                            capture_done_d ;
   wire [NB_TIMER-1:0]            n_ticks_w ;
   //==========================================================================
   // ALGORITHM.
   //==========================================================================

   // Control FSM.
   //--------------------------------------------------------------------------

   assign n_ticks_w = 1 + (NB_BUFFER) / NB_DATA ;

   always @( posedge i_clock )
   begin
      if ( i_reset /*|| !i_trigger*/ )
       begin
         buffer_ext <= {N_TICKS*NB_DATA{1'b0}} ;
         buffer_ext[ N_TICKS*NB_DATA-1-i_offset -: NB_BUFFER ] <= RESET_VAL ;
       end else if ( i_valid && enable_capture )
         buffer_ext <= { buffer_ext, i_data } ;
   end

   always @( posedge i_clock )
   begin
      if ( i_reset || i_valid && capture_done )
       enable_capture_r <= 1'b0 ;
     else if ( i_valid && i_trigger )
       enable_capture_r <= 1'b1 ;
   end

   assign  enable_capture = enable_capture_r | i_trigger ;

   always @( posedge i_clock )
     begin
        if ( i_reset || i_valid && capture_done )
          timer <= 0 ;
        else if ( i_valid && enable_capture )
          timer <= timer + 1'b1 ;
     end
   assign  capture_done = ( timer == n_ticks_w ) ;


   assign  o_data = buffer_ext[ N_TICKS*NB_DATA - 1 - i_offset -: NB_BUFFER ] ;

   always @( posedge i_clock )
     begin
        if ( i_reset )
          capture_done_d <= 1'b0 ;
        else if ( i_valid )
          capture_done_d <= capture_done ;
     end

   always @ ( posedge i_clock )
     if ( i_reset )
       o_ready <= 1'b0 ;
     else if ( i_valid )
       o_ready <= capture_done & ~capture_done_d ;

endmodule
