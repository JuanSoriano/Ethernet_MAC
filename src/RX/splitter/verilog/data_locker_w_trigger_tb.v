module data_locker_w_trigger_tb ();

    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================    
    // Parameters.
    localparam                                   NB_DATA     = 64 ;
    localparam                                   NB_BUFFER   = 16 ;
    localparam                                   OFFSET      = 48 ;
    localparam                                   RESET_VAL   = 0 ;

    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
    // Outputs.
    wire    [NB_BUFFER-1:0]             data_tb_o ;
    wire                                ready_tb_o ;

    // Inputs.
    wire    [NB_DATA-1:0]               data_tb_i ;
    wire                                valid_tb_i ;
    wire                                trigger_tb_i ;
    wire                                reset_tb_i ;
    wire                                clock_tb_i ;

    wire                                reset ;
    reg                                 clock = 1'b0 ;
    integer                             timer = 0 ;

    reg                                 test_passed = 1'b0 ;

    //==========================================================================
    // CONNECTION TO DUT
    //==========================================================================

    data_locker_w_trigger
    #(
        .NB_DATA    ( NB_DATA    ),
        .NB_BUFFER  ( NB_BUFFER  ),
        .OFFSET     ( OFFSET     ),
        .RESET_VAL  ( RESET_VAL  )
    )
    u_data_locker_w_trigger
    (
        .o_data     ( data_tb_o     ),
        .o_ready    ( ready_tb_o    ),
        .i_data     ( data_tb_i     ),
        .i_valid    ( valid_tb_i    ),
        .i_trigger  ( trigger_tb_i  ),
        .i_reset    ( reset         ),
        .i_clock    ( clock         )
    ) ;
     // END: of Quick Instance.


    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    always
    begin
        #(50) clock = ~clock ;
    end

    always @ ( posedge clock )
    begin
        timer   <= timer + 1;
    end

    assign reset = (timer == 2) ; // Reset at time 2
    assign valid_tb_i = 1'b1 ;

endmodule