module common_memory
#(
    // PARAMETERS.
    parameter                                           NB_DATA                 = 16    ,
    parameter                                           DEPTH                   = 4     ,
    parameter                                           NB_ADDRESS              = 3
)
(
    // OUTPUTS.
    output  wire    [NB_DATA-1:0]                       o_data              ,
    // INPUTS.
    input   wire    [NB_DATA-1:0]                       i_data              ,
    input   wire    [NB_ADDRESS-1:0]                    i_address_write     ,
    input   wire    [NB_ADDRESS-1:0]                    i_address_read      ,
    input   wire                                        i_write_enable      ,
    input   wire                                        i_read_enable       ,
    input   wire                                        i_clock
) ;


    reg             [NB_DATA-1:0]                       m_array     [DEPTH-1:0] ;
    reg             [NB_DATA-1:0]                       data ;


    always @( posedge i_clock )
    begin
        if ( i_write_enable )
            m_array[ i_address_write ] <= i_data ;
    end

    always @( posedge i_clock )
    begin
        if ( i_read_enable )
            data <= m_array[ i_address_read ] ;
    end

    assign o_data = data ;

endmodule