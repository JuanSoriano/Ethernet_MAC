/*------------------------------------------------------------------------------
 -- Project     : CL100GC
 -------------------------------------------------------------------------------
 -- File        : prbs_generator_n_fixtypes.v
 -- Author      : Ramiro R. Lopez
 -- Originator  : Clariphy Argentina S.A.
 -- Date        : 2014/05/21
 --
 -- Rev 0       : Initial release. RRL.
 --
 --
 -- $Id: data_retimer.v 3333 2017-04-21 22:50:16Z rlopez $
 -------------------------------------------------------------------------------
 -- Description : .
 -------------------------------------------------------------------------------
 -- Copyright (C) 2010 ClariPhy Argentina S.A.  All rights reserved.
 -----------------------------------------------------------------------------*/

module data_retimer_w_counter
#(
    parameter                                   NB_DATA         = 96 ,
    parameter                                   MAX_DEL         = 4 ,
    parameter                                   LOG2_MAX_DEL    = 2
)
(
    output  wire    [NB_DATA-1:0]               o_data ,
    output  wire                                o_valid ,
    output  wire                                o_not_empty ,
    output  reg                                 o_overflow ,
    input   wire    [NB_DATA-1:0]               i_data ,
    input   wire                                i_valid ,
    input   wire                                i_inc_delay ,
    input   wire                                i_bypass ,      //spyglass disable MuxSelConst -- This port might be tied low in some aplications.
    input   wire                                i_enable ,
    input   wire                                i_reset ,
    input   wire                                clock
) ;

    // [NOTE] This blocked has been tested so far only for MAX_DEL=1.

    /* // BEGIN: Quick instance.
    data_retimer
    #(
        .NB_DATA        (   ),
        .MAX_DEL        (   ),
        .LOG2_MAX_DEL   (   )
    )
    u_data_retimer
    (
        .o_data         (   ),
        .o_valid        (   ),
        .o_not_empty    (   ),
        .o_overflow     (   ),
        .i_data         (   ),
        .i_valid        (   ),
        .i_inc_delay    (   ),
        .i_bypass       (   ),
        .i_enable       (   ),
        .i_reset        (   ),
        .clock          (   )
    ) ;
    // END: Quick instance */


    // LOCAL PARAMETERS.
    // None so far.


    // INTERNAL SIGNALS.
    wire                                        index_is_max ;
    wire                                        index_is_min ;
    reg             [LOG2_MAX_DEL-1:0]          index_next ;
    reg             [LOG2_MAX_DEL-1:0]          index ;
    wire            [LOG2_MAX_DEL-1:0]          index_used ;
    reg             [MAX_DEL*NB_DATA-1:0]       data_mem ;
    wire            [(1+MAX_DEL)*NB_DATA-1:0]   data_shifter ;



    // ALGORITHM BEGIN.


    // Calculate value of delay index (live).
    assign  index_is_max
                = ( index == MAX_DEL ) ;
    assign  index_is_min
                = ( index == {LOG2_MAX_DEL{1'b0}} ) ;
    always @( * )
    begin
        index_next
            = index ;
        if ( i_valid && i_inc_delay && (!index_is_max) )
            index_next
                = index + 1'b1 ;    // [HINT] Mismatch OK!
        else if ( (!i_valid) && (!i_inc_delay) && (!index_is_min) )
            index_next
                = index - 1'b1 ;
    end

    // Output valid generation.
    assign  o_valid
                = ( i_valid | ~index_is_min ) & ~i_inc_delay & ~i_reset ;
    assign  o_not_empty                     //spyglass disable DeadCode -- This port might be unused in certain aplicaitons.
                = o_valid | ~index_is_min ; //spyglass disable DeadCode -- This port might be unused in certain aplicaitons.


    // Save index for next clock.
    always @( posedge clock )
    begin
        if ( i_reset || !i_enable || o_overflow )
            index
                <= {LOG2_MAX_DEL{1'b0}} ;
        else
            index
                <= index_next ;
    end


    // If index needs to be decremented, wait one cycle to update the value used,
    // to allow "trapped" data to move to output.
    assign  index_used
                = ( (!i_valid) && (!i_inc_delay) )? index : index_next ;


    // Data delay line.
    always @( posedge clock )
    begin
        if ( i_valid && i_enable && !i_reset )
            data_mem                        //spyglass disable STARC05-2.10.3.2b W164a ShiftReg -- (1) [HINT] MSBs drop is intentional to simplify coding. (2) FIXME: Rewrite as ciclic buffer.
                <= { data_mem, i_data } ;   //spyglass disable STARC05-2.10.3.2b W164a ShiftReg -- (1) [HINT] MSBs drop is intentional to simplify coding. (2) FIXME: Rewrite as ciclic buffer.
    end
    assign  data_shifter
                = { data_mem, i_data } ;


    // Output data selection, based on delay index.
    assign  o_data                                                                          //spyglass disable MuxSelConst FLAT_504 -- This port might be tied low in some aplications.
                = ( i_bypass )? i_data : (data_shifter[ index_used * NB_DATA +: NB_DATA ] & {NB_DATA{o_valid}} ) ;  //spyglass disable MuxSelConst FLAT_504 -- This port might be tied low in some aplications.


    // Overflow indication.
    always @( posedge clock )
    begin
        if ( i_reset || !i_enable )
            o_overflow 
                <= 1'b0 ;
        else
            o_overflow
                <= i_valid & i_inc_delay & index_is_max ;
    end


endmodule // elm_marker_handler_tx_prbs_generator
