/*o_rx_status posibles:
 receiveOK. La recepción se llevó a cabo exitosamente.
 frameTooLong. Indica que el último frame recibido tiene un tamaño superior al máximo permitido.
 frameCheckError. Indica que el frame que se recibió fue dañado durante la transmisión.
 lengthError. Indica que el campo Length/Type ofrece una interpretación de largo del frame inconsistente con el tamaño del frame recibido.
 alignmentError. Indica que el frame recibido se encuentra dañado y que no está formado por un número entero de bytes.
 */
module reception_status
  #(
    parameter                                               N_RX_STATUS                 = 5 ,
    parameter                                               LOG2_N_RX_STATUS            = 3 ,
    parameter                                               NBYTES_MAX_FRAME_SIZE       = 2026 ,
    parameter                                               NBYTES_MAX_DATA_SIZE        = 1982 ,
    parameter                                               LOG2_NBYTES_MAX_DATA_SIZE   = 11 ,
    parameter                                               NB_LENTYP                   = 16 ,
    parameter                                               LOG2_NB_LENTYP              = 4
    )
   (
    // Outputs.
    output wire [LOG2_N_RX_STATUS-1:0]                  o_status , // Status outṕut
    output reg                                          o_status_valid , // Valid signal

    // Inputs.
    input wire [clogb2(NBYTES_MAX_FRAME_SIZE * 8) -1:0] i_frame_size_bits , // Size of the entire frame in bits
    input wire                                          i_frame_size_bits_ready,
    //input wire                                          i_missmatch_dv_len,
    input wire                                          i_ignore_length_check,
    input wire                                          i_frame_too_long,
    input wire [NB_LENTYP-1:0]                          i_lentyp , // Length/type field in the header
    input wire                                          i_fcs_valid , // CRC32 comparation is valid
    input wire                                          i_fcs_forward ,
    input wire                                          i_destination_address_match ,
    input wire                                          i_RS_error,

    input wire                                          i_calculate_state , // Begin to calculate the reception status

    input wire                                          i_valid , // Throughput control.
    input wire                                          i_reset ,
    input wire                                          i_clock

    ) ;

   //==========================================================================
   // LOCAL PARAMETERS.
   //==========================================================================
   localparam                                      max_frame_size_bits = NBYTES_MAX_FRAME_SIZE * 8;
   localparam                                      log2_max_frame_size_bits = clogb2(max_frame_size_bits);

   localparam                                      NB_HEADER_W_CRC = 2*48+16+32;
   localparam                                      MIN_LENTYP=46;

   localparam        [LOG2_N_RX_STATUS-1:0]        receiveOK = 0 ;
   localparam        [LOG2_N_RX_STATUS-1:0]        frameTooLong = 1 ;
   localparam        [LOG2_N_RX_STATUS-1:0]        frameCheckError = 2 ;
   localparam        [LOG2_N_RX_STATUS-1:0]        lengthError = 3 ;
   localparam        [LOG2_N_RX_STATUS-1:0]        alignmentError = 4 ;
   localparam        [LOG2_N_RX_STATUS-1:0]        addressError = 5 ;

   //==========================================================================
   // INTERNAL SIGNALS.
   //==========================================================================
   reg [LOG2_N_RX_STATUS-1:0]                           state ;
   reg [clogb2(NBYTES_MAX_FRAME_SIZE * 8) -1:0]         data_size_bits; // Size of the payload in bits
   reg [clogb2(NBYTES_MAX_FRAME_SIZE * 8) -1:0]         frame_size_bits; // Size of the entire frame in bits
   wire [NB_LENTYP-1:0]                                 min_lentyp;
   //==========================================================================
   // ALGORITHM.
   //==========================================================================
   always @ (posedge i_clock) begin
      if (i_reset) begin
         frame_size_bits <= 'b0;
         data_size_bits <= 'b0;
      end else if (i_valid & i_frame_size_bits_ready) begin
        frame_size_bits <= i_frame_size_bits;
        data_size_bits <= i_frame_size_bits - NB_HEADER_W_CRC;
      end
   end

   assign min_lentyp = i_lentyp < MIN_LENTYP ? MIN_LENTYP : i_lentyp;

   always @ ( posedge i_clock )
     begin
     if (i_reset)begin
        state <= 'b0;
        o_status_valid <= 1'b0;
     end else if (i_valid & i_calculate_state)
       begin
        o_status_valid <= 1'b1;
        if ( i_frame_too_long )
          state <= frameTooLong ;
        else if ( (~i_fcs_valid & ~i_fcs_forward) | i_RS_error)
          state <= frameCheckError ;
        else if ((data_size_bits != min_lentyp*8) & ~i_ignore_length_check)
          state <= lengthError ;
         else if ( (frame_size_bits % 8) != 0) //This condition can't happen
             state <= alignmentError ;
        else if ( !i_destination_address_match)
          state <= addressError ;
        else
          state <= receiveOK;
       end else // if (i_valid && i_calculate_state)
         o_status_valid <= 1'b0;
     end // always @ ( posedge i_clock )

   assign o_status = state;

   function integer clogb2;
      input integer depth;
      for (clogb2=0; depth>0; clogb2=clogb2+1)
        depth = depth >> 1;
   endfunction // clogb2

endmodule
