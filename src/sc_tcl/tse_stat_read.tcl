# =======================================================================================
# ALTERA Confidential and Proprietary
# Copyright 2010 (c) Altera Corporation
# All rights reserved
#
# Project     : Triple Speed Ethernet Hardware Test By Using System Console
#
# Description : Ethernet Generator/Monitor Configuration Setting Top Script
#
# Revision Control Information
#
# Author      : IP Apps
# Revision    : #1
# Date        : 2010/11/10
# ======================================================================================

set jtag_master [lindex [get_service_paths master] 0];

open_service master $jtag_master;
puts "\nInfo: Opened JTAG Master Service\n\n";

puts "TSE MAC Statistics Counters Map\n";
puts "==============================\n";
puts "\n";
puts "Addr\t Name\t\t\t\t Read Value\n";
puts "----\t ----\t\t\t\t ----------\n";


# Cambiar la direccion base segun el qsys
for {set AddressOffset 0x468} {$AddressOffset < 0x4E4} {incr AddressOffset 0x4} {

set AddressOffset_hex [format "%#x" $AddressOffset];

switch -exact -- $AddressOffset_hex {
0x468 { set mac_register_name "aFramesTransmittedOK\t" }
0x46c { set mac_register_name "aFramesReceivedOK\t\t" }
0x470 { set mac_register_name "aFrameCheckSequenceErrors\t" }
0x474 { set mac_register_name "aAlignmentErrors\t\t" }
0x478 { set mac_register_name "aOctetsTransmittedOK\t" }
0x47c { set mac_register_name "aOctetsReceivedOK\t\t" }
0x480 { set mac_register_name "aTxPAUSEMACCtrlFrames\t" }
0x484 { set mac_register_name "aRxPAUSEMACCtrlFrames\t" }
0x488 { set mac_register_name "ifInErrors\t\t" }
0x48c { set mac_register_name "ifOutErrors\t\t" }
0x490 { set mac_register_name "ifInUcastPkts\t\t" }
0x494 { set mac_register_name "ifInMulticastPkts\t\t" }
0x498 { set mac_register_name "ifInBroadcastPkts\t\t" }
0x49c { set mac_register_name "ifOutDiscards\t\t" }
0x4a0 { set mac_register_name "ifOutUcastPkts\t\t" }
0x4a4 { set mac_register_name "ifOutMulticastPkts\t\t" }
0x4a8 { set mac_register_name "ifOutBroadcastPkts\t\t" }
0x4ac { set mac_register_name "etherStatsDropEvents\t" }
0x4b0 { set mac_register_name "etherStatsOctets\t\t" }
0x4b4 { set mac_register_name "etherStatsPkts\t\t" }
0x4b8 { set mac_register_name "etherStatsUndersizePkts\t" }
0x4bc { set mac_register_name "etherStatsOversizePkts\t" }
0x4c0 { set mac_register_name "etherStatsPkts64Octets\t" }
0x4c4 { set mac_register_name "etherStatsPkts65to127Octets\t" }
0x4c8 { set mac_register_name "etherStatsPkts128to255Octets\t" }
0x4cc { set mac_register_name "etherStatsPkts256to511Octets\t" }
0x4d0 { set mac_register_name "etherStatsPkts512to1023Octets\t" }
0x4d4 { set mac_register_name "etherStatsPkts1024to1518Octets" }
0x4d8 { set mac_register_name "etherStatsPkts1519toXOctets\t" }
0x4dc { set mac_register_name "etherStatsJabbers\t\t" }
0x4e0 { set mac_register_name "etherStatsFragments\t\t" }
default { set mac_register_name "Unknown" }
};


set mac_register_read_value [master_read_32 $jtag_master $AddressOffset 1];


puts "$AddressOffset_hex\t $mac_register_name\t $mac_register_read_value\n";


};

close_service master $jtag_master;
puts "\nInfo: Closed JTAG Master Service\n\n";
