set jtag_master [lindex [get_service_paths master] 0];

open_service master $jtag_master;
puts "\nInfo: Opened JTAG Master Service\n\n";
puts "\nInfo: Test toggle\n\n"

master_write_32 $jtag_master 0x0 0x20000000
master_write_32 $jtag_master 0x0 0x00000000

close_service master $jtag_master;
puts "\nInfo: Closed JTAG Master Service\n\n";
