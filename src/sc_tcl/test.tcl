# =======================================================================================
# ALTERA Confidential and Proprietary
# Copyright 2010 (c) Altera Corporation
# All rights reserved
#
# Project     : Triple Speed Ethernet Hardware Test By Using System Console
#
# Description : Ethernet Generator/Monitor Configuration Setting Top Script
#
# Revision Control Information
#
# Author      : IP Apps
# Revision    : #1
# Date        : 2010/11/10
# ======================================================================================

set jtag_master [lindex [get_service_paths master] 0];

open_service master $jtag_master;
puts "\nInfo: Opened JTAG Master Service\n\n";

master_write_32 $jtag_master 0x0 0x01

close_service master $jtag_master;
puts "\nInfo: Closed JTAG Master Service\n\n";
