module frame_checker
  #(
    parameter NB_DATA                   = 64,
    parameter LOG2_NB_DATA              = 6,
    parameter MIN_FRAME_SIZE_BYTES      = 0,
    parameter MAX_FRAME_SIZE_BYTES      = 1500,
    parameter LOG2_MAX_FRAME_SIZE_BYTES = 11,
    parameter LOG2_MAX_FRAME_SIZE_BITS  = 14,

    parameter NB_DST_ADDR               = 48,
    parameter NB_SRC_ADDR               = 48,
    parameter NB_LENTYP                 = 16,
    parameter NB_CRC32                  = 32,
    parameter CRC32_POLYNOMIAL          = 32,

    parameter NB_DA_TYPE                = 2,
    parameter NB_SA_TYPE                = 2,
    parameter NB_LENT_TYPE              = 2,
    parameter NB_DATA_TYPE              = 2,
    parameter NB_CRC_TYPE               = 1,

    parameter N_ADDRESSES               = 10,
    parameter LOG2_N_ADDRESSES          = 4
    )
   (
    output reg                        o_missmatch, //Signal error

    input wire [NB_DATA-1:0]          i_data_llcformat,
    input wire                        i_data_valid_llcformat,
    input wire [LOG2_NB_DATA-1:0]     i_tail_size_bits_llcformat,

    //Signals to configure frame generator
    input wire [NB_DA_TYPE-1:0]       i_da_type,
    input wire [NB_DST_ADDR-1:0]      i_da_set,
    input wire [LOG2_N_ADDRESSES-1:0] i_da_list_selection,
    input wire [NB_DST_ADDR-1:0]      i_da_received,
    input wire                        i_da_received_valid,
    input wire [NB_SA_TYPE-1:0]       i_sa_type ,
    input wire [NB_SRC_ADDR-1:0]      i_sa_set,
    input wire [LOG2_N_ADDRESSES-1:0] i_sa_list_selection ,
    input wire [NB_DST_ADDR-1:0]      i_sa_received,
    input wire                        i_sa_received_valid,
    input wire [NB_LENT_TYPE-1:0]     i_lent_type,
    input wire [NB_LENTYP-1:0]        i_lentyp_set,
    input wire [NB_DATA_TYPE-1:0]     i_data_type,
    input wire [NB_DATA-1:0]          i_data_pattern,
    input wire [NB_DATA-1:0]          i_data_received,
    input wire                        i_data_received_valid,
    input wire [LOG2_NB_DATA-1:0]     i_data_received_tailsize,
    input wire [NB_CRC_TYPE-1:0]      i_crc_type ,
    input wire                        i_generate_crc ,
    input wire [NB_CRC32-1:0]         i_set_crc,
    input wire [NB_CRC32-1:0]         i_crc_received,
    input wire                        i_crc_received_valid,
    input wire                        i_llc_frame_format,
    input wire                        i_eof,

    input wire                        i_valid,
    input wire                        i_reset,
    input wire                        i_clock
    );

   localparam CRC_DELAY_CLOCKS = 5+2;
   localparam NB_BUFFER = CRC_DELAY_CLOCKS*NB_DATA;
   localparam NB_BUFFER_LLC = 2000;
   localparam NB_TIMER = 10;

   wire [NB_DATA-1:0]                  fg_data_o;
   wire [NB_DST_ADDR-1:0]              fg_destination_address_o;
   wire [NB_SRC_ADDR-1:0]              fg_source_address_o;
   wire [NB_LENTYP-1:0]                fg_lentype_o;
   wire [NB_CRC32-1:0]                 fg_fcs_o;
   wire                                fg_fcs_included_o;
   wire                                fg_data_valid_o;
   wire                                fg_frame_transmitted_o;
   wire [LOG2_MAX_FRAME_SIZE_BITS-1:0] fg_frame_size_bits_o;
   wire [LOG2_NB_DATA-1:0]             fg_tail_size_bits_o;
   wire                                fg_llc_frame_format_i;
   wire                                fg_request_frame_i;

   reg                                 fg_fields_ready;
   reg                                 data_valid_d;
   wire                                data_valid_pos;
   //wire                                data_valid_neg;

   reg [NB_TIMER-1:0]                  timer;
   reg [NB_TIMER-1:0]                  timer_llc;
   integer                             i;

   //Registers to compare
   reg [NB_BUFFER-1:0]                 frame_buffer;
   reg [NB_BUFFER_LLC-1:0]                 frame_buffer_llc;
   reg [NB_DST_ADDR-1:0]               dest_addr;
   reg [NB_SRC_ADDR-1:0]               src_addr;
   // reg [NB_LENTYP-1:0]                 lentyp;
   reg [NB_DATA-1:0]                   data;
   reg [NB_CRC32-1:0]                  crc;

   reg [NB_BUFFER-1:0]                 concat_lentyp_data;
   reg [NB_DATA-1:0]                   data_mask;
   reg                                 fg_o_data_valid_d;
   wire                                fg_data_valid_o_neg;

   always @(posedge i_clock)
     if (i_reset) begin
        data_valid_d <= 1'b0;
        fg_fields_ready <= 1'b0;
        fg_o_data_valid_d <= 1'b0;
     end else if (i_valid) begin
        data_valid_d <= (i_data_valid_llcformat & i_llc_frame_format) | (i_data_received_valid & !i_llc_frame_format);
        fg_fields_ready <= fg_request_frame_i;
        fg_o_data_valid_d <= fg_o_data_valid_d;
     end

   assign data_valid_pos = ((i_data_valid_llcformat & i_llc_frame_format) | (i_data_received_valid & !i_llc_frame_format)) & ~data_valid_d;
   //assign data_valid_neg = ~(i_data_valid_llcformat & i_llc_frame_format) | (i_data_received_valid & !i_llc_frame_format) & data_valid_d;
   assign fg_data_valid_o_neg = ~fg_data_valid_o & fg_data_valid_o;

   assign fg_request_frame_i = (i_llc_frame_format) ? i_eof : data_valid_pos;

   always @(posedge i_clock)
     begin
        if (i_reset || data_valid_pos)
          timer <= {NB_TIMER{1'b0}};
        else if (i_valid & fg_data_valid_o) begin
           timer <= timer + 1'b1;
        end
     end

   always @(posedge i_clock)
     begin
        if (i_reset | i_eof | fg_data_valid_o_neg)
          timer_llc <= {NB_TIMER{1'b0}};
        else if (i_valid & (fg_data_valid_o | i_data_valid_llcformat))
          timer_llc <= timer_llc + 1'b1;
     end

   always @(posedge i_clock)
     begin
        if (i_reset) begin
           frame_buffer <= {NB_BUFFER{1'b0}};
           frame_buffer_llc <= {NB_BUFFER_LLC{1'b0}};
        end else if (i_valid & i_data_valid_llcformat & i_llc_frame_format)
          frame_buffer_llc[NB_BUFFER_LLC-(timer_llc*NB_DATA)-1-:NB_DATA] <= i_data_llcformat;
        else if (i_valid & !i_llc_frame_format) begin
           if (i_data_received_valid)
             frame_buffer <= {frame_buffer[NB_BUFFER-NB_DATA-1:0],i_data_received & ~{NB_DATA{i_eof}} & data_mask};//Mask last NB_DATA bits with tailsize valid bits
           if (fg_fields_ready) begin
              dest_addr <= fg_destination_address_o;
              src_addr <= fg_source_address_o;
              // lentyp <= fg_lentype_o;
              concat_lentyp_data <= {{NB_BUFFER-NB_LENTYP{1'b0}}, fg_lentype_o};
              if (i_generate_crc)
                crc <= fg_fcs_o;
           end else begin
              if (fg_data_valid_o)
                concat_lentyp_data <= {concat_lentyp_data[NB_BUFFER-NB_DATA-1:0], fg_data_o & ~{NB_DATA{fg_data_valid_o_neg}} & data_mask}; //Mask last NB_DATA bits with tailsize valid bits
           end // else: !if(data_valid_pos)
        end
     end

   always @(*)
     begin
        casez ({i_llc_frame_format, fg_data_valid_o, i_generate_crc})
          3'b11?: o_missmatch = frame_buffer_llc[NB_BUFFER_LLC-(timer_llc*NB_DATA)-1-:NB_DATA] != fg_data_o;
          3'b10?: o_missmatch = 1'b0;
          //Not LLC frame format
          3'b011: begin
             o_missmatch = ((frame_buffer[NB_DATA-1:0] != concat_lentyp_data[NB_DATA+NB_LENTYP-1-:NB_DATA]) & fg_o_data_valid_d) | //Masked data
                           ((dest_addr != i_da_received) & i_da_received_valid) |
                           ((src_addr != i_sa_received) & i_sa_received_valid) |
                           ((crc != i_crc_received) & i_crc_received_valid) ;
          end
          3'b010: begin
             o_missmatch = ((frame_buffer[NB_DATA-1:0] != concat_lentyp_data[NB_DATA+NB_LENTYP-1-:NB_DATA]) & fg_o_data_valid_d) | //Masked data
                           ((dest_addr != i_da_received) & i_da_received_valid) |
                           ((src_addr != i_sa_received) & i_sa_received_valid);
          end
          default: o_missmatch = 1'b0;
        endcase
     end

   always @(*)
     begin
        data_mask = {NB_DATA{1'b0}};
        if (i_llc_frame_format) begin
           for (i=0; i<i_tail_size_bits_llcformat; i=i+1)
             data_mask[i] = 1'b1;
        end else begin
           for (i=0; i<i_data_received_tailsize; i=i+1)
             data_mask[i] = 1'b1;
        end
     end


   frame_generator
     #(
       .NB_DATA                   (NB_DATA                   ),
       .LOG2_NB_DATA              (LOG2_NB_DATA              ),
       .MIN_FRAME_SIZE_BYTES      (MIN_FRAME_SIZE_BYTES      ),
       .MAX_FRAME_SIZE_BYTES      (MAX_FRAME_SIZE_BYTES      ),
       .LOG2_MAX_FRAME_SIZE_BYTES (LOG2_MAX_FRAME_SIZE_BYTES ),
       .LOG2_MAX_FRAME_SIZE_BITS  (LOG2_MAX_FRAME_SIZE_BITS  ),
       .NB_DST_ADDR               (NB_DST_ADDR               ),
       .NB_SRC_ADDR               (NB_SRC_ADDR               ),
       .NB_LENTYP                 (NB_LENTYP                 ),
       .NB_CRC32                  (NB_CRC32                  ),
       .CRC32_POLYNOMIAL          (CRC32_POLYNOMIAL          ),
       .NB_DA_TYPE                (NB_DA_TYPE                ),
       .NB_SA_TYPE                (NB_SA_TYPE                ),
       .NB_LENT_TYPE              (NB_LENT_TYPE              ),
       .NB_DATA_TYPE              (NB_DATA_TYPE              ),
       .NB_CRC_TYPE               (NB_CRC_TYPE               ),
       .N_ADDRESSES               (N_ADDRESSES               ),
       .LOG2_N_ADDRESSES          (LOG2_N_ADDRESSES          )
       )
   u_frame_generator
     (
      .o_data                     (fg_data_o                 ),
      .o_destination_address      (fg_destination_address_o  ),
      .o_source_address           (fg_source_address_o       ),
      .o_lentype                  (fg_lentype_o              ),
      .o_fcs                      (fg_fcs_o                  ),
      .o_fcs_included             (fg_fcs_included_o         ),
      .o_data_valid               (fg_data_valid_o           ),
      .o_frame_transmitted        (fg_frame_transmitted_o    ),
      .o_frame_size_bits          (fg_frame_size_bits_o      ),
      .o_tail_size_bits           (fg_tail_size_bits_o       ),
      .i_llc_frame_format         (i_llc_frame_format        ),
      .i_request_frame            (fg_request_frame_i        ),
      .i_da_type                  (i_da_type                 ),
      .i_da_set                   (i_da_set                  ),
      .i_da_list_selection        (i_da_list_selection       ),
      .i_sa_type                  (i_sa_type                 ),
      .i_sa_set                   (i_sa_set                  ),
      .i_sa_list_selection        (i_sa_list_selection       ),
      .i_lent_type                (i_lent_type               ),
      .i_lentyp_set               (i_lentyp_set              ),
      .i_data_type                (i_data_type               ),
      .i_data_pattern             (i_data_pattern            ),
      .i_crc_type                 (i_crc_type                ),
      .i_generate_crc             (i_generate_crc            ),
      .i_set_crc                  (i_set_crc                 ),
      .i_valid                    (i_valid                   ),
      .i_reset                    (i_reset                   ),
      .i_clock                    (i_clock                   )
      );

endmodule
