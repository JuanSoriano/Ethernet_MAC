/*
This block is the interface between frame generator/checker and avalon streaming interface when using
MAC with FIFO
*/
module avalon_st_framegen_interface
  #(
    parameter NB_DATA                   = 32,
    parameter LOG2_NB_DATA              = 5,
    parameter MIN_FRAME_SIZE_BYTES      = 0,
    parameter MAX_FRAME_SIZE_BYTES      = 1500,
    parameter LOG2_MAX_FRAME_SIZE_BYTES = 11,
    parameter LOG2_MAX_FRAME_SIZE_BITS  = 14,

    parameter NB_DST_ADDR               = 48,
    parameter NB_SRC_ADDR               = 48,
    parameter NB_LENTYP                 = 16,
    parameter NB_CRC32                  = 32,
    parameter CRC32_POLYNOMIAL          = 33'h104C11DB7,

    parameter NB_DA_TYPE                = 2,
    parameter NB_SA_TYPE                = 2,
    parameter NB_LENT_TYPE              = 2,
    parameter NB_DATA_TYPE              = 2,
    parameter NB_CRC_TYPE               = 1,

    parameter N_ADDRESSES               = 10,
    parameter LOG2_N_ADDRESSES          = 4,

    parameter LOG2_NBDATA_DIV4          = 2  //log2(NB_DATA/4)
    )
   (

    //Egress
    //Avalon-ST TX signals
    output wire [NB_DATA-1:0]                 o_ff_tx_data, //LSB is newer
    output wire                               o_ff_tx_sop, //Start of packet
    output wire                               o_ff_tx_eop, //End of packet
    output wire                               o_tx_rdy,
    output wire                               o_ff_tx_wren, //Acts like data valid
    output wire                               o_ff_tx_crc_fwd,
    output wire                               o_tx_err, //Asserted in last byte, indicates that the transmit frame is invalid. MAC forwards the invalid frame to GMII with error
    output reg [LOG2_NBDATA_DIV4-1:0]         o_ff_tx_mod, //11: [23:0] not valid; 10: [15:0] not valid; 01: [7:0] not valid; 00: All valid

    output wire                               o_ff_tx_septy, //To indicate to stop writing to the FIFO buffer and initiate backpressure
    output wire                               o_tx_ff_uflow, //Underflow in FIFO buffer
    //Frame generator signals
    input wire [NB_DATA-1:0]                  i_data,
    input wire                                i_fcs_included,
    input wire                                i_data_valid,
    input wire                                i_frame_transmitted,

    input wire [LOG2_MAX_FRAME_SIZE_BITS-1:0] i_frame_size_bits,
    input wire [LOG2_NB_DATA-1:0]             i_tail_size_bits,

    //Ingress
    //Frame checker signals
    output wire [NB_DATA-1:0]                 o_data,
    output wire                               o_data_valid,
    output reg [LOG2_NB_DATA-1:0]             o_tail_size_bits,
    //Avalon-ST RX signals
    input wire [NB_DATA-1:0]                  i_ff_rx_data,
    input wire                                i_ff_rx_sop, //Start of packet asserted on the same clock as first data
    input wire                                i_ff_rx_eop, //End of packet asserted on the same clock as last data
    input wire                                i_ff_rx_rdy, //Signals that the user application is ready to receive data
    input wire                                i_ff_rx_dval,
    input wire                                i_ff_rx_dsav, //Indicates that the internal FIFO contains some data to be read (not necessarily a complete frame)
    input wire [4-1:0]                        i_rx_frm_type, //VLAN, BROADCAST, MULTICAST, UNICAST (in that order)
    input wire [17-1:0]                       i_rx_err_stat, //17: stacked vlan received; 16: vlan or stacked vlan received; 15-0: lentyp
    input wire [5-1:0]                        i_rx_err, //5: collision error; 4: corrupted receive frame; 3: truncated receive frame (FIFO OF); 2: CRC error; 1: Invalid length; 0: error has occurred
    input wire [LOG2_NBDATA_DIV4-1:0]         i_ff_rx_mod, //11: [23:0] not valid; 10: [15:0] not valid; 01: [7:0] not valid; 00: All valid

    input wire                                i_clock,
    input wire                                i_valid,
    input wire                                i_reset
    ) ;

   reg                                        datavalid_d;
   wire                                       datavalid_pos;

   ///////////
   //TX LOGIC
   //////////
   assign o_ff_tx_data = i_data;
   assign o_ff_tx_sop = datavalid_pos;
   assign o_ff_tx_eop = i_frame_transmitted;
   assign o_tx_rdy = 1'b1;
   assign o_ff_tx_wren = i_data_valid;
   assign o_ff_tx_crc_fwd = i_fcs_included;
   assign o_tx_err = 1'b0;

   always @(*)
     begin
        case(i_tail_size_bits)
          'd0: o_ff_tx_mod = 2'b00;
          'd8: o_ff_tx_mod = 2'b11;
          'd16: o_ff_tx_mod = 2'b10;
          'd24: o_ff_tx_mod = 2'b01;
          default: o_ff_tx_mod = 2'b00;
        endcase // case (i_frame_size_bits)
     end

   always @(posedge i_clock)
     begin
        if (i_reset)
          datavalid_d <= 1'b0;
        else if (i_valid)
          datavalid_d <= i_data_valid;
     end

   assign datavalid_pos = i_data_valid & ~datavalid_d;

   ///////////
   //RX LOGIC
   //////////
   assign o_data = i_ff_rx_data;
   assign o_data_valid = i_ff_rx_dval;

   always @(*)
     begin
        case(i_ff_rx_mod)
          2'b00: o_tail_size_bits = 5'd0;
          2'b11: o_tail_size_bits = 5'd8;
          2'b10: o_tail_size_bits = 5'd16;
          2'b01: o_tail_size_bits = 5'd24;
          default: o_tail_size_bits = 5'd0;
        endcase // case (i_ff_rx_mod)
     end

endmodule
