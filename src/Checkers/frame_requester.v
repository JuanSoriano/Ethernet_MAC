/* To request frames from frame generator and checker
 */

module frame_requester
  #(
    parameter NB_DATA                   = 64,
    parameter NB_DATA_PRBS              = 64,
    parameter LOG2_NB_DATA              = 6,
    parameter MIN_FRAME_SIZE_BYTES      = 0,
    parameter MAX_FRAME_SIZE_BYTES      = 1500,
    parameter LOG2_MAX_FRAME_SIZE_BYTES = 11,
    parameter LOG2_MAX_FRAME_SIZE_BITS  = 14,

    parameter NB_DST_ADDR               = 48,
    parameter NB_SRC_ADDR               = 48,
    parameter NB_LENTYP                 = 16,
    parameter NB_CRC32                  = 32,
    parameter CRC32_POLYNOMIAL          = 33'h104C11DB7,

    parameter NB_DA_TYPE                = 2,
    parameter NB_SA_TYPE                = 2,
    parameter NB_LENT_TYPE              = 2,
    parameter NB_DATA_TYPE              = 2,
    parameter NB_CRC_TYPE               = 1,

    parameter N_ADDRESSES               = 10,
    parameter LOG2_N_ADDRESSES          = 4
    )
   (
    output reg                         o_request_frame, //A 1 clock pulse requesting a frame generation

    output wire [NB_DA_TYPE-1:0]       o_da_type, //00: Fixed, 01: from list, 10: random not in list
    output wire [NB_DST_ADDR-1:0]      o_da_set, //If i_da_type fixed, use this value
    output wire [LOG2_N_ADDRESSES-1:0] o_da_list_selection,
    output wire [NB_SA_TYPE-1:0]       o_sa_type, //00: Fixed, 01: from list, 10: random not in list
    output wire [NB_SRC_ADDR-1:0]      o_sa_set, //If i_sa_type fixed, use this value
    output wire [LOG2_N_ADDRESSES-1:0] o_sa_list_selection,
    output wire [NB_LENT_TYPE-1:0]     o_lent_type, //00: Fixed, 01: PRBS, 10: too long
    output wire [NB_LENTYP-1:0]        o_lentyp_set,
    output wire [NB_DATA_TYPE-1:0]     o_data_type, //00: Repeat pattern, 01: byte count, 10: prbs31
    output wire [NB_DATA-1:0]          o_data_pattern,
    output wire [NB_CRC_TYPE-1:0]      o_crc_type, // 0: good, 1: bad
    output wire                        o_generate_crc,
    output wire [NB_CRC32-1:0]         o_set_crc,

    input wire                         i_frame_transmitted,

    input                              i_reset,
    input                              i_clock
    );
   localparam N_FRAMES_PRESET_ONE = 10;
   localparam NB_TIMER = 16;

   wire [NB_DST_ADDR-1:0]              dst_addr_list [N_ADDRESSES-1:0];

   wire [NB_DST_ADDR-1:0]              DA_preset_ONE [N_FRAMES_PRESET_ONE-1:0];
   wire [NB_SRC_ADDR-1:0]              SA_preset_ONE [N_FRAMES_PRESET_ONE-1:0];
   wire [NB_LENTYP-1:0]                LENTYP_preset_ONE [N_FRAMES_PRESET_ONE-1:0];
   //wire [NB_DATA-1:0]                  DATA_preset_ONE [N_FRAMES_PRESET_ONE-1:0];

   //Inputs for frame gen
   wire [2-1:0]                        da_type [N_FRAMES_PRESET_ONE-1:0];
   wire [2-1:0]                        sa_type [N_FRAMES_PRESET_ONE-1:0];
   wire [2-1:0]                        lent_type [N_FRAMES_PRESET_ONE-1:0];
   wire [2-1:0]                        data_type [N_FRAMES_PRESET_ONE-1:0];
   wire [N_FRAMES_PRESET_ONE-1:0]      CRC_type ;
   wire [N_FRAMES_PRESET_ONE-1:0]      CRC_generate ;

   reg [NB_TIMER-1:0]                  timer;
   wire                                timer_reset;


   assign timer_reset = timer == 1499;//(timer == N_FRAMES_PRESET_ONE);

   always @(posedge i_clock) begin
     if(i_reset/* | timer_reset*/)
       timer <= {NB_TIMER{1'b0}};
     else if (timer_reset)
       timer <= 1;
     else if (i_frame_transmitted & !timer_reset)
       timer <= timer + 1'b1;
  end

   always @(posedge i_clock) begin
      o_request_frame <= (i_frame_transmitted | i_reset); /*& ~timer_reset & (timer<1000);*/
   end

   // assign o_da_type = da_type[timer];
   // assign o_da_set = DA_preset_ONE[timer];
   // assign o_da_list_selection = 'b0;
   // assign o_sa_type = sa_type[timer];
   // assign o_sa_set = SA_preset_ONE[timer];
   // assign o_sa_list_selection = 'b0;
   // assign o_lent_type = lent_type[timer];
   // assign o_lentyp_set = LENTYP_preset_ONE[timer];
   // assign o_data_type = data_type[timer];
   // assign o_data_pattern = 64'h1234_5678_ABCD_EFFF;
   // assign o_crc_type = CRC_type[timer];
   // assign o_generate_crc = CRC_generate[timer];
   // assign o_set_crc = 'b0;

   assign o_da_type = da_type[0];
   assign o_da_set = DA_preset_ONE[0];
   assign o_da_list_selection = 'b0;
   assign o_sa_type = sa_type[0];
   assign o_sa_set = SA_preset_ONE[0];
   assign o_sa_list_selection = 'b0;
   assign o_lent_type = lent_type[0];
   assign o_lentyp_set = timer+1;
   assign o_data_type = /*2'b00;*/data_type[3];
   assign o_data_pattern = 64'hFAFA_FEFE_FAFA_FEFE;
   assign o_crc_type = CRC_type[0];
   assign o_generate_crc = CRC_generate[0];
   assign o_set_crc = 'b0;

   //DST_ADDR list
   assign   dst_addr_list[ 0 ]     = 48'hcafe_fe00_aaaa ; //local
   assign   dst_addr_list[ 1 ]     = 48'd0 ; //broadcast
   assign   dst_addr_list[ 2 ]     = 48'd2 ; //multicast
   assign   dst_addr_list[ 3 ]     = 48'd3 ;
   assign   dst_addr_list[ 4 ]     = 48'd4 ;
   assign   dst_addr_list[ 5 ]     = 48'd5 ;
   assign   dst_addr_list[ 6 ]     = 48'd6 ;
   assign   dst_addr_list[ 7 ]     = 48'd7 ;
   assign   dst_addr_list[ 8 ]     = 48'd8 ;
   assign   dst_addr_list[ 9 ]     = 48'd9 ;

   //Preset start
   assign DA_preset_ONE[0] = dst_addr_list[0];
   assign DA_preset_ONE[1] = dst_addr_list[0];
   assign DA_preset_ONE[2] = dst_addr_list[0];
   assign DA_preset_ONE[3] = dst_addr_list[0];
   assign DA_preset_ONE[4] = dst_addr_list[0];
   assign DA_preset_ONE[5] = dst_addr_list[1];//broadcast
   assign DA_preset_ONE[6] = dst_addr_list[2];//multicast
   assign DA_preset_ONE[7] = 48'hFFFF_FFFF_FFFF;//bad
   assign DA_preset_ONE[8] = dst_addr_list[0];
   assign DA_preset_ONE[9] = dst_addr_list[0];

   assign SA_preset_ONE[0] = {48'hABCD_ABCD_ABCD};
   assign SA_preset_ONE[1] = {48'hABCD_ABCD_ABCD};
   assign SA_preset_ONE[2] = {48'hABCD_ABCD_ABCD};
   assign SA_preset_ONE[3] = {48'hABCD_ABCD_ABCD};
   assign SA_preset_ONE[4] = {48'hABCD_ABCD_ABCD};
   assign SA_preset_ONE[5] = {48'hABCD_ABCD_ABCD};
   assign SA_preset_ONE[6] = {48'hABCD_ABCD_ABCD};
   assign SA_preset_ONE[7] = {48'hABCD_ABCD_ABCD};
   assign SA_preset_ONE[8] = {48'hABCD_ABCD_ABCD};
   assign SA_preset_ONE[9] = {48'hABCD_ABCD_ABCD};

   assign LENTYP_preset_ONE[0] = 16'd1;
   assign LENTYP_preset_ONE[1] = 16'd2;
   assign LENTYP_preset_ONE[2] = 16'd3;
   assign LENTYP_preset_ONE[3] = 16'd22;
   assign LENTYP_preset_ONE[4] = 16'd70;
   assign LENTYP_preset_ONE[5] = 16'd71;
   assign LENTYP_preset_ONE[6] = 16'd72;
   assign LENTYP_preset_ONE[7] = 16'd107;
   assign LENTYP_preset_ONE[8] = 16'd208;
   assign LENTYP_preset_ONE[9] = 16'd3000;

   //Signals to select options in frame generator
   assign da_type[0] = 2'b00;
   assign da_type[1] = 2'b00;
   assign da_type[2] = 2'b00;
   assign da_type[3] = 2'b00;
   assign da_type[4] = 2'b00;
   assign da_type[5] = 2'b00;
   assign da_type[6] = 2'b00;
   assign da_type[7] = 2'b00;
   assign da_type[8] = 2'b00;
   assign da_type[9] = 2'b00;

   assign sa_type[0] = 2'b00;
   assign sa_type[1] = 2'b00;
   assign sa_type[2] = 2'b00;
   assign sa_type[3] = 2'b00;
   assign sa_type[4] = 2'b01;//PRBS
   assign sa_type[5] = 2'b00;
   assign sa_type[6] = 2'b00;
   assign sa_type[7] = 2'b00;
   assign sa_type[8] = 2'b00;
   assign sa_type[9] = 2'b00;

   assign lent_type[0] = 2'b00;
   assign lent_type[1] = 2'b00;
   assign lent_type[2] = 2'b00;
   assign lent_type[3] = 2'b00;
   assign lent_type[4] = 2'b01;//PRBS
   assign lent_type[5] = 2'b00;
   assign lent_type[6] = 2'b00;
   assign lent_type[7] = 2'b00;
   assign lent_type[8] = 2'b00;
   assign lent_type[9] = 2'b00;

   assign data_type[0] = 2'b00;
   assign data_type[1] = 2'b00;
   assign data_type[2] = 2'b00;
   assign data_type[3] = 2'b10;//PRBS
   assign data_type[4] = 2'b10;//PRBS
   assign data_type[5] = 2'b00;
   assign data_type[6] = 2'b00;
   assign data_type[7] = 2'b00;
   assign data_type[8] = 2'b01;
   assign data_type[9] = 2'b01;

   assign CRC_type[0] = 0;
   assign CRC_type[1] = 0;
   assign CRC_type[2] = 0;
   assign CRC_type[3] = 0;
   assign CRC_type[4] = 0;
   assign CRC_type[5] = 0;
   assign CRC_type[6] = 0;
   assign CRC_type[7] = 0;
   assign CRC_type[8] = 1; //BAD
   assign CRC_type[9] = 0;

   assign CRC_generate[0] = 0;
   assign CRC_generate[1] = 0;
   assign CRC_generate[2] = 0;
   assign CRC_generate[3] = 0;
   assign CRC_generate[4] = 0;
   assign CRC_generate[5] = 0;
   assign CRC_generate[6] = 0;
   assign CRC_generate[7] = 0;
   assign CRC_generate[8] = 1; //BAD
   assign CRC_generate[9] = 0;


endmodule
