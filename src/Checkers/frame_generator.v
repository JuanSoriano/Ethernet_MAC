module frame_generator
  #(
    parameter NB_DATA                   = 64,
    parameter NB_DATA_PRBS              = 64,
    parameter LOG2_NB_DATA              = 6,
    parameter MIN_FRAME_SIZE_BYTES      = 0,
    parameter MAX_FRAME_SIZE_BYTES      = 1500,
    parameter LOG2_MAX_FRAME_SIZE_BYTES = 11,
    parameter LOG2_MAX_FRAME_SIZE_BITS  = 14,

    parameter NB_DST_ADDR               = 48,
    parameter NB_SRC_ADDR               = 48,
    parameter NB_LENTYP                 = 16,
    parameter NB_CRC32                  = 32,
    parameter CRC32_POLYNOMIAL          = 33'h104C11DB7,

    parameter NB_DA_TYPE                = 2,
    parameter NB_SA_TYPE                = 2,
    parameter NB_LENT_TYPE              = 2,
    parameter NB_DATA_TYPE              = 2,
    parameter NB_CRC_TYPE               = 1,

    parameter N_ADDRESSES               = 10,
    parameter LOG2_N_ADDRESSES          = 4
    )
   (
    output reg [NB_DATA-1:0]                  o_data,
    output reg [NB_DST_ADDR-1:0]              o_destination_address,
    output reg [NB_SRC_ADDR-1:0]              o_source_address,
    output reg [NB_LENTYP-1:0]                o_lentype,
    output wire [NB_CRC32-1:0]                o_fcs,
    output reg                                o_fcs_included,
    output wire                               o_fcs_ready,

    output reg                                o_data_valid,
    output reg                                o_frame_transmitted,

    output reg [LOG2_MAX_FRAME_SIZE_BITS-1:0] o_frame_size_bits,
    output reg [LOG2_NB_DATA-1:0]             o_tail_size_bits,

    input wire                                i_llc_frame_format, //If 1, pass the frame already assembled NB_DATA at a time via o_data
    input wire                                i_request_frame, //A 1 clock pulse requesting a frame generation

    input wire [NB_DA_TYPE-1:0]               i_da_type, //00: Fixed, 01: from list, 10: random not in list
    input wire [NB_DST_ADDR-1:0]              i_da_set, //If i_da_type fixed, use this value
    input wire [LOG2_N_ADDRESSES-1:0]         i_da_list_selection,
    input wire [NB_SA_TYPE-1:0]               i_sa_type, //00: Fixed, 01: from list, 10: random not in list
    input wire [NB_SRC_ADDR-1:0]              i_sa_set, //If i_sa_type fixed, use this value
    input wire [LOG2_N_ADDRESSES-1:0]         i_sa_list_selection,
    input wire [NB_LENT_TYPE-1:0]             i_lent_type, //00: Fixed, 01: PRBS, 10: too long
    input wire [NB_LENTYP-1:0]                i_lentyp_set,
    input wire [NB_DATA_TYPE-1:0]             i_data_type, //00: Repeat pattern, 01: byte count, 10: prbs31
    input wire [NB_DATA-1:0]                  i_data_pattern,
    input wire [NB_CRC_TYPE-1:0]              i_crc_type, // 0: good, 1: bad
    input wire                                i_generate_crc,
    input wire [NB_CRC32-1:0]                 i_set_crc,

    input wire                                i_valid,
    input wire                                i_reset,
    input wire                                i_clock
    ) ;

   //localparam NB_BUFFER   = NB_DST_ADDR + NB_SRC_ADDR + NB_LENTYP + NB_CRC32 + MAX_FRAME_SIZE_BYTES ;
   localparam CRC_DELAY_CLOCKS = 4+2; //Clocks needed for CRC32 block to generate an output
   localparam NB_HEADER = NB_DST_ADDR + NB_SRC_ADDR + NB_LENTYP;
   localparam NB_BUFFER = NB_DST_ADDR + NB_SRC_ADDR + NB_LENTYP + (CRC_DELAY_CLOCKS)*NB_DATA;
   localparam NB_TIMER = 10;
   localparam MOD_A = 1024; //Highest power of 2 that is < MAX_FRAME_SIZE_BITS
   localparam MOD_B = 512; //Second highest

   //Addresses arrays
   wire [NB_DST_ADDR-1:0]                     dst_addr_list [N_ADDRESSES-1:0];
   wire [NB_SRC_ADDR-1:0]                     src_addr_list [N_ADDRESSES-1:0];

   reg                                        request_frame_d;
   wire                                       request_frame_pos;

   wire [LOG2_MAX_FRAME_SIZE_BITS-1:0]        framesizebits ;

   reg [NB_BUFFER-1:0]                        frame_buffer;
   reg [NB_DST_ADDR-1:0]                      dest_addr;
   reg [NB_SRC_ADDR-1:0]                      src_addr;
   reg [NB_LENTYP-1:0]                        lentyp;
   reg [NB_LENTYP-1:0]                        lentyp_aux;
   reg [NB_DATA-1:0]                          data;
   reg [NB_CRC32-1:0]                         crc;

   reg                                        prbs_lentyp_en;
   reg                                        prbs_data_en;

   reg [NB_TIMER-1:0]                         timer;
   integer                                    i;

   //Signals to CRC
   wire                                       crc_calc_ready;
   wire [NB_CRC32-1:0]                        crc_calc;
   wire [NB_DATA-1:0]                         data_to_crc;
   wire                                       datavalid_crc;
   wire [LOG2_NB_DATA-1:0]                    tailsize_crc;

   //Signals to prbs
   wire [NB_DATA_PRBS-1:0]                    data_prbs;
   //reg [NB_DATA-1:0]                          captured_data_prbs;

   reg                                        processing_reg;

   //reg                                        datavalid_d;
   //wire                                       datavalid_neg;

   assign   dst_addr_list[ 0 ]     = 48'hcafe_fe00_aaaa ;
   assign   dst_addr_list[ 1 ]     = 48'd1 ;
   assign   dst_addr_list[ 2 ]     = 48'd2 ;
   assign   dst_addr_list[ 3 ]     = 48'd3 ;
   assign   dst_addr_list[ 4 ]     = 48'd4 ;
   assign   dst_addr_list[ 5 ]     = 48'd5 ;
   assign   dst_addr_list[ 6 ]     = 48'd6 ;
   assign   dst_addr_list[ 7 ]     = 48'd7 ;
   assign   dst_addr_list[ 8 ]     = 48'd8 ;
   assign   dst_addr_list[ 9 ]     = 48'd9 ;

   assign   src_addr_list[ 0 ]     = 48'habcd_abcd_abcd;
   assign   src_addr_list[ 1 ]     = 48'd1 ;
   assign   src_addr_list[ 2 ]     = 48'd2 ;
   assign   src_addr_list[ 3 ]     = 48'd3 ;
   assign   src_addr_list[ 4 ]     = 48'd4 ;
   assign   src_addr_list[ 5 ]     = 48'd5 ;
   assign   src_addr_list[ 6 ]     = 48'd6 ;
   assign   src_addr_list[ 7 ]     = 48'd7 ;
   assign   src_addr_list[ 8 ]     = 48'd8 ;
   assign   src_addr_list[ 9 ]     = 48'd9 ;

   //##########
   //OUTPUTS
   //##########
   always @(posedge i_clock)
     if (i_valid) begin
        o_frame_size_bits <= framesizebits;
        if (i_llc_frame_format)
           o_tail_size_bits <= o_frame_size_bits % NB_DATA; //This can only be done for NB_DATA power of 2 to reduce hardware
        else
          o_tail_size_bits <= lentyp_aux*8 % NB_DATA;
     end

   //assign framesizebits = (i_generate_crc) ? NB_DST_ADDR+NB_SRC_ADDR+NB_LENTYP+lentyp_aux*8+NB_CRC32 : NB_DST_ADDR+NB_SRC_ADDR+NB_LENTYP+lentyp_aux*8;
   assign framesizebits = NB_DST_ADDR+NB_SRC_ADDR+NB_LENTYP+lentyp_aux*8+(i_generate_crc*NB_CRC32);

   // always @(posedge i_clock)
   //   if (i_valid) begin
   //      if (~i_llc_frame_format) begin
   //         o_data <= data;
   //         if (request_frame_pos) begin
   //            o_destination_address <= dest_addr;
   //            o_source_address <= src_addr;
   //            o_lentype <= lentyp;
   //            o_fcs_included <= i_generate_crc;
   //         end
   //      end else if (i_llc_frame_format) begin
   //         o_data <= frame_buffer[NB_BUFFER-1-:NB_DATA];
   //         o_fcs_included <= i_generate_crc;
   //      end
   //   end // if (i_valid)
   always @(*) begin
      o_data = 'b0;
      o_destination_address = 'b0;
      o_source_address = 'b0;
      o_lentype = 'b0;
      o_fcs_included = 'b0;
     if (i_valid)
       casez({i_llc_frame_format})
         1'b0: begin
            o_data = data;
            o_destination_address = dest_addr;
            o_source_address = src_addr;
            o_lentype = lentyp_aux;
            o_fcs_included = i_generate_crc;
         end
         1'b1: begin
            o_data = frame_buffer[NB_BUFFER-1-:NB_DATA];
            o_fcs_included = i_generate_crc;
         end
       endcase // casez ({i_llc_frame_format, request_frame_pos})
   end
   /*
	always @(posedge i_clock) begin
		o_data <= frame_buffer[NB_BUFFER-1-:NB_DATA];
		if (i_valid && request_frame_pos) begin
			o_destination_address <= dest_addr;
         o_source_address <= src_addr;
         o_lentype <= lentyp;
			o_fcs_included <= i_generate_crc;
		end
	end
    */


   //assign o_frame_transmitted = datavalid_neg;
   assign o_fcs = crc;
   assign o_fcs_ready = crc_calc_ready;

   always @(posedge i_clock)
     begin
        if (i_reset)
          o_frame_transmitted <= 1'b0;
        else if (~i_llc_frame_format & i_valid & (timer*NB_DATA >= o_frame_size_bits+(CRC_DELAY_CLOCKS-1)*NB_DATA) & (timer*NB_DATA < o_frame_size_bits+(CRC_DELAY_CLOCKS)*NB_DATA))
          o_frame_transmitted <= 1'b1;
        else if (i_llc_frame_format & i_valid & (timer*NB_DATA >= (o_frame_size_bits+(CRC_DELAY_CLOCKS-2)*NB_DATA)) & (timer*NB_DATA < (o_frame_size_bits+(CRC_DELAY_CLOCKS-1)*NB_DATA)))
          o_frame_transmitted <= 1'b1;
        else
          o_frame_transmitted <= 1'b0;
     end

   //Data valid signal
   always @(posedge i_clock)
     begin
        if (i_reset) begin
           o_data_valid <= 1'b0;
        end else if (i_valid) begin
           if (i_llc_frame_format) begin
              o_data_valid <= (processing_reg & (timer*NB_DATA >= (CRC_DELAY_CLOCKS-1)*NB_DATA) & (timer*NB_DATA < (o_frame_size_bits+(CRC_DELAY_CLOCKS-1)*NB_DATA)));
           end else begin
             if (timer*NB_DATA >= (lentyp_aux*8))
               o_data_valid <= 1'b0;
             else if (processing_reg)
               o_data_valid <= 1'b1;
             else
               o_data_valid <= 1'b0;
           end
        end // if (i_valid)

     end
   //assign datavalid_neg = ~o_data_valid & datavalid_d;

   always @(posedge i_clock)
     begin
        if (i_reset)
          request_frame_d <= 1'b0;
        else if (i_valid)
          request_frame_d <= i_request_frame;
     end
   assign request_frame_pos = i_request_frame & ~request_frame_d;

   //Signals that the block is working
   always @(posedge i_clock)
     begin
        if (i_reset)
          processing_reg <= 1'b0;
        else if (i_valid && request_frame_pos)
          processing_reg <= 1'b1;
        else if (i_valid && o_frame_transmitted)
          processing_reg <= 1'b0;
     end

   always @(posedge i_clock)
     begin
        if (i_reset || request_frame_pos || o_frame_transmitted)
          timer <= {NB_TIMER{1'b0}};
        else if (i_valid & processing_reg) begin
           timer <= timer + 1'b1;
        end
     end

   always @(posedge i_clock)
     if (i_valid && request_frame_pos)
       lentyp_aux <= lentyp;

   //Capture prbs once
   // always @(posedge i_clock)
   //   if (i_reset || (i_valid & request_frame_pos))
   //     captured_data_prbs <= data_prbs;

   //Fields
   always @( * )
     begin
        case (i_da_type)
          2'b00: dest_addr = i_da_set;
          2'b01: dest_addr = dst_addr_list[0];
          2'b10: dest_addr = data_prbs[NB_DATA_PRBS-1-:NB_DST_ADDR];
			 default: dest_addr = {NB_DST_ADDR{1'b0}};
          //2'b10: dest_addr = {{(NB_DST_ADDR/NB_TIMER){timer}}, timer[NB_TIMER-:NB_DST_ADDR%NB_TIMER]};
        endcase // case (i_da_type)
        case (i_sa_type)
          2'b00: src_addr = i_sa_set;
          2'b01: src_addr = src_addr_list[0];
          2'b10: src_addr = data_prbs[NB_DST_ADDR-1:0];
			 default: src_addr = {NB_SRC_ADDR{1'b0}};
          //2'b10: src_addr = {{(NB_SRC_ADDR/NB_TIMER){timer}}, timer[NB_TIMER-:NB_SRC_ADDR%NB_TIMER]};
        endcase // case (i_sa_type)
        case (i_lent_type)
          2'b00: lentyp = i_lentyp_set;
          2'b01: begin
             lentyp = data_prbs[NB_LENTYP-1:0] % MOD_A + data_prbs[NB_LENTYP-1:0] % MOD_B;//lentyp = i_lentyp_set; //prbs
             if (lentyp > 36)
               lentyp = lentyp - 36;
             if (lentyp == 0)
               lentyp = 1;
          end
          2'b10: lentyp = 16'h0601+{{NB_LENTYP-NB_TIMER{1'b0}},timer}; //toolong
			 default: lentyp = {NB_LENTYP{1'b0}};
        endcase // case (i_lent_type)
        case (i_data_type)
          2'b00: data = i_data_pattern; //Repeat Pattern
          2'b01: data = {{NB_DATA-NB_TIMER{1'b0}},timer}; //Byte count
          2'b10: data = data_prbs[NB_DATA-1:0]; //PRBS
			 default: data = {NB_DATA{1'b0}};
        endcase // case (i_data_type)
        casez ({i_generate_crc,i_crc_type})
          2'b0?: crc = i_set_crc;
          2'b11: crc = ~crc_calc;
          2'b10: crc = crc_calc;
          default: crc = {NB_CRC32{1'b0}};
            /*begin
             if (i_crc_type) //bad
               crc = ~crc_calc;
             else if (!i_crc_type) //good
               crc = crc_calc;
          end*/
			 //default: crc = {NB_CRC32{1'b0}};
        endcase
     end // always @ ( * )

   always @(posedge i_clock)
     begin
        if (i_reset) begin
           frame_buffer <= {NB_BUFFER{1'b0}};
        end else if (i_valid) begin
           if (timer == 0)
             frame_buffer <= {{dest_addr, src_addr, lentyp, data},{(CRC_DELAY_CLOCKS-1)*NB_DATA{1'b0}}};
           else if (timer < CRC_DELAY_CLOCKS)
             frame_buffer[NB_BUFFER-NB_DST_ADDR-NB_SRC_ADDR-NB_LENTYP-(timer*NB_DATA)-1-:NB_DATA] <= data;
           else if (crc_calc_ready & i_generate_crc) //Insert CRC
             frame_buffer[NB_BUFFER-tailsize_crc-1-:NB_CRC32] <= crc;
           else begin
              frame_buffer <= {frame_buffer[NB_BUFFER-NB_DATA-1:0],data};
           end
        end
     end // always @ (posedge i_clock)

   assign data_to_crc = (timer<CRC_DELAY_CLOCKS) ? frame_buffer[NB_BUFFER-((timer-1)*NB_DATA)-1-:NB_DATA] : frame_buffer[NB_BUFFER-((CRC_DELAY_CLOCKS-1)*NB_DATA)-1-:NB_DATA];

   //Datavalid for crc calculator
   assign datavalid_crc = (processing_reg) ? (timer-1)*NB_DATA <= framesizebits-(i_generate_crc*NB_CRC32) : 1'b0;
   assign tailsize_crc = (NB_DST_ADDR+NB_SRC_ADDR+NB_LENTYP+lentyp_aux*8)%NB_DATA;

   t_prbs_generator
     #(
       .NB_DATA          ( NB_DATA_PRBS        ), //Note that this prbs must be atleast of max(NB_DST,NB_SRC,NB_DATA)
       .TAP1             ( 31                  ),
       .TAP2             ( 28                  ),  // TAP1 > TAP2.
       .MSB_IS_NEWER     ( 1                   )
       )
   u_t_prbs_generator
     (
      .o_data_out        ( data_prbs           ),
      .i_start_gen       ( i_reset             ),
      .i_valid           ( request_frame_pos | processing_reg ),
      .i_enable          ( i_valid             ),
      .i_reset           ( i_reset             ),
      .clock             ( i_clock             )
      );

   CRC32
     #(
       .NB_DATA          (NB_DATA              ),
       .NB_CRC32         (NB_CRC32             ),
       .CRC32_POLYNOMIAL (CRC32_POLYNOMIAL     ),
       .REF_IN           (1                    ),
       .REF_OUT          (1                    )
       )
   u_CRC32
     (
      .o_crc32_valid     (/*NOT CONNECTED   */ ),
      .o_crc_ready       (crc_calc_ready       ),
      .o_crc32           (crc_calc             ),

      .i_data            (data_to_crc          ),
      .i_data_valid      (datavalid_crc        ),
      .i_crc32           (/*NOT CONNECTED   */ ),
      .i_crc32_valid     (/*NOT CONNECTED   */ ),
      .i_tail_size_bits  (tailsize_crc         ),

      .i_valid           (i_valid              ),

      .i_reset           (i_reset              ),
      .i_clock           (i_clock              )
      ) ;
endmodule
