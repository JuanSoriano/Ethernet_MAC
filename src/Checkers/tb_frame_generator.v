module tb_frame_generator ();

   localparam NB_DATA                   = 32 ;
   localparam LOG2_NB_DATA              = 6 ;
   localparam MIN_FRAME_SIZE_BYTES      = 0 ;
   localparam MAX_FRAME_SIZE_BYTES      = 1500 ;
   localparam LOG2_MAX_FRAME_SIZE_BYTES = 11;
   localparam NB_DST_ADDR               = 48;
   localparam NB_SRC_ADDR               = 48;
   localparam NB_LENTYP                 = 16;
   localparam NB_CRC32                  = 32;
   localparam CRC32_POLYNOMIAL          = 33'h104C11DB7;
   localparam NB_DA_TYPE                = 2;
   localparam NB_SA_TYPE                = 2;
   localparam NB_LENT_TYPE              = 2;
   localparam NB_DATA_TYPE              = 2;
   localparam NB_CRC_TYPE               = 1;
   localparam N_ADDRESSES               = 10;
   localparam LOG2_N_ADDRESSES          = 4;

   wire [NB_DATA-1:0]          data_o;
   wire [NB_DST_ADDR-1:0]      destination_addres_o;
   wire [NB_SRC_ADDR-1:0]      source_address_o;
   wire [NB_LENTYP-1:0]        lentype_o;
   wire [NB_CRC32-1:0]         fcs_o;
   wire                        fcs_included_o;
   wire                        data_valid_o;
   wire                        frame_transmitted_o;
   wire                        llc_frame_format_i;
   reg                         request_frame_i;
   wire [NB_DA_TYPE-1:0]       da_type_i;
   wire [NB_DST_ADDR-1:0]      da_set_i;
   wire [LOG2_N_ADDRESSES-1:0] da_list_selection_i;
   wire [NB_SA_TYPE-1:0]       sa_type_i;
   wire [NB_SRC_ADDR-1:0]      sa_set_i;
   wire [LOG2_N_ADDRESSES-1:0] sa_list_selection_i;
   wire [NB_LENT_TYPE-1:0]     lent_type_i;
   wire [NB_LENTYP-1:0]        lentyp_set_i;
   wire [NB_DATA_TYPE-1:0]     data_type_i;
   wire [NB_DATA-1:0]          data_pattern_i;
   wire [NB_CRC_TYPE-1:0]      crc_type_i;
   wire                        generate_crc_i;
   wire [NB_CRC32-1:0]         set_crc_i;

   wire                        missmatch_o;

   wire valid;
   wire reset;
   reg  clock = 1'b0 ;
   integer timer = 0 ;

  frame_generator
    #(
      .NB_DATA                   (NB_DATA                   ),
      .LOG2_NB_DATA              (LOG2_NB_DATA              ),
      .MIN_FRAME_SIZE_BYTES      (MIN_FRAME_SIZE_BYTES      ),
      .MAX_FRAME_SIZE_BYTES      (MAX_FRAME_SIZE_BYTES      ),
      .LOG2_MAX_FRAME_SIZE_BYTES (LOG2_MAX_FRAME_SIZE_BYTES ),
      .NB_DST_ADDR               (NB_DST_ADDR               ),
      .NB_SRC_ADDR               (NB_SRC_ADDR               ),
      .NB_LENTYP                 (NB_LENTYP                 ),
      .NB_CRC32                  (NB_CRC32                  ),
      .CRC32_POLYNOMIAL          (CRC32_POLYNOMIAL          ),
      .NB_DA_TYPE                (NB_DA_TYPE                ),
      .NB_SA_TYPE                (NB_SA_TYPE                ),
      .NB_LENT_TYPE              (NB_LENT_TYPE              ),
      .NB_DATA_TYPE              (NB_DATA_TYPE              ),
      .NB_CRC_TYPE               (NB_CRC_TYPE               ),
      .N_ADDRESSES               (N_ADDRESSES               ),
      .LOG2_N_ADDRESSES          (LOG2_N_ADDRESSES          )
      )
   u_frame_generator
     (
      .o_data                    (data_o                    ),
      .o_destination_address     (destination_addres_o      ),
      .o_source_address          (source_address_o          ),
      .o_lentype                 (lentype_o                 ),
      .o_fcs                     (fcs_o                     ),
      .o_fcs_included            (fcs_included_o            ),
      .o_data_valid              (data_valid_o              ),
      .o_frame_transmitted       (frame_transmitted_o       ),
      .i_llc_frame_format        (llc_frame_format_i        ),
      .i_request_frame           (request_frame_i           ),
      .i_da_type                 (da_type_i                 ),
      .i_da_set                  (da_set_i                  ),
      .i_da_list_selection       (da_list_selection_i       ),
      .i_sa_type                 (sa_type_i                 ),
      .i_sa_set                  (sa_set_i                  ),
      .i_sa_list_selection       (sa_list_selection_i       ),
      .i_lent_type               (lent_type_i               ),
      .i_lentyp_set              (lentyp_set_i              ),
      .i_data_type               (data_type_i               ),
      .i_data_pattern            (data_pattern_i            ),
      .i_crc_type                (crc_type_i                ),
      .i_generate_crc            (generate_crc_i            ),
      .i_set_crc                 (set_crc_i                 ),
      .i_valid                   (valid                     ),
      .i_reset                   (reset                     ),
      .i_clock                   (clock                     )
      );

   frame_checker
     #(
      .NB_DATA                   (NB_DATA                   ),
      .LOG2_NB_DATA              (LOG2_NB_DATA              ),
      .MIN_FRAME_SIZE_BYTES      (MIN_FRAME_SIZE_BYTES      ),
      .MAX_FRAME_SIZE_BYTES      (MAX_FRAME_SIZE_BYTES      ),
      .LOG2_MAX_FRAME_SIZE_BYTES (LOG2_MAX_FRAME_SIZE_BYTES ),
      .NB_DST_ADDR               (NB_DST_ADDR               ),
      .NB_SRC_ADDR               (NB_SRC_ADDR               ),
      .NB_LENTYP                 (NB_LENTYP                 ),
      .NB_CRC32                  (NB_CRC32                  ),
      .CRC32_POLYNOMIAL          (CRC32_POLYNOMIAL          ),
      .NB_DA_TYPE                (NB_DA_TYPE                ),
      .NB_SA_TYPE                (NB_SA_TYPE                ),
      .NB_LENT_TYPE              (NB_LENT_TYPE              ),
      .NB_DATA_TYPE              (NB_DATA_TYPE              ),
      .NB_CRC_TYPE               (NB_CRC_TYPE               ),
       .N_ADDRESSES              (N_ADDRESSES               ),
      .LOG2_N_ADDRESSES          (LOG2_N_ADDRESSES          )
       )
   u_frame_checker
     (
      .o_missmatch               (missmatch_o               ),
      .i_data                    (data_o                    ),
      .i_data_valid              (data_valid_o              ),
      .i_tail_size_bits          (                          ),
      .i_da_type                 (da_type_i                 ),
      .i_da_set                  (da_set_i                  ),
      .i_da_list_selection       (da_list_selection_i       ),
      .i_sa_type                 (sa_type_i                 ),
      .i_sa_set                  (sa_set_i                  ),
      .i_sa_list_selection       (sa_list_selection_i       ),
      .i_lent_type               (lent_type_i               ),
      .i_lentyp_set              (lentyp_set_i              ),
      .i_data_type               (data_type_i               ),
      .i_data_pattern            (data_pattern_i            ),
      .i_crc_type                (crc_type_i                ),
      .i_generate_crc            (generate_crc_i            ),
      .i_set_crc                 (set_crc_i                 ),
      .i_llc_frame_format        (llc_frame_format_i        ),
      .i_valid                   (valid                     ),
      .i_reset                   (reset                     ),
      .i_clock                   (clock                     )
      );

   always
     begin
        #(50) clock = ~clock ;
     end

   always @ ( posedge clock )
     begin
        timer   <= timer + 1;
     end

   always @ ( posedge clock )
     if (reset)
       request_frame_i <= 1'b0;
     else if (valid)
       request_frame_i <= (timer == 5) | frame_transmitted_o;

   assign reset = (timer == 2) ; // Reset at time 2
   assign valid = 1'b1;
   //assign request_frame_i = (timer == 5) | ;
   assign llc_frame_format_i = 1'b1;
   assign da_type_i = 2'b00;
   assign da_set_i = 48'hAAAAAAAAAAAA;
   //assign da_set_i = 48'h111111111111;
   assign sa_type_i = 2'b00;
   assign sa_set_i = 48'hBBBBBBBBBBBB;
   //assign sa_set_i = 48'h222222222222;
   assign lent_type_i = 2'b00;
   assign lentyp_set_i = 16'h0008;
   assign data_type_i = 2'b10;
   assign data_pattern_i = 64'hABCDABCDABCDABCD;
   //assign data_pattern_i = 32'haaaabbbb;
   assign crc_type_i = 1'b0;
   assign generate_crc_i = 1'b1;
   assign set_crc_i = 32'hFFFFFFFF;

endmodule
