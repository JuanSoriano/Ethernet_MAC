module framegen_llcformat_tb ();

   localparam NB_DATA                   = 32 ;
   localparam LOG2_NB_DATA              = 5 ;
   localparam MIN_FRAME_SIZE_BYTES      = 0 ;
   localparam MAX_FRAME_SIZE_BYTES      = 1500 ;
   localparam LOG2_MAX_FRAME_SIZE_BYTES = 11;
   localparam LOG2_MAX_FRAME_SIZE_BITS  = 14;
   localparam NB_DST_ADDR               = 48;
   localparam NB_SRC_ADDR               = 48;
   localparam NB_LENTYP                 = 16;
   localparam NB_CRC32                  = 32;
   localparam CRC32_POLYNOMIAL          = 33'h104C11DB7;
   localparam NB_DA_TYPE                = 2;
   localparam NB_SA_TYPE                = 2;
   localparam NB_LENT_TYPE              = 2;
   localparam NB_DATA_TYPE              = 2;
   localparam NB_CRC_TYPE               = 1;
   localparam N_ADDRESSES               = 10;
   localparam LOG2_N_ADDRESSES          = 4;
   localparam LOG2_NBDATA_DIV4           = 2;  //log2(NB_DATA/4)
   localparam NB_GMII                    = 8;

   localparam NB_TIMER = 10;
   localparam NB_GPIO = 8;

   //Avalon-ST TX signals
   wire [NB_DATA-1:0]     o_ff_tx_data; //LSB is newer
   wire                   o_ff_sop; //Start of packet
   wire                   o_ff_tx_eop; //End of packet
   wire                   o_tx_rdy;
   wire                   o_ff_tx_wren; //Acts like data valid
   wire                   o_ff_tx_crc_fwd;
   wire                   o_tx_err; //Asserted in last byte, indicates that the transmit frame is invalid. MAC forwards the invalid frame to GMII with error
   reg [LOG2_NBDATA_DIV4-1:0] o_ff_tx_mod; //11: [23:0] not valid; 10: [15:0] not valid; 01: [7:0] not valid; 00: All valid
   wire                       o_ff_tx_septy; //To indicate to stop writing to the FIFO buffer and initiate backpressure
   wire                       tx_ff_uflow; //Underflow in FIFO buffer
   //Frame generator signals
   wire [NB_DATA-1:0]         i_data;
   wire                       i_fcs_included;
   wire                       i_data_valid;
   wire                       i_frame_transmitted;
   wire [LOG2_MAX_FRAME_SIZE_BITS-1:0] i_frame_size_bits;
   wire [LOG2_NB_DATA-1:0]             i_tail_size_bits;
   //Ingress
   //Frame checker signals
   wire [NB_DATA-1:0]                  o_data;
   wire                                o_data_valid;
   reg [LOG2_NB_DATA-1:0]              o_tail_size_bits;

   //Frame Gen/Checker
   wire [NB_DATA-1:0]                   data_o;
   wire [NB_DST_ADDR-1:0]               destination_addres_o;
   wire [NB_SRC_ADDR-1:0]               source_address_o;
   wire [NB_LENTYP-1:0]                 lentype_o;
   wire [NB_CRC32-1:0]                  fcs_o;
   wire                                 fcs_included_o;
   wire                                 data_valid_o;
   wire                                 frame_transmitted_o;
   wire                                 llc_frame_format_i;
   reg                                  request_frame_i;

   reg                                  llc_frame_format;
   reg [NB_DA_TYPE-1:0]                 da_type;
   reg [NB_DST_ADDR-1:0]                da_set;
   reg [LOG2_N_ADDRESSES-1:0]           da_list_selection;
   reg [NB_SA_TYPE-1:0]                 sa_type;
   reg [NB_SRC_ADDR-1:0]                sa_set;
   reg [LOG2_N_ADDRESSES-1:0]           sa_list_selection;
   reg [NB_LENT_TYPE-1:0]               lent_type;
   reg [NB_LENTYP-1:0]                  lentyp_set;
   reg [NB_DATA_TYPE-1:0]               data_type;
   reg [NB_DATA-1:0]                    data_pattern;
   reg [NB_CRC_TYPE-1:0]                crc_type;
   reg                                  generate_crc;
   reg [NB_CRC32-1:0]                   set_crc;

   wire [NB_DA_TYPE-1:0]                da_type_i;
   wire [NB_DST_ADDR-1:0]               da_set_i;
   wire [LOG2_N_ADDRESSES-1:0]          da_list_selection_i;
   wire [NB_SA_TYPE-1:0]                sa_type_i;
   wire [NB_SRC_ADDR-1:0]               sa_set_i;
   wire [LOG2_N_ADDRESSES-1:0]          sa_list_selection_i;
   wire [NB_LENT_TYPE-1:0]              lent_type_i;
   wire [NB_LENTYP-1:0]                 lentyp_set_i;
   wire [NB_DATA_TYPE-1:0]              data_type_i;
   wire [NB_DATA-1:0]                   data_pattern_i;
   wire [NB_CRC_TYPE-1:0]               crc_type_i;
   wire                                 generate_crc_i;
   wire [NB_CRC32-1:0]                  set_crc_i;

   wire                                 missmatch_o;

   //Frame generator
   wire [NB_DATA-1:0]                  fg_data_o;
   wire [NB_DST_ADDR-1:0]              fg_destination_address_o;
   wire [NB_SRC_ADDR-1:0]              fg_source_address_o;
   wire [NB_LENTYP-1:0]                fg_lentype_o;
   wire [NB_CRC32-1:0]                 fg_fcs_o;
   wire                                fg_fcs_included_o;
   wire                                fg_data_valid_o;
   wire                                fg_frame_transmitted_o;
   wire [LOG2_MAX_FRAME_SIZE_BITS-1:0] fg_frame_size_bits_o;
   wire [LOG2_NB_DATA-1:0]             fg_tail_size_bits_o;
   wire                                fg_llc_frame_format_i;


   //Avalon-ST interface signals
   wire [NB_DATA-1:0]                  av_ff_tx_data_fgen;
   wire                                av_ff_tx_sop_fgen;
   wire                                av_ff_tx_eop_fgen;
   wire                                av_tx_rdy_fgen;
   wire                                av_ff_tx_wren_fgen;
   wire                                av_ff_tx_crc_fwd_fgen;
   wire                                av_tx_err_fgen;
   wire [LOG2_NBDATA_DIV4-1:0]         av_ff_tx_mod_fgen;
   wire                                av_ff_tx_septy_fgen;
   wire                                av_tx_ff_uflow_fgen;

   wire [NB_DATA-1:0]                  fgen_ff_rx_data_av;
   wire                                fgen_ff_rx_sop_av;
   wire                                fgen_ff_rx_eop_av;
   wire                                fgen_ff_rx_rdy_av;
   wire                                fgen_ff_rx_dval_av;
   wire                                fgen_ff_rx_dsav_av;
   wire [4-1:0]                        fgen_rx_frm_type_av;
   wire [17-1:0]                       fgen_rx_err_stat_av;
   wire [5-1:0]                        fgen_rx_err_av;
   wire [LOG2_NBDATA_DIV4-1:0]         fgen_ff_rx_mod_av;

   wire [LOG2_NB_DATA-1:0]             av_tailsizebits_checker;

   wire                                tse_tx_empty_o;
   wire                                tse_tx_ready_o;
   wire [6-1:0]                        tse_rx_error_o;
   wire                                tse_rx_empty_o;

   wire               valid=1'b1;
   wire               reset;
   reg                clock = 1'b0 ;
   reg [NB_TIMER-1:0] timer=10'h0;

   always
     begin
        #(50) clock = ~clock ;
     end

   always @ ( posedge clock )
     begin if (timer < 1000)
       timer <= timer + 1'b1;
     else
       timer <= timer;
     end

   reg fg_frame_transmitted_o_d;

   always @ ( posedge clock )
     fg_frame_transmitted_o_d <= timer%100 == 0;//fg_frame_transmitted_o;

   assign da_type_i = 2'b00;
   assign da_set_i = 48'hABCDABCDABCD;
   assign da_list_selection_i = 'b0;
   assign sa_type_i = 2'b00;
   assign sa_set_i = 48'hCAFECAFECAFE;
   assign sa_list_selection_i = 'b0;
   assign lent_type_i = 2'b00;
   assign lentyp_set_i = 16'd1;
   assign data_type_i = 2'b10;
   assign data_pattern_i = 32'h11111111;
   assign crc_type_i = 1'b0;
   assign generate_crc_i = 1'b0;
   assign set_crc_i = 32'hDDDDDDDD;
   //assign frame_transmitted_o = fg_frame_transmitted_o;
   assign llc_frame_format_i = 1'b1;

   always @ ( posedge clock )
     if (reset)
       request_frame_i <= 1'b0;
     else if (valid)
       request_frame_i <= (timer == 10'h10) | fg_frame_transmitted_o_d;

   assign reset = (timer >= 2) & (timer <= 5); // Reset at time 2


   avalon_st_framegen_interface
     #(
       .NB_DATA                   (NB_DATA                   ),
       .LOG2_NB_DATA              (LOG2_NB_DATA              ),
       .MIN_FRAME_SIZE_BYTES      (MIN_FRAME_SIZE_BYTES      ),
       .MAX_FRAME_SIZE_BYTES      (MAX_FRAME_SIZE_BYTES      ),
       .LOG2_MAX_FRAME_SIZE_BYTES (LOG2_MAX_FRAME_SIZE_BYTES ),
       .LOG2_MAX_FRAME_SIZE_BITS  (LOG2_MAX_FRAME_SIZE_BITS  ),
       .NB_DST_ADDR               (NB_DST_ADDR               ),
       .NB_SRC_ADDR               (NB_SRC_ADDR               ),
       .NB_LENTYP                 (NB_LENTYP                 ),
       .NB_CRC32                  (NB_CRC32                  ),
       .CRC32_POLYNOMIAL          (CRC32_POLYNOMIAL          ),
       .NB_DA_TYPE                (NB_DA_TYPE                ),
       .NB_SA_TYPE                (NB_SA_TYPE                ),
       .NB_LENT_TYPE              (NB_LENT_TYPE              ),
       .NB_DATA_TYPE              (NB_DATA_TYPE              ),
       .NB_CRC_TYPE               (NB_CRC_TYPE               ),
       .N_ADDRESSES               (N_ADDRESSES               ),
       .LOG2_N_ADDRESSES          (LOG2_N_ADDRESSES          ),
       .LOG2_NBDATA_DIV4          (LOG2_NBDATA_DIV4          )
       )

   u_avalon_st_framegen_interface
     (
      //From AV_ST_IF to Altera MAC
      .o_ff_tx_data                 (av_ff_tx_data_fgen        ),
      .o_ff_tx_sop                  (av_ff_tx_sop_fgen         ),
      .o_ff_tx_eop                  (av_ff_tx_eop_fgen         ),
      .o_tx_rdy                     (av_tx_rdy_fgen            ),
      .o_ff_tx_wren                 (av_ff_tx_wren_fgen        ),
      .o_ff_tx_crc_fwd              (av_ff_tx_crc_fwd_fgen     ),
      .o_tx_err                     (av_tx_err_fgen            ),
      .o_ff_tx_mod                  (av_ff_tx_mod_fgen         ),
      .o_ff_tx_septy                (av_ff_tx_septy_fgen       ),
      .o_tx_ff_uflow                (av_tx_ff_uflow_fgen       ),

      //From Fgen to AV_ST_IF
      .i_data                       (fg_data_o                 ),
      .i_fcs_included               (fg_fcs_included_o         ),
      .i_data_valid                 (fg_data_valid_o           ),
      .i_frame_transmitted          (fg_frame_transmitted_o    ),
      .i_frame_size_bits            (fg_frame_size_bits_o      ),
      .i_tail_size_bits             (fg_tail_size_bits_o       ),

      //From AV_ST_IF to Fchecker
      .o_data                       (data_o                    ),
      .o_data_valid                 (data_valid_o              ),
      .o_tail_size_bits             (av_tailsizebits_checker   ),

      //From Altera MAC to AV_ST_IF
      .i_ff_rx_data                 (fgen_ff_rx_data_av        ),
      .i_ff_rx_sop                  (fgen_ff_rx_sop_av         ),
      .i_ff_rx_eop                  (fgen_ff_rx_eop_av         ),
      .i_ff_rx_rdy                  (fgen_ff_rx_rdy_av         ),
      .i_ff_rx_dval                 (fgen_ff_rx_dval_av        ),
      .i_ff_rx_dsav                 (fgen_ff_rx_dsav_av        ),
      .i_rx_frm_type                (fgen_rx_frm_type_av       ),
      .i_rx_err_stat                (fgen_rx_err_stat_av       ),
      .i_rx_err                     (fgen_rx_err_av            ),
      .i_ff_rx_mod                  (fgen_ff_rx_mod_av         ),

      .i_clock                      (clock              ),
      .i_valid                      (valid                     ),
      .i_reset                      (reset                     )
      );


   frame_generator
     #(
       .NB_DATA                   (NB_DATA                   ),
       .LOG2_NB_DATA              (LOG2_NB_DATA              ),
       .MIN_FRAME_SIZE_BYTES      (MIN_FRAME_SIZE_BYTES      ),
       .MAX_FRAME_SIZE_BYTES      (MAX_FRAME_SIZE_BYTES      ),
       .LOG2_MAX_FRAME_SIZE_BYTES (LOG2_MAX_FRAME_SIZE_BYTES ),
       .LOG2_MAX_FRAME_SIZE_BITS  (LOG2_MAX_FRAME_SIZE_BITS  ),
       .NB_DST_ADDR               (NB_DST_ADDR               ),
       .NB_SRC_ADDR               (NB_SRC_ADDR               ),
       .NB_LENTYP                 (NB_LENTYP                 ),
       .NB_CRC32                  (NB_CRC32                  ),
       .CRC32_POLYNOMIAL          (CRC32_POLYNOMIAL          ),
       .NB_DA_TYPE                (NB_DA_TYPE                ),
       .NB_SA_TYPE                (NB_SA_TYPE                ),
       .NB_LENT_TYPE              (NB_LENT_TYPE              ),
       .NB_DATA_TYPE              (NB_DATA_TYPE              ),
       .NB_CRC_TYPE               (NB_CRC_TYPE               ),
       .N_ADDRESSES               (N_ADDRESSES               ),
       .LOG2_N_ADDRESSES          (LOG2_N_ADDRESSES          )
       )
   u_frame_generator
     (
      //From Fgen to AV_ST_IF
      .o_data                     (fg_data_o                ),
      .o_destination_address      (fg_destination_address_o ),
      .o_source_address           (fg_source_address_o      ),
      .o_lentype                  (fg_lentype_o             ),
      .o_fcs                      (fg_fcs_o                 ),
      .o_fcs_included             (fg_fcs_included_o        ),
      .o_data_valid               (fg_data_valid_o          ),
      .o_frame_transmitted        (fg_frame_transmitted_o   ),
      .o_frame_size_bits          (fg_frame_size_bits_o     ),
      .o_tail_size_bits           (fg_tail_size_bits_o      ),

      //From top to Fgen
      .i_llc_frame_format         (llc_frame_format_i       ),
      .i_request_frame            (request_frame_i          ),
      .i_da_type                  (da_type_i                ),
      .i_da_set                   (da_set_i                 ),
      .i_da_list_selection        (da_list_selection_i      ),
      .i_sa_type                  (sa_type_i                ),
      .i_sa_set                   (sa_set_i                 ),
      .i_sa_list_selection        (sa_list_selection_i      ),
      .i_lent_type                (lent_type_i              ),
      .i_lentyp_set               (lentyp_set_i             ),
      .i_data_type                (data_type_i              ),
      .i_data_pattern             (data_pattern_i           ),
      .i_crc_type                 (crc_type_i               ),
      .i_generate_crc             (generate_crc_i           ),
      .i_set_crc                  (set_crc_i                ),
      .i_valid                    (valid                    ),
      .i_reset                    (reset                    ),
      .i_clock                    (clock             )
      );

   frame_checker
     #(
       .NB_DATA                   (NB_DATA                   ),
       .LOG2_NB_DATA              (LOG2_NB_DATA              ),
       .MIN_FRAME_SIZE_BYTES      (MIN_FRAME_SIZE_BYTES      ),
       .MAX_FRAME_SIZE_BYTES      (MAX_FRAME_SIZE_BYTES      ),
       .LOG2_MAX_FRAME_SIZE_BYTES (LOG2_MAX_FRAME_SIZE_BYTES ),
       .NB_DST_ADDR               (NB_DST_ADDR               ),
       .NB_SRC_ADDR               (NB_SRC_ADDR               ),
       .NB_LENTYP                 (NB_LENTYP                 ),
       .NB_CRC32                  (NB_CRC32                  ),
       .CRC32_POLYNOMIAL          (CRC32_POLYNOMIAL          ),
       .NB_DA_TYPE                (NB_DA_TYPE                ),
       .NB_SA_TYPE                (NB_SA_TYPE                ),
       .NB_LENT_TYPE              (NB_LENT_TYPE              ),
       .NB_DATA_TYPE              (NB_DATA_TYPE              ),
       .NB_CRC_TYPE               (NB_CRC_TYPE               ),
       .N_ADDRESSES               (N_ADDRESSES               ),
       .LOG2_N_ADDRESSES          (LOG2_N_ADDRESSES          )
       )
   u_frame_checker
     (
      //From Fchecker to top
      .o_missmatch                (missmatch_o             ),

      //From AV_ST_IF to Fcheck
      .i_data_llcformat           (fg_data_o                  ),
      .i_data_valid_llcformat     (fg_data_valid_o            ),
      .i_tail_size_bits_llcformat (fg_tail_size_bits_o        ),

      //From top to Fcheck
      .i_da_type                  (da_type_i               ),
      .i_da_set                   (da_set_i                ),
      .i_da_list_selection        (da_list_selection_i     ),
      .i_da_received              ('b0                     ),
      .i_da_received_valid        ( 'b0                    ),
      .i_sa_type                  (sa_type_i               ),
      .i_sa_set                   (sa_set_i                ),
      .i_sa_list_selection        (sa_list_selection_i     ),
      .i_sa_received              ( 'b0                    ),
      .i_sa_received_valid        ( 'b0                    ),
      .i_lent_type                (lent_type_i             ),
      .i_lentyp_set               (lentyp_set_i            ),
      .i_data_type                (data_type_i             ),
      .i_data_pattern             (data_pattern_i          ),
      .i_data_received            ( 'b0                    ),
      .i_data_received_valid      ( 'b0                    ),
      .i_data_received_tailsize   ( 'b0                    ),
      .i_crc_type                 (crc_type_i              ),
      .i_generate_crc             (generate_crc_i          ),
      .i_set_crc                  (set_crc_i               ),
      .i_crc_received             ( 'b0                    ),
      .i_crc_received_valid       ( 'b0                    ),
      .i_llc_frame_format         (llc_frame_format_i      ),
      .i_eof                      (fg_frame_transmitted_o  ),
      .i_valid                    (valid                   ),
      .i_reset                    (reset                   ),
      .i_clock                    (clock            )
      );

endmodule
