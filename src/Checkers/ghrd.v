`define ENABLE_HPS

module ghrd
  (
   ///////// FPGA /////////
   input         FPGA_CLK1_50,
   input         FPGA_CLK2_50,
   input         FPGA_CLK3_50,

`ifdef ENABLE_HPS
   ///////// HPS /////////
   inout         HPS_CONV_USB_N,
   output [14:0] HPS_DDR3_ADDR,
   output [2:0]  HPS_DDR3_BA,
   output        HPS_DDR3_CAS_N,
   output        HPS_DDR3_CKE,
   output        HPS_DDR3_CK_N,
   output        HPS_DDR3_CK_P,
   output        HPS_DDR3_CS_N,
   output [3:0]  HPS_DDR3_DM,
   inout [31:0]  HPS_DDR3_DQ,
   inout [3:0]   HPS_DDR3_DQS_N,
   inout [3:0]   HPS_DDR3_DQS_P,
   output        HPS_DDR3_ODT,
   output        HPS_DDR3_RAS_N,
   output        HPS_DDR3_RESET_N,
   input         HPS_DDR3_RZQ,
   output        HPS_DDR3_WE_N,
   output        HPS_ENET_GTX_CLK,
   inout         HPS_ENET_INT_N,
   output        HPS_ENET_MDC,
   inout         HPS_ENET_MDIO,
   input         HPS_ENET_RX_CLK,
   input [3:0]   HPS_ENET_RX_DATA,
   input         HPS_ENET_RX_DV,
   output [3:0]  HPS_ENET_TX_DATA,
   output        HPS_ENET_TX_EN,
   inout         HPS_GSENSOR_INT,
   inout         HPS_I2C0_SCLK,
   inout         HPS_I2C0_SDAT,
   inout         HPS_I2C1_SCLK,
   inout         HPS_I2C1_SDAT,
   inout         HPS_KEY,
   inout         HPS_LED,
   inout         HPS_LTC_GPIO,
   output        HPS_SD_CLK,
   inout         HPS_SD_CMD,
   inout [3:0]   HPS_SD_DATA,
   output        HPS_SPIM0_CLK,
   input         HPS_SPIM0_MISO,
   output        HPS_SPIM0_MOSI,
   inout         HPS_SPIM0_SS,
   inout         HPS_SPIM_CLK,
   inout         HPS_SPIM_MISO,
   inout         HPS_SPIM_MOSI,
   inout         HPS_SPIM_SS,
   input         HPS_UART_RX,
   output        HPS_UART_TX,
   input         HPS_USB_CLKOUT,
   inout [7:0]   HPS_USB_DATA,
   input         HPS_USB_DIR,
   input         HPS_USB_NXT,
   output        HPS_USB_STP,
`endif /*ENABLE_HPS*/


   output        I2C2_CLK,
   inout         I2C2_DATA,

   inout         HPS_OVERTEMPN,
   inout         HPS_SPISW_A0,
   inout         HPS_SPISW_ENN,

   //J124
   inout         HPS_3p3V_GPIO0,
   inout         HPS_3p3V_GPIO1,

   inout         HPS_USB_RESET,
   inout         HPS_ENET_RESET_N,

   inout         HPS_ALERTN,

   input         MISO_GPIO_1_0,
   output        MOSI_GPIO_1_1,
   output        SCLK_GPIO_1_2,
   output        SS_N_GPIO_1_3

   );

   localparam NB_DATA                   = 32 ;
   localparam LOG2_NB_DATA              = 6 ;
   localparam MIN_FRAME_SIZE_BYTES      = 0 ;
   localparam MAX_FRAME_SIZE_BYTES      = 1500 ;
   localparam LOG2_MAX_FRAME_SIZE_BYTES = 11;
   localparam LOG2_MAX_FRAME_SIZE_BITS  = 14;
   localparam NB_DST_ADDR               = 48;
   localparam NB_SRC_ADDR               = 48;
   localparam NB_LENTYP                 = 16;
   localparam NB_CRC32                  = 32;
   localparam CRC32_POLYNOMIAL          = 33'h104C11DB7;
   localparam NB_DA_TYPE                = 2;
   localparam NB_SA_TYPE                = 2;
   localparam NB_LENT_TYPE              = 2;
   localparam NB_DATA_TYPE              = 2;
   localparam NB_CRC_TYPE               = 1;
   localparam N_ADDRESSES               = 10;
   localparam LOG2_N_ADDRESSES          = 4;
   localparam LOG2_NBDATA_DIV4           = 2;  //log2(NB_DATA/4)
   localparam NB_GMII                    = 8;

   localparam NB_TIMER = 10;

   localparam LOAN_IO_LEN = 67;

   //=======================================================
   //  REG/WIRE declarations
   //=======================================================
   // internal wires and registers declaration
   wire [1:0]             fpga_debounced_buttons;
   wire [7:0]             fpga_led_internal;
   wire                   hps_fpga_reset_n;
   wire [2:0]             hps_reset_req;
   wire                   hps_cold_reset;
   wire                   hps_warm_reset;
   wire                   hps_debug_reset;
   wire [27:0]            stm_hw_events;

	 wire                   miso;
	 wire                   mosi;
	 wire                   sclk;
	 wire                   ss_n;

	 wire [LOAN_IO_LEN-1:0] loan_io_in;
	 wire [LOAN_IO_LEN-1:0] loan_io_out;
	 wire [LOAN_IO_LEN-1:0] loan_io_oe;

	 assign loan_io_oe[65] = 1'b1;
	 assign loan_io_out[65] = sclk;
   //	assign loan_io_in[65] = HPS_KEY;

	 assign loan_io_oe[66] = 1'b1;
	 assign loan_io_out[66] = ss_n;
   //	assign loan_io_in[66] = HPS_KEY;

   // connection of internal logics
   assign stm_hw_events    = {{13{1'b0}}, fpga_led_internal, fpga_debounced_buttons};


   //=======================================================
   //  Structural coding
   //=======================================================


   wire [7:0]             hardware_revision;

   assign hardware_revision = 8'b00010001;


   /******************************/
   /* MY CODE */
   /******************************/

   //*************************
   //AVALON ST INTERFACE
   //*************************
   //Egress
   //Avalon-ST TX signals
   wire [NB_DATA-1:0]     o_ff_tx_data; //LSB is newer
   wire                   o_ff_sop; //Start of packet
   wire                   o_ff_tx_eop; //End of packet
   wire                   o_tx_rdy;
   wire                   o_ff_tx_wren; //Acts like data valid
   wire                   o_ff_tx_crc_fwd;
   wire                   o_tx_err; //Asserted in last byte, indicates that the transmit frame is invalid. MAC forwards the invalid frame to GMII with error
   reg [LOG2_NBDATA_DIV4-1:0] o_ff_tx_mod; //11: [23:0] not valid; 10: [15:0] not valid; 01: [7:0] not valid; 00: All valid
   wire                       o_ff_tx_septy; //To indicate to stop writing to the FIFO buffer and initiate backpressure
   wire                       tx_ff_uflow; //Underflow in FIFO buffer
   //Frame generator signals
   wire [NB_DATA-1:0]         i_data;
   wire                       i_fcs_included;
   wire                       i_data_valid;
   wire                       i_frame_transmitted;
   wire [LOG2_MAX_FRAME_SIZE_BITS-1:0] i_frame_size_bits;
   wire [LOG2_NB_DATA-1:0]             i_tail_size_bits;
   //Ingress
   //Frame checker signals
   wire [NB_DATA-1:0]                  o_data;
   wire                                o_data_valid;
   reg [LOG2_NB_DATA-1:0]              o_tail_size_bits;

   //Frame Gen/Checker
   wire [NB_DATA-1:0]                   data_o;
   wire [NB_DST_ADDR-1:0]               destination_addres_o;
   wire [NB_SRC_ADDR-1:0]               source_address_o;
   wire [NB_LENTYP-1:0]                 lentype_o;
   wire [NB_CRC32-1:0]                  fcs_o;
   wire                                 fcs_included_o;
   wire                                 data_valid_o;
   wire                                 frame_transmitted_o;
   wire                                 llc_frame_format_i;
   reg                                  request_frame_i;

   reg                                  llc_frame_format;
   reg [NB_DA_TYPE-1:0]                 da_type;
   reg [NB_DST_ADDR-1:0]                da_set;
   reg [LOG2_N_ADDRESSES-1:0]           da_list_selection;
   reg [NB_SA_TYPE-1:0]                 sa_type;
   reg [NB_SRC_ADDR-1:0]                sa_set;
   reg [LOG2_N_ADDRESSES-1:0]           sa_list_selection;
   reg [NB_LENT_TYPE-1:0]               lent_type;
   reg [NB_LENTYP-1:0]                  lentyp_set;
   reg [NB_DATA_TYPE-1:0]               data_type;
   reg [NB_DATA-1:0]                    data_pattern;
   reg [NB_CRC_TYPE-1:0]                crc_type;
   reg                                  generate_crc;
   reg [NB_CRC32-1:0]                   set_crc;

   wire [NB_DA_TYPE-1:0]                da_type_i;
   wire [NB_DST_ADDR-1:0]               da_set_i;
   wire [LOG2_N_ADDRESSES-1:0]          da_list_selection_i;
   wire [NB_SA_TYPE-1:0]                sa_type_i;
   wire [NB_SRC_ADDR-1:0]               sa_set_i;
   wire [LOG2_N_ADDRESSES-1:0]          sa_list_selection_i;
   wire [NB_LENT_TYPE-1:0]              lent_type_i;
   wire [NB_LENTYP-1:0]                 lentyp_set_i;
   wire [NB_DATA_TYPE-1:0]              data_type_i;
   wire [NB_DATA-1:0]                   data_pattern_i;
   wire [NB_CRC_TYPE-1:0]               crc_type_i;
   wire                                 generate_crc_i;
   wire [NB_CRC32-1:0]                  set_crc_i;

   wire                                 missmatch_o;

   reg                                  valid=1'b1;
   wire                                 reset;
   //wire                                clock;
   reg [NB_TIMER-1:0]                   timer=10'h0;

   //Frame generator
   wire [NB_DATA-1:0]                  fg_data_o;
   wire [NB_DST_ADDR-1:0]              fg_destination_address_o;
   wire [NB_SRC_ADDR-1:0]              fg_source_address_o;
   wire [NB_LENTYP-1:0]                fg_lentype_o;
   wire [NB_CRC32-1:0]                 fg_fcs_o;
   wire                                fg_fcs_included_o;
   wire                                fg_data_valid_o;
   wire                                fg_frame_transmitted_o;
   wire [LOG2_MAX_FRAME_SIZE_BITS-1:0] fg_frame_size_bits_o;
   wire [LOG2_NB_DATA-1:0]             fg_tail_size_bits_o;
   wire                                fg_llc_frame_format_i;


   //Avalon-ST interface signals
   wire [NB_DATA-1:0]                  av_ff_tx_data_fgen;
   wire                                av_ff_tx_sop_fgen;
   wire                                av_ff_tx_eop_fgen;
   wire                                av_tx_rdy_fgen;
   wire                                av_ff_tx_wren_fgen;
   wire                                av_ff_tx_crc_fwd_fgen;
   wire                                av_tx_err_fgen;
   wire [LOG2_NBDATA_DIV4-1:0]         av_ff_tx_mod_fgen;
   wire                                av_ff_tx_septy_fgen;
   wire                                av_tx_ff_uflow_fgen;

   wire [NB_DATA-1:0]                  fgen_ff_rx_data_av;
   wire                                fgen_ff_rx_sop_av;
   wire                                fgen_ff_rx_eop_av;
   wire                                fgen_ff_rx_rdy_av;
   wire                                fgen_ff_rx_dval_av;
   wire                                fgen_ff_rx_dsav_av;
   wire [4-1:0]                        fgen_rx_frm_type_av;
   wire [17-1:0]                       fgen_rx_err_stat_av;
   wire [5-1:0]                        fgen_rx_err_av;
   wire [LOG2_NBDATA_DIV4-1:0]         fgen_ff_rx_mod_av;

   wire [LOG2_NB_DATA-1:0]             av_tailsizebits_checker;
   //GMII
   wire [NB_GMII-1:0]                  eth0_gmii_d_eth1   ;
   wire                                eth0_gmii_dv_eth1  ;
   wire                                eth0_gmii_err_eth1 ;

   wire                                tx0_ready_rx1;
   wire                                tx1_ready_rx0;

   wire [NB_DATA-1:0]                  dummy_data;
   wire                                dummy_eop;
   wire                                dummy_sop;
   wire [2-1:0]                        dummy_mod;
   wire [5-1:0]                        dummy_error;
   wire                                dummy_rx_valid;
   reg [NB_DATA-1:0]                   dummy_data_reg;
   reg                                 dummy_eop_reg;
   reg                                 dummy_sop_reg;
   reg [2-1:0]                         dummy_mod_reg;
   reg [5-1:0]                         dummy_error_reg;
   reg                                 dummy_rx_valid_reg;



   //*******************
   //LOGIC
   //*******************


   //assign clock = FPGA_CLK1_50;

   always @ ( posedge FPGA_CLK1_50 )
     begin if (timer < 10000000)
        timer   <= timer + 1'b1;
     end

   always @ ( posedge FPGA_CLK1_50 )
     if (reset)
       request_frame_i <= 1'b0;
     else if (valid)
       request_frame_i <= (timer == 10'h10) | frame_transmitted_o;

   always @(posedge FPGA_CLK1_50)
     begin
        dummy_data_reg <= dummy_data;
        dummy_eop_reg <= dummy_eop;
        dummy_mod_reg <= dummy_mod;
        dummy_error_reg <= dummy_error;
        dummy_rx_valid_reg <= dummy_rx_valid;
     end

   assign reset = (timer >= 10'h2) & (timer <= 10'h6); // Reset at time 2

   always @(posedge FPGA_CLK1_50) begin
      valid <= 1'b1;
      llc_frame_format <= 1'b1;
      da_type <= 2'b00;
      da_set <= 48'hAAAAAAAAAAAA;
      //da_set <= 48'h111111111111;
      sa_type <= 2'b00;
      sa_set <= 48'hBBBBBBBBBBBB;
      //sa_set <= 48'h222222222222;
      lent_type <= 2'b00;
      lentyp_set <= 16'h0008;
      data_type <= 2'b00;
      //data_pattern <= 64'hABCDABCDABCDABCD;
      data_pattern <= 32'haaaabbbb;
      crc_type <= 1'b0;
      generate_crc <= 1'b1;
      set_crc <= 32'hFFFFFFFF;
      //request_frame_i <= (timer == 5) | ;
   end // always @ (posedge FPGA_CLK1_50)

   assign da_type_i = da_type;
   assign da_set_i = da_set;
   assign da_list_selection_i = da_list_selection;
   assign sa_type_i = sa_type;
   assign sa_set_i = sa_set;
   assign sa_list_selection_i = sa_list_selection;
   assign lent_type_i = lent_type;
   assign lentyp_set_i = lentyp_set;
   assign data_type_i = data_type;
   assign data_pattern_i = data_pattern;
   assign crc_type_i = crc_type;
   assign generate_crc_i = generate_crc;
   assign set_crc_i = set_crc;
   assign frame_transmitted_o = fg_frame_transmitted_o;
   assign llc_frame_format_i = llc_frame_format;


   soc_system u0
     (
		  //.i2c_opencores_0_export_scl_pad_io(I2C2_CLK),
		  //.i2c_opencores_0_export_sda_pad_io(I2C2_DATA),


		  .hardware_revision_external_connection_export(hardware_revision),   				//   hardware_revision_external_connection.export

		  //Clock&Reset
	    .clk_clk                               (FPGA_CLK1_50 ),                        //  clk.clk
	    .reset_reset_n                         (reset        ),                        //  reset.reset_n
	    //HPS ddr3
	    .memory_mem_a                          ( HPS_DDR3_ADDR),                       //  memory.mem_a
	    .memory_mem_ba                         ( HPS_DDR3_BA),                         // .mem_ba
	    .memory_mem_ck                         ( HPS_DDR3_CK_P),                       // .mem_ck
	    .memory_mem_ck_n                       ( HPS_DDR3_CK_N),                       // .mem_ck_n
	    .memory_mem_cke                        ( HPS_DDR3_CKE),                        // .mem_cke
	    .memory_mem_cs_n                       ( HPS_DDR3_CS_N),                       // .mem_cs_n
	    .memory_mem_ras_n                      ( HPS_DDR3_RAS_N),                      // .mem_ras_n
	    .memory_mem_cas_n                      ( HPS_DDR3_CAS_N),                      // .mem_cas_n
	    .memory_mem_we_n                       ( HPS_DDR3_WE_N),                       // .mem_we_n
	    .memory_mem_reset_n                    ( HPS_DDR3_RESET_N),                    // .mem_reset_n
	    .memory_mem_dq                         ( HPS_DDR3_DQ),                         // .mem_dq
	    .memory_mem_dqs                        ( HPS_DDR3_DQS_P),                      // .mem_dqs
	    .memory_mem_dqs_n                      ( HPS_DDR3_DQS_N),                      // .mem_dqs_n
	    .memory_mem_odt                        ( HPS_DDR3_ODT),                        // .mem_odt
	    .memory_mem_dm                         ( HPS_DDR3_DM),                         // .mem_dm
	    .memory_oct_rzqin                      ( HPS_DDR3_RZQ),                        // .oct_rzqin
	    //HPS ethernet
	    .hps_0_hps_io_hps_io_emac1_inst_TX_CLK ( HPS_ENET_GTX_CLK),       					//  hps_0_hps_io.hps_io_emac1_inst_TX_CLK
	    .hps_0_hps_io_hps_io_emac1_inst_TXD0   ( HPS_ENET_TX_DATA[0] ),   					// .hps_io_emac1_inst_TXD0
	    .hps_0_hps_io_hps_io_emac1_inst_TXD1   ( HPS_ENET_TX_DATA[1] ),  					// .hps_io_emac1_inst_TXD1
	    .hps_0_hps_io_hps_io_emac1_inst_TXD2   ( HPS_ENET_TX_DATA[2] ),   					// .hps_io_emac1_inst_TXD2
	    .hps_0_hps_io_hps_io_emac1_inst_TXD3   ( HPS_ENET_TX_DATA[3] ),   					// .hps_io_emac1_inst_TXD3
	    .hps_0_hps_io_hps_io_emac1_inst_RXD0   ( HPS_ENET_RX_DATA[0] ),   					// .hps_io_emac1_inst_RXD0
	    .hps_0_hps_io_hps_io_emac1_inst_MDIO   ( HPS_ENET_MDIO ),         					// .hps_io_emac1_inst_MDIO
	    .hps_0_hps_io_hps_io_emac1_inst_MDC    ( HPS_ENET_MDC  ),         					// .hps_io_emac1_inst_MDC
	    .hps_0_hps_io_hps_io_emac1_inst_RX_CTL ( HPS_ENET_RX_DV),         					// .hps_io_emac1_inst_RX_CTL
	    .hps_0_hps_io_hps_io_emac1_inst_TX_CTL ( HPS_ENET_TX_EN),         					// .hps_io_emac1_inst_TX_CTL
	    .hps_0_hps_io_hps_io_emac1_inst_RX_CLK ( HPS_ENET_RX_CLK),        					// .hps_io_emac1_inst_RX_CLK
	    .hps_0_hps_io_hps_io_emac1_inst_RXD1   ( HPS_ENET_RX_DATA[1] ),   					// .hps_io_emac1_inst_RXD1
	    .hps_0_hps_io_hps_io_emac1_inst_RXD2   ( HPS_ENET_RX_DATA[2] ),   					// .hps_io_emac1_inst_RXD2
	    .hps_0_hps_io_hps_io_emac1_inst_RXD3   ( HPS_ENET_RX_DATA[3] ),   					// .hps_io_emac1_inst_RXD3
	    //HPS SD card
	    .hps_0_hps_io_hps_io_sdio_inst_CMD     ( HPS_SD_CMD    ),           				// .hps_io_sdio_inst_CMD
	    .hps_0_hps_io_hps_io_sdio_inst_D0      ( HPS_SD_DATA[0]     ),      				// .hps_io_sdio_inst_D0
	    .hps_0_hps_io_hps_io_sdio_inst_D1      ( HPS_SD_DATA[1]     ),      				// .hps_io_sdio_inst_D1
	    .hps_0_hps_io_hps_io_sdio_inst_CLK     ( HPS_SD_CLK   ),            				// .hps_io_sdio_inst_CLK
	    .hps_0_hps_io_hps_io_sdio_inst_D2      ( HPS_SD_DATA[2]     ),      				// .hps_io_sdio_inst_D2
	    .hps_0_hps_io_hps_io_sdio_inst_D3      ( HPS_SD_DATA[3]     ),      				// .hps_io_sdio_inst_D3
	    //HPS USB
	    .hps_0_hps_io_hps_io_usb1_inst_D0      ( HPS_USB_DATA[0]    ),      				// .hps_io_usb1_inst_D0
	    .hps_0_hps_io_hps_io_usb1_inst_D1      ( HPS_USB_DATA[1]    ),      				// .hps_io_usb1_inst_D1
	    .hps_0_hps_io_hps_io_usb1_inst_D2      ( HPS_USB_DATA[2]    ),      				// .hps_io_usb1_inst_D2
	    .hps_0_hps_io_hps_io_usb1_inst_D3      ( HPS_USB_DATA[3]    ),      				// .hps_io_usb1_inst_D3
	    .hps_0_hps_io_hps_io_usb1_inst_D4      ( HPS_USB_DATA[4]    ),      				// .hps_io_usb1_inst_D4
	    .hps_0_hps_io_hps_io_usb1_inst_D5      ( HPS_USB_DATA[5]    ),      				// .hps_io_usb1_inst_D5
	    .hps_0_hps_io_hps_io_usb1_inst_D6      ( HPS_USB_DATA[6]    ),      				// .hps_io_usb1_inst_D6
	    .hps_0_hps_io_hps_io_usb1_inst_D7      ( HPS_USB_DATA[7]    ),      				// .hps_io_usb1_inst_D7
	    .hps_0_hps_io_hps_io_usb1_inst_CLK     ( HPS_USB_CLKOUT    ),       				// .hps_io_usb1_inst_CLK
	    .hps_0_hps_io_hps_io_usb1_inst_STP     ( HPS_USB_STP    ),          				// .hps_io_usb1_inst_STP
	    .hps_0_hps_io_hps_io_usb1_inst_DIR     ( HPS_USB_DIR    ),          				// .hps_io_usb1_inst_DIR
	    .hps_0_hps_io_hps_io_usb1_inst_NXT     ( HPS_USB_NXT    ),          				// .hps_io_usb1_inst_NXT

		  //HPS SPI 0
		  .hps_0_hps_io_hps_io_spim0_inst_CLK		(HPS_SPIM0_CLK),   							// .hps_io_spim0_inst_CLK
		  .hps_0_hps_io_hps_io_spim0_inst_MOSI	(HPS_SPIM0_MOSI),  							// .hps_io_spim0_inst_MOSI
		  .hps_0_hps_io_hps_io_spim0_inst_MISO	(HPS_SPIM0_MISO),  							// .hps_io_spim0_inst_MISO
		  .hps_0_hps_io_hps_io_spim0_inst_SS0		(HPS_SPIM0_SS),   							// .hps_io_spim0_inst_SS0

		  //HPS UART
	    .hps_0_hps_io_hps_io_uart0_inst_RX     ( HPS_UART_RX   ),          				// .hps_io_uart0_inst_RX
	    .hps_0_hps_io_hps_io_uart0_inst_TX     ( HPS_UART_TX   ),          				// .hps_io_uart0_inst_TX
		  //HPS I2C1
	    .hps_0_hps_io_hps_io_i2c0_inst_SDA     ( HPS_I2C0_SDAT  ),        					//  .hps_io_i2c0_inst_SDA
	    .hps_0_hps_io_hps_io_i2c0_inst_SCL     ( HPS_I2C0_SCLK  ),        					// .hps_io_i2c0_inst_SCL
		  //HPS I2C2
	    .hps_0_hps_io_hps_io_i2c1_inst_SDA     ( HPS_I2C1_SDAT  ),        					// .hps_io_i2c1_inst_SDA
	    .hps_0_hps_io_hps_io_i2c1_inst_SCL     ( HPS_I2C1_SCLK  ),        					// .hps_io_i2c1_inst_SCL
		  //GPIO
	    .hps_0_hps_io_hps_io_gpio_inst_GPIO00  ( HPS_OVERTEMPN ),
	    .hps_0_hps_io_hps_io_gpio_inst_GPIO09  ( HPS_CONV_USB_N ),  							//.hps_io_gpio_inst_GPIO09
	    .hps_0_hps_io_hps_io_gpio_inst_GPIO35  ( HPS_ENET_INT_N ),  							//.hps_io_gpio_inst_GPIO35
	    .hps_0_hps_io_hps_io_gpio_inst_GPIO37  ( HPS_3p3V_GPIO1),           				//.hps_io_gpio_inst_GPIO37
	    .hps_0_hps_io_hps_io_gpio_inst_GPIO40  ( HPS_SPISW_A0   ),  							//.hps_io_gpio_inst_GPIO40
	    .hps_0_hps_io_hps_io_gpio_inst_GPIO41  ( HPS_SPISW_ENN),           				//.hps_io_gpio_inst_GPIO41
	    .hps_0_hps_io_hps_io_gpio_inst_GPIO42  ( HPS_USB_RESET),           				//.hps_io_gpio_inst_GPIO42
	    .hps_0_hps_io_hps_io_gpio_inst_GPIO43  ( HPS_ENET_RESET_N),           			//.hps_io_gpio_inst_GPIO43
	    .hps_0_hps_io_hps_io_gpio_inst_GPIO48  ( HPS_3p3V_GPIO0),           				//.hps_io_gpio_inst_GPIO48

	    .hps_0_hps_io_hps_io_gpio_inst_GPIO61  ( HPS_ALERTN ),  								// .hps_io_gpio_inst_GPIO61
	    .hps_0_hps_io_hps_io_gpio_inst_GPIO53  ( HPS_LED   ),  								// .hps_io_gpio_inst_GPIO53
	    .hps_0_hps_io_hps_io_gpio_inst_GPIO54  ( HPS_KEY   ),  								// .hps_io_gpio_inst_GPIO54

	    .hps_0_f2h_stm_hw_events_stm_hwevents  (stm_hw_events),  								//  hps_0_f2h_stm_hw_events.stm_hwevents
	    //   .hps_0_h2f_reset_reset_n               (hps_fpga_reset_n),   							//  hps_0_h2f_reset.reset_n
      .hps_0_f2h_warm_reset_req_reset_n      (~hps_warm_reset),      						//  hps_0_f2h_warm_reset_req.reset_n
	    .hps_0_f2h_debug_reset_req_reset_n     (~hps_debug_reset),     						//  hps_0_f2h_debug_reset_req.reset_n
	    .hps_0_f2h_cold_reset_req_reset_n      (~hps_cold_reset),     						//  hps_0_f2h_cold_reset_req.reset_n


      // TSE
      //Only RX
      .eth_tse_0_receive_data                    (fgen_ff_rx_data_av        ),                    //              eth_tse_0_receive.data
      .eth_tse_0_receive_endofpacket             (fgen_ff_rx_eop_av         ),             //                               .endofpacket
      .eth_tse_0_receive_error                   (fgen_rx_err_av            ),                   //                               .error
      .eth_tse_0_receive_empty                   (fgen_ff_rx_mod_av         ) ,                   //                               .empty
      .eth_tse_0_receive_ready                   (tx1_ready_rx0             ),                   //                               .ready
      .eth_tse_0_receive_startofpacket           (fgen_ff_rx_sop_av         ),           //                               .startofpacket
      .eth_tse_0_receive_valid                   (fgen_ff_rx_dval_av        ),                   //                               .valid
      .eth_tse_0_transmit_data                   (av_ff_tx_data_fgen        ),                   //             eth_tse_0_transmit.data
      .eth_tse_0_transmit_endofpacket            (av_ff_tx_eop_fgen         ),            //                               .endofpacket
      .eth_tse_0_transmit_error                  (av_tx_err_fgen            ),                  //                               .error
      .eth_tse_0_transmit_empty                  (av_ff_tx_mod_fgen         ),                  //                               .empty
      .eth_tse_0_transmit_ready                  (tx0_ready_rx1             ),                  //                               .ready
      .eth_tse_0_transmit_startofpacket          (av_ff_tx_sop_fgen         ),          //                               .startofpacket
      .eth_tse_0_transmit_valid                  (av_ff_tx_wren_fgen        ),                  //                               .valid

      //Only TX
      .eth_tse_1_receive_data                    (dummy_data                ),                    //              eth_tse_1_receive.data
      .eth_tse_1_receive_endofpacket             (dummy_eop                 ),             //                               .endofpacket
      .eth_tse_1_receive_error                   (dummy_error               ),                   //                               .error
      .eth_tse_1_receive_empty                   (dummy_mod                 ),                   //                               .empty
      .eth_tse_1_receive_ready                   (tx0_ready_rx1             ),                   //                               .ready
      .eth_tse_1_receive_startofpacket           (dummy_sop                 ),           //                               .startofpacket
      .eth_tse_1_receive_valid                   (dummy_rx_valid            ),                   //                               .valid
      .eth_tse_1_transmit_data                   (av_ff_tx_data_fgen        ),                   //             eth_tse_1_transmit.data
      .eth_tse_1_transmit_endofpacket            (av_ff_tx_eop_fgen         ),            //                               .endofpacket
      .eth_tse_1_transmit_error                  (av_tx_err_fgen            ),                  //                               .error
      .eth_tse_1_transmit_empty                  (av_ff_tx_mod_fgen/*MODto0*/),//ALIAS MOD
      .eth_tse_1_transmit_ready                  (tx1_ready_rx0             ),                  //                               .ready
      .eth_tse_1_transmit_startofpacket          (av_ff_tx_sop_fgen         ),          //                               .startofpacket
      .eth_tse_1_transmit_valid                  (av_ff_tx_wren_fgen        ),                  //                               .valid

      .eth_tse_1_mac_gmii_connection_gmii_rx_d   (eth0_gmii_d_eth1          ),   // eth_tse_1_mac_gmii_connection.gmii_rx_d
      .eth_tse_1_mac_gmii_connection_gmii_rx_dv  (eth0_gmii_dv_eth1         ),  //                              .gmii_rx_dv
      .eth_tse_1_mac_gmii_connection_gmii_rx_err (eth0_gmii_err_eth1        ), //                              .gmii_rx_err
      .eth_tse_1_mac_gmii_connection_gmii_tx_d   (eth0_gmii_d_eth1          ),   //                              .gmii_tx_d
      .eth_tse_1_mac_gmii_connection_gmii_tx_en  (eth0_gmii_dv_eth1         ),  //                              .gmii_tx_en
      .eth_tse_1_mac_gmii_connection_gmii_tx_err (eth0_gmii_err_eth1        )  //                              .gmii_tx_err
      );

   avalon_st_framegen_interface
     #(
       .NB_DATA                   (NB_DATA                   ),
       .LOG2_NB_DATA              (LOG2_NB_DATA              ),
       .MIN_FRAME_SIZE_BYTES      (MIN_FRAME_SIZE_BYTES      ),
       .MAX_FRAME_SIZE_BYTES      (MAX_FRAME_SIZE_BYTES      ),
       .LOG2_MAX_FRAME_SIZE_BYTES (LOG2_MAX_FRAME_SIZE_BYTES ),
       .LOG2_MAX_FRAME_SIZE_BITS  (LOG2_MAX_FRAME_SIZE_BITS  ),
       .NB_DST_ADDR               (NB_DST_ADDR               ),
       .NB_SRC_ADDR               (NB_SRC_ADDR               ),
       .NB_LENTYP                 (NB_LENTYP                 ),
       .NB_CRC32                  (NB_CRC32                  ),
       .CRC32_POLYNOMIAL          (CRC32_POLYNOMIAL          ),
       .NB_DA_TYPE                (NB_DA_TYPE                ),
       .NB_SA_TYPE                (NB_SA_TYPE                ),
       .NB_LENT_TYPE              (NB_LENT_TYPE              ),
       .NB_DATA_TYPE              (NB_DATA_TYPE              ),
       .NB_CRC_TYPE               (NB_CRC_TYPE               ),
       .N_ADDRESSES               (N_ADDRESSES               ),
       .LOG2_N_ADDRESSES          (LOG2_N_ADDRESSES          ),
       .LOG2_NBDATA_DIV4          (LOG2_NBDATA_DIV4          )
       )

   u_avalon_st_framegen_interface
     (
      //From AV_ST_IF to Altera MAC
      .o_ff_tx_data                 (av_ff_tx_data_fgen        ),
      .o_ff_tx_sop                  (av_ff_tx_sop_fgen         ),
      .o_ff_tx_eop                  (av_ff_tx_eop_fgen         ),
      .o_tx_rdy                     (av_tx_rdy_fgen            ),
      .o_ff_tx_wren                 (av_ff_tx_wren_fgen        ),
      .o_ff_tx_crc_fwd              (av_ff_tx_crc_fwd_fgen     ),
      .o_tx_err                     (av_tx_err_fgen            ),
      .o_ff_tx_mod                  (av_ff_tx_mod_fgen         ),
      .o_ff_tx_septy                (av_ff_tx_septy_fgen       ),
      .o_tx_ff_uflow                (av_tx_ff_uflow_fgen       ),

      //From Fgen to AV_ST_IF
      .i_data                       (fg_data_o                 ),
      .i_fcs_included               (fg_fcs_included_o         ),
      .i_data_valid                 (fg_data_valid_o           ),
      .i_frame_transmitted          (fg_frame_transmitted_o    ),
      .i_frame_size_bits            (fg_frame_size_bits_o      ),
      .i_tail_size_bits             (fg_tail_size_bits_o       ),

      //From AV_ST_IF to Fchecker
      .o_data                       (data_o                    ),
      .o_data_valid                 (data_valid_o              ),
      .o_tail_size_bits             (av_tailsizebits_checker   ),

      //From Altera MAC to AV_ST_IF
      .i_ff_rx_data                 (fgen_ff_rx_data_av        ),
      .i_ff_rx_sop                  (fgen_ff_rx_sop_av         ),
      .i_ff_rx_eop                  (fgen_ff_rx_eop_av         ),
      .i_ff_rx_rdy                  (fgen_ff_rx_rdy_av         ),
      .i_ff_rx_dval                 (fgen_ff_rx_dval_av        ),
      .i_ff_rx_dsav                 (fgen_ff_rx_dsav_av        ),
      .i_rx_frm_type                (fgen_rx_frm_type_av       ),
      .i_rx_err_stat                (fgen_rx_err_stat_av       ),
      .i_rx_err                     (fgen_rx_err_av            ),
      .i_ff_rx_mod                  (fgen_ff_rx_mod_av         ),

      .i_clock                      (FPGA_CLK1_50              ),
      .i_valid                      (valid                     ),
      .i_reset                      (reset                     )
      );


   frame_generator
     #(
       .NB_DATA                   (NB_DATA                   ),
       .LOG2_NB_DATA              (LOG2_NB_DATA              ),
       .MIN_FRAME_SIZE_BYTES      (MIN_FRAME_SIZE_BYTES      ),
       .MAX_FRAME_SIZE_BYTES      (MAX_FRAME_SIZE_BYTES      ),
       .LOG2_MAX_FRAME_SIZE_BYTES (LOG2_MAX_FRAME_SIZE_BYTES ),
       .LOG2_MAX_FRAME_SIZE_BITS  (LOG2_MAX_FRAME_SIZE_BITS  ),
       .NB_DST_ADDR               (NB_DST_ADDR               ),
       .NB_SRC_ADDR               (NB_SRC_ADDR               ),
       .NB_LENTYP                 (NB_LENTYP                 ),
       .NB_CRC32                  (NB_CRC32                  ),
       .CRC32_POLYNOMIAL          (CRC32_POLYNOMIAL          ),
       .NB_DA_TYPE                (NB_DA_TYPE                ),
       .NB_SA_TYPE                (NB_SA_TYPE                ),
       .NB_LENT_TYPE              (NB_LENT_TYPE              ),
       .NB_DATA_TYPE              (NB_DATA_TYPE              ),
       .NB_CRC_TYPE               (NB_CRC_TYPE               ),
       .N_ADDRESSES               (N_ADDRESSES               ),
       .LOG2_N_ADDRESSES          (LOG2_N_ADDRESSES          )
       )
   u_frame_generator
     (
      //From Fgen to AV_ST_IF
      .o_data                     (fg_data_o                ),
      .o_destination_address      (fg_destination_address_o ),
      .o_source_address           (fg_source_address_o      ),
      .o_lentype                  (fg_lentype_o             ),
      .o_fcs                      (fg_fcs_o                 ),
      .o_fcs_included             (fg_fcs_included_o        ),
      .o_data_valid               (fg_data_valid_o          ),
      .o_frame_transmitted        (fg_frame_transmitted_o   ),
      .o_frame_size_bits          (fg_frame_size_bits_o     ),
      .o_tail_size_bits           (fg_tail_size_bits_o      ),

      //From top to Fgen
      .i_llc_frame_format         (llc_frame_format_i       ),
      .i_request_frame            (request_frame_i          ),
      .i_da_type                  (da_type_i                ),
      .i_da_set                   (da_set_i                 ),
      .i_da_list_selection        (da_list_selection_i      ),
      .i_sa_type                  (sa_type_i                ),
      .i_sa_set                   (sa_set_i                 ),
      .i_sa_list_selection        (sa_list_selection_i      ),
      .i_lent_type                (lent_type_i              ),
      .i_lentyp_set               (lentyp_set_i             ),
      .i_data_type                (data_type_i              ),
      .i_data_pattern             (data_pattern_i           ),
      .i_crc_type                 (crc_type_i               ),
      .i_generate_crc             (generate_crc_i           ),
      .i_set_crc                  (set_crc_i                ),
      .i_valid                    (valid                    ),
      .i_reset                    (reset                    ),
      .i_clock                    (FPGA_CLK1_50             )
      );

   frame_checker
     #(
       .NB_DATA                   (NB_DATA                   ),
       .LOG2_NB_DATA              (LOG2_NB_DATA              ),
       .MIN_FRAME_SIZE_BYTES      (MIN_FRAME_SIZE_BYTES      ),
       .MAX_FRAME_SIZE_BYTES      (MAX_FRAME_SIZE_BYTES      ),
       .LOG2_MAX_FRAME_SIZE_BYTES (LOG2_MAX_FRAME_SIZE_BYTES ),
       .NB_DST_ADDR               (NB_DST_ADDR               ),
       .NB_SRC_ADDR               (NB_SRC_ADDR               ),
       .NB_LENTYP                 (NB_LENTYP                 ),
       .NB_CRC32                  (NB_CRC32                  ),
       .CRC32_POLYNOMIAL          (CRC32_POLYNOMIAL          ),
       .NB_DA_TYPE                (NB_DA_TYPE                ),
       .NB_SA_TYPE                (NB_SA_TYPE                ),
       .NB_LENT_TYPE              (NB_LENT_TYPE              ),
       .NB_DATA_TYPE              (NB_DATA_TYPE              ),
       .NB_CRC_TYPE               (NB_CRC_TYPE               ),
       .N_ADDRESSES               (N_ADDRESSES               ),
       .LOG2_N_ADDRESSES          (LOG2_N_ADDRESSES          )
       )
   u_frame_checker
     (
      //From Fchecker to top
      .o_missmatch               (missmatch_o               ),

      //From AV_ST_IF to Fcheck
      .i_data                    (data_o                    ),
      .i_data_valid              (data_valid_o              ),
      .i_tail_size_bits          (av_tailsizebits_checker   ),

      //From top to Fcheck
      .i_da_type                 (da_type_i                 ),
      .i_da_set                  (da_set_i                  ),
      .i_da_list_selection       (da_list_selection_i       ),
      .i_sa_type                 (sa_type_i                 ),
      .i_sa_set                  (sa_set_i                  ),
      .i_sa_list_selection       (sa_list_selection_i       ),
      .i_lent_type               (lent_type_i               ),
      .i_lentyp_set              (lentyp_set_i              ),
      .i_data_type               (data_type_i               ),
      .i_data_pattern            (data_pattern_i            ),
      .i_crc_type                (crc_type_i                ),
      .i_generate_crc            (generate_crc_i            ),
      .i_set_crc                 (set_crc_i                 ),
      .i_llc_frame_format        (llc_frame_format_i        ),
      .i_valid                   (valid                     ),
      .i_reset                   (reset                     ),
      .i_clock                   (FPGA_CLK1_50              )
      );



   // Source/Probe megawizard instance
   hps_reset hps_reset_inst (
                             .source_clk (FPGA_CLK1_50),
                             .source     (hps_reset_req)
                             );

   altera_edge_detector pulse_cold_reset (
                                          .clk       (FPGA_CLK1_50),
                                          .rst_n     (hps_fpga_reset_n),
                                          .signal_in (hps_reset_req[0]),
                                          .pulse_out (hps_cold_reset)
                                          );
   defparam pulse_cold_reset.PULSE_EXT = 6;
   defparam pulse_cold_reset.EDGE_TYPE = 1;
   defparam pulse_cold_reset.IGNORE_RST_WHILE_BUSY = 1;

   altera_edge_detector pulse_warm_reset (
                                          .clk       (FPGA_CLK1_50),
                                          .rst_n     (hps_fpga_reset_n),
                                          .signal_in (hps_reset_req[1]),
                                          .pulse_out (hps_warm_reset)
                                          );
   defparam pulse_warm_reset.PULSE_EXT = 2;
   defparam pulse_warm_reset.EDGE_TYPE = 1;
   defparam pulse_warm_reset.IGNORE_RST_WHILE_BUSY = 1;

   altera_edge_detector pulse_debug_reset (
                                           .clk       (FPGA_CLK1_50),
                                           .rst_n     (hps_fpga_reset_n),
                                           .signal_in (hps_reset_req[2]),
                                           .pulse_out (hps_debug_reset)
                                           );
   defparam pulse_debug_reset.PULSE_EXT = 32;
   defparam pulse_debug_reset.EDGE_TYPE = 1;
   defparam pulse_debug_reset.IGNORE_RST_WHILE_BUSY = 1;

endmodule

