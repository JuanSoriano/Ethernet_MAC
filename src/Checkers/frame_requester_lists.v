module frame_requester_lists
  #(
    parameter NB_DATA                   = 64,
    parameter NB_DATA_PRBS              = 64,
    parameter LOG2_NB_DATA              = 6,
    parameter MIN_FRAME_SIZE_BYTES      = 0,
    parameter MAX_FRAME_SIZE_BYTES      = 1500,
    parameter LOG2_MAX_FRAME_SIZE_BYTES = 11,
    parameter LOG2_MAX_FRAME_SIZE_BITS  = 14,

    parameter NB_DST_ADDR               = 48,
    parameter NB_SRC_ADDR               = 48,
    parameter NB_LENTYP                 = 16,
    parameter NB_CRC32                  = 32,
    parameter CRC32_POLYNOMIAL          = 33'h104C11DB7,

    parameter NB_DA_TYPE                = 2,
    parameter NB_SA_TYPE                = 2,
    parameter NB_LENT_TYPE              = 2,
    parameter NB_DATA_TYPE              = 2,
    parameter NB_CRC_TYPE               = 1,

    parameter N_ADDRESSES               = 10,
    parameter LOG2_N_ADDRESSES          = 4
    )
   (
    output reg                         o_request_frame, //A 1 clock pulse requesting a frame generation

    output wire [NB_DA_TYPE-1:0]       o_da_type, //00: Fixed, 01: from list, 10: random not in list
    output wire [NB_DST_ADDR-1:0]      o_da_set, //If i_da_type fixed, use this value
    output wire [LOG2_N_ADDRESSES-1:0] o_da_list_selection,
    output wire [NB_SA_TYPE-1:0]       o_sa_type, //00: Fixed, 01: from list, 10: random not in list
    output wire [NB_SRC_ADDR-1:0]      o_sa_set, //If i_sa_type fixed, use this value
    output wire [LOG2_N_ADDRESSES-1:0] o_sa_list_selection,
    output wire [NB_LENT_TYPE-1:0]     o_lent_type, //00: Fixed, 01: PRBS, 10: too long
    output wire [NB_LENTYP-1:0]        o_lentyp_set,
    output wire [NB_DATA_TYPE-1:0]     o_data_type, //00: Repeat pattern, 01: byte count, 10: prbs31
    output wire [NB_DATA-1:0]          o_data_pattern,
    output wire [NB_CRC_TYPE-1:0]      o_crc_type, // 0: good, 1: bad
    output wire                        o_generate_crc,
    output wire [NB_CRC32-1:0]         o_set_crc,

    input wire                         i_frame_transmitted,

    input wire                         i_brute_force_toggle,

    input                              i_reset,
    input                              i_clock
    );
   localparam N_FRAMES_PRESET_ONE = 10;
   localparam NB_TIMER = 20;

   wire [NB_DST_ADDR-1:0]              dst_addr_list [N_ADDRESSES-1:0];

   wire [NB_DST_ADDR-1:0]              DA_preset_ONE [N_FRAMES_PRESET_ONE-1:0];
   wire [NB_SRC_ADDR-1:0]              SA_preset_ONE [N_FRAMES_PRESET_ONE-1:0];
   wire [NB_LENTYP-1:0]                LENTYP_preset_ONE [N_FRAMES_PRESET_ONE-1:0];

   //Inputs for frame gen
   wire [2-1:0]                        da_type [N_FRAMES_PRESET_ONE-1:0];
   wire [2-1:0]                        sa_type [N_FRAMES_PRESET_ONE-1:0];
   wire [2-1:0]                        lent_type [N_FRAMES_PRESET_ONE-1:0];
   wire [2-1:0]                        data_type [N_FRAMES_PRESET_ONE-1:0];
   wire [N_FRAMES_PRESET_ONE-1:0]      CRC_type ;
   wire [N_FRAMES_PRESET_ONE-1:0]      CRC_generate ;

   reg [NB_TIMER-1:0]                  timer;
   reg [NB_TIMER-1:0]                  timer_lentyp;
   wire                                timer_lentyp_timeout;
   wire                                timer_reset;
   reg                                 brute_force_on;
   reg                                 brute_force_done;
   wire                                timer_done;

   always @(posedge i_clock)
     begin
        if (i_reset)
          brute_force_on <= 1'b0;
        else if (i_brute_force_toggle)
          brute_force_on <= ~brute_force_on;
     end

   assign timer_reset = (brute_force_on) ? timer == 1501 : timer == 7 ;
   assign timer_done = (brute_force_on) ? timer == 1500 : timer == 6;

   always @(posedge i_clock) begin
      if (i_reset)
        brute_force_done <= 1'b0;
      else if (brute_force_on & (timer_reset | timer_done))
        brute_force_done <= 1'b1;
   end

   always @(posedge i_clock) begin
     if(i_reset | timer_reset & ~brute_force_on)
       timer <= {NB_TIMER{1'b0}};
     else if (i_frame_transmitted | (brute_force_on & timer_lentyp_timeout ) & ~timer_done)
       timer <= timer + 1'b1;
  end

   always @(posedge i_clock) begin
      if (i_reset)
        o_request_frame <= 1'b0;
      else
        casez ({brute_force_on,brute_force_done})
          2'b0?: o_request_frame <= i_frame_transmitted;
          2'b10: o_request_frame <= timer_lentyp_timeout;
          2'b11: o_request_frame <= 1'b0;
          default: o_request_frame <= 1'b0;
        endcase
   end

   always @(posedge i_clock) begin
      if (i_reset | timer_lentyp_timeout)
        timer_lentyp <= 'b0;
      else if (brute_force_on)
        timer_lentyp <= timer_lentyp + 1'b1;
   end

   assign timer_lentyp_timeout = (timer_lentyp == 5000);

   assign o_da_type = (brute_force_on) ? da_type[0]: da_type[timer];
   assign o_da_set = (brute_force_on) ? DA_preset_ONE[0]: DA_preset_ONE[timer];
   assign o_da_list_selection = 'b0;
   assign o_sa_type = (brute_force_on) ? sa_type[0]: sa_type[timer];
   assign o_sa_set = (brute_force_on) ? SA_preset_ONE[0]: SA_preset_ONE[timer];
   assign o_sa_list_selection = 'b0;
   assign o_lent_type = (brute_force_on) ? lent_type[0]: lent_type[timer];
   assign o_lentyp_set = (brute_force_on) ? timer : LENTYP_preset_ONE[timer];
   assign o_data_type = (brute_force_on) ? data_type[3] : data_type[timer];
   assign o_data_pattern = (brute_force_on) ? 64'h1234_5678_1234_5678 : 64'h1234_5678_1234_5678;
   assign o_crc_type = (brute_force_on) ? CRC_type[0] : CRC_type[timer];
   assign o_generate_crc = (brute_force_on) ? CRC_generate[0] : CRC_generate[timer];
   assign o_set_crc = 'b0;

   //DST_ADDR list
   assign   dst_addr_list[ 0 ]     = 48'h00CD_ABCD_ABCD ;
   assign   dst_addr_list[ 1 ]     = 48'h00CD_ABCD_ABCD ;
   assign   dst_addr_list[ 2 ]     = 48'h00CD_ABCD_ABCD ;
   assign   dst_addr_list[ 3 ]     = 48'h00CD_ABCD_ABCD ;
   assign   dst_addr_list[ 4 ]     = 48'h00CD_FFFF_ABCD ; //bad
   assign   dst_addr_list[ 5 ]     = 48'h00CD_ABCD_ABCD ;
   assign   dst_addr_list[ 6 ]     = 48'h00CD_ABCD_ABCD ;
   assign   dst_addr_list[ 7 ]     = 48'd7 ;
   assign   dst_addr_list[ 8 ]     = 48'd8 ;
   assign   dst_addr_list[ 9 ]     = 48'd9 ;

   //Preset start
   assign DA_preset_ONE[0] = dst_addr_list[0];
   assign DA_preset_ONE[1] = dst_addr_list[1];
   assign DA_preset_ONE[2] = dst_addr_list[2];
   assign DA_preset_ONE[3] = dst_addr_list[3];
   assign DA_preset_ONE[4] = dst_addr_list[4];
   assign DA_preset_ONE[5] = dst_addr_list[5];
   assign DA_preset_ONE[6] = dst_addr_list[6];
   assign DA_preset_ONE[7] = dst_addr_list[7];
   assign DA_preset_ONE[8] = dst_addr_list[8];
   assign DA_preset_ONE[9] = dst_addr_list[9];

   assign SA_preset_ONE[0] = {48'hABCD_FAFA_ABCD};
   assign SA_preset_ONE[1] = {48'hABCD_FAFA_ABCD};
   assign SA_preset_ONE[2] = {48'hABCD_FAFA_ABCD};
   assign SA_preset_ONE[3] = {48'hABCD_FAFA_ABCD};
   assign SA_preset_ONE[4] = {48'hABCD_FAFA_ABCD};
   assign SA_preset_ONE[5] = {48'hABCD_FAFA_ABCD};
   assign SA_preset_ONE[6] = {48'hABCD_FAFA_ABCD};
   assign SA_preset_ONE[7] = {48'hABCD_FAFA_ABCD};
   assign SA_preset_ONE[8] = {48'hABCD_FAFA_ABCD};
   assign SA_preset_ONE[9] = {48'hABCD_FAFA_ABCD};

   assign LENTYP_preset_ONE[0] = 16'd100;
   assign LENTYP_preset_ONE[1] = 16'd200;
   assign LENTYP_preset_ONE[2] = 16'd20;
   assign LENTYP_preset_ONE[3] = 16'd1200;
   assign LENTYP_preset_ONE[4] = 16'd100;
   assign LENTYP_preset_ONE[5] = 16'd200;
   assign LENTYP_preset_ONE[6] = 16'd50;
   assign LENTYP_preset_ONE[7] = 16'd50;
   assign LENTYP_preset_ONE[8] = 16'd50;
   assign LENTYP_preset_ONE[9] = 16'd50;

   //Signals to select options in frame generator
   //00: Fixed, 01: from list, 10: random not in list
   assign da_type[0] = 2'b00;
   assign da_type[1] = 2'b00;
   assign da_type[2] = 2'b00;
   assign da_type[3] = 2'b00;
   assign da_type[4] = 2'b00;
   assign da_type[5] = 2'b00;
   assign da_type[6] = 2'b00;
   assign da_type[7] = 2'b00;
   assign da_type[8] = 2'b00;
   assign da_type[9] = 2'b00;

   //00: Fixed, 01: from list, 10: random not in list
   assign sa_type[0] = 2'b00;
   assign sa_type[1] = 2'b00;
   assign sa_type[2] = 2'b00;
   assign sa_type[3] = 2'b00;
   assign sa_type[4] = 2'b00;//PRBS
   assign sa_type[5] = 2'b00;
   assign sa_type[6] = 2'b00;
   assign sa_type[7] = 2'b00;
   assign sa_type[8] = 2'b00;
   assign sa_type[9] = 2'b00;

   //00: Fixed, 01: PRBS, 10: too long
   assign lent_type[0] = 2'b00;
   assign lent_type[1] = 2'b00;
   assign lent_type[2] = 2'b00;
   assign lent_type[3] = 2'b00;
   assign lent_type[4] = 2'b00;
   assign lent_type[5] = 2'b00;
   assign lent_type[6] = 2'b00;
   assign lent_type[7] = 2'b00;
   assign lent_type[8] = 2'b00;
   assign lent_type[9] = 2'b00;

   //00: Repeat pattern, 01: byte count, 10: prbs31
   assign data_type[0] = 2'b00;
   assign data_type[1] = 2'b00;
   assign data_type[2] = 2'b00;
   assign data_type[3] = 2'b10;
   assign data_type[4] = 2'b00;
   assign data_type[5] = 2'b00;
   assign data_type[6] = 2'b00;
   assign data_type[7] = 2'b00;
   assign data_type[8] = 2'b01;
   assign data_type[9] = 2'b01;

   // 0: good, 1: bad
   assign CRC_type[0] = 0;
   assign CRC_type[1] = 0;
   assign CRC_type[2] = 0;
   assign CRC_type[3] = 0;
   assign CRC_type[4] = 0;
   assign CRC_type[5] = 1;
   assign CRC_type[6] = 0;
   assign CRC_type[7] = 0;
   assign CRC_type[8] = 0;
   assign CRC_type[9] = 0;

   assign CRC_generate[0] = 0;
   assign CRC_generate[1] = 0;
   assign CRC_generate[2] = 0;
   assign CRC_generate[3] = 0;
   assign CRC_generate[4] = 0;
   assign CRC_generate[5] = 1;
   assign CRC_generate[6] = 0;
   assign CRC_generate[7] = 0;
   assign CRC_generate[8] = 0;
   assign CRC_generate[9] = 0;

endmodule
