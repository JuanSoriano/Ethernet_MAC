/* preamble:7*8, sofd:8, dst-addr:6*8,  src-addr:6*8, length/dt:2*8, data:46*8--1500*8/1504*8--1982*8, crc:4*8, ext:??? */
//7+1+6+6+2=22 --> +46

/* dst-addr:6*8,  src-addr:6*8, length/dt:2*8, data:46*8--1500*8/1504*8--1982*8, crc:4*8, ext:??? */



// [NOTE] The block is designed to work with a parallesim multiple of 8bits, and no greater than 512bits.
// [NOTE] We assume that extension bits are not required for the above layer and are thus dropped.

// P=C*f*(V**2).


module frame_generator_b
  #(
    // Parameters.
    parameter                                   NB_DATA         = 64 ,
    parameter                                   LOG2_NB_DATA    = 6 ,
    parameter                                   LOG2_FRAME_SIZE = 14 ,
    parameter                                   NB_DST_ADDR     = 48 ,    // HINT: This is the only legal value.
    parameter                                   NB_SRC_ADDR     = 48 ,    // HINT: This is the only legal value.
    parameter                                   NB_LENTYP       = 16 ,    // HINT: This is the only legal value.
    parameter                                   NB_CRC          = 32      // HINT: This is the only legal value.
    )
   (
    // Outputs.
    output reg [NB_DATA-1:0]                                                 o_data ,
    output reg                                                               o_data_valid ,
    output reg [1982-1:0]                                                    o_datasize_bytes,
    output reg [NB_DST_ADDR + NB_SRC_ADDR + NB_LENTYP + NB_CRC + 1982*8-1:0] o_framesize_bits,

    // Inputs.
    input wire                                                               i_valid ,
    input wire                                                               i_frame_ready , // It is a pulse that indicates that a frame has arrived to the RX FIFO
    input wire                                                               i_reset ,
    input wire                                                               i_clock
    ) ;

   //==========================================================================
   // LOCAL PARAMETERS.
   //==========================================================================
   localparam                                  NB_TIMER                = LOG2_FRAME_SIZE ;

   // FSM States
   localparam                                  NB_STATE                = 3 ;
   localparam                                  N_LEGAL_DST_ADDR        = 10 ;
   localparam                                  N_LEGAL_SRC_ADDR        = 10 ;
   localparam                                  MIN_LENGTH_A_BYTES      = 46 ;
   localparam                                  MAX_LENGTH_A_BYTES      = 1500 ;
   localparam                                  MIN_LENGTH_B_BYTES      = 1504 ;
   localparam                                  MAX_LENGTH_B_BYTES      = 1982 ;
   localparam                                  NB_BYTE                 = 8 ;
   localparam                                  MAX_IF_GAP              = 100 ;
   localparam                                  MIN_IF_GAP              = 10 ;
   localparam                                  NB_BUFFER               = NB_DST_ADDR + NB_SRC_ADDR + NB_LENTYP + NB_CRC + MAX_LENGTH_B_BYTES*8 ;



   //==========================================================================
   // INTERNAL SIGNALS.
   //==========================================================================
   reg [NB_DATA-1:0]                                                         buffer_array [NB_BUFFER/NB_DATA-1:0] ;

   reg                                                                       frame_ready_d ;
   wire                                                                      frame_ready_pos ;


   reg [6-1:0]                                                               dst_addr      ;
   reg [6-1:0]                                                               src_addr      ;
   reg                                                                       data_mode     ;
   reg [LOG2_FRAME_SIZE-1:0]                                                 length        ;
   reg [LOG2_FRAME_SIZE-1:0]                                                 length_clocks ;
   integer                                                                   if_gap        ;
   reg [LOG2_FRAME_SIZE-1:0]                                                 frame_size    ;
   integer                                                                   frame_clocks  ;

   reg                                                                       load_buffer   ;
   reg                                                                       transmitting  ;
   reg                                                                       ifg_done      ;

   reg [NB_BUFFER-1:0]                                                       buffer        ;
   reg [NB_CRC-1:0]                                                          crc           ;

   reg [MAX_LENGTH_B_BYTES-1:0]                                              data_aux ;



   wire [NB_DST_ADDR-1:0]                                                    dst_addr_list   [N_LEGAL_DST_ADDR-1:0] ;
   wire [NB_SRC_ADDR-1:0]                                                    src_addr_list   [N_LEGAL_DST_ADDR-1:0] ;

   integer                                                                   n = 0 ;
   integer                                                                   timer = 0 ;

   assign   dst_addr_list[ 0 ]     = 48'hcafe_fe00_aaaa ;
   assign   dst_addr_list[ 1 ]     = 48'd1 ;
   assign   dst_addr_list[ 2 ]     = 48'd2 ;
   assign   dst_addr_list[ 3 ]     = 48'd3 ;
   assign   dst_addr_list[ 4 ]     = 48'd4 ;
   assign   dst_addr_list[ 5 ]     = 48'd5 ;
   assign   dst_addr_list[ 6 ]     = 48'd6 ;
   assign   dst_addr_list[ 7 ]     = 48'd7 ;
   assign   dst_addr_list[ 8 ]     = 48'd8 ;
   assign   dst_addr_list[ 9 ]     = 48'd9 ;

   assign   src_addr_list[ 0 ]     = 48'habcd_abcd_abcd;
   assign   src_addr_list[ 1 ]     = 48'd1 ;
   assign   src_addr_list[ 2 ]     = 48'd2 ;
   assign   src_addr_list[ 3 ]     = 48'd3 ;
   assign   src_addr_list[ 4 ]     = 48'd4 ;
   assign   src_addr_list[ 5 ]     = 48'd5 ;
   assign   src_addr_list[ 6 ]     = 48'd6 ;
   assign   src_addr_list[ 7 ]     = 48'd7 ;
   assign   src_addr_list[ 8 ]     = 48'd8 ;
   assign   src_addr_list[ 9 ]     = 48'd9 ;



   //==========================================================================
   // ALGORITHM.
   //==========================================================================

   always @( posedge i_clock )
     if ( i_reset )
       frame_ready_d
         <= 1'b0 ;
     else if ( i_valid )
       frame_ready_d
         <= i_frame_ready ;

   assign  frame_ready_pos
     = i_frame_ready & ~frame_ready_d ;


   always @( frame_ready_pos, ifg_done ) //Ver esto
     begin
        if ( frame_ready_pos )
          begin
             dst_addr        = dst_addr_list[ {$random()} % N_LEGAL_DST_ADDR ] ;
             src_addr        = src_addr_list[ {$random()} % N_LEGAL_SRC_ADDR ] ;
             data_mode       = {$random}%2 ; // 0:random; 1:count.
             length          = ({$random()} % ( MAX_LENGTH_A_BYTES - MIN_LENGTH_A_BYTES + 1 ) + MIN_LENGTH_A_BYTES) ;    // FIXME: Ver como generar frames tabulados.
             length_clocks   = ( (length*8) % NB_DATA == 0 )? ( (length*8) / NB_DATA ) : ( 1 + (length*8) / NB_DATA )  ;
             if_gap          = {$random()} % ( MAX_IF_GAP - MIN_IF_GAP + 1 ) + MIN_IF_GAP ;
             frame_size      = (NB_DST_ADDR + NB_SRC_ADDR + NB_LENTYP + length *8 + NB_CRC);
             frame_clocks    = ( frame_size % NB_DATA == 0 )? ( frame_size / NB_DATA ) : ( 1 + frame_size / NB_DATA );

             load_buffer     = 1'b1 ;
             transmitting    = 1'b0 ;
             ifg_done        = 1'b0 ;
          end
     end


   always @( load_buffer )
     begin
        if ( load_buffer )
          begin
             buffer
               = {NB_BUFFER{1'b0}} ;
        buffer[ NB_BUFFER-1 -: NB_DST_ADDR ]
          = dst_addr ;
        buffer[ NB_BUFFER-1-NB_DST_ADDR -: NB_SRC_ADDR ]
          = src_addr ;
        buffer[ NB_BUFFER-1-NB_DST_ADDR-NB_SRC_ADDR -: NB_LENTYP ]
          = length ;
        crc
          = 0 ;
        if ( data_mode==0/*random*/ )
          for ( n=0; n<length_clocks; n=n+1 )
            begin
               data_aux
                  = {$random(), $random()} ;
               buffer[ NB_BUFFER-1-NB_DST_ADDR-NB_SRC_ADDR-NB_LENTYP - n*NB_DATA -: NB_DATA ]
                 = data_aux ;
               crc
                 = crc ^ data_aux[ 0*NB_DATA/2 +: NB_DATA/2 ] ^ data_aux[ 1*NB_DATA/2 +: NB_DATA/2 ] ;
            end
        else // ( data_mode==1/*count*/ )
          for ( n=0; n<length_clocks; n=n+1 )
            begin
               data_aux
                  = n ;
               buffer[ NB_BUFFER-1-NB_DST_ADDR-NB_SRC_ADDR-NB_LENTYP - n*NB_DATA -: NB_DATA ]
                 = data_aux ;
               crc
                 = crc ^ data_aux[ 0*NB_DATA/2 +: NB_DATA/2 ] ^ data_aux[ 1*NB_DATA/2 +: NB_DATA/2 ] ;
            end
        buffer[ NB_BUFFER-1-NB_DST_ADDR-NB_SRC_ADDR-NB_LENTYP-(length*8) -: NB_DATA ]
          = {NB_DATA{1'b1}} ;
        buffer[ NB_BUFFER-1-NB_DST_ADDR-NB_SRC_ADDR-NB_LENTYP-(length*8) -: NB_CRC ]
          = crc ;

        timer
          = 0 ;

        load_buffer
          = 1'b0 ;
        transmitting
          = 1'b1 ;
     end
     end



   always @( posedge i_clock )
     if ( i_valid )
       begin
          if ( transmitting )
            begin
               o_data
                 <= buffer[ NB_BUFFER-1 - timer*NB_DATA -: NB_DATA ] ;
            end
          if ( transmitting || !ifg_done )
            begin
               timer
                 <= timer + 1'b1 ;
               if ( timer*NB_DATA >= frame_size )
                 transmitting
                   <= 1'b0 ;
               if ( timer >= (frame_clocks + if_gap) )
                 ifg_done
                   <= 1'b1 ;
            end
       end

   always @( posedge i_clock )
     begin
        if ( i_valid )
          begin
             if ( transmitting ) begin
                o_data_valid <= 1'b1 ;
                o_framesize_bits <= frame_size ;
                o_datasize_bytes <= length ;
             end else
               o_data_valid <= 1'b0 ;
          end
     end

   always @ (*)
     begin
        for (n=0; n<NB_BUFFER/NB_DATA ; n = n +1 )
          begin
             buffer_array[n] = buffer[n*NB_DATA +: NB_DATA] ;
          end
     end


endmodule
