module reconciliation_sublayer_fifo_GMII_new
  #(
    parameter NB_DATA                = 64,
    parameter LOG2_NB_DATA           = 6,
    parameter NB_GMII                = 8,
    parameter NB_MAX_FRAME_SIZE_BYTES = 2048
    )
   (
    //TX Egress
    output wire [NB_GMII-1:0]      o_TXD,
    output wire                    o_TX_EN,
    output wire                    o_TX_ER,
    output wire                    o_GTX_CLK,

    //PLS RX
    output wire [NB_DATA-1:0]      o_PLS_DATA_indication,
    output wire                    o_PLS_DATA_VALID_indication,
    output wire [LOG2_NB_DATA-1:0] o_tail_size_bits,
    output wire                    o_tail_size_valid,
    output wire                    o_RS_error,

    //RX Ingress
    input wire [NB_GMII-1:0]       i_RXD,
    input wire                     i_RX_DV,
    input wire                     i_RX_ER,
    input wire                     i_RX_CLK,

    //PLS TX
    input wire [NB_DATA-1:0]       i_PLS_DATA_request,
    input wire                     i_data_request_valid,
    input wire [LOG2_NB_DATA-1:0]  i_tail_size_bits,

    input wire                     i_clock,
    input wire                     i_valid,
    input wire                     i_reset
    //Signals to RX
    ) ;
   localparam NB_TIMER = 20;

   reg [NB_DATA-1:0]              PLS_DATA_request_d;
   reg                            data_request_valid_d;
   reg [LOG2_NB_DATA-1:0]         tail_size_bits_d;
   wire                           data_request_valid_neg;

   reg                            rx_dv_d;
   //wire                           rx_dv_pos;
   wire                           rx_dv_neg;

   reg [NB_TIMER-1:0]             tx_timer;
   reg [NB_TIMER-1:0]             tx_pointer;
   reg [NB_TIMER-1:0]             rx_timer;
   reg [NB_TIMER-1:0]             rx_pointer;

   reg [NB_DATA-1:0]              tx_fifo [NB_MAX_FRAME_SIZE_BYTES/8-1:0];
   reg [NB_DATA-1:0]              rx_fifo [NB_MAX_FRAME_SIZE_BYTES/8-1:0];

   reg [NB_DATA-1:0]              rx_serial_to_parallel;
   wire                           serial_to_parallel_valid;

   // wire [NB_DATA-1:0]             PLS_DATA_indication;
   // wire                           PLS_DATA_VALID_indication;
   // wire [LOG2_NB_DATA-1:0]        tail_size_bits;
   // wire                           tail_size_valid;

   /*******************
    TRANSMISION LOGIC
    *******************/
   assign o_TXD = (o_TX_EN) ? tx_fifo[tx_pointer/8][NB_DATA-(NB_GMII*(tx_pointer%NB_GMII))-1-:NB_GMII] : {NB_GMII{1'b0}};
   assign o_TX_EN = ~data_request_valid_d & (tx_pointer<tx_timer);
   assign o_GTX_CLK = i_clock;
   assign o_TX_ER = 1'b0;

   always @(posedge i_clock) begin
     if (i_reset) begin
        PLS_DATA_request_d <= {NB_DATA{1'b0}};
        data_request_valid_d <= 1'b0;
        tail_size_bits_d <= {LOG2_NB_DATA{1'b0}};
     end else if (i_valid) begin
        PLS_DATA_request_d <= i_PLS_DATA_request;
        data_request_valid_d <= i_data_request_valid;
        tail_size_bits_d <= i_tail_size_bits;
     end
   end // always @ (posedge i_clock)

   assign data_request_valid_neg = ~i_data_request_valid & data_request_valid_d;

   //TX FIFO size pointer
   always @(posedge i_clock) begin
     if (i_reset)
       tx_timer <= {NB_TIMER{1'b0}};
     else if (i_valid & data_request_valid_d)
       tx_timer <= tx_timer + NB_DATA/NB_GMII - ((((NB_DATA-tail_size_bits_d)%NB_DATA)/NB_GMII)*data_request_valid_neg);
     else if (i_valid & tx_pointer>=tx_timer)
       tx_timer <= {NB_TIMER{1'b0}};
   end

   //TX FIFO data pointer
   always @(posedge i_clock) begin
      if (i_reset)
        tx_pointer <= {NB_TIMER{1'b0}};
      else if (i_valid & ~data_request_valid_d & (tx_pointer<tx_timer))
        tx_pointer <= tx_pointer + 1'b1;
      else if (i_valid & (tx_pointer>=tx_timer))
        tx_pointer <= {NB_TIMER{1'b0}};
   end

   always @(posedge i_clock) begin
      if (i_valid & data_request_valid_d)
        tx_fifo[tx_timer/8] <= PLS_DATA_request_d;
   end

   /*******************
    RECEPTION LOGIC
    *******************/

   // always @(posedge i_RX_CLK) begin
   //    o_PLS_DATA_indication <= PLS_DATA_indication;
   //    o_PLS_DATA_VALID_indication <= PLS_DATA_VALID_indication;
   //    o_tail_size_bits <= tail_size_bits;
   //    o_tail_size_valid <= tail_size_valid;
   // end
/*
   assign PLS_DATA_indication = (o_PLS_DATA_VALID_indication) ? rx_fifo[rx_pointer/8] : {NB_DATA{1'b0}};
   assign PLS_DATA_VALID_indication = ~i_RX_DV & (rx_pointer < rx_timer);
   assign tail_size_bits = (o_tail_size_valid) ? (rx_pointer%NB_GMII)*(NB_DATA/NB_GMII) : {LOG2_NB_DATA{1'b0}};
   assign tail_size_valid = o_PLS_DATA_VALID_indication & (rx_pointer+NB_DATA/NB_GMII >= rx_timer) ;
   assign o_RS_error = i_RX_DV & i_RX_ER;
*/
   assign o_PLS_DATA_indication = (o_PLS_DATA_VALID_indication) ? rx_fifo[rx_pointer/8] : {NB_DATA{1'b0}};
   assign o_PLS_DATA_VALID_indication = ~i_RX_DV & (rx_pointer < rx_timer);
   // assign o_tail_size_bits = (o_tail_size_valid) ? (rx_pointer%NB_GMII)*(NB_DATA/NB_GMII) : {LOG2_NB_DATA{1'b0}};
   assign o_tail_size_bits = (o_tail_size_valid) ? (rx_timer*NB_GMII)%NB_DATA : {LOG2_NB_DATA{1'b0}};
   assign o_tail_size_valid = o_PLS_DATA_VALID_indication & (rx_pointer+NB_DATA/NB_GMII >= rx_timer) ;
   assign o_RS_error = i_RX_DV & i_RX_ER;

 //RX FIFO size pointer
   always @(posedge i_RX_CLK) begin
      if (i_reset)
        rx_timer <= {NB_TIMER{1'b0}};
      else if (i_valid & i_RX_DV)
        rx_timer <= rx_timer + 1'b1;
      else if (i_valid & rx_pointer>=rx_timer)
        rx_timer <= {NB_TIMER{1'b0}};
   end

   //RX FIFO data pointer
   always @(posedge i_RX_CLK) begin
      if (i_reset)
        rx_pointer <= {NB_TIMER{1'b0}};
      else if (i_valid & ~i_RX_DV & rx_pointer < rx_timer)
        rx_pointer <= rx_pointer + NB_DATA/NB_GMII;
      else
        rx_pointer <= {NB_TIMER{1'b0}};
   end

   always @(posedge i_RX_CLK)
     if (i_valid & i_RX_DV)
       rx_serial_to_parallel[NB_DATA-((rx_timer%NB_GMII)*NB_GMII)-1-:NB_GMII] <= i_RXD;

   assign serial_to_parallel_valid = (rx_timer%NB_GMII==7) | rx_dv_neg;

   always @(posedge i_RX_CLK)
     if (i_reset)
       rx_dv_d <= 1'b0;
     else if (i_valid)
       rx_dv_d <= i_RX_DV;

   //assign rx_dv_pos = i_RX_DV & ~rx_dv_d;
   assign rx_dv_neg = ~i_RX_DV & rx_dv_d;

   always @(posedge i_RX_CLK) begin
      if (i_valid & serial_to_parallel_valid)
        rx_fifo[rx_timer/8] <= {rx_serial_to_parallel[NB_DATA-1:NB_GMII],i_RXD};
   end

endmodule
