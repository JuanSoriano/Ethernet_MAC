module reconciliation_sublayer_GMII
  #(
    parameter NB_DATA                = 64,
    parameter LOG2_NB_DATA           = 6,
    parameter NB_GMII                = 8
    )
   (
    //TX Egress
    output reg [NB_GMII-1:0]      o_TXD,
    output reg                    o_TX_EN,
    output wire                   o_TX_ER,
    output wire                   o_GTX_CLK,

    //PLS RX
    output reg [NB_DATA-1:0]      o_PLS_DATA_indication,
    output reg                    o_PLS_DATA_VALID_indication,
    output reg [LOG2_NB_DATA-1:0] o_tail_size_bits,
    output reg                    o_tail_size_valid,
    output wire                   o_RS_error,
    //output wire [3-1:0]           o_stats_linkfault,

    //RX Ingress
    input wire [NB_GMII-1:0]      i_RXD,
    input wire                    i_RX_DV,
    input wire                    i_RX_ER,
    input wire                    i_RX_CLK,

    //PLS TX
    input wire [NB_DATA-1:0]      i_PLS_DATA_request,
    input wire                    i_data_request_valid,
    input wire [LOG2_NB_DATA-1:0] i_tail_size_bits,

    input wire                    i_clock,
    input wire                    i_valid,
    input wire                    i_reset
    //Signals to RX
    ) ;
   localparam NB_TIMER = 3;

   reg                            valid_reg;

   reg                            data_req_valid_aux;
   wire                           valid_pos;
   wire                           data_req_valid_neg;
   reg                            data_req_valid_neg_reg;
   wire                           data_req_valid_neg_or;

   reg                            rx_dv_reg;
   wire                           rx_dv_neg;

   reg [NB_TIMER-1:0]             tx_timer;
   reg [NB_TIMER-1:0]             rx_timer;

   reg [NB_DATA-1:0]              latched_tx_data;
   reg [NB_DATA-1:0]              latched_rx_data;

   reg                            rx_datavalid;
   reg [LOG2_NB_DATA-1:0]         rx_tailsize;
   reg                            rx_tailsize_valid;

   //
   reg [NB_DATA-1:0]              PLS_DATA_request_reg;
   reg [LOG2_NB_DATA-1:0]         tail_size_bits_aux;
   reg [LOG2_NB_DATA-1:0]         tail_size_bits_reg;
   wire [LOG2_NB_DATA-1:0]        tail_size_bits;

   reg                            datavalid_req_valid_reg;


   /*******************
    TRANSMISION LOGIC
    *******************/
   assign o_TX_ER = 1'b0;
   assign o_GTX_CLK = i_clock;
   always @(*)
     casez({data_req_valid_neg_or, i_valid & data_req_valid_neg_reg})
       2'b10: o_TX_EN = (tail_size_bits==0) ? tx_timer<(NB_DATA/NB_GMII) : tx_timer<(tail_size_bits/NB_GMII);
       2'b00: o_TX_EN = (data_req_valid_aux & i_valid ) | (datavalid_req_valid_reg);
       2'b11: o_TX_EN = 1'b0;
       default: o_TX_EN = 1'b0;
     endcase // casez ({data_req_valid_neg_or})

   always @(*)
     casez ({o_TX_EN, valid_pos})
       2'b11: o_TXD = PLS_DATA_request_reg[NB_DATA-1-:NB_GMII];
       2'b10: o_TXD = latched_tx_data[NB_DATA-(tx_timer*NB_GMII)-1-:NB_GMII];
       default: o_TXD = {NB_GMII{1'b0}};
     endcase

   always @(posedge i_clock)
     if (i_reset) begin
        PLS_DATA_request_reg <= {NB_DATA{1'b0}};
        data_req_valid_aux <= 1'b0;
        datavalid_req_valid_reg <= 1'b0;
        tail_size_bits_aux <= {LOG2_NB_DATA{1'b0}};
        tail_size_bits_reg <= {LOG2_NB_DATA{1'b0}};
     end else if (i_valid)begin
        PLS_DATA_request_reg <= i_PLS_DATA_request;
        data_req_valid_aux <= i_data_request_valid;
        datavalid_req_valid_reg <= data_req_valid_aux;
        tail_size_bits_aux <= i_tail_size_bits;
        tail_size_bits_reg <= tail_size_bits_aux;
     end

   //This signal has the tail bits during the last valid entirely
   assign tail_size_bits = (i_valid & data_req_valid_neg_or) ? tail_size_bits_aux : tail_size_bits_reg;

   always @(posedge i_clock)
     if (i_reset)
       valid_reg <= 1'b0;
     else
       valid_reg <= i_valid;

   assign valid_pos = i_valid & ~valid_reg;
   assign data_req_valid_neg = ~i_data_request_valid & data_req_valid_aux;

   always @(posedge i_clock)
     if (i_reset)
       data_req_valid_neg_reg <= 1'b0;
     else if (i_valid)
       data_req_valid_neg_reg <= data_req_valid_neg;

   assign data_req_valid_neg_or = data_req_valid_neg | (data_req_valid_neg_reg & ~i_valid);

   always @(posedge i_clock)
     if (i_reset)
       rx_timer <= {NB_TIMER{1'b0}};
     else
       rx_timer <= rx_timer + 1'b1;

   //Data to be sent which is latched for 1 valid
   always @(posedge i_clock)
     if (i_reset)
       latched_tx_data <= {NB_DATA{1'b0}};
     else if (i_valid)
       latched_tx_data <= PLS_DATA_request_reg;

   /*******************
    RECEPTION LOGIC
    *******************/
  always @(posedge i_RX_CLK) begin
     if (i_reset) begin
        o_PLS_DATA_indication <= {NB_DATA{1'b0}};
        o_PLS_DATA_VALID_indication <= 1'b0;
        o_tail_size_bits <= {LOG2_NB_DATA{1'b0}};
        o_tail_size_valid <= 1'b0;
     end else if (i_valid) begin
        o_PLS_DATA_indication <= latched_rx_data;
        o_PLS_DATA_VALID_indication <= rx_datavalid;
        o_tail_size_bits <= rx_tailsize;
        o_tail_size_valid <= rx_tailsize_valid;
     end
   end

   assign o_RS_error = i_RX_DV & i_RX_ER;

   always @(posedge i_RX_CLK)
     if (i_reset)
       tx_timer <= {NB_TIMER{1'b0}};
     else
       tx_timer <= tx_timer + 1'b1;

   always @(posedge i_RX_CLK)
     if (i_reset)
       latched_rx_data <= {NB_DATA{1'b0}};
     else
       latched_rx_data[NB_DATA-(tx_timer*NB_GMII)-1-:NB_GMII] <= i_RXD;


   always @(posedge i_RX_CLK)
     if (i_reset)
       rx_dv_reg <= 1'b0;
     else
       rx_dv_reg <= i_RX_DV;
   assign rx_dv_neg = ~i_RX_DV & rx_dv_reg;

   //Indicate that there was a datavalid asserted in the last NB_DATA/NB_GMII clocks
   always @(posedge i_RX_CLK)
     if (i_reset | i_valid)
       rx_datavalid <= 1'b0;
     else if (i_RX_DV)
       rx_datavalid <= 1'b1;

   //Capture tailsize according to the number of datavalids
   always @(posedge i_RX_CLK)
     if (i_reset)
       rx_tailsize <= {LOG2_NB_DATA{1'b0}};
     else if (rx_dv_neg)
       rx_tailsize <= rx_timer*8;

   //Indicate the tailsize is valid
   always @(posedge i_RX_CLK)
     if (rx_dv_neg)
       rx_tailsize_valid <= 1'b1;
     else if (i_reset | i_valid)
       rx_tailsize_valid <= 1'b0;

endmodule
