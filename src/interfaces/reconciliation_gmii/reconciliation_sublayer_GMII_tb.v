module reconciliation_sublayer_GMII_tb ();

   localparam NB_DATA                = 64;
   localparam LOG2_NB_DATA           = 6;
   localparam NB_GMII                = 8;


   //TX Egress
   wire [NB_GMII-1:0]     TXD_o;
   wire                   TX_EN_o;
   wire                   TX_ER_o;
   wire                   GTX_CLK_o;
   wire [NB_DATA-1:0]     PLS_DATA_indication_o;
   wire                   PLS_DATA_VALID_indication_o;
   wire [LOG2_NB_DATA-1:0] tail_size_bits_o;
   wire                    tail_size_valid_o;
   wire                    RS_error_o;
   wire [NB_GMII-1:0]      RXD_i;
   wire                    RX_DV_i;
   wire                    RX_ER_i;
   wire                    RX_CLK_i;
   reg [NB_DATA-1:0]       PLS_DATA_request_i;
   reg                     data_request_valid_i;
   reg [LOG2_NB_DATA-1:0]  tail_size_bits_i;

   wire                    valid;
   wire                    reset;
   reg                     clock = 1'b0 ;
   integer                 timer = 0 ;
   reg [3-1:0]             timer_valid = 0;
   wire                    reset_timer_valid;

   always
     begin
        #(50) clock = ~clock ;
     end

   always @ ( posedge clock )
     begin
        timer   <= timer + 1;
     end

   always @ (posedge clock)
     begin
        if (reset | reset_timer_valid)
          timer_valid <= {3{1'b0}};
        else
          timer_valid <= timer_valid + 1'b1;
     end

   assign reset = (timer == 2) ; // Reset at time 2
   assign valid = 1'b1;
   //assign reset_timer_valid = (timer%8==0);

   always @(*)
     case (timer)
       10: begin
          PLS_DATA_request_i = 64'hAAAA_BBBB_CCCC_DDDD;
          data_request_valid_i = 1'b1;
          tail_size_bits_i = 6'd0;
       end
       11: begin
          PLS_DATA_request_i = 64'h5D5D_5D5D_5D5D_5D5D;
          data_request_valid_i = 1'b1;
          tail_size_bits_i = 6'd00;
       end
       12: begin
          PLS_DATA_request_i = 64'hAAAA_BBBB_CCCC_DDDD;
          data_request_valid_i = 1'b1;
          tail_size_bits_i = 6'd32;
       end
       default: begin
          PLS_DATA_request_i = 64'h0; //local_fault
          data_request_valid_i = 1'b0;
          tail_size_bits_i = 6'd14;
       end
     endcase
   reconciliation_sublayer_fifo_GMII_new
     #(
       .NB_DATA                    (NB_DATA                     ),
       .LOG2_NB_DATA               (LOG2_NB_DATA                ),
       .NB_GMII                    (NB_GMII                     ),
       .NB_MAX_FRAME_SIZE_BYES     (2048                        )
       )
   u_reconciliation_sublayer_GMII
     (
      .o_TXD                       (TXD_o                       ),
      .o_TX_EN                     (TX_EN_o                     ),
      .o_TX_ER                     (TX_ER_o                     ),
      .o_GTX_CLK                   (GTX_CLK_o                   ),
      .o_PLS_DATA_indication       (PLS_DATA_indication_o       ),
      .o_PLS_DATA_VALID_indication (PLS_DATA_VALID_indication_o ),
      .o_tail_size_bits            (tail_size_bits_o            ),
      .o_tail_size_valid           (tail_size_valid_o           ),
      .o_RS_error                  (RS_error_o                  ),
      .i_RXD                       (TXD_o                       ),
      .i_RX_DV                     (TX_EN_o                     ),
      .i_RX_ER                     (TX_ER_o                     ),
      .i_RX_CLK                    (GTX_CLK_o                   ),
      .i_PLS_DATA_request          (PLS_DATA_request_i          ),
      .i_data_request_valid        (data_request_valid_i        ),
      .i_tail_size_bits            (tail_size_bits_i            ),
      .i_clock                     (clock                       ),
      .i_valid                     (valid                       ),
      .i_reset                     (reset                       )
      );

endmodule
