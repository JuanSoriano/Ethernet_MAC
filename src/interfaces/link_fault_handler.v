module link_fault_handler
  #(
    parameter NB_DATA = 64,
    parameter LOCAL_FAULT_SEQUENCE = 64'h9C00_0001_0000_0000 //Local fault sequence
    )
   (
    output wire [NB_DATA-1:0] o_RXD_with_linkfault,
    output wire               o_RXC0_with_linkfault,

    input wire [NB_DATA-1:0]  i_RXD,
    input wire                i_RXC0,
    input wire                i_local_fault
    );

   assign o_RXD_with_linkfault = (i_local_fault) ? LOCAL_FAULT_SEQUENCE : i_RXD;
   assign o_RXC0_with_linkfault = (i_local_fault) ? 1'b1 : i_RXC0;

endmodule
