module tb_reconciliation_sublayer_CGMII ();
   parameter NB_DATA                = 64;
   parameter LOG2_NB_DATA           = 6;
   parameter NB_XC                  = 8;
   parameter N_LANES                = 8;


   wire [NB_DATA-1:0]      TXD_o;
   wire [NB_XC-1:0]        TXC_o;
   wire                    TX_CLK_o;
   wire [NB_DATA-1:0]      PLS_DATA_indication_o;
   wire                    PLS_DATA_VALID_indication_o;
   wire [LOG2_NB_DATA-1:0] tail_size_bits_o;
   wire                    RS_error_o;
   wire [NB_DATA-1:0]      RXD_i;
   wire [NB_XC-1:0]        RXC_i;
   wire                    RX_CLK_i;
   reg [NB_DATA-1:0]       PLS_DATA_request_i;
   reg                     data_request_valid_i;
   reg [LOG2_NB_DATA-1:0]  tail_size_bits_i;

   wire                    valid;
   wire                    reset;
   reg                     clock = 1'b0 ;
   integer                 timer = 0 ;

   always
     begin
        #(50) clock = ~clock ;
     end

   always @ ( posedge clock )
     begin
        timer   <= timer + 1;
     end

   assign reset = (timer == 2) ; // Reset at time 2
   assign valid = 1'b1;

   //Loopback
   assign RXD_i = (timer >200 & timer < 300 | timer > 400) ? 'b0 :TXD_o;
   assign RXC_i = (timer >11) ? 8'h80 : TXC_o;
   assign RX_CLK_i = TX_CLK_o;

   always @(*)
     case (timer)
       3: begin
          PLS_DATA_request_i = 64'hAAAA_BBBB_CCCC_DDDD;
          data_request_valid_i = 1'b0;
          tail_size_bits_i = 6'd40;
       end
       4: begin
          PLS_DATA_request_i = 64'h5D5D_5D5D_5D5D_5D5D;
          data_request_valid_i = 1'b1;
          tail_size_bits_i = 6'd40;
       end
       5: begin
          PLS_DATA_request_i = 64'hAAAA_BBBB_CCCC_DDDD;
          data_request_valid_i = 1'b1;
          tail_size_bits_i = 6'd40;
       end
       6: begin
          PLS_DATA_request_i = 64'hAAAA_BBBB_CCCC_DDDD;
          data_request_valid_i = 1'b1;
          tail_size_bits_i = 6'd40;
       end
       7: begin
          PLS_DATA_request_i = 64'hAAAA_BBBB_CCCC_DDDD;
          data_request_valid_i = 1'b1;
          tail_size_bits_i = 6'd40;
       end
       8: begin
          PLS_DATA_request_i = 64'hAAAA_BBBB_CCCC_DDDD;
          data_request_valid_i = 1'b1;
          tail_size_bits_i = 6'd40;
       end
       9: begin
          PLS_DATA_request_i = 64'hAAAA_BBBB_CCCC_DDDD;
          data_request_valid_i = 1'b1;
          tail_size_bits_i = 6'd40;
       end
       10: begin
          PLS_DATA_request_i = 64'hAAAA_BBBB_CCCC_DDDD;
          data_request_valid_i = 1'b1;
          tail_size_bits_i = 6'd40;
       end
       11: begin
          PLS_DATA_request_i = 64'hAAAA_BBBB_CCCC_DDDD;
          data_request_valid_i = 1'b1;
          tail_size_bits_i = 6'd40;
       end
       12: begin
          PLS_DATA_request_i = 64'h9C00_0001_0000_0000; //local_fault
          data_request_valid_i = 1'b1;
          tail_size_bits_i = 6'd40;
       end
       13: begin
          PLS_DATA_request_i = 64'h9C00_0001_0000_0000; //local_fault
          data_request_valid_i = 1'b1;
          tail_size_bits_i = 6'd40;
       end
       14: begin
          PLS_DATA_request_i = 64'h9C00_0002_0000_0000; //remote_fault
          data_request_valid_i = 1'b1;
          tail_size_bits_i = 6'd40;
       end
       15: begin
          PLS_DATA_request_i = 64'h9C00_0002_0000_0000; //remote_fault
          data_request_valid_i = 1'b1;
          tail_size_bits_i = 6'd40;
       end
       16: begin
          PLS_DATA_request_i = 64'h9C00_0002_0000_0000; //remote_fault
          data_request_valid_i = 1'b1;
          tail_size_bits_i = 6'd40;
       end
       default: begin
          PLS_DATA_request_i = 64'h9C00_0001_0000_0000; //local_fault
          data_request_valid_i = 1'b1;
          tail_size_bits_i = 6'd40;
       end
     endcase


   reconciliation_sublayer_CGMII
     #(
       .NB_DATA                    (NB_DATA     ),
       .LOG2_NB_DATA               (LOG2_NB_DATA),
       .NB_XC                      (NB_XC       ),
       .N_LANES                    (N_LANES     )
       )
   u_reconciliation_sublayer_CGMII
     (
      .o_TXD                       (TXD_o                      ),
      .o_TXC                       (TXC_o                      ),
      .o_TX_CLK                    (TX_CLK_o                   ),
      .o_PLS_DATA_indication       (PLS_DATA_indication_o      ),
      .o_PLS_DATA_VALID_indication (PLS_DATA_VALID_indication_o),
      .o_tail_size_bits            (tail_size_bits_o           ),
      .o_RS_error                  (RS_error_o                 ),
      .i_RXD                       (RXD_i                      ),
      .i_RXC                       (RXC_i                      ),
      .i_RX_CLK                    (RX_CLK_i                   ),
      .i_PLS_DATA_request          (PLS_DATA_request_i         ),
      .i_data_request_valid        (data_request_valid_i       ),
      .i_tail_size_bits            (tail_size_bits_i           ),
      .i_clock                     (clock                      ),
      .i_valid                     (valid                      ),
      .i_reset                     (reset                      )
      );

endmodule
