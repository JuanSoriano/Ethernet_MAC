module reconciliation_sublayer_CGMII
  #(
    parameter NB_DATA                = 64,
    parameter LOG2_NB_DATA           = 6,
    parameter NB_XC                  = 8,
    parameter N_LANES                = 8
    )
   (
    //TX Egress
    output reg [NB_DATA-1:0]      o_TXD,
    output reg [NB_XC-1:0]        o_TXC,
    output wire                   o_TX_CLK,

    //PLS RX
    output wire [NB_DATA-1:0]     o_PLS_DATA_indication,
    output reg                    o_PLS_DATA_VALID_indication,
    output reg [LOG2_NB_DATA-1:0] o_tail_size_bits,
    output reg                    o_tail_size_valid,
    output reg                    o_RS_error,
    output wire [3-1:0]           o_stats_linkfault,

    //RX Ingress
    input wire [NB_DATA-1:0]      i_RXD,
    input wire [NB_XC-1:0]        i_RXC,
    input wire                    i_RX_CLK,

    //PLS TX
    input wire [NB_DATA-1:0]      i_PLS_DATA_request,
    input wire                    i_data_request_valid,
    input wire [LOG2_NB_DATA-1:0] i_tail_size_bits,

    input wire                    i_clock,
    input wire                    i_valid,
    input wire                    i_reset
    //Signals to RX
    /*
     DATA_VALID_STATUS shall assume the value DATA_VALID when a PLS_DATA.indication transaction
     is generated in response to reception of a Start control character on lane 0 if the prior RXC<7:0> and
     RXD<63:0> contained eight Idle characters, a Sequence ordered set, or a Terminate character.
     DATA_VALID_STATUS shall assume the value DATA_NOT_VALID when RXC of the current lane in
     sequence is asserted for anything except an Error control character. In the absence of errors,
     DATA_NOT_VALID is caused by a Terminate control character. When DATA_VALID_STATUS changes
     from DATA_VALID to DATA_NOT_VALID because of a control character other than Terminate, the RS
     shall ensure that the MAC will detect a FrameCheckError prior to indicating DATA_NOT_VALID to the
     MAC (see 81.3.3.1) .
     */
    ) ;

   //Buscar start character en LANE 0 sino no generar data valid
   //Buscar terminate y no errors antes
   //Si todos los bits de TXC estan en 1 y el codigo es 8'FE en todos los TXD, ocurrio un error

   //Parameters
   localparam IDLE_CHAR                           = 8'h07; //Only valid in lane 0
   localparam SEQUENCE_CHAR                       = 8'h9C;
   localparam START_CHAR                          = 8'hFB;
   localparam TERMINATE_CHAR                      = 8'hFD;
   localparam ERROR_CHAR                          = 8'hFE;
   localparam RESERVE0_START_CHAR                 = 8'h00;
   localparam RESERVE0_END_CHAR                   = 8'h06;
   localparam RESERVE1_START_CHAR                 = 8'h08;
   localparam RESERVE1_END_CHAR                   = 8'h9B;
   localparam RESERVE2_CHAR                       = 8'hFC;
   localparam RESERVE3_CHAR                       = 8'hFF;

   localparam FIRST_PREAMBLE_OCTET                = 8'h55;
   localparam LOCAL_FAULT_SEQUENCE                = 56'h00_0001_0000_0000;
   localparam REMOTE_FAULT_SEQUENCE               = 56'h00_0002_0000_0000;
   // FSM
   localparam                NB_STATE             = 3;
   localparam [NB_STATE-1:0] ST_0_IDLE            = 0;
   localparam [NB_STATE-1:0] ST_1_START           = 1;
   localparam [NB_STATE-1:0] ST_2_ERROR           = 3;
   localparam [NB_STATE-1:0] ST_3_LINKSTATE_ERROR = 4;

   localparam NB_COUNTER                          = 10;

   localparam NB_BYTE                             = 8;
   localparam LOG2_NB_BYTE                        = 4;

   //==================================================
   // INTERNAL SIGNALS.
   //==================================================
   // FSM
   reg [NB_STATE-1:0]             state ;
   reg [NB_STATE-1:0]             next_state ;

   //Other
   //Latched data
   reg [NB_DATA-1:0]              latched_PLS_DATA_request;
   reg                            latched_data_request_valid;
   reg [LOG2_NB_DATA-1:0]         latched_tail_size_bits;

   reg                            data_request_valid_d;
   reg                            latched_data_request_valid_d;
   wire                           data_request_valid_pos;
   wire                           data_request_valid_neg;
   reg                            terminate_pending;
   reg                            set_pending;
   reg                            clear_pending;
   reg                            insert_first_preamble_octet;
   reg [LOG2_NB_BYTE-1-1:0]       index_end_control_char;
   wire                           rxc_has_control_character;
   reg [LOG2_NB_BYTE-1-1:0]       index_first_control_asserted;
   integer                        i,j;

   wire                           local_fault_detected;
   wire                           remote_fault_detected;
   wire                           error_detected;
   reg                            linkfault_recovery_counter_enable;
   wire                           linkfault_done;

   //Link fault handling
   reg                            count_local_faults;
   reg                            count_remote_faults;
   reg [2-1:0]                    last_fault;
   reg [NB_COUNTER-1:0]           fault_counter ;
   wire                           new_fault_detected;
   reg [NB_COUNTER-1:0]           linkfault_recovery_counter;

   wire [NB_BYTE-1:0]             RXD_0;

   wire                           RXC_0;

   assign o_TX_CLK = i_clock;

   /*******************
    TRANSMISION LOGIC
    *******************/

   assign o_PLS_DATA_indication = (insert_first_preamble_octet) ? {FIRST_PREAMBLE_OCTET, i_RXD[NB_XC*N_LANES-NB_XC-1:0]} : i_RXD;
   assign RXC_0= i_RXC[NB_BYTE-1];
   assign RXD_0= i_RXD[NB_BYTE*N_LANES-1-:NB_BYTE];

   //Delay input data and valid to allow terminate char to be inserted
   always @(posedge o_TX_CLK)
     begin
        if (i_reset) begin
           latched_PLS_DATA_request <= {NB_DATA{1'b0}};
           latched_data_request_valid <= 1'b0;
           latched_tail_size_bits <= {LOG2_NB_DATA{1'b0}};
        end else if (i_valid) begin
           latched_PLS_DATA_request <= i_PLS_DATA_request;
           latched_data_request_valid <= i_data_request_valid;
           latched_tail_size_bits <= i_tail_size_bits;
        end
     end

   always @(posedge o_TX_CLK) begin
      if (i_reset) begin
         data_request_valid_d <= 1'b0;
         latched_data_request_valid_d <= 1'b0;
      end else if (i_valid) begin
         data_request_valid_d <= i_data_request_valid;
         latched_data_request_valid_d <= latched_data_request_valid;
      end
   end

   assign data_request_valid_pos = latched_data_request_valid & ~latched_data_request_valid_d;
   assign data_request_valid_neg = ~i_data_request_valid & data_request_valid_d;

   always @(posedge o_TX_CLK)
     begin
        if (i_reset | clear_pending)
          terminate_pending <= 1'b0;
        else if (i_valid & set_pending)
          terminate_pending <= 1'b1;
     end

   always @(*)
     begin
        index_end_control_char=0;
        set_pending = 1'b0;
        if (data_request_valid_neg) begin
           index_end_control_char = latched_tail_size_bits/NB_BYTE;
           if (index_end_control_char == 0)
             set_pending = 1'b1;
           else if (clear_pending)
             set_pending = 1'b0;
        end
     end

   always @( * )
     begin
        clear_pending = 1'b0;
        j=0;
        if (state == ST_3_LINKSTATE_ERROR) begin //Indicate linkstate error
           o_TXD = {N_LANES{IDLE_CHAR}};
           o_TXC = {1'b1,{(NB_XC-1){1'b0}}};
        end else begin
           o_TXD = latched_PLS_DATA_request;
           o_TXC = {NB_XC{1'b0}};
           if (data_request_valid_pos) begin //Insert START CHAR
              o_TXC = 8'b1000_0000;
              o_TXD[N_LANES*NB_XC-1-:NB_BYTE] = START_CHAR;
           end else if (data_request_valid_neg & ~set_pending) begin
              for (j=0; j<NB_XC; j=j+1) begin
                 if(j>=index_end_control_char) begin
                    o_TXC[NB_XC-j-1] = 1'b1;
                    o_TXD[(N_LANES-j)*NB_BYTE-1-:NB_BYTE] = IDLE_CHAR;
                 end
              end
              o_TXD[(N_LANES-index_end_control_char)*NB_BYTE-1-:NB_BYTE] = TERMINATE_CHAR;
           end else if (terminate_pending == 1'b1) begin //Terminate from previous parallelism
              clear_pending = 1'b1;
              o_TXC = 8'b1111_1111;
              o_TXD[8*NB_BYTE-1-:NB_BYTE] = TERMINATE_CHAR;
              o_TXD[7*NB_BYTE-1:0] = {(N_LANES-1){IDLE_CHAR}};
           end else if (latched_data_request_valid) begin
              o_TXC = {NB_XC{1'b0}};
              o_TXD = latched_PLS_DATA_request;
           end else begin
              o_TXC = {NB_XC{1'b1}};
              o_TXD = {N_LANES{IDLE_CHAR}};
           end
        end
     end

   /*******************
    RECEPTION LOGIC
    *******************/


   assign rxc_has_control_character = |i_RXC;
   assign local_fault_detected = i_RXC==8'h80 & i_RXD=={SEQUENCE_CHAR,LOCAL_FAULT_SEQUENCE};
   assign remote_fault_detected = i_RXC==8'h80 & i_RXD=={SEQUENCE_CHAR,REMOTE_FAULT_SEQUENCE};
   assign new_fault_detected = (last_fault[0]==count_local_faults | last_fault[1]==count_remote_faults) & (count_local_faults & count_remote_faults);
   assign error_detected = (i_RXC == 8'hFF) && (i_RXD == {N_LANES{ERROR_CHAR}});
   assign linkfault_done = linkfault_recovery_counter >= 128;
   assign o_stats_linkfault = {local_fault_detected & new_fault_detected, remote_fault_detected & new_fault_detected, error_detected};

   always @(*)
     begin
		  index_first_control_asserted = 'b0;
        for (i=0;i<NB_XC;i=i+1) begin
           if ( i_RXC[i] == 1'b1)
             index_first_control_asserted=NB_XC-1-i;
        end
     end

   always @(posedge i_RX_CLK)
     begin
        if (i_reset | i_valid & ~(count_local_faults|count_remote_faults)) //No faults detected or already in linkfault state
          fault_counter <= {NB_COUNTER{1'b0}};
        else if (i_valid && fault_counter=={NB_COUNTER{1'b0}} && (count_local_faults|count_remote_faults)) //First fault after no faults
          fault_counter <= fault_counter + 1'b1;
        else if (i_valid & new_fault_detected) //Same fault as before, increment timer
          fault_counter <= fault_counter + 1'b1;
        else fault_counter <= {{(NB_COUNTER-1){1'b0}},1'b1}; //New fault after different fault (Strange occurence)
     end

   always @(posedge i_RX_CLK)
     begin
        if (i_reset | !linkfault_recovery_counter_enable)
          linkfault_recovery_counter <= {NB_COUNTER{1'b0}};
        else if (i_valid & linkfault_recovery_counter_enable)
          linkfault_recovery_counter <= linkfault_recovery_counter + 1'b1;
     end


   always @(posedge i_RX_CLK)
     begin
        if (i_reset)
          last_fault <= 2'd0;
        else if (i_valid) begin
           if (count_local_faults) //Set lsb bit to indicate local fault
             last_fault <= 2'b01;
           else if (count_remote_faults) //Set msb bit to indicate remote fault
             last_fault <= 2'b10;
           else
             last_fault <= 2'd0;
        end
     end


   // Reception FSM

   // State update.
   always @( posedge i_RX_CLK )
     begin
        if ( i_reset )
          state <= ST_0_IDLE ;
        else if ( i_valid )
          state <= next_state ;
     end

   always @( * )
     begin
        next_state = ST_0_IDLE;
        o_PLS_DATA_VALID_indication = 1'b0;
        o_tail_size_bits ='b0;
        o_tail_size_valid = 1'b0;
        o_RS_error = 1'b1;
        count_local_faults = 1'b0;
        count_remote_faults = 1'b0;
        linkfault_recovery_counter_enable = 1'b0;
        insert_first_preamble_octet = 1'b0;

        case ( state )
          ST_0_IDLE :
            begin
               o_PLS_DATA_VALID_indication = 1'b0;
               o_RS_error = 1'b0;
               count_local_faults = 1'b0;
               count_remote_faults = 1'b0;
               linkfault_recovery_counter_enable = 1'b0;
               insert_first_preamble_octet = 1'b0;
               if (local_fault_detected) begin //Local fault
                  count_local_faults = 1'b1;
                  if (fault_counter >=3)
                    next_state = ST_3_LINKSTATE_ERROR;
               end else if (remote_fault_detected) begin //Remote fault
                  count_remote_faults = 1'b1;
                  if (fault_counter >=3)
                    next_state = ST_3_LINKSTATE_ERROR;
               end else if (error_detected) //Error
                 next_state = ST_2_ERROR;
               else if ((RXC_0 == 1'b1) & (RXD_0 == START_CHAR)) begin //Normal operation
                  next_state = ST_1_START;
                  o_PLS_DATA_VALID_indication = 1'b1;
                  insert_first_preamble_octet = 1'b1;
               end
               else
                 next_state = ST_0_IDLE;
            end

          ST_1_START :
            begin
               o_PLS_DATA_VALID_indication = 1'b1;
               o_RS_error = 1'b0;
               o_tail_size_bits = 'b0;
               count_local_faults = 1'b0;
               count_remote_faults = 1'b0;
               linkfault_recovery_counter_enable = 1'b0;
               insert_first_preamble_octet = 1'b0;
               if (local_fault_detected) begin //Local fault
                  count_local_faults = 1'b1;
                  if (fault_counter >=3)
                    next_state = ST_3_LINKSTATE_ERROR;
               end else if (remote_fault_detected) begin //Remote fault
                  count_remote_faults = 1'b1;
                  if (fault_counter >=3)
                    next_state = ST_3_LINKSTATE_ERROR;
               end else if (error_detected) //Error
                 next_state = ST_2_ERROR;
               else if (rxc_has_control_character & (i_RXD[(N_LANES-index_first_control_asserted)*NB_BYTE-1-:NB_BYTE] == TERMINATE_CHAR)) begin //Terminate char found
                  o_tail_size_bits = index_first_control_asserted*NB_BYTE;
                  o_tail_size_valid = 1'b1;
                  next_state = ST_0_IDLE;
                  if (i_RXD[N_LANES*NB_BYTE-1-:NB_BYTE] == TERMINATE_CHAR & (index_first_control_asserted == 0)) begin //Saving case with end char in first position
                     o_PLS_DATA_VALID_indication = 1'b0;
                     //o_tail_size_valid = 1'b0;
                  end
               end
               else
                 next_state = ST_1_START;
            end

          ST_2_ERROR :
            begin
               o_PLS_DATA_VALID_indication = 1'b1;
               o_RS_error = 1'b1;
               o_tail_size_bits = 'b0;
               count_local_faults = 1'b0;
               count_remote_faults = 1'b0;
               linkfault_recovery_counter_enable = 1'b0;
               insert_first_preamble_octet = 1'b0;
               if (local_fault_detected) begin //Local fault
                  count_local_faults = 1'b1;
                  if (fault_counter >=3)
                    next_state = ST_3_LINKSTATE_ERROR;
               end else if (remote_fault_detected) begin //Remote fault
                  count_remote_faults = 1'b1;
                  if (fault_counter >=3)
                    next_state = ST_3_LINKSTATE_ERROR;
               end else if (error_detected) //Error
                 next_state = ST_2_ERROR;
               else
                 next_state = ST_0_IDLE;
            end

          ST_3_LINKSTATE_ERROR :
            begin
               o_PLS_DATA_VALID_indication = 1'b0;
               o_tail_size_bits = 'b0;
               o_RS_error = 1'b1;
               count_local_faults = 1'b0;
               count_remote_faults = 1'b0;
               insert_first_preamble_octet = 1'b0;
               if (~(local_fault_detected | remote_fault_detected))
                 linkfault_recovery_counter_enable = 1'b1;
               if (linkfault_done)
                 next_state = ST_0_IDLE;
               else if (new_fault_detected)
                 next_state = ST_3_LINKSTATE_ERROR;
               else
                 next_state = ST_3_LINKSTATE_ERROR;
            end

          default :
            begin //This should never happen
               next_state = ST_0_IDLE;

               o_PLS_DATA_VALID_indication = 1'b0;
               o_tail_size_bits ='b0;
               o_RS_error = 1'b1;
               count_local_faults = 1'b0;
               count_remote_faults = 1'b0;
               linkfault_recovery_counter_enable = 1'b0;
               insert_first_preamble_octet = 1'b0;
            end
        endcase
     end

endmodule
