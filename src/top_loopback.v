module top_loopback
  (
   ///////// FPGA /////////
   input         FPGA_CLK1_50
   );

   MAC_top

   u_MAC_top
     (
      .i_clock (FPGA_CLK1_50)
      );

endmodule
