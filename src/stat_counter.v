module stat_counter
  #(
    parameter NB_COUNTER = 32,
    parameter LOG2_N_RX_STATUS = 3
    )
   (
    //RX Stats
    output reg [NB_COUNTER-1:0]       o_soffound_counter,
    output reg [NB_COUNTER-1:0]       o_goodframe_counter,
    output reg [NB_COUNTER-1:0]       o_frametoolong_counter,
    output reg [NB_COUNTER-1:0]       o_framecheckerror_counter,
    output reg [NB_COUNTER-1:0]       o_lengtherror_counter,
    output reg [NB_COUNTER-1:0]       o_alignment_counter,
    output reg [NB_COUNTER-1:0]       o_addresserror_counter,

    output reg [NB_COUNTER-1:0]       o_localaddrmatch_counter,
    output reg [NB_COUNTER-1:0]       o_bcastaddrmatch_counter,
    output reg [NB_COUNTER-1:0]       o_multicast_counter,

    //TX Stats
    output reg [NB_COUNTER-1:0]       o_frametransmitted_counter,

    //Checker stats
    output reg [NB_COUNTER-1:0]       o_checkermissmatch_counter,

    //Fault conditions counter
    output reg [NB_COUNTER-1:0]       o_RSfault_counter,
    output reg [NB_COUNTER-1:0]       o_locallinkfault_counter,
    output reg [NB_COUNTER-1:0]       o_remotelinkfault_counter,
    output reg [NB_COUNTER-1:0]       o_rserror_counter,
    output reg [NB_COUNTER-1:0]       o_txdatavalidmiss_counter,
    output reg [NB_COUNTER-1:0]       o_rxdatavalidmiss_counter,


    input wire                        i_soffound,
    input wire [LOG2_N_RX_STATUS-1:0] i_rx_status,
    input wire                        i_rx_status_valid,
    input wire [3-1:0]                i_address_match,
    input wire                        i_frametransmitted,
    input wire                        i_RS_fault,
    input wire [3-1:0]                i_linkfault, //100: local fault; 010: remote fault; 001: error
    input wire                        i_checkermissmatch,
    input wire                        i_txdatavalidmiss,
    input wire                        i_rxdatavalidmiss,

    input wire                        i_read_status,
    input wire                        i_clear_on_read_mode,

    input wire                        i_clock,
    input wire                        i_valid,
    input wire                        i_reset
    );

   reg [NB_COUNTER-1:0]                         soffound_counter;
   reg [NB_COUNTER-1:0]                         goodframe_counter;
   reg [NB_COUNTER-1:0]                         frametoolong_counter;
   reg [NB_COUNTER-1:0]                         framecheckerror_counter;
   reg [NB_COUNTER-1:0]                         lengtherror_counter;
   reg [NB_COUNTER-1:0]                         alignment_counter;
   reg [NB_COUNTER-1:0]                         addresserror_counter;
   reg [NB_COUNTER-1:0]                         localaddrmatch_counter;
   reg [NB_COUNTER-1:0]                         bcastaddrmatch_counter;
   reg [NB_COUNTER-1:0]                         multicast_counter;
   reg [NB_COUNTER-1:0]                         frametransmitted_counter;
   reg [NB_COUNTER-1:0]                         RSfault_counter;
   reg [NB_COUNTER-1:0]                         locallinkfault_counter;
   reg [NB_COUNTER-1:0]                         remotelinkfault_counter;
   reg [NB_COUNTER-1:0]                         checkermissmatch_counter;
   reg [NB_COUNTER-1:0]                         rserror_counter;
   reg [NB_COUNTER-1:0]                         txdatavalidmiss_counter;
   reg [NB_COUNTER-1:0]                         rxdatavalidmiss_counter;

   reg                                          read_status_ms ;
   reg                                          read_status_dm ;
   reg                                          read_status_d ; //Added
   wire                                         read_status_pos ;


   //Main output assignment
   always @(posedge i_clock) begin
      if (i_reset) begin
         o_soffound_counter <= {NB_COUNTER{1'b0}};
         o_goodframe_counter <= {NB_COUNTER{1'b0}};
         o_frametoolong_counter <= {NB_COUNTER{1'b0}};
         o_framecheckerror_counter <= {NB_COUNTER{1'b0}};
         o_lengtherror_counter <= {NB_COUNTER{1'b0}};
         o_alignment_counter <= {NB_COUNTER{1'b0}};
         o_addresserror_counter <= {NB_COUNTER{1'b0}};
         o_localaddrmatch_counter <= {NB_COUNTER{1'b0}};
         o_bcastaddrmatch_counter <= {NB_COUNTER{1'b0}};
         o_multicast_counter <= {NB_COUNTER{1'b0}};
         o_frametransmitted_counter <= {NB_COUNTER{1'b0}};
         o_RSfault_counter <= {NB_COUNTER{1'b0}};
         o_locallinkfault_counter <= {NB_COUNTER{1'b0}};
         o_remotelinkfault_counter <= {NB_COUNTER{1'b0}};
         o_rserror_counter <= {NB_COUNTER{1'b0}};
         o_checkermissmatch_counter <= {NB_COUNTER{1'b0}};
         o_txdatavalidmiss_counter <= {NB_COUNTER{1'b0}};
         o_rxdatavalidmiss_counter <= {NB_COUNTER{1'b0}};
      end else if (read_status_pos) begin
         o_soffound_counter <= soffound_counter;
         o_goodframe_counter <= goodframe_counter;
         o_frametoolong_counter <= frametoolong_counter;
         o_framecheckerror_counter <= framecheckerror_counter;
         o_lengtherror_counter <= lengtherror_counter;
         o_alignment_counter <= alignment_counter;
         o_addresserror_counter <= addresserror_counter;
         o_localaddrmatch_counter <= localaddrmatch_counter;
         o_bcastaddrmatch_counter <= bcastaddrmatch_counter;
         o_multicast_counter <= multicast_counter;
         o_frametransmitted_counter <= frametransmitted_counter;
         o_RSfault_counter <= RSfault_counter;
         o_locallinkfault_counter <= locallinkfault_counter;
         o_remotelinkfault_counter <= remotelinkfault_counter;
         o_rserror_counter <= rserror_counter;
         o_checkermissmatch_counter <= checkermissmatch_counter;
         o_txdatavalidmiss_counter <= txdatavalidmiss_counter;
         o_rxdatavalidmiss_counter <= rxdatavalidmiss_counter;
      end
   end

   // Double Flop Sync. for read control
   always @( posedge i_clock )
     begin
        read_status_ms <= i_read_status ;
        read_status_dm <= read_status_ms ;
        read_status_d <= read_status_dm ;
     end
   assign  read_status_pos = read_status_dm & ~read_status_d ;


   always @(posedge i_clock) begin
      if (i_reset || i_clear_on_read_mode) begin
         soffound_counter <= 'b0;
         goodframe_counter <= 'b0;
         frametoolong_counter <= 'b0;
         framecheckerror_counter <= 'b0;
         lengtherror_counter <= 'b0;
         alignment_counter <= 'b0;
         addresserror_counter <= 'b0;
         frametransmitted_counter <= 'b0;
         checkermissmatch_counter <= 'b0;
      end else if (i_valid) begin
         if (i_rx_status_valid) begin
            case (i_rx_status)
              'd0: goodframe_counter <= goodframe_counter + 1'b1;
              'd1: frametoolong_counter <= frametoolong_counter + 1'b1;
              'd2: framecheckerror_counter <= framecheckerror_counter + 1'b1;
              'd3: lengtherror_counter <= lengtherror_counter + 1'b1;
              'd4: alignment_counter <= alignment_counter + 1'b1;
              'd5: addresserror_counter <= addresserror_counter + 1'b1;
              default: begin
                 goodframe_counter <= goodframe_counter;
                 frametoolong_counter <= frametoolong_counter;
                 framecheckerror_counter <= framecheckerror_counter;
                 lengtherror_counter <= lengtherror_counter;
                 alignment_counter <= alignment_counter;
                 addresserror_counter <= addresserror_counter;
              end
            endcase // casez (i_rx_status)
         end if (i_soffound)
           soffound_counter <= soffound_counter + 1'b1;
         if (i_frametransmitted)
           frametransmitted_counter <= frametransmitted_counter + 1'b1;
         if (i_checkermissmatch)
           checkermissmatch_counter <= checkermissmatch_counter + 1'b1;
      end
   end

   //Faults stat counter
   always @(posedge i_clock) begin
      if (i_reset || i_clear_on_read_mode) begin
        locallinkfault_counter <= {NB_COUNTER{1'b0}};
        remotelinkfault_counter <= {NB_COUNTER{1'b0}};
        rserror_counter <= {NB_COUNTER{1'b0}};
        RSfault_counter <= {NB_COUNTER{1'b0}};
      end else if (i_valid & i_RS_fault) begin
         RSfault_counter <= RSfault_counter + 1'b1;
         casez (i_linkfault)
           3'b100: locallinkfault_counter <= locallinkfault_counter + 1'b1;
           3'b010: remotelinkfault_counter <= remotelinkfault_counter + 1'b1;
           3'b001: rserror_counter <= rserror_counter + 1'b1;
           default: begin
              locallinkfault_counter <= locallinkfault_counter;
              remotelinkfault_counter <= remotelinkfault_counter;
              rserror_counter <= rserror_counter;
           end
         endcase // casez (i_linkfault)
      end
   end // always @ (posedge i_clock)

   //Adress stat counter
   always @(posedge i_clock)begin
      if (i_reset || i_clear_on_read_mode) begin
         localaddrmatch_counter <= {NB_COUNTER{1'b0}};
         bcastaddrmatch_counter <= {NB_COUNTER{1'b0}};
         multicast_counter <= {NB_COUNTER{1'b0}};
      end else if (i_valid & i_rx_status_valid & i_rx_status == 0)
        if (i_address_match[2]==1'b1) //MSB bit
          localaddrmatch_counter <= localaddrmatch_counter +1'b1;
        else if (i_address_match[1]==1'b1) //MSB-1 bit
          bcastaddrmatch_counter <= bcastaddrmatch_counter + 1'b1;
        else if (i_address_match[0])//Multicast match otherwise
          multicast_counter <= multicast_counter + 1'b1;
   end

   //Datavalid missmatches counters
   always @(posedge i_clock) begin
      if (i_reset || i_clear_on_read_mode) begin
         txdatavalidmiss_counter <= {NB_COUNTER{1'b0}};
      rxdatavalidmiss_counter <= {NB_COUNTER{1'b0}};
      end else if (i_valid) begin
         if (i_txdatavalidmiss)
           txdatavalidmiss_counter <= txdatavalidmiss_counter + 1'b1;
         if (i_rxdatavalidmiss)
           rxdatavalidmiss_counter <= rxdatavalidmiss_counter + 1'b1;
      end
   end



   function integer clogb2;
      input integer depth;
      for (clogb2=0; depth>0; clogb2=clogb2+1)
        depth = depth >> 1;
   endfunction // clogb2

endmodule
