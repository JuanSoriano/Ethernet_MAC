import sys
import random
import binascii
from crccheck.crc import Crc32Bzip2
import math

#File parameters
output_data_file_path = "./sim_data.mem"
output_control_file_path = "./sim_control.mem"
output_datavalid_file_path = "./sim_datavalid.mem"
output_datarequest_file_path = "./data_request_fields.mem"
output_payloadrequest_file_path = "./data_request_payload.mem"
output_request_valid_file_path = "./data_request_valid.mem"

if len(sys.argv) < 2:
    print ("Ingrese el numero de frames a crear")
    sys.exit()
else :
    nframes = int(sys.argv[1])

######################
# STANDARD PARAMETERS
######################
NB_DATA = 64
NBITS_SRC_ADDR = 6*8
NBITS_DST_ADDR = 6*8
NBITS_LENTYP = 2*8
NBITS_CRC = 32
NBITS_IPG = 96

######################
# SIMULATOR PARAMETERS
######################

broken_crc_rate = 1000000 #once every this number, a broken frame is generated
random_dst_addr_rate = 1000000 #once this number, a random dst address is generated
broken_payloadsize_rate = 1000000 #once every this number, a missmatch between length and payload is generated
broken_framesize_rate = 10000000 #once every this number, a frame too big is generated


frame_type = 0 #If frame_type = 0 => max_nbits_data: 1500*8 - Basic frame
               #                1 => max_nbits_data: 1504*8 - Q-tagged frame
               #                2 => max_nbits_data: 1982*8 - Envelope frame
#####################

dst_addr_int_list = [int(x) for x in range(nframes)] #256 frames with known dst addresses
dst_addr_list = list()
src_addr_list = list()
len_typ_list = list()
payload_list = list()
crc_list = list()

PREAMBLE = 7 * str(10101010)
SOF = str(10101011)

def generate_frames(nframes):

    #For data output
    data_list = list()
    concat_data = ''

    #For control output
    concat_control = ''
    data_valid = ''
    receive_data_valid = 0
    frame_size_bits = 0
    data_size_bytes = 0
    tail_size_bytes = 0

    valid_tail = 0

    #Create nframes
    for i in range(nframes):

        dst_addr_rand  =  0#(random.randint(1,random_dst_addr_rate)==1)
        payload_rand   =  0#(random.randint(1,broken_payloadsize_rate)==1)
        crc_rand       =  0#(random.randint(1,broken_crc_rate)==1)
        framesize_rand =  0#(random.randint(1,broken_framesize_rate)==1)

        if frame_type == 0:
            nbits_payload = random.randint(46,1500)*8
        elif frame_type == 1:
            nbits_payload = random.randint(46,1500)*8
        elif frame_type == 2:
            nbits_payload = random.randint(46,1982)*8

        #Generate destination address
        if (dst_addr_rand):
            dst_addr = bin(random.getrandbits(NBITS_DST_ADDR))[2:].zfill(NBITS_DST_ADDR)
        else:
            dst_addr = bin(dst_addr_int_list[i])[2:].zfill(NBITS_DST_ADDR)
        print("DST_ADDR: ",dst_addr)
        dst_addr_list.append(dst_addr)

        #Generate source address
        src_addr = bin(random.getrandbits(NBITS_SRC_ADDR))[2:].zfill(NBITS_SRC_ADDR)
        src_addr_list.append(src_addr)


        #Generate lentyp
        lentyp = bin(nbits_payload//8)[2:].zfill(NBITS_LENTYP) #Put the length of the nbits_payload and fill with 0's
        len_typ_list.append(lentyp)

        #Generate payload
        if (payload_rand):
            random_payload_size = nbits_payload+random.randint(0,NB_DATA)*8
            payload = bin(random.getrandbits(random_payload_size))[2:].zfill(random_payload_size) #Add random bits
        else:
            payload = bin(random.getrandbits(nbits_payload))[2:].zfill(nbits_payload)
        payload_list.append(payload)

        frame = dst_addr+src_addr+lentyp+payload

        #CRC32 generation
        if (crc_rand):
            crc32 = bin(random.getrandbits(NBITS_CRC))[2:].zfill(NBITS_CRC) #random CRC
        else:
                try:
                    frame_hex = hex(int('1000'+frame,2))[3:] #FIXME: Implementar de otra forma mas prolija, no tomar a pàrtir del segundo caracter
                    crc32_int = Crc32Bzip2.calc(bytes.fromhex(frame_hex))
                    crc32 = bin(crc32_int)[2:].zfill(32)
                except ValueError:
                    print (len(frame))
                    print ("src:",src_addr,"src_len:",len(src_addr))
                    print ("dst",dst_addr,"dst_len",len(dst_addr))
                    print ("lentyp",lentyp,"lentyp_len",len(lentyp))
                    print ("payload",payload,"payload_len",len(payload))
                    print ("frame",frame,"frame_len",len(frame))
                    print("ValueError exc")
                    import pdb; pdb.set_trace()
        crc_list.append(crc32)

        #Create a frame too big if broken_framesize_rate
        if (framesize_rand):
            frame = frame+crc32+bin(random.getrandbits(random.randint(1982*8,2*1982*8)))
        else:
            frame = frame+crc32

        #Add atleast ipg random bits between frames
        ipg = bin(random.getrandbits(NBITS_IPG+random.randint(0,4*NB_DATA)))[2:]

        data = PREAMBLE + SOF + frame + ipg

        '''#Control logic
        rdv: add a 1 for each parallelism containing atleast a bit of valid data and 0's otherwise
        concat_control contains the previous iteration's concat_control + this iteration's control signals
        Same goes for data_valid
        '''

        data_length = len(PREAMBLE+SOF+frame)
        trash_length = len(ipg)
        remaining_trash_length = trash_length - ((NB_DATA-((data_length+valid_tail)%NB_DATA))%NB_DATA)
        receive_data_valid = math.ceil((valid_tail+data_length)/NB_DATA)*'1'+math.floor(remaining_trash_length/NB_DATA)*'0'

        valid_tail = remaining_trash_length%NB_DATA
        data_valid = data_valid + receive_data_valid
        frame_size_bits = bin(len(frame))[2:]
        #import pdb; pdb.set_trace()
        data_size_bytes = bin(nbits_payload//8)[2:]

        #if (math.ceil(len(frame)/NB_DATA) != math.ceil((valid_tail+data_length)/NB_DATA)):
        #    import pdb; pdb.set_trace()

        print ("nbits_payload: ", nbits_payload);
        print ("data_size_bytes: ",data_size_bytes);
        print ("data_size_bytes: ", int(data_size_bytes));
        tail_size_bytes = bin(len(frame)%64)[2:]

        concat_control = concat_control+frame_size_bits+' '+data_size_bytes+' '+tail_size_bytes+' '
        concat_data = concat_data + data

    for i in range(0, len(concat_data), 64):
        data_list.append(concat_data[i:i+64])

    data_request_list = list()
    data_request_valid = ''
    for i in range(nframes):
        dataReqStr = dst_addr_list[i]+' '+src_addr_list[i]+' '+len_typ_list[i]+' '+crc_list[i]+' '
        data_request_list.append(dataReqStr)
        data_request_valid = data_request_valid+math.ceil(len(payload_list[i])/NB_DATA)*'1'+20*'0'


    length = 0
    for i in range (nframes):
        length += len(data_list)

  #  if ((len(data_valid)+1)!=length):
  #      import pdb; pdb.set_trace()

    print("DEBUG: nb data parallelism counter: ", len(data_list))
    print("DEBUG: datavalid length: ", len(data_valid)+1)

    print("writing upper layers data_request file...")
    data_request = ''.join(data_request_list)
    data_file = open(output_datarequest_file_path, "w")
    data_file.write(data_request)
    data_file.close()

    print("writing data_request_payload file...")
    data_request_payload = ' '.join(payload_list)
    data_file = open(output_payloadrequest_file_path, "w")
    data_file.write(data_request_payload)
    data_file.close()

    print("writing data_request_valid file...")
    data_request_valid_file = open(output_request_valid_file_path, "w")
    data_request_valid_file.write(data_request_valid)
    data_request_valid_file.close()

    print("writing sim_data file...")
    data = ' '.join(data_list)
    data_file = open(output_data_file_path, "w")
    data_file.write(data)
    data_file.close()

    print("writing sim_control file...")
    control_file = open(output_control_file_path, "w")
    control_file.write(concat_control)
    control_file.close()

    print("writing sim_datavalid file...")
    datavalid_file = open(output_datavalid_file_path, "w")
    datavalid_file.write(data_valid+'0')#Add a zero for the last iteration, as it contains invalid data
    datavalid_file.close()


    print("DONE!")

generate_frames(nframes)
