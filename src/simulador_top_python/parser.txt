datavalid length:  11169
frame_size_bits_list:  [4208, 7688, 7560, 3768, 2600, 4288, 11720, 7744, 5520, 1440, 8352, 7680, 8272, 7296, 2848, 11064, 5816, 9824, 5960, 6512, 8376, 9248, 9568, 624, 8752, 3088, 4856, 6856, 6496, 3632, 3288, 2512, 9312, 8432, 3592, 3600, 2320, 6736, 1808, 5112, 9824, 5024, 10784, 3552, 10224, 7368, 2816, 4024, 10160, 11128, 6352, 6144, 1584, 4288, 9024, 10200, 5288, 7864, 10728, 11408, 8176, 10984, 8632, 6992, 11096, 5248, 11216, 1544, 12032, 5816, 2120, 10200, 8984, 9264, 9936, 4920, 5568, 6664, 9008, 12072, 1208, 9568, 1344, 5408, 7648, 6304, 6632, 11088, 7208, 6856, 6160, 2984, 7656, 11928, 3400, 4496, 7488, 9296, 11904, 10864]
data_size_bytes_list:  [508, 943, 927, 453, 307, 518, 1447, 950, 672, 162, 1026, 942, 1016, 894, 338, 1365, 709, 1210, 727, 796, 1029, 1138, 1178, 60, 1076, 368, 589, 839, 794, 436, 393, 296, 1146, 1036, 431, 432, 272, 824, 208, 621, 1210, 610, 1330, 426, 1260, 903, 334, 485, 1252, 1373, 776, 750, 180, 518, 1110, 1257, 643, 965, 1323, 1408, 1004, 1355, 1061, 856, 1369, 638, 1384, 175, 1486, 709, 247, 1257, 1105, 1140, 1224, 597, 678, 815, 1108, 1491, 133, 1178, 150, 658, 938, 770, 811, 1368, 883, 839, 752, 355, 939, 1473, 407, 544, 918, 1144, 1470, 1340]
tail_size_bytes_list:  [48, 8, 8, 56, 40, 0, 8, 0, 16, 32, 32, 0, 16, 0, 32, 56, 56, 32, 8, 48, 56, 32, 32, 48, 48, 16, 56, 8, 32, 48, 24, 16, 32, 48, 8, 16, 16, 16, 16, 56, 32, 32, 32, 32, 48, 8, 0, 56, 48, 56, 16, 0, 48, 0, 0, 24, 40, 56, 40, 16, 48, 40, 56, 16, 24, 0, 16, 8, 0, 56, 8, 24, 24, 48, 16, 56, 0, 8, 48, 40, 56, 32, 0, 32, 32, 32, 40, 16, 40, 8, 16, 40, 40, 24, 8, 16, 0, 16, 0, 48]
len(string_file_list):  11169
len(data_valid):  11169
Frame nr:  0 
DST addr:  0x0 
SRC addr:  0x89aa650e5104 
Lentyp:  0x1fc 
Captured fcs:  0xb0bd264a 
Calculated CRC:  0xb0bd264a 
Status:  0 
Frame size in bits:  4208 
Payload size in bytes:  508 
Tail size in bits:  48 

Frame nr:  1 
DST addr:  0x1 
SRC addr:  0x91e00578ce4d 
Lentyp:  0x3af 
Captured fcs:  0x30906b9c 
Calculated CRC:  0x30906b9c 
Status:  0 
Frame size in bits:  7688 
Payload size in bytes:  943 
Tail size in bits:  8 

Frame nr:  2 
DST addr:  0x2 
SRC addr:  0x9fcc6a092010 
Lentyp:  0x39f 
Captured fcs:  0xc4d14329 
Calculated CRC:  0xc4d14329 
Status:  0 
Frame size in bits:  7560 
Payload size in bytes:  927 
Tail size in bits:  8 

Frame nr:  3 
DST addr:  0x3 
SRC addr:  0x2cd7967ccf05 
Lentyp:  0x1c5 
Captured fcs:  0x52c98f0a 
Calculated CRC:  0x52c98f0a 
Status:  5 
Frame size in bits:  3768 
Payload size in bytes:  453 
Tail size in bits:  56 

Frame nr:  4 
DST addr:  0x4 
SRC addr:  0xd846b5e37f9e 
Lentyp:  0x133 
Captured fcs:  0xa7b3ca7d 
Calculated CRC:  0xa7b3ca7d 
Status:  5 
Frame size in bits:  2600 
Payload size in bytes:  307 
Tail size in bits:  40 

Frame nr:  5 
DST addr:  0x5 
SRC addr:  0xfa6484278fc9 
Lentyp:  0x206 
Captured fcs:  0x2ab434d8 
Calculated CRC:  0x2ab434d8 
Status:  5 
Frame size in bits:  4288 
Payload size in bytes:  518 
Tail size in bits:  0 

Frame nr:  6 
DST addr:  0x6 
SRC addr:  0x7a643d9974b3 
Lentyp:  0x5a7 
Captured fcs:  0x91ca3260 
Calculated CRC:  0x91ca3260 
Status:  5 
Frame size in bits:  11720 
Payload size in bytes:  1447 
Tail size in bits:  8 

Frame nr:  7 
DST addr:  0x7 
SRC addr:  0xefdb1dd39aaf 
Lentyp:  0x3b6 
Captured fcs:  0xc35c0c5b 
Calculated CRC:  0xc35c0c5b 
Status:  5 
Frame size in bits:  7744 
Payload size in bytes:  950 
Tail size in bits:  0 

Frame nr:  8 
DST addr:  0x8 
SRC addr:  0x1948151a6cc1 
Lentyp:  0x2a0 
Captured fcs:  0x74b6140d 
Calculated CRC:  0x74b6140d 
Status:  5 
Frame size in bits:  5520 
Payload size in bytes:  672 
Tail size in bits:  16 

Frame nr:  9 
DST addr:  0x9 
SRC addr:  0x8d4cb6445d04 
Lentyp:  0xa2 
Captured fcs:  0x8fd1fb2f 
Calculated CRC:  0x8fd1fb2f 
Status:  0 
Frame size in bits:  1440 
Payload size in bytes:  162 
Tail size in bits:  32 

Frame nr:  10 
DST addr:  0xa 
SRC addr:  0xff43245b2d4f 
Lentyp:  0x402 
Captured fcs:  0x37be257 
Calculated CRC:  0x37be257 
Status:  5 
Frame size in bits:  8352 
Payload size in bytes:  1026 
Tail size in bits:  32 

Frame nr:  11 
DST addr:  0xb 
SRC addr:  0x6f5d5b1d429 
Lentyp:  0x3ae 
Captured fcs:  0xe26ce410 
Calculated CRC:  0xe26ce410 
Status:  5 
Frame size in bits:  7680 
Payload size in bytes:  942 
Tail size in bits:  0 

Frame nr:  12 
DST addr:  0xc 
SRC addr:  0xfbf1d155fbb2 
Lentyp:  0x3f8 
Captured fcs:  0xfa5a1b95 
Calculated CRC:  0xfa5a1b95 
Status:  5 
Frame size in bits:  8272 
Payload size in bytes:  1016 
Tail size in bits:  16 

Frame nr:  13 
DST addr:  0xd 
SRC addr:  0xd64155cf1dab 
Lentyp:  0x37e 
Captured fcs:  0x8f3693c1 
Calculated CRC:  0x8f3693c1 
Status:  5 
Frame size in bits:  7296 
Payload size in bytes:  894 
Tail size in bits:  0 

Frame nr:  14 
DST addr:  0xe 
SRC addr:  0xb3a60bca15fc 
Lentyp:  0x152 
Captured fcs:  0xedb79962 
Calculated CRC:  0xedb79962 
Status:  5 
Frame size in bits:  2848 
Payload size in bytes:  338 
Tail size in bits:  32 

Frame nr:  15 
DST addr:  0xf 
SRC addr:  0x48013e002ef4 
Lentyp:  0x555 
Captured fcs:  0x1f5379b1 
Calculated CRC:  0x1f5379b1 
Status:  5 
Frame size in bits:  11064 
Payload size in bytes:  1365 
Tail size in bits:  56 

Frame nr:  16 
DST addr:  0x10 
SRC addr:  0x5531dd390602 
Lentyp:  0x2c5 
Captured fcs:  0x2097ca74 
Calculated CRC:  0x2097ca74 
Status:  5 
Frame size in bits:  5816 
Payload size in bytes:  709 
Tail size in bits:  56 

Frame nr:  17 
DST addr:  0x11 
SRC addr:  0x718a5d31d044 
Lentyp:  0x4ba 
Captured fcs:  0x4acd1995 
Calculated CRC:  0x4acd1995 
Status:  5 
Frame size in bits:  9824 
Payload size in bytes:  1210 
Tail size in bits:  32 

Frame nr:  18 
DST addr:  0x12 
SRC addr:  0x30fb63164c3c 
Lentyp:  0x2d7 
Captured fcs:  0xacb74041 
Calculated CRC:  0xacb74041 
Status:  5 
Frame size in bits:  5960 
Payload size in bytes:  727 
Tail size in bits:  8 

Frame nr:  19 
DST addr:  0x13 
SRC addr:  0x1502daadf506 
Lentyp:  0x31c 
Captured fcs:  0xc8eba559 
Calculated CRC:  0xc8eba559 
Status:  5 
Frame size in bits:  6512 
Payload size in bytes:  796 
Tail size in bits:  48 

Frame nr:  20 
DST addr:  0x14 
SRC addr:  0xb09946988dca 
Lentyp:  0x405 
Captured fcs:  0xba9bfbcb 
Calculated CRC:  0xba9bfbcb 
Status:  5 
Frame size in bits:  8376 
Payload size in bytes:  1029 
Tail size in bits:  56 

Frame nr:  21 
DST addr:  0x15 
SRC addr:  0x6a65defb3919 
Lentyp:  0x472 
Captured fcs:  0x9a82a4dd 
Calculated CRC:  0x9a82a4dd 
Status:  5 
Frame size in bits:  9248 
Payload size in bytes:  1138 
Tail size in bits:  32 

Frame nr:  22 
DST addr:  0x16 
SRC addr:  0x1d55475fe269 
Lentyp:  0x49a 
Captured fcs:  0xc327b932 
Calculated CRC:  0xc327b932 
Status:  5 
Frame size in bits:  9568 
Payload size in bytes:  1178 
Tail size in bits:  32 

Frame nr:  23 
DST addr:  0x17 
SRC addr:  0x90b487f73cc0 
Lentyp:  0x3c 
Captured fcs:  0x9f6c9d69 
Calculated CRC:  0x9f6c9d69 
Status:  5 
Frame size in bits:  624 
Payload size in bytes:  60 
Tail size in bits:  48 

Frame nr:  24 
DST addr:  0x18 
SRC addr:  0xb110dd78d2a2 
Lentyp:  0x434 
Captured fcs:  0xc209f7a7 
Calculated CRC:  0xc209f7a7 
Status:  5 
Frame size in bits:  8752 
Payload size in bytes:  1076 
Tail size in bits:  48 

Frame nr:  25 
DST addr:  0x19 
SRC addr:  0xa76ad5fb5b32 
Lentyp:  0x170 
Captured fcs:  0xf32a2432 
Calculated CRC:  0xf32a2432 
Status:  5 
Frame size in bits:  3088 
Payload size in bytes:  368 
Tail size in bits:  16 

Frame nr:  26 
DST addr:  0x1a 
SRC addr:  0x946aa1235118 
Lentyp:  0x24d 
Captured fcs:  0xd4b28b41 
Calculated CRC:  0xd4b28b41 
Status:  5 
Frame size in bits:  4856 
Payload size in bytes:  589 
Tail size in bits:  56 

Frame nr:  27 
DST addr:  0x1b 
SRC addr:  0x48cb6e338857 
Lentyp:  0x347 
Captured fcs:  0xc54f051c 
Calculated CRC:  0xc54f051c 
Status:  5 
Frame size in bits:  6856 
Payload size in bytes:  839 
Tail size in bits:  8 

Frame nr:  28 
DST addr:  0x1c 
SRC addr:  0x8a8643aa3bd4 
Lentyp:  0x31a 
Captured fcs:  0x3692a7dc 
Calculated CRC:  0x3692a7dc 
Status:  5 
Frame size in bits:  6496 
Payload size in bytes:  794 
Tail size in bits:  32 

Frame nr:  29 
DST addr:  0x1d 
SRC addr:  0x79fd997b99c5 
Lentyp:  0x1b4 
Captured fcs:  0x92bbe445 
Calculated CRC:  0x92bbe445 
Status:  5 
Frame size in bits:  3632 
Payload size in bytes:  436 
Tail size in bits:  48 

Frame nr:  30 
DST addr:  0x1e 
SRC addr:  0x7bcc9f92d7ba 
Lentyp:  0x189 
Captured fcs:  0x5aaaadbe 
Calculated CRC:  0x5aaaadbe 
Status:  5 
Frame size in bits:  3288 
Payload size in bytes:  393 
Tail size in bits:  24 

Frame nr:  31 
DST addr:  0x1f 
SRC addr:  0x8209a8b4e085 
Lentyp:  0x128 
Captured fcs:  0x6b165ba3 
Calculated CRC:  0x6b165ba3 
Status:  5 
Frame size in bits:  2512 
Payload size in bytes:  296 
Tail size in bits:  16 

Frame nr:  32 
DST addr:  0x20 
SRC addr:  0xe8041b4b22ed 
Lentyp:  0x47a 
Captured fcs:  0x3e31f3db 
Calculated CRC:  0x3e31f3db 
Status:  5 
Frame size in bits:  9312 
Payload size in bytes:  1146 
Tail size in bits:  32 

Frame nr:  33 
DST addr:  0x21 
SRC addr:  0x924394d43e32 
Lentyp:  0x40c 
Captured fcs:  0x92fe16f7 
Calculated CRC:  0x92fe16f7 
Status:  5 
Frame size in bits:  8432 
Payload size in bytes:  1036 
Tail size in bits:  48 

Frame nr:  34 
DST addr:  0x22 
SRC addr:  0xb7cef95bcc9b 
Lentyp:  0x1af 
Captured fcs:  0xf118b785 
Calculated CRC:  0xf118b785 
Status:  5 
Frame size in bits:  3592 
Payload size in bytes:  431 
Tail size in bits:  8 

Frame nr:  35 
DST addr:  0x23 
SRC addr:  0xafe805c5335c 
Lentyp:  0x1b0 
Captured fcs:  0x146d9d3f 
Calculated CRC:  0x146d9d3f 
Status:  5 
Frame size in bits:  3600 
Payload size in bytes:  432 
Tail size in bits:  16 

Frame nr:  36 
DST addr:  0x24 
SRC addr:  0xc9c684fdb52c 
Lentyp:  0x110 
Captured fcs:  0x8a42523b 
Calculated CRC:  0x8a42523b 
Status:  5 
Frame size in bits:  2320 
Payload size in bytes:  272 
Tail size in bits:  16 

Frame nr:  37 
DST addr:  0x25 
SRC addr:  0xaa3504a9be62 
Lentyp:  0x338 
Captured fcs:  0xc17fb36f 
Calculated CRC:  0xc17fb36f 
Status:  5 
Frame size in bits:  6736 
Payload size in bytes:  824 
Tail size in bits:  16 

Frame nr:  38 
DST addr:  0x26 
SRC addr:  0xee09a64d4132 
Lentyp:  0xd0 
Captured fcs:  0xbfb401d 
Calculated CRC:  0xbfb401d 
Status:  5 
Frame size in bits:  1808 
Payload size in bytes:  208 
Tail size in bits:  16 

Frame nr:  39 
DST addr:  0x27 
SRC addr:  0xd920a5b7ff6a 
Lentyp:  0x26d 
Captured fcs:  0x2c191e19 
Calculated CRC:  0x2c191e19 
Status:  5 
Frame size in bits:  5112 
Payload size in bytes:  621 
Tail size in bits:  56 

Frame nr:  40 
DST addr:  0x28 
SRC addr:  0x4bed4db03ce7 
Lentyp:  0x4ba 
Captured fcs:  0x521f3e66 
Calculated CRC:  0x521f3e66 
Status:  5 
Frame size in bits:  9824 
Payload size in bytes:  1210 
Tail size in bits:  32 

Frame nr:  41 
DST addr:  0x29 
SRC addr:  0x26420b6b6f9b 
Lentyp:  0x262 
Captured fcs:  0x37c3958e 
Calculated CRC:  0x37c3958e 
Status:  5 
Frame size in bits:  5024 
Payload size in bytes:  610 
Tail size in bits:  32 

Frame nr:  42 
DST addr:  0x2a 
SRC addr:  0xcf32cdcaa781 
Lentyp:  0x532 
Captured fcs:  0xfe0677d3 
Calculated CRC:  0xfe0677d3 
Status:  5 
Frame size in bits:  10784 
Payload size in bytes:  1330 
Tail size in bits:  32 

Frame nr:  43 
DST addr:  0x2b 
SRC addr:  0x18f28365bc13 
Lentyp:  0x1aa 
Captured fcs:  0x1cc94d77 
Calculated CRC:  0x1cc94d77 
Status:  5 
Frame size in bits:  3552 
Payload size in bytes:  426 
Tail size in bits:  32 

Frame nr:  44 
DST addr:  0x2c 
SRC addr:  0xde63c38428f3 
Lentyp:  0x4ec 
Captured fcs:  0x7cef540c 
Calculated CRC:  0x7cef540c 
Status:  5 
Frame size in bits:  10224 
Payload size in bytes:  1260 
Tail size in bits:  48 

Frame nr:  45 
DST addr:  0x2d 
SRC addr:  0x7083e84f20e7 
Lentyp:  0x387 
Captured fcs:  0xdecec71 
Calculated CRC:  0xdecec71 
Status:  5 
Frame size in bits:  7368 
Payload size in bytes:  903 
Tail size in bits:  8 

Frame nr:  46 
DST addr:  0x2e 
SRC addr:  0x7306503f764b 
Lentyp:  0x14e 
Captured fcs:  0x6c965243 
Calculated CRC:  0x6c965243 
Status:  5 
Frame size in bits:  2816 
Payload size in bytes:  334 
Tail size in bits:  0 

Frame nr:  47 
DST addr:  0x2f 
SRC addr:  0x2d782958604a 
Lentyp:  0x1e5 
Captured fcs:  0xee03c32a 
Calculated CRC:  0xee03c32a 
Status:  5 
Frame size in bits:  4024 
Payload size in bytes:  485 
Tail size in bits:  56 

Frame nr:  48 
DST addr:  0x30 
SRC addr:  0x9cf439252e8 
Lentyp:  0x4e4 
Captured fcs:  0xf75fe61e 
Calculated CRC:  0xf75fe61e 
Status:  5 
Frame size in bits:  10160 
Payload size in bytes:  1252 
Tail size in bits:  48 

Frame nr:  49 
DST addr:  0x31 
SRC addr:  0x6bd769a934ef 
Lentyp:  0x55d 
Captured fcs:  0xe4fa9c4a 
Calculated CRC:  0xe4fa9c4a 
Status:  5 
Frame size in bits:  11128 
Payload size in bytes:  1373 
Tail size in bits:  56 

Frame nr:  50 
DST addr:  0x32 
SRC addr:  0x79fb83eb15ad 
Lentyp:  0x308 
Captured fcs:  0x15a476b1 
Calculated CRC:  0x15a476b1 
Status:  5 
Frame size in bits:  6352 
Payload size in bytes:  776 
Tail size in bits:  16 

Frame nr:  51 
DST addr:  0x33 
SRC addr:  0xcbe119bb75f9 
Lentyp:  0x2ee 
Captured fcs:  0x4049b398 
Calculated CRC:  0x4049b398 
Status:  5 
Frame size in bits:  6144 
Payload size in bytes:  750 
Tail size in bits:  0 

Frame nr:  52 
DST addr:  0x34 
SRC addr:  0x10f71e359152 
Lentyp:  0xb4 
Captured fcs:  0x86bb654f 
Calculated CRC:  0x86bb654f 
Status:  5 
Frame size in bits:  1584 
Payload size in bytes:  180 
Tail size in bits:  48 

Frame nr:  53 
DST addr:  0x35 
SRC addr:  0x5b1aad21b622 
Lentyp:  0x206 
Captured fcs:  0x26e5d975 
Calculated CRC:  0x26e5d975 
Status:  5 
Frame size in bits:  4288 
Payload size in bytes:  518 
Tail size in bits:  0 

Frame nr:  54 
DST addr:  0x36 
SRC addr:  0xc4e4520c87ef 
Lentyp:  0x456 
Captured fcs:  0x1e02d0cb 
Calculated CRC:  0x1e02d0cb 
Status:  5 
Frame size in bits:  9024 
Payload size in bytes:  1110 
Tail size in bits:  0 

Frame nr:  55 
DST addr:  0x37 
SRC addr:  0x8eb8df66736a 
Lentyp:  0x4e9 
Captured fcs:  0x9cf61538 
Calculated CRC:  0x9cf61538 
Status:  5 
Frame size in bits:  10200 
Payload size in bytes:  1257 
Tail size in bits:  24 

Frame nr:  56 
DST addr:  0x38 
SRC addr:  0x7ddc65a2298b 
Lentyp:  0x283 
Captured fcs:  0x7946718d 
Calculated CRC:  0x7946718d 
Status:  5 
Frame size in bits:  5288 
Payload size in bytes:  643 
Tail size in bits:  40 

Frame nr:  57 
DST addr:  0x39 
SRC addr:  0xf8498cd003 
Lentyp:  0x3c5 
Captured fcs:  0x438545de 
Calculated CRC:  0x438545de 
Status:  5 
Frame size in bits:  7864 
Payload size in bytes:  965 
Tail size in bits:  56 

Frame nr:  58 
DST addr:  0x3a 
SRC addr:  0x70bb0c019f3a 
Lentyp:  0x52b 
Captured fcs:  0x8e72b1b3 
Calculated CRC:  0x8e72b1b3 
Status:  5 
Frame size in bits:  10728 
Payload size in bytes:  1323 
Tail size in bits:  40 

Frame nr:  59 
DST addr:  0x3b 
SRC addr:  0xa29906584692 
Lentyp:  0x580 
Captured fcs:  0x3d935b34 
Calculated CRC:  0x3d935b34 
Status:  5 
Frame size in bits:  11408 
Payload size in bytes:  1408 
Tail size in bits:  16 

Frame nr:  60 
DST addr:  0x3c 
SRC addr:  0xfdcbd1734e5e 
Lentyp:  0x3ec 
Captured fcs:  0x84a1d7b9 
Calculated CRC:  0x84a1d7b9 
Status:  5 
Frame size in bits:  8176 
Payload size in bytes:  1004 
Tail size in bits:  48 

Frame nr:  61 
DST addr:  0x3d 
SRC addr:  0x29a731b1abee 
Lentyp:  0x54b 
Captured fcs:  0x114de526 
Calculated CRC:  0x114de526 
Status:  5 
Frame size in bits:  10984 
Payload size in bytes:  1355 
Tail size in bits:  40 

Frame nr:  62 
DST addr:  0x3e 
SRC addr:  0xde40e606e564 
Lentyp:  0x425 
Captured fcs:  0x44cfc482 
Calculated CRC:  0x44cfc482 
Status:  5 
Frame size in bits:  8632 
Payload size in bytes:  1061 
Tail size in bits:  56 

Frame nr:  63 
DST addr:  0x3f 
SRC addr:  0x9cca4005ceb6 
Lentyp:  0x358 
Captured fcs:  0x5768b23a 
Calculated CRC:  0x5768b23a 
Status:  5 
Frame size in bits:  6992 
Payload size in bytes:  856 
Tail size in bits:  16 

Frame nr:  64 
DST addr:  0x40 
SRC addr:  0xa23d0d905eeb 
Lentyp:  0x559 
Captured fcs:  0x420af5da 
Calculated CRC:  0x420af5da 
Status:  5 
Frame size in bits:  11096 
Payload size in bytes:  1369 
Tail size in bits:  24 

Frame nr:  65 
DST addr:  0x41 
SRC addr:  0xd5e030da9aee 
Lentyp:  0x27e 
Captured fcs:  0x6aa5ba41 
Calculated CRC:  0x6aa5ba41 
Status:  5 
Frame size in bits:  5248 
Payload size in bytes:  638 
Tail size in bits:  0 

Frame nr:  66 
DST addr:  0x42 
SRC addr:  0x2cd639439e4d 
Lentyp:  0x568 
Captured fcs:  0xc0a8fb53 
Calculated CRC:  0xc0a8fb53 
Status:  5 
Frame size in bits:  11216 
Payload size in bytes:  1384 
Tail size in bits:  16 

Frame nr:  67 
DST addr:  0x43 
SRC addr:  0x2ec88806a5cf 
Lentyp:  0xaf 
Captured fcs:  0x1b764ff7 
Calculated CRC:  0x1b764ff7 
Status:  5 
Frame size in bits:  1544 
Payload size in bytes:  175 
Tail size in bits:  8 

Frame nr:  68 
DST addr:  0x44 
SRC addr:  0xc30be2223c37 
Lentyp:  0x5ce 
Captured fcs:  0x7fbded83 
Calculated CRC:  0x7fbded83 
Status:  5 
Frame size in bits:  12032 
Payload size in bytes:  1486 
Tail size in bits:  0 

Frame nr:  69 
DST addr:  0x45 
SRC addr:  0x6065152da6dc 
Lentyp:  0x2c5 
Captured fcs:  0x12ae02c4 
Calculated CRC:  0x12ae02c4 
Status:  5 
Frame size in bits:  5816 
Payload size in bytes:  709 
Tail size in bits:  56 

Frame nr:  70 
DST addr:  0x46 
SRC addr:  0xf187450c484 
Lentyp:  0xf7 
Captured fcs:  0xe14712dd 
Calculated CRC:  0xe14712dd 
Status:  5 
Frame size in bits:  2120 
Payload size in bytes:  247 
Tail size in bits:  8 

Frame nr:  71 
DST addr:  0x47 
SRC addr:  0x9673309f761d 
Lentyp:  0x4e9 
Captured fcs:  0xa98c0979 
Calculated CRC:  0xa98c0979 
Status:  5 
Frame size in bits:  10200 
Payload size in bytes:  1257 
Tail size in bits:  24 

Frame nr:  72 
DST addr:  0x48 
SRC addr:  0xeff980f6c9f6 
Lentyp:  0x451 
Captured fcs:  0xf476bfa9 
Calculated CRC:  0xf476bfa9 
Status:  5 
Frame size in bits:  8984 
Payload size in bytes:  1105 
Tail size in bits:  24 

Frame nr:  73 
DST addr:  0x49 
SRC addr:  0x7a5f03498d0d 
Lentyp:  0x474 
Captured fcs:  0x3b7e15ce 
Calculated CRC:  0x3b7e15ce 
Status:  5 
Frame size in bits:  9264 
Payload size in bytes:  1140 
Tail size in bits:  48 

Frame nr:  74 
DST addr:  0x4a 
SRC addr:  0x373a6beb3f46 
Lentyp:  0x4c8 
Captured fcs:  0xf651bc56 
Calculated CRC:  0xf651bc56 
Status:  5 
Frame size in bits:  9936 
Payload size in bytes:  1224 
Tail size in bits:  16 

Frame nr:  75 
DST addr:  0x4b 
SRC addr:  0x7dfffbccb044 
Lentyp:  0x255 
Captured fcs:  0xf04951c4 
Calculated CRC:  0xf04951c4 
Status:  5 
Frame size in bits:  4920 
Payload size in bytes:  597 
Tail size in bits:  56 

Frame nr:  76 
DST addr:  0x4c 
SRC addr:  0xeac99aac2543 
Lentyp:  0x2a6 
Captured fcs:  0x2cd482dd 
Calculated CRC:  0x2cd482dd 
Status:  5 
Frame size in bits:  5568 
Payload size in bytes:  678 
Tail size in bits:  0 

Frame nr:  77 
DST addr:  0x4d 
SRC addr:  0xcb9c228b3354 
Lentyp:  0x32f 
Captured fcs:  0xf393f25c 
Calculated CRC:  0xf393f25c 
Status:  5 
Frame size in bits:  6664 
Payload size in bytes:  815 
Tail size in bits:  8 

Frame nr:  78 
DST addr:  0x4e 
SRC addr:  0xfb6ff21d4236 
Lentyp:  0x454 
Captured fcs:  0xe80d7e92 
Calculated CRC:  0xe80d7e92 
Status:  5 
Frame size in bits:  9008 
Payload size in bytes:  1108 
Tail size in bits:  48 

Frame nr:  79 
DST addr:  0x4f 
SRC addr:  0x746e6b2e849f 
Lentyp:  0x5d3 
Captured fcs:  0xf757ee18 
Calculated CRC:  0xf757ee18 
Status:  5 
Frame size in bits:  12072 
Payload size in bytes:  1491 
Tail size in bits:  40 

Frame nr:  80 
DST addr:  0x50 
SRC addr:  0xecea2225699c 
Lentyp:  0x85 
Captured fcs:  0x5bdaa327 
Calculated CRC:  0x5bdaa327 
Status:  5 
Frame size in bits:  1208 
Payload size in bytes:  133 
Tail size in bits:  56 

Frame nr:  81 
DST addr:  0x51 
SRC addr:  0xe11d2b570e35 
Lentyp:  0x49a 
Captured fcs:  0xdc391a60 
Calculated CRC:  0xdc391a60 
Status:  5 
Frame size in bits:  9568 
Payload size in bytes:  1178 
Tail size in bits:  32 

Frame nr:  82 
DST addr:  0x52 
SRC addr:  0x51b0fdfde260 
Lentyp:  0x96 
Captured fcs:  0xd6fcd600 
Calculated CRC:  0xd6fcd600 
Status:  5 
Frame size in bits:  1344 
Payload size in bytes:  150 
Tail size in bits:  0 

Frame nr:  83 
DST addr:  0x53 
SRC addr:  0x33501c0464ee 
Lentyp:  0x292 
Captured fcs:  0xd25187d6 
Calculated CRC:  0xd25187d6 
Status:  5 
Frame size in bits:  5408 
Payload size in bytes:  658 
Tail size in bits:  32 

Frame nr:  84 
DST addr:  0x54 
SRC addr:  0xb00a6aeff10d 
Lentyp:  0x3aa 
Captured fcs:  0xdd00bba 
Calculated CRC:  0xdd00bba 
Status:  5 
Frame size in bits:  7648 
Payload size in bytes:  938 
Tail size in bits:  32 

Frame nr:  85 
DST addr:  0x55 
SRC addr:  0x1f166f84f8da 
Lentyp:  0x302 
Captured fcs:  0x8244f712 
Calculated CRC:  0x8244f712 
Status:  5 
Frame size in bits:  6304 
Payload size in bytes:  770 
Tail size in bits:  32 

Frame nr:  86 
DST addr:  0x56 
SRC addr:  0xab663d6467f5 
Lentyp:  0x32b 
Captured fcs:  0x619b5e27 
Calculated CRC:  0x619b5e27 
Status:  5 
Frame size in bits:  6632 
Payload size in bytes:  811 
Tail size in bits:  40 

Frame nr:  87 
DST addr:  0x57 
SRC addr:  0xec8b1d4a6ee0 
Lentyp:  0x558 
Captured fcs:  0xcef2c84a 
Calculated CRC:  0xcef2c84a 
Status:  5 
Frame size in bits:  11088 
Payload size in bytes:  1368 
Tail size in bits:  16 

Frame nr:  88 
DST addr:  0x58 
SRC addr:  0x4ef17a7fd837 
Lentyp:  0x373 
Captured fcs:  0x59c5b626 
Calculated CRC:  0x59c5b626 
Status:  5 
Frame size in bits:  7208 
Payload size in bytes:  883 
Tail size in bits:  40 

Frame nr:  89 
DST addr:  0x59 
SRC addr:  0xe3cce0d98694 
Lentyp:  0x347 
Captured fcs:  0x89394493 
Calculated CRC:  0x89394493 
Status:  5 
Frame size in bits:  6856 
Payload size in bytes:  839 
Tail size in bits:  8 

Frame nr:  90 
DST addr:  0x5a 
SRC addr:  0x89f81b8691 
Lentyp:  0x2f0 
Captured fcs:  0x562cfe1a 
Calculated CRC:  0x562cfe1a 
Status:  5 
Frame size in bits:  6160 
Payload size in bytes:  752 
Tail size in bits:  16 

Frame nr:  91 
DST addr:  0x5b 
SRC addr:  0x2e03839f347 
Lentyp:  0x163 
Captured fcs:  0xe4f50d08 
Calculated CRC:  0xe4f50d08 
Status:  5 
Frame size in bits:  2984 
Payload size in bytes:  355 
Tail size in bits:  40 

Frame nr:  92 
DST addr:  0x5c 
SRC addr:  0xcdadf86c0781 
Lentyp:  0x3ab 
Captured fcs:  0x3071537f 
Calculated CRC:  0x3071537f 
Status:  5 
Frame size in bits:  7656 
Payload size in bytes:  939 
Tail size in bits:  40 

Frame nr:  93 
DST addr:  0x5d 
SRC addr:  0x20a9c46ffda7 
Lentyp:  0x5c1 
Captured fcs:  0x8552a537 
Calculated CRC:  0x8552a537 
Status:  5 
Frame size in bits:  11928 
Payload size in bytes:  1473 
Tail size in bits:  24 

Frame nr:  94 
DST addr:  0x5e 
SRC addr:  0xe6f42e891015 
Lentyp:  0x197 
Captured fcs:  0x627143da 
Calculated CRC:  0x627143da 
Status:  5 
Frame size in bits:  3400 
Payload size in bytes:  407 
Tail size in bits:  8 

Frame nr:  95 
DST addr:  0x5f 
SRC addr:  0xd2fa7dadc5b6 
Lentyp:  0x220 
Captured fcs:  0x68f4f35d 
Calculated CRC:  0x68f4f35d 
Status:  5 
Frame size in bits:  4496 
Payload size in bytes:  544 
Tail size in bits:  16 

Frame nr:  96 
DST addr:  0x60 
SRC addr:  0xa356886b062d 
Lentyp:  0x396 
Captured fcs:  0x883a4cdf 
Calculated CRC:  0x883a4cdf 
Status:  5 
Frame size in bits:  7488 
Payload size in bytes:  918 
Tail size in bits:  0 

Frame nr:  97 
DST addr:  0x61 
SRC addr:  0xf9bc133e125 
Lentyp:  0x478 
Captured fcs:  0x98d70d9c 
Calculated CRC:  0x98d70d9c 
Status:  5 
Frame size in bits:  9296 
Payload size in bytes:  1144 
Tail size in bits:  16 

Frame nr:  98 
DST addr:  0x62 
SRC addr:  0xd32d69026a90 
Lentyp:  0x5be 
Captured fcs:  0x8b90af69 
Calculated CRC:  0x8b90af69 
Status:  5 
Frame size in bits:  11904 
Payload size in bytes:  1470 
Tail size in bits:  0 

Frame nr:  99 
DST addr:  0x63 
SRC addr:  0xbb0a46dbe665 
Lentyp:  0x53c 
Captured fcs:  0x9a69108 
Calculated CRC:  0x9a69108 
Status:  5 
Frame size in bits:  10864 
Payload size in bytes:  1340 
Tail size in bits:  48 

