import sys
import random
import binascii
import csv
import re
from crccheck.crc import Crc32Bzip2

input_data_file_path = "./sim_data.mem"
input_control_file_path = "./sim_control.mem"
input_datavalid_file_path = "./sim_datavalid.mem"
output_file_path = "./sim_data_indication.mem"

###########################
NB_DATA = 64
NBITS_SRC_ADDR = 6*8
NBITS_DST_ADDR = 6*8
NBITS_LENTYP = 2*8
NBITS_CRC = 32
NBITS_IPG = 96
PREAMBLE = 7 * str(10101010)
SOF = str(10101011)
MAX_FRAME_SIZE_BITS = 2000*8
###########################
local_addr = "9"
broadcast_addr = "a2b2"
multicast_addr = ["0","1","2"]
accepted_addr = [bin(int(local_addr,16))[2:].zfill(NBITS_SRC_ADDR)]+[bin(int(broadcast_addr,16))[2:].zfill(NBITS_SRC_ADDR)]+[bin(int(x,16))[2:].zfill(NBITS_SRC_ADDR) for x in multicast_addr]


#Control
data_valid = ''
frame_size_bits_list = list()
data_size_bytes_list = list()
tail_size_bytes_list = list()

sof_list = list()
dst_addr_list = list()
src_addr_list = list()
lentyp_list = list()
payload_list = list()
fcs_list = list()
calc_crc_list = list()
rxstatus_list = list()

def parse_frames():
    #DATAVALID parsing
    datavalid_file = open(input_datavalid_file_path, "r")
    data_valid = datavalid_file.read()
    print("datavalid length: ", len(data_valid))
    datavalid_file.close()

    #CONTROL SIGNALS parsing
    control_file = open(input_control_file_path, "r")
    string_control_list = control_file.read().split(" ")
    for i in range (0, len(string_control_list)-2, 3):
        frame_size_bits_list.append(int(string_control_list[i],2))
        data_size_bytes_list.append(int(string_control_list[i+1],2))
        tail_size_bytes_list.append(int(string_control_list[i+2],2))
    control_file.close()
    print("frame_size_bits_list: ", frame_size_bits_list);
    print("data_size_bytes_list: ", data_size_bytes_list);
    print("tail_size_bytes_list: ", tail_size_bytes_list);

    #DATA parsing
    data_file = open(input_data_file_path, "r")
    string_file_list = data_file.read().split(" ")
    #string_file = ''.join(string_file_list)
    string_file = ''

    print("len(string_file_list): ", len(string_file_list))
    print("len(data_valid): ", len(data_valid))
    #import pdb; pdb.set_trace()
    for s in range (len(string_file_list)):
        if data_valid[s]=='1':
            string_file = string_file + string_file_list[s]

    data_file.close()


    #Begin fields capturing
    find_sof_preamble(string_file)
    capture_fields(string_file)
    crc_calc()
    rx_status_gen()


def find_sof_preamble(string_file):
    '''
    @brief Takes a string containing the binary data and finds all the preamble
    and starts of frame in the data influx
    @param string_file is the string containing the data influx
    '''
    preamble_and_sof = PREAMBLE+SOF
    string_to_compare = string_file

    for match in re.finditer(preamble_and_sof, string_to_compare):
        sof_list.append(match.end())


def capture_fields(string_file):
    '''
    @brief Takes a string containing the binary data and gets all the frames fields.
    It uses the sof_list() which contains the addresses of the end of the PRE+SOF
    @param string_file is the string containing the data influx
    Parses the data
    '''
    for i in range (len(sof_list)):
        pointer = sof_list[i]
        dst_addr_list.append(string_file[pointer:pointer+NBITS_DST_ADDR])
        pointer += NBITS_DST_ADDR
        src_addr_list.append(string_file[pointer:pointer+NBITS_SRC_ADDR])
        pointer += NBITS_DST_ADDR
        lentyp_list.append(string_file[pointer:pointer+NBITS_LENTYP])
        pointer += NBITS_LENTYP
        payloadsize_bits = int(lentyp_list[i],2)*8
        payload_list.append(string_file[pointer:pointer+payloadsize_bits])
        pointer += payloadsize_bits
        fcs_list.append(string_file[pointer:pointer+NBITS_CRC])
    #import pdb; pdb.set_trace()


def rx_status_gen():
    '''
    @brief Generates a status code according to the reception status
    0: receiveOK.
    1: frameTooLong. Frame exceeds maximum permitted size
    2: frameCheckError. CRC32 generated and the one encapsulated in incoming frame don't match
    3: lengthError. lentyp and payloadsize don't match
    4: alignmentError. Ammount of bits received are not a multiple of 8
    5: addressError. DST address doesn't match with local station's list of accepted addresses
    '''
    for i in range (len(sof_list)):
        #import pdb; pdb.set_trace()
        rx_status = 0
        frame_length = len(dst_addr_list[i]+src_addr_list[i]+lentyp_list[i]+payload_list[i]+fcs_list[i]);
        if (frame_size_bits_list[i]>MAX_FRAME_SIZE_BITS):
            rx_status = 1 #frameTooLong
        elif (calc_crc_list[i]!=fcs_list[i]):
            rx_status = 2 #frameCheckError
        elif (int(lentyp_list[i],2)!=data_size_bytes_list[i]): #lentyp_list needs to be cast to int(x,2), it's a binary str
            rx_status = 3 #lengthError
        elif ((frame_length)%8!=0):
            rx_status = 4 #alignmentError
        elif (not dst_addr_list[i] in accepted_addr):
            rx_status = 5 #addressError
        else:
            rx_status = 0
        rxstatus_list.append(rx_status)

def crc_calc():
    '''
    @brief Calculate a CRC-32/Bzip2 according to 802.3 IEEE Standard for Ethernet
    '''
    for i in range (len(sof_list)):
        frame_hex = hex(int('1000'+dst_addr_list[i]+src_addr_list[i]+lentyp_list[i]+payload_list[i],2))[3:] #FIXME: Implementar de otra forma mas prolija, no tomar a pàrtir del segundo caracter
        crc32_int = Crc32Bzip2.calc(bytes.fromhex(frame_hex))
        crc32 = bin(crc32_int)[2:].zfill(32)
        calc_crc_list.append(crc32)


parse_frames()

output_file = open(output_file_path, "w")
for i in range (len(sof_list)):
    output_file.write(str(dst_addr_list[i])+" ")
    output_file.write(str(src_addr_list[i])+" ")
    output_file.write(str(lentyp_list[i])+" ")
    output_file.write(str(payload_list[i])+" ")
    output_file.write(str(fcs_list[i])+" ")
    output_file.write(str(calc_crc_list[i])+" ")
    output_file.write(str(bin(rxstatus_list[i])[2:].zfill(3))+" ")
output_file.close()

'''
print("Nº sofs: ", len(sof_list), "\nEn las posiciones: ", sof_list)
print ("Lentyp: ", [hex(int(s,2)) for s in lentyp_list])
print ("Codidos de estado: ", rxstatus_list)
print ("Captured fcs: ", [hex(int(s,2)) for s in fcs_list])
print ("Calculated crc: ", [hex(int(s,2)) for s in calc_crc_list])
'''

#print(hex(int(payload_list[0],2))[-128:])
#import pdb; pdb.set_trace()
for f in range(len(sof_list)):
    print ("Frame nr: ", f, "\nDST addr: ", hex(int(dst_addr_list[f],2)), "\nSRC addr: ", hex(int(src_addr_list[f],2)), "\nLentyp: ", hex(int(lentyp_list[f],2)), "\nCaptured fcs: ", hex(int(fcs_list[f],2)), "\nCalculated CRC: ", hex(int(calc_crc_list[f],2)), "\nStatus: ", rxstatus_list[f], "\nFrame size in bits: ", frame_size_bits_list[f], "\nPayload size in bytes: ", data_size_bytes_list[f], "\nTail size in bits: ", tail_size_bytes_list[f], "\n")
