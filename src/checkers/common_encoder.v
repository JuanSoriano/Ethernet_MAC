module common_encoder
#(
    // PARAMETERS.
    parameter                                           NB_VECTOR           = 8 ,
    parameter                                           NB_ENCODER          = 4
)
(
    // OUTPUTS.
    output  wire    [NB_ENCODER-1:0]                    o_encode                 ,
    // INPUTS.
    input   wire    [NB_VECTOR-1:0]                     i_vector
);

    // LOCAL PARAMETERS.
    // None so far.

    // INTERNAL SIGNALS.
    integer                                             i                       ;
    reg    [NB_ENCODER-1:0]                             encode                  ;

    // ALGORITHM BEGIN.
    always  @( * )
    begin : encoder_block
        encode  =  {NB_ENCODER{1'b0}};
        for( i=0; i<NB_VECTOR; i=i+1 )
            encode  = encode +i_vector[i];
    end


    assign o_encode = encode;

/*
    `ifdef FUNCTIONAL_ASSERTIONS
        wire enable_assertion = 1'b0;
        sva_probando1 : assert property(
            @(i_vector)
            disable iff(enable_assertion)
            !$isunknown(o_encode)
        );
    `endif
*/
endmodule // common_encoder