

/* preamble:7*8, sofd:8, dst-addr:6*8,  src-addr:6*8, length/dt:2*8, data:46*8--1500*8/1504*8--1982*8, crc:4*8, ext:??? */
//7+1+6+6+2=22 --> +46

/* dst-addr:6*8,  src-addr:6*8, length/dt:2*8, data:46*8--1500*8/1504*8--1982*8, crc:4*8, ext:??? */



// [NOTE] The block is designed to work with a parallesim multiple of 8bits, and no greater than 512bits.
// [NOTE] We assume that extension bits are not required for the above layer and are thus dropped.

// P=C*f*(V**2).


module frame_generator_a
#(
    // Parameters.
    parameter                                   NB_DATA         = 64 ,
    parameter                                   LOG2_NB_DATA    = 6 ,
    parameter                                   LOG2_FRAME_SIZE = 9 , //256
    parameter                                   NB_DST_ADDR     = 48 ,    // HINT: This is the only legal value.
    parameter                                   NB_SRC_ADDR     = 48 ,    // HINT: This is the only legal value.
    parameter                                   NB_LENTYP       = 16 ,    // HINT: This is the only legal value.
    parameter                                   NB_CRC          = 32      // HINT: This is the only legal value.
)
(
    // Outputs.
    output  wire    [NB_DATA-1:0]               o_data ,
    output  wire                                o_data_valid ,

    // Inputs.
    input   wire                                i_valid ,
    input   wire                                i_frame_ready , // It is a pulse that indicates that a frame has arrived to the RX FIFO
    input   wire                                i_reset ,
    input   wire                                i_clock
) ;

    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================
    localparam                                  NB_TIMER                = LOG2_FRAME_SIZE ;
    
    // FSM States
    localparam                                  NB_STATE                = 3 ;
    localparam                                  N_LEGAL_DST_ADDR        = 10 ;
    localparam                                  N_LEGAL_SRC_ADDR        = 10 ;
    localparam                                  MIN_LENGTH_A_BYTES      = 46 ;
    localparam                                  MAX_LENGTH_A_BYTES      = 1500 ;
    localparam                                  MIN_LENGTH_B_BYTES      = 1504 ;
    localparam                                  MAX_LENGTH_B_BYTES      = 1982 ;
    localparam                                  NB_BYTE                 = 8 ;
    localparam                                  MAX_IF_GAP              = 100 ;
    localparam                                  MIN_IF_GAP              = 10 ;
    


    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
    reg             [NB_STATE-1:0]              state ;
    reg             [NB_STATE-1:0]              state_next ;


    reg             [NB_DST_ADDR-1:0]           dst_addr_list   [N_LEGAL_DST_ADDR-1:0] ;
    reg             [NB_SRC_ADDR-1:0]           src_addr_list   [N_LEGAL_DST_ADDR-1:0] ;



    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    // State update.
    always @( posedge i_clock )
    begin
        if ( i_reset )
            state <= ST_0_LOAD_DST_ADDR ;
        else if ( i_valid )
            state <= state_next ;
    end


    always @( * )
    begin

        fsmo_reset_timer    = ~i_frame_ready ;
        fsmo_frame_end      = 1'b0 ;
        state_next          = ST_0_INIT ;

        case ( state )

            ST_0_LOAD_DST_ADDR :
            begin
                state_next      = ST_1_LOAD_SRC_ADDR ;
                dst_addr        = dst_addr_list[ {$random()} % N_LEGAL_DST_ADDR ] ;
                src_addr        = src_addr_list[ {$random()} % N_LEGAL_SRC_ADDR ] ;
                data_mode       = {$random}%2 ;
                length          = ({$random()} % ( MAX_LENGTH_A - MIN_LENGTH_A + 1 ) + MIN_LENGTH_A)*NB_BYTE ;    // FIXME: Ver como generar frames tabulados.
                if_gap          = {$random()} % ( MAX_IF_GAP - MIN_IF_GAP + 1 ) + MIN_IF_GAP ;
                reset_timer     = 1'b1 ;
                frame_size      = NB_DST_ADDR + NB_SRC_ADDR + length + NB_CRC ;
                frame_clocks    = ( frame_size % NB_DATA == 0 )? ( frame_size / NB_DATA ) : ( 1 + frame_size / NB_DATA ) ;
                data_tail_size  = ( length - (NB_DATA - (NB_SRC_ADDR - (NB_DATA-NB_DST_ADDR))) ) % NB_DATA ;
                crc_tail_size   = ( ( frame_size % NB_DATA ) <= NB_CRC )? ( frame_size % NB_DATA ) : 0 ;
                crc_tail_size_b = NB_DATA - data_tail_size ;
                crc_extra_clock = ( frame_size % NB_DATA <= NB_CRC ) ;
                o_data          = { dst_addr, src_addr[ NB_SRC_ADDR-1 -: (NB_DATA-NB_DST_ADDR) ] } ;
            end

            ST_1_LOAD_SRC_ADDR :
            begin
                state_next  = ST_2_LOAD_DATA ;
                if ( data_mode==0/*random*/ )
                    data_word   = { $random(), $random() } ;
                else /*counter*/
                    data_word   = 0 ;
                reset_timer = 1'b1 ;
                o_data      = { src_addr[ 0 +: NB_SRC_ADDR - (NB_DATA-NB_DST_ADDR) ], data_word[ NB_DATA-1 -: (NB_DATA - (NB_SRC_ADDR - (NB_DATA-NB_DST_ADDR))) ] } ;
            end

            ST_2_LOAD_DATA :
            begin
                if ( timer_done && crc_extra_clock )
                    state_next  = ST_4_LOAD_LAST_DATA_AND_PART_CRC ;
                else
                    state_next  = ST_3_LOAD_LAST_DATA_AND_FULL_CRC ;
                if ( data_mode==0/*random*/ )
                    data_word   = { $random(), $random() } ;
                else /*counter*/
                    data_word   = timer ;
                reset_timer = 1'b0 ;
                o_data      = data_word ;
            end

            ST_3_LOAD_LAST_DATA_AND_FULL_CRC :
            begin
                state_next  = ST_6_WAIT_IF_GAP ;
                if ( data_mode==0/*random*/ )
                    data_word   = { $random(), $random() } ;
                else /*counter*/
                    data_word   = timer ;
                reset_timer = 1'b1 ;
                o_data = 0 ;
                o_data[ NB_DATA-1 - data_tail_size -: NB_CRC ]  = crc ;
                for ( n=0; n<NB_DATA; n=n+1 )
                    if ( n<data_tail_size )
                        o_data[ NB_DATA-1-n ]
                            = data_word[ NB_DATA-1-n ] ;
            end

            ST_4_LOAD_LAST_DATA_AND_FULL_CRC
            begin
                state_next  = ST_5_LOAD_LAST_CRC ;
                for ( n=0; n<NB_DATA; n=n+1 )
                    if ( n<data_tail_size )
                        o_data[ NB_DATA-1-n ]
                            = data_word[ NB_DATA-1-n ] ;
                    else
                        o_data[ NB_DATA-1-n ]
                            = crc[NB_CRC-1-n] ;
            end

            ST_5_LOAD_LAST_CRC :
            begin
                state_next  = ST_6_WAIT_IF_GAP ;
                o_data = 0 ;
                for ( n=0; n<NB_DATA; n=n+1 )
                    if ( n<crc_tail_size )
                        o_data[ NB_DATA-1-n ]
                            = crc[ crc_tail_size-1-n ] ;
            end


        endcase
    end


    assign  sizeb_frame
                = ( i_frame_ready )? SIZEB_HEADER + O_LEN_TYP_MIN : SIZEB_HEADER + o_len_typ ;
    always @( posedge i_clock )
    begin
        if ( i_reset || i_valid && fsmo_reset_timer )
            timer
                <= {NB_TIMER{1'b0}} ;
        else if ( i_valid && !frame_done_timer )
            timer
                <= timer + 1'b1 ;
    end
    assign  frame_done_timer
                = (timer*NB_DATA) >= sizeb_frame ;


    assign  get_dst_addr_level
                = i_frame_ready ;
    assign  get_src_addr_level
                = ( timer * NB_DATA ) >= NB_DST_ADDR ;
    assign  get_len_typ_level
                = ( timer * NB_DATA ) >= ( NB_DST_ADDR + NB_SRC_ADDR );
    assign  enable_o_data
                = ( timer * NB_DATA ) >= ( NB_DST_ADDR + NB_SRC_ADDR + NB_LENTYP );
    assign  stop_o_data
                = ( timer * NB_DATA ) >= ( NB_DST_ADDR + NB_SRC_ADDR + NB_LENTYP + o_len_typ  );

    assign   data_valid_o
                = enable_o_data & ~stop_o_data & i_dst_addr_ok ;
    always @( posedge i_clock )
        if ( i_valid )
            o_data_valid
                <= data_valid_o ;

    always @( posedge i_clock )
    begin
        if ( i_valid )
            data_i_d
                <= i_data ;
        if ( i_valid && data_valid_o )
            o_data
                <= data_i_ext[ NB_DATA_EXT - OFFSET_DATA - 1 -: NB_DATA  ] ;
    end
    assign  data_i_ext
                = { data_i_d, i_data } ;

    assign  o_crc_valid
                = get_dst_addr_level & ~stop_o_data ;


    data_locker_w_trigger
    #(
        .NB_DATA    ( NB_DATA               ),
        .NB_BUFFER  ( NB_DST_ADDR           ),
        .OFFSET     ( 0                     ),
        .RESET_VAL  ( 0                     )
    )
    u_data_locker_w_trigger__dst
    (
        .o_data     ( o_dst_addr            ),
        .o_ready    ( /*unused*/            ),
        .i_data     ( i_data                ),
        .i_valid    ( i_valid               ),
        .i_trigger  ( get_dst_addr          ),
        .i_reset    ( i_reset               ),
        .i_clock    ( i_clock               )
    ) ;


    data_locker_w_trigger
    #(
        .NB_DATA    ( NB_DATA               ),
        .NB_BUFFER  ( NB_SRC_ADDR           ),
        .OFFSET     ( NB_DST_ADDR % NB_DATA ),
        .RESET_VAL  ( 0                     )
    )
    u_data_locker_w_trigger__src
    (
        .o_data     ( o_src_addr            ),
        .o_ready    ( /*unused*/            ),
        .i_data     ( i_data                ),
        .i_valid    ( i_valid               ),
        .i_trigger  ( get_src_addr          ),
        .i_reset    ( i_reset               ),
        .i_clock    ( i_clock               )
    ) ;


    data_locker_w_trigger
    #(
        .NB_DATA    ( NB_DATA               ),
        .NB_BUFFER  ( NB_LENTYP             ),
        .OFFSET     ( (NB_DST_ADDR + NB_DST_ADDR) % NB_DATA ),
        .RESET_VAL  ( O_LEN_TYP_MIN         )
    )
    u_data_locker_w_trigger__typ
    (
        .o_data     ( o_len_typ             ),
        .o_ready    ( /*unused*/            ),
        .i_data     ( i_data                ),
        .i_valid    ( i_valid               ),
        .i_trigger  ( get_len_typ           ),
        .i_reset    ( i_reset               ),
        .i_clock    ( i_clock               )
    ) ;


    data_locker_w_trigger
    #(
        .NB_DATA    ( NB_DATA               ),
        .NB_BUFFER  ( NB_DATA+NB_CRC        ),
        .OFFSET     ( 0                     )
    )
    u_data_locker_w_trigger__crc
    (
        .o_data     ( last_data_and_crc     ),
        .o_ready    ( /*unused*/            ),
        .i_data     ( i_data                ),
        .i_valid    ( i_valid               ),
        .i_trigger  ( stop_o_data_pos_e     ),
        .i_reset    ( i_reset               ),
        .i_clock    ( i_clock               )
    ) ;
    assign  crc_index
                = ( SIZEB_HEADER + o_len_typ ) % NB_DATA ;
    assign  o_crc
                = last_data_and_crc[ NB_DATA+NB_CRC-1 - crc_index -: NB_CRC ] ;


    // Posedge generation

    // DST ADDR
    always @ (posedge i_clock)
    begin

        pos_edge_dst <= get_dst_addr_level ;
    end

    assign pe = get_dst_addr_level & ~ pos_edge_dst ;

    // SRC ADDR
    always @ (posedge i_clock)
    begin

        pos_edge_src <= get_dst_addr_level ;
    end

    assign pe = get_dst_addr_level & ~ pos_edge_src ;

    // LENTYP ADDR
    always @ (posedge i_clock)
    begin

        pos_edge_lentyp <= get_len_typ_level ;
    end

    assign pe = get_len_typ_level & ~ pos_edge_lentyp ;


    


endmodule