module prbs_test();
   localparam NB_DATA = 64;
   localparam LOG2_NB_DATA = 6;

   wire [NB_DATA-1:0]    data_o;
   wire                  start_gen_i;
   wire                  valid_i;
   wire                  enable_i;
   wire                  reset;

   wire                  prbs_lock;

   reg                   clock = 1'b0;
   integer               timer = 0;
   always
     begin
        #(50) clock = ~clock ;
     end

   always @ ( posedge clock )
     begin
        timer   <= timer + 1;
     end

   assign reset = (timer == 2) ; // Reset at time 2
   assign start_gen_i = timer == 6;
   assign enable_i = 1'b1;
   assign valid_i = timer > 5;

   t_prbs_generator
     #(
       .NB_DATA                  ( NB_DATA      ),
       .TAP1                     ( 7            ),
       .TAP2                     ( 6            ),  // TAP1 > TAP2.
       .MSB_IS_NEWER             ( 1            )
       )
   u_t_prbs_generator
     (
      .o_data_out                ( data_o       ),
      .i_start_gen               ( start_gen_i  ),
      .i_valid                   ( valid_i      ),
      .i_enable                  ( enable_i     ),
      .i_reset                   ( reset        ),
      .clock                     ( clock        )
      );

   t_prbs_checker
     #(
       .NB_DATA                  ( NB_DATA      ),
       .F_MINLOG2_NB_DATA        ( LOG2_NB_DATA ),
       .NB_COUNTER               ( 10           ),
       .TAP1                     ( 7            ),
       .TAP2                     ( 6            ),  // TAP1 > TAP2.
       .MSB_IS_NEWER             ( 1            )
       )
   u_t_prbs_checker
     (
      .o_prbs_in_lock            ( prbs_lock    ),
      .i_data_in                 ( data_o       ),
      .i_valid                   ( valid_i      ),
      .i_bad_word_ones_threshold ( 6'd1        ),
      .i_bad_word_limit          ( 10'd3       ),
      .i_reset                   ( reset        ),
      .clock                     ( clock        )
      );
endmodule
