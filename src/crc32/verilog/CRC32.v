/*
 This block is an implementation of CRC32-32 calculation without reflection according to IEEE Standard for Ethernet 802.3.
 It computes the CRC32 for each instance of i_data until i_data_valid drops. Once the calculation is completed,
 o_crc32_valid outputs if it's a match between the i_crc input and the FCS calculated in this block. o_crc32 will output
 the calculated FCS and o_crc_ready will notify when the o_crc32 has a valid output.
 */
module CRC32
  #(
    // Parameters.
    parameter NB_DATA          = 64 ,
    parameter LOG2_NB_DATA     = 6 ,
    parameter CRC32_POLYNOMIAL = 33'h104C11DB7, //1 0000 0100 1100 0001 0001 1101 1011 0111
    parameter NB_CRC32         = 32,
    parameter REF_IN           = 1,
    parameter REF_OUT          = 1
    )
   (
    // Outputs.
    output wire                   o_crc32_valid , // Output a 1 to signal a match in CRC32
    output reg                    o_crc_ready ,
    output reg [NB_CRC32-1:0]     o_crc32 ,

    // Inputs.
    input wire [NB_DATA-1:0]      i_data , // Data to compute and compare with i_crc32
    input wire                    i_data_valid , // i_data is a valid input to compute
    input wire [NB_CRC32-1:0]     i_crc32 , // Received CRC32 to compare
    input wire                    i_crc32_valid , //i_crc32 is a valid input to capture
    input wire [LOG2_NB_DATA-1:0] /*NOT CONNECTED*/ i_tail_size_bits , // Number of valid bits in the last i_data_valid clock

    input wire                    i_valid , // Throughput control.
    input wire                    i_reset ,
    input wire                    i_clock
    ) ;
   //==========================================================================
   // LOCAL PARAMETERS.
   //==========================================================================
   localparam NB_POLY = NB_CRC32 + 1 ;
   localparam LOGICAL_SHIFTING = 0;

   localparam NB_BYTE = 8;
   //==========================================================================
   // INTERNAL SIGNALS.
   //==========================================================================
   reg                            data_valid_d ;
   reg                            data_valid_dd ;
   reg                            data_valid_ddd;

   reg [NB_DATA-1:0]              data_d;
   wire                           data_valid_pos_d;
   wire                           data_valid_neg ;
   wire                           data_valid_neg_d ;
   wire                           data_valid_neg_dd;

   reg [NB_DATA+NB_DATA+NB_CRC32-1:0] data_ext_aux ;

   reg                                 crc_32_valid ;
   reg [NB_CRC32-1:0]                  data_tmp ;  //Contains the calculation to be carried over to the next iteration
   wire [NB_CRC32-1:0]                 compl_data_tmp ;
   reg [NB_CRC32+NB_DATA-1:0]          data_ext ;  //Contains the 32 bits carried over from previous iteration, the new i_data and 32 0s to compute next data_tmp

   reg [NB_CRC32+NB_DATA-1:0]          shifted_poly_ext ;  //Contains crc32 polynomial and is shifted in the algorithm to perform XOR
   reg [NB_CRC32-1:0]                  local_i_crc32 ;

   integer                             i,j,k ;

   reg [NB_DATA-1:0]                   flipped_data_in;
   reg [NB_CRC32-1:0]                  flipped_data_out;
   //==========================================================================
   // ALGORITHM.
   //==========================================================================

   //The signal o_crc_ready needs to be delayed
   always @( posedge i_clock )
     if ( data_valid_neg_dd )
       o_crc32 <= flipped_data_out ;
   always @( posedge i_clock )
     if ( i_reset )
       o_crc_ready <= 1'b0 ;
     else
       o_crc_ready <= data_valid_neg_dd ;

   //crc_32_valid comparator to check if it's a match
   always @ (posedge i_clock)
     begin
        if (i_reset)
          crc_32_valid <= 1'b0 ;
        else if (i_valid && data_valid_neg_dd)
          crc_32_valid <= (local_i_crc32 == flipped_data_out) ;
     end

   assign o_crc32_valid = crc_32_valid ;
   assign compl_data_tmp = ~ data_tmp;


   // Local copy of i_crc32
   always @ (posedge i_clock)
     begin
        if (i_reset)
          local_i_crc32 <= {NB_CRC32{1'b0}} ;
        else if (i_valid && i_crc32_valid)
          local_i_crc32 <= i_crc32;
     end

   //Flip data in
   always @ (*)
     begin
	     j = 0;
		  k = 0;
        if (REF_IN == 1)
          for (j=0;j<NB_DATA/NB_BYTE;j=j+1)
            for (k=0;k<NB_BYTE;k=k+1)
              flipped_data_in[j*NB_BYTE+k] = i_data[j*NB_BYTE+(NB_BYTE-1-k)];
        else
          flipped_data_in = i_data;
     end

   //Flip data out
   always @ (*)
     begin
		  j = 0;
		  k = 0;
        if (REF_OUT == 1)
          for (j=0;j<NB_CRC32/NB_BYTE;j=j+1)
            for (k=0;k<NB_BYTE;k=k+1)
              flipped_data_out[j*NB_BYTE+k] = compl_data_tmp[j*NB_BYTE+(NB_BYTE-1-k)];
        else
          flipped_data_out = compl_data_tmp;
     end

   //Data valid posedge and negedge logic
   always @( posedge i_clock )
     begin
        if ( i_reset ) begin
           data_valid_d <= 1'b0 ;
           data_valid_dd <= 1'b0;
           data_valid_ddd <= 1'b0;
           data_d <= 'b0;
        end else if ( i_valid ) begin
           data_valid_d <= i_data_valid ;
           data_valid_dd <= data_valid_d;
           data_valid_ddd <= data_valid_dd;
           data_d <= flipped_data_in;
        end
     end
   assign data_valid_pos_d = ~data_valid_dd & data_valid_d & i_valid ;
   assign data_valid_neg = data_valid_d & ~i_data_valid & i_valid;
   assign data_valid_neg_d = data_valid_dd & ~data_valid_d & i_valid;
   assign data_valid_neg_dd = data_valid_ddd & ~ data_valid_dd & i_valid;


   //data_tmp has the 32 lsb from data ext and is the carry over to the next iteration
   always @( posedge i_clock )
     begin
        if ( i_reset )
          data_tmp <= {NB_CRC32{1'b0}} ;
        else if ( i_valid )
          data_tmp <= data_ext[NB_CRC32-1:0] ;
     end

   /*The CRC32 algorithm. When there is a posedge detected in i_data_valid, the first 32 bits of i_data are complemented.
    As long as i_data_valid is in a high state, shifted_poly_ext will continue shifting to the right and doing a bitwise XOR with each bit in data_ext
    When i_data_valid changes state to 0, 32 0's are appended in LSB and the last iteration is carried over
    */
// assign  data_ext_aux = { {NB_DATA{1'b0}}, data_tmp,  data_d } ;
   //assign error = (data_tmp == {NB_CRC32{1'b1}});
   always @( * )
     begin
        data_ext_aux = { {NB_DATA{1'b0}}, data_tmp,  data_d } ;
        if (data_valid_pos_d) begin //Start
          if (NB_DATA==NB_CRC32)
            data_ext = { {NB_CRC32{1'b0}} , ~data_d} ;
          else
            data_ext = { {NB_DATA-NB_CRC32{1'b0}} , ~data_d[NB_DATA-1-:NB_CRC32], data_d[NB_DATA-NB_CRC32-1:0] } ;
        end else if (data_valid_neg) begin//Shift tail size bits
          if (LOGICAL_SHIFTING) //Use a logic shift
            data_ext = { {data_tmp, data_d} >>((NB_DATA-i_tail_size_bits)%NB_DATA) };
          else //Use bitwise selection
            data_ext = data_ext_aux[ (NB_DATA-i_tail_size_bits)%NB_DATA +: NB_CRC32+NB_DATA ] ;
        end
        else if (data_valid_neg_d) begin//Add 32 0's
           if (NB_DATA==NB_CRC32)
             data_ext = {data_tmp, {NB_CRC32{1'b0}}} ;
           else
             data_ext = { {NB_CRC32{1'b0}} , data_tmp , {NB_CRC32{1'b0}} };
        end else begin//Operate normally
          data_ext = { data_tmp, data_d };
           end
        shifted_poly_ext = { CRC32_POLYNOMIAL, {NB_DATA-1{1'b0}} } ;
        for ( i=0; i<NB_DATA; i=i+1 )
          begin
             if ( data_ext[NB_DATA+NB_CRC32-1-i]==1'b1 )
               data_ext = data_ext ^ shifted_poly_ext ;
             shifted_poly_ext = {1'b0, shifted_poly_ext[NB_DATA+NB_POLY-1-1:1]};
          end
          end // always @ ( * )
endmodule
