module CRC32_tb ();

    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================
    localparam                                      NB_DATA                     = 32 ;
    localparam                                      NB_CRC32                    = 32 ;
    localparam                                      CRC32_POLYNOMIAL            = 33'h104C11DB7 ; //100 1100 0001 0001 1101 1011 01113
    localparam                                      LOG2_NB_DATA                = 6;

    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
    wire                                            tb_crc32_valid_o;
    wire                                            tb_crc_ready_o;
    wire     [NB_CRC32-1:0]                         tb_crc32_o;

    // Inputs.
    reg     [NB_DATA-1:0]                           tb_data_i;
    wire                                            tb_data_valid_i;
    reg     [NB_CRC32-1:0]                          tb_crc32_i;
    wire                                            tb_crc32_valid_i;
    reg     [LOG2_NB_DATA-1:0]                      tb_tail_size_bits_i;

    wire                                            valid;

    wire                                            reset ;
    reg                                             clock = 1'b0 ;
    integer                                         timer = 0 ;
    integer                                         i = 0 ;

    reg     [NB_CRC32*4-1:0]                        ext_i_data = 68'h6E160B0C6E160B0CC;
    //Expected result 0x6E160B0C
    reg     [NB_CRC32*4-1:0]                        ext_i_data2 = 128'hAA1ABB2BCC3CDD4DD5DDC6CCB7BBA8AA;
    //Expected result 0xEE3BEE20

    //reg     [NB_CRC32*4-1:0]                        ext_i_data = 128'h000a0023000000F00000007182000000e;
    //Expected result 0x161343F7


    //==========================================================================
    // CONNECTION TO DUT
    //==========================================================================
    CRC32
    #(
      .NB_DATA              (NB_DATA         ),
      .NB_CRC32             (NB_CRC32        ),
      .CRC32_POLYNOMIAL     (CRC32_POLYNOMIAL)
    )
    u_CRC32
    (
      .o_crc32_valid        (tb_crc32_valid_o    ),
      .o_crc_ready          (tb_crc_ready_o      ),
      .o_crc32              (tb_crc32_o          ),

      .i_data               (tb_data_i           ),
      .i_data_valid         (tb_data_valid_i     ),
      .i_crc32              (tb_crc32_i          ),
      .i_crc32_valid        (tb_crc32_valid_i    ),
      .i_tail_size_bits     (tb_tail_size_bits_i ),

      .i_valid              (valid               ),

      .i_reset              (reset               ),
      .i_clock              (clock               )
    ) ;

    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    always
    begin
        #(50) clock = ~clock ;
    end

    always @ ( posedge clock )
    begin
        timer   <= timer + 1;
    end

   assign reset = (timer == 2) ; // Reset at time 2
   assign tb_data_valid_i =  ((timer >= 4) && (timer <= 9));
   assign valid = 1'b1;
   assign tb_crc32_valid_i = (timer >= 4) ;

   always @(*)
     begin
        case (timer)
          4: begin
             tb_data_i = 32'haaaaaaaa;
             tb_tail_size_bits_i = 6'd16;
          end
          5: begin
             tb_data_i = 32'haaaabbbb;
             tb_tail_size_bits_i = 6'd16;
             end
          6: begin
             tb_data_i = 32'hbbbbbbbb;
             tb_tail_size_bits_i = 6'd16;
             end
          7: begin
             tb_data_i = 32'h0008abcd;
             tb_tail_size_bits_i = 6'd16;
             end
          8: begin
             tb_data_i = 32'habcdabcd;
             tb_tail_size_bits_i = 6'd16;
             end
          9: begin
             tb_data_i = 32'habcdabcd;
             tb_tail_size_bits_i = 6'd16;
             end
        endcase // case (timer)
     end


/*
    assign reset = (timer == 2) ; // Reset at time 2
    assign tb_data_valid_i = ( ((timer >= 4) && (timer <= 8)) | ((timer >= 18) && (timer<20)) ) | ((timer >= 25) && (timer<=26));
    assign valid = 1'b1;
    assign tb_crc32_valid_i = (timer >= 4) ;
    always @ (*)
    begin
       case(timer)
         4: begin
            tb_data_i = 64'h6E160B0C6E160B0C;
            tb_crc32_i = 32'hDA66282F;
            tb_tail_size_bits_i = 6'b001000;
         end
         5: begin
            tb_data_i = 64'h6E160B0C6E160B0C;
         end
         6: begin
            tb_data_i = 64'h6E160B0C6E160B0C;
         end
         7: begin
            tb_data_i = 64'h6E160B0C6E160B0C;
         end
         8:
           tb_data_i = 64'hCC00000000000000;
         18: begin
            tb_data_i = ext_i_data2[NB_DATA*2-1-:NB_DATA];
            tb_crc32_i = 32'hEE3BEE20 ;
            tb_tail_size_bits_i = 6'b000000;
         end
         19:
           tb_data_i = ext_i_data2[NB_DATA-1-:NB_DATA];

         25: begin
            tb_data_i = 64'h6E160B0C6E160BAA;
            tb_crc32_i = 32'h8A1B4D7A;
            tb_tail_size_bits_i = 6'b111000;
         end
         26:
           tb_data_i = 64'hCCCCCCCCCCCCCC00;
         default:
           tb_data_i = 'b0;

       endcase
    end
*/

endmodule
