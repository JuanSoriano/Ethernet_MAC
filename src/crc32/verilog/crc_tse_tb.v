module crc_tse_tb ();

    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================
    localparam                                      NB_DATA                     = 32 ;
    localparam                                      NB_CRC32                    = 32 ;
    localparam                                      CRC32_POLYNOMIAL            = 33'h104C11DB7 ; //100 1100 0001 0001 1101 1011 01113
    localparam                                      LOG2_NB_DATA                = 6;
   localparam NB_BYTE = 8;

    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
    wire                                            tb_crc32_valid_o;
    wire                                            tb_crc_ready_o;
    wire     [NB_CRC32-1:0]                         tb_crc32_o;

    // Inputs.
   wire [NB_DATA-1:0]                               tb_data_i;
    wire                                            tb_data_valid_i;
   wire [NB_CRC32-1:0]                              tb_crc32_i;
    wire                                            tb_crc32_valid_i;
   wire [LOG2_NB_DATA-1:0]                          tb_tail_size_bits_i;

    wire                                            valid;

    wire                                            reset ;
    reg                                             clock = 1'b0 ;
    integer                                         timer = 0 ;
    integer                                         i = 0 ;
   integer                                          j,k;

    reg     [NB_CRC32*4-1:0]                        ext_i_data = 68'h6E160B0C6E160B0CC;
    //Expected result 0x6E160B0C
   reg [480-1:0]                                    ext_i_data2 = 480'hABCDABCDABCDCAFECAFECAFE002EFAFAFEFEFAFAFEFEFAFAFEFEFAFAFEFEFAFAFEFEFAFAFEFEFAFAFEFEFAFAFEFEFAFAFEFEFAFAFEFEFAFAFEFEFAFA;
    //Expected result 0xEE3BEE20

   reg [NB_DATA-1:0]                                flipped_data;
   reg [NB_DATA-1:0]                                flipped_data_out;
    //reg     [NB_CRC32*4-1:0]                        ext_i_data = 128'h000a0023000000F00000007182000000e;
    //Expected result 0x161343F7


    //==========================================================================
    // CONNECTION TO DUT
    //==========================================================================
    CRC32
    #(
      .NB_DATA              (NB_DATA          ),
      .NB_CRC32             (NB_CRC32         ),
      .CRC32_POLYNOMIAL     (CRC32_POLYNOMIAL ),
      .REF_IN               (1                ),
      .REF_OUT              (1                )
    )
    u_CRC32
    (
      .o_crc32_valid        (tb_crc32_valid_o    ),
      .o_crc_ready          (tb_crc_ready_o      ),
      .o_crc32              (tb_crc32_o          ),

      .i_data               (tb_data_i           ),
      .i_data_valid         (tb_data_valid_i     ),
      .i_crc32              (tb_crc32_i          ),
      .i_crc32_valid        (tb_crc32_valid_i    ),
      .i_tail_size_bits     (tb_tail_size_bits_i ),

      .i_valid              (valid               ),

      .i_reset              (reset               ),
      .i_clock              (clock               )
    ) ;

    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    always
    begin
        #(50) clock = ~clock ;
    end

    always @ ( posedge clock )
    begin
        timer   <= timer + 1;
    end

   assign reset = (timer == 2) ; // Reset at time 2
   assign tb_data_valid_i =  ((timer >= 4) && (timer <= 18));
   assign valid = 1'b1;
   assign tb_crc32_valid_i = (timer >= 4) ;
   assign tb_tail_size_bits_i = 6'd0;
   assign tb_crc32_i = 32'hb62a3fcd;

   assign tb_data_i = ext_i_data2[480-(timer-4)*NB_DATA-1-:NB_DATA];

   always @(*)
     begin
        for (j=0;j<NB_DATA/NB_BYTE;j=j+1)
          for (k=0;k<NB_BYTE;k=k+1)
            flipped_data[j*NB_BYTE+k] = tb_data_i[j*NB_BYTE+(NB_BYTE-1-k)];
     end

   always @(*)
     begin
        for (j=0;j<NB_DATA/NB_BYTE;j=j+1)
          for (k=0;k<NB_BYTE;k=k+1)
            flipped_data_out[j*NB_BYTE+k] = tb_crc32_o[j*NB_BYTE+(NB_BYTE-1-k)];
     end

endmodule
