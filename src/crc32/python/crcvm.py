import sys
import random
import binascii
from crccheck.crc import Crc32Bzip2
import math

#File parameters
output_data_file_path = "crc_data"
output_crc_file_path = "crc_fcs"
output_datavalid_file_path = "crc_datavalid"
output_tailsize_file_path = "crc_tailsize"

if len(sys.argv) < 2:
    print ("Ingrese el numero de frames a crear")
    sys.exit()
else :
    nframes = int(sys.argv[1])

######################
# STANDARD PARAMETERS
######################
NB_DATA = 64

#Data
data_list = list()
#Frame generator
frame_list = list()
#FCS
fcs_list = list()
#Datavalid
datavalid = ''
#Tailsize
tailsize = ''

def generateCrcFrames(nframes):
    new_frame = ''
    accumulated_frame = ''
    accumulated_frame = bin(random.getrandbits(NB_DATA))[2:].zfill(NB_DATA); #First frame without tail size
    frame_list.append(accumulated_frame)
    for i in range (nframes-1):
        new_frame = bin(random.getrandbits(NB_DATA+8))[2:].zfill(NB_DATA+8) #Add 1 tail size
        accumulated_frame = accumulated_frame + new_frame
        frame_list.append(accumulated_frame)

def generateCrcFcs(nframes):
    for i in range (nframes):
        frame_hex = hex(int('1000'+frame_list[i],2))[3:] #FIXME: Implementar de otra forma mas prolija, no tomar a pàrtir del segundo caracter
        crc32_int = Crc32Bzip2.calc(bytes.fromhex(frame_hex))
        crc32 = bin(crc32_int)[2:].zfill(32)
        fcs_list.append(crc32)


def generateDataValid(nframes):
    datavalid_temp = ''
    counter = -1;
    for i in range (nframes):
        no_tailsize = ((i*8)%NB_DATA == 0)
        counter += no_tailsize;
        if (no_tailsize):
            datavalid_temp = datavalid_temp+((i+1+counter)*'1')+(4*'0')
        else:
            datavalid_temp = datavalid_temp+((i+2+counter)*'1')+(3*'0')
    #for i in range (nframes):
    #    datavalid_temp = datavalid_temp+((i+1+((i*8)%NB_DATA == 0)+math.floor(i*8//NB_DATA)))*'1'+(4*'0')
    return datavalid_temp

def generateTailSize(nframes):
    tailsize_temp = ''
    for i in range (nframes):
        tailsize_temp = tailsize_temp + bin((i*8)%NB_DATA)[2:].zfill(6) + ' '
    return tailsize_temp


def dataToFiles():
    for i in range (len(frame_list)):
        data_list.append(frame_list[i]+(256-((i*8)%NB_DATA))*'0')

    data = ''.join(data_list)
    data_file = open(output_data_file_path, "w")
    data_file.write(data)
    data_file.close()

    fcss = ' '.join(fcs_list)
    fcs_file = open(output_crc_file_path, "w")
    fcs_file.write(fcss)
    fcs_file.close()

    datavalid_file = open(output_datavalid_file_path, "w")
    print("datavalid length: ", len(datavalid)) #nr of 1s + 0s
    datavalid_file.write(datavalid)
    datavalid_file.close()

    tailsize_file = open(output_tailsize_file_path, "w")
    tailsize_file.write(tailsize)
    tailsize_file.close()


print("Generating ", nframes, " frames")
generateCrcFrames(nframes)
generateCrcFcs(nframes)
datavalid = generateDataValid(nframes)
tailsize = generateTailSize(nframes)
dataToFiles()
