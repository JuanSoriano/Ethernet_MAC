/*------------------------------------------------------------------------------
 -- Project     : CL100GC
 -------------------------------------------------------------------------------
 -- File        : common_memory.v
 -- Author      : Ramiro R. Lopez
 -- Originator  : Clariphy Argentina S.A.
 -- Date        : 2012/12/05
 --
 -- Rev 0       : Initial release. RRL.
 --
 --
 -- $Id: common_synchronous_fifo.v 1397 2015-12-31 16:56:27Z ppinzani $
 -------------------------------------------------------------------------------
 -- Description : .
 -------------------------------------------------------------------------------
 -- Copyright (C) 2010 ClariPhy Argentina S.A.  All rights reserved.
 -----------------------------------------------------------------------------*/

module common_synchronous_fifo
#(
    // PARAMETERS.
    parameter                                           NB_DATA                 = 16    ,
    parameter                                           DEPTH                   = 4     ,
    parameter                                           NB_ADDR                 = 3
)
(
    // OUTPUTS.
    output  wire    [NB_DATA-1:0]                       o_data              ,
    output  reg                                         o_fifo_fault        ,
    output  reg                                         o_underflow         ,
    output  reg                                         o_overflow          ,
    // INPUTS.
    input   wire    [NB_DATA-1:0]                       i_data              ,
    input   wire    [NB_ADDR-1:0]                       i_read_init_level   ,   // [HINT] Must be smaller or equal to "DEPTH-i_offset".
    input   wire    [NB_ADDR-1:0]                       i_offset            ,   // [HINT] Must be smaller than "DEPTH". Must meet: i_read_init_level+i_offset<NB_DEPTH-1.
    input   wire                                        i_valid_wr          ,
    input   wire                                        i_valid_rd          ,
    input   wire                                        i_enable_auto_reset ,
    input   wire                                        i_enable            ,
    input   wire                                        i_reset             ,
    input   wire                                        i_clock
) ;

    /* // BEGIN: QI.
    common_synchronous_fifo
    #(
        .NB_DATA                (   ),
        .DEPTH                  (   ),
        .NB_ADDR                (   )
    )
    u_common_synchronous_fifo
    (
        .o_data                 (   ),
        .o_fifo_fault           (   ),
        .o_underflow            (   ),
        .o_overflow             (   ),
        .i_data                 (   ),
        .i_read_init_level      (   ),
        .i_offset               (   ),
        .i_valid_wr             (   ),
        .i_valid_rd             (   ),
        .i_enable_auto_reset    (   ),
        .i_enable               (   ),
        .i_reset                (   ),
        .i_clock                (   )
    ) ;
    // END: QI. */


    // INTERNAL PARAMETERS.
    // None so far.


    // INTERNAL SIGNALS.
    wire                                                reset_or                    ;
    wire            [NB_ADDR-1:0]                       p_depth_m1_                 ;
    // WR.
    reg             [NB_ADDR-1:0]                       addr_wr                     ;
    wire            [NB_ADDR-1:0]                       addr_wr_next                ;
    wire                                                addr_wr_at_limit            ;
    reg                                                 lap_wr                      ;
    reg             [NB_ADDR-1:0]                       addr_wr_p_offset            ;
    wire            [NB_ADDR-1:0]                       addr_wr_p_offset_next       ;
    wire                                                addr_wr_p_offset_at_limit   ;
    reg                                                 lap_wr_w_off                ;
    // RD.
    reg                                                 enable_read                 ;
    reg             [NB_ADDR-1:0]                       addr_rd                     ;
    wire            [NB_ADDR-1:0]                       addr_rd_next                ;
    wire                                                addr_rd_at_limit            ;
    reg                                                 lap_rd                      ;
    reg             [NB_ADDR-1:0]                       addr_rd_p_offset            ;
    wire            [NB_ADDR-1:0]                       addr_rd_p_offset_next       ;
    wire                                                addr_rd_p_offset_at_limit   ;
    reg                                                 lap_rd_w_off                ;
    // ST.
    wire                                                fifo_ok_a                   ;
    wire                                                fifo_ok_b                   ;
    wire                                                fault                       ;
    reg                                                 last_event_was_wr           ;
    wire                                                valid_wr ;
    wire                                                valid_rd ;



    // ALGORITHM BEGIN.


    // Constant cast.
    assign  p_depth_m1_
                = DEPTH-1 ; // [HINT] Intentional truncation.


    // Memory ------------------------------------------------------------------

    // Memory instance.
    common_memory
    #(
        .NB_DATA                ( NB_DATA       ),
        .DEPTH                  ( DEPTH         ),
        .NB_ADDRESS             ( NB_ADDR       )
    )
    u_common_memory
    (
        .o_data                 ( o_data        ),
        .i_data                 ( i_data        ),
        .i_address_write        ( addr_wr       ),
        .i_address_read         ( addr_rd       ),
        .i_write_enable         ( valid_wr      ),
        .i_read_enable          ( valid_rd      ),
        .i_reset                ( reset_or      ),
        .i_clock                ( i_clock       )
    ) ;


    // Write pointer -----------------------------------------------------------

    // Update write index to next value.
    always @( posedge i_clock )
    begin : l_update_addr_wr
        if ( reset_or )
            addr_wr
                <= {NB_ADDR{1'b0}} ;
        else if ( i_valid_wr )
            addr_wr
                <= addr_wr_next ;
    end // l_update_addr_wr
    assign  addr_wr_next
                = ( addr_wr_at_limit )? {NB_ADDR{1'b0}} : addr_wr + 1'b1 ;              // [HINT] Intentional carry drop.
    assign  addr_wr_at_limit
                = ( addr_wr == p_depth_m1_ ) ;

    // Check write index "lap". Further used to check FIFO status.
    always @( posedge i_clock )
    begin : l_calc_lap_wr
        if ( reset_or )
            lap_wr
                <= 1'b0 ;
        else if ( i_valid_wr && addr_wr_at_limit )
            lap_wr
                <= ~lap_wr ;
    end // l_calc_lap_wr

    // Calculate write index plus security offset margin to allow underflow/overflow calculations.
    always @( posedge i_clock )
    begin : l_calc_addr_wr_p_offset
        if ( reset_or )
            addr_wr_p_offset
                <= {NB_ADDR{1'b0}} ;
        else if ( i_valid_wr )
            addr_wr_p_offset
                <= addr_wr_p_offset_next ;
    end // l_calc_addr_wr_p_offset
    assign  addr_wr_p_offset_next
                = ( addr_wr_p_offset_at_limit )? {NB_ADDR{1'b0}} : addr_wr + i_offset + 1'b1 ; // [HINT] Intentional carry drop.
    assign  addr_wr_p_offset_at_limit
                = ( addr_wr_p_offset == p_depth_m1_ ) ;

    // Check write index "lap" (with offset). Further used to check FIFO status.
    always @( posedge i_clock )
    begin : l_calc_lap_wr_w_off
        if ( reset_or )
            lap_wr_w_off
                <= 1'b0 ;
        else if ( i_valid_wr && addr_wr_p_offset_at_limit )
            lap_wr_w_off
                <= ~lap_wr_w_off ;
    end // l_calc_lap_wr_off

    // Memory write enable.
    assign  valid_wr
                = i_valid_wr & ~reset_or ;


    // Read pointer ------------------------------------------------------------

    // Read will be enabled after the FIFO has been written at some level.
    always @( posedge i_clock )
    begin
        if ( reset_or )
            enable_read
                <= 1'b0 ;
        else if ( addr_wr >= i_read_init_level )
            enable_read
                <= 1'b1 ;
    end

    // Update read index to next value.
    always @( posedge i_clock )
    begin : l_update_addr_rd
        if ( reset_or )
            addr_rd
                <= {NB_ADDR{1'b0}} ;
        else if ( i_valid_rd && enable_read )
            addr_rd
                <= addr_rd_next ;
    end // l_update_addr_rd
    assign  addr_rd_next
                = ( addr_rd_at_limit )? {NB_ADDR{1'b0}} : addr_rd + 1'b1 ;              // [HINT] Intentional carry drop.
    assign  addr_rd_at_limit
                = ( addr_rd == p_depth_m1_ ) ;

    // Check read index "lap". Further used to check FIFO status.
    always @( posedge i_clock )
    begin : l_calc_lap_rd
        if ( reset_or )
            lap_rd
                <= 1'b0 ;
        else if ( i_valid_rd && addr_rd_at_limit )
            lap_rd
                <= ~lap_rd ;
    end // l_calc_lap_rd

    // Calculate read index plus security offset margin to allow underflow/overflow calculations.
    always @( posedge i_clock )
    begin : l_calc_addr_rd_p_offset
        if ( reset_or )
            addr_rd_p_offset
                <= {NB_ADDR{1'b0}} ;
        else if ( i_valid_rd && enable_read )
            addr_rd_p_offset
                <= addr_rd_p_offset_next ;
    end // l_calc_addr_rd_p_offset
    assign  addr_rd_p_offset_next
                = ( addr_rd_p_offset_at_limit )? {NB_ADDR{1'b0}} : addr_rd + i_offset + 1'b1 ; // [HINT] Intentional carry drop.
    assign  addr_rd_p_offset_at_limit
                = ( addr_rd_p_offset == p_depth_m1_ ) ;

    // Check read index "lap" (with offset). Further used to check FIFO status.
    always @( posedge i_clock )
    begin : l_calc_lap_rd_w_off
        if ( reset_or )
            lap_rd_w_off
                <= 1'b0 ;
        else if ( i_valid_rd && addr_rd_p_offset_at_limit )
            lap_rd_w_off
                <= ~lap_rd_w_off ;
    end // l_calc_lap_rd_off

    // Memory read enable.
    assign  valid_rd
                = i_valid_rd & enable_read & ~reset_or ;



    // FIFO status -------------------------------------------------------------

    // Compare read and write address, considering the lap relation.
    assign  fifo_ok_a
                = ( lap_wr == lap_rd_w_off ) & ( addr_wr > addr_rd_p_offset ) ;
    assign  fifo_ok_b
                = ( lap_wr_w_off != lap_rd ) & ( addr_wr_p_offset < addr_rd ) ;

    always @( posedge i_clock )
    begin : l_reg_err
        if ( i_reset )
            o_fifo_fault
                <= 1'b0 ;
        else
            o_fifo_fault
                <= fault ;
    end // l_reg_err
    assign  fault
                = ~( fifo_ok_a | fifo_ok_b | ~enable_read ) ;

    // Common signal used for overflow and underflow flags.
    always @( posedge i_clock )
    begin : l_reg_last_ev
        if ( i_reset )
            last_event_was_wr
                <= 1'b0 ;
        else if ( i_valid_wr )
            last_event_was_wr
                <= 1'b1 ;
        else if ( i_valid_rd )
            last_event_was_wr
                <= 1'b0 ;
    end // l_reg_last_ev

    // Underflow logic.
    always @( posedge i_clock )
    begin : l_reg_underf
        if ( i_reset )
            o_underflow
                <= 1'b0 ;
        else
            o_underflow
                <= fault & ~last_event_was_wr ;
    end // l_reg_under

    // Overflow logic.
    always @( posedge i_clock )
    begin : l_reg_ovrf
        if ( i_reset )
            o_overflow
                <= 1'b0 ;
        else
            o_overflow
                <= fault &  last_event_was_wr ;
    end // l_reg_ovrf

    assign  reset_or
                = i_reset | ~i_enable | ( i_enable_auto_reset & o_fifo_fault ) ;



 // // BEGIN: debug/quick test -------------------------------------------------
 //     reg             x_clock     = 1'b0 ;
 //     wire            x_reset ;
 //     integer         x_counter   = 0 ;
 //     always
 //     begin
 //         #( 50 ) x_clock
 //                     = ~x_clock ;
 //     end
 //     always @( posedge x_clock )
 //         x_counter
 //             = x_counter + 1 ;
 //     assign  x_reset
 //                 = ( x_counter > 3 ) & ( x_counter < 5 ) ;
 //     assign  i_data              = x_counter ;
 //     assign  i_read_init_level   = 3 ;
 //     assign  i_offset            = 1'b1 ;
 //     assign  i_valid_wr          = 1'b1 ;
 //     assign  i_valid_rd          = 1'b1 ;
 //     assign  i_enable_auto_reset = 1'b1 ;
 //     assign  i_enable            = 1'b1 ;
 //     assign  i_reset             = x_reset ;
 //     assign  i_clock             = x_clock ;
 //     integer        count_wr ;
 //     integer        count_rd ;
 //     always @( posedge i_clock )
 //     begin
 //         if ( reset_or )
 //         begin
 //             count_wr   <= 0 ;
 //             count_rd   <= 0 ;
 //         end
 //         else
 //         begin
 //             if ( i_valid_wr )
 //                 count_wr   <= count_wr + 1 ;
 //             if ( i_valid_rd )
 //                 count_rd   <= count_rd + 1 ;
 //         end
 //     end
 // // END: debug --------------------------------------------------------------


endmodule // common_memory
