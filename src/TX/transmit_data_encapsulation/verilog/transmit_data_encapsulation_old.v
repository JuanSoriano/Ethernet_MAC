module transmit_data_encapsulation
  #(
    // Parameters.
    parameter NB_DATA                = 64 ,
    parameter LOG2_NB_DATA           = 6 ,
    parameter NB_ADDRESS             = 48,
    parameter NB_LENTYP              = 16 ,
    parameter LOG2_NB_LENTYP         = 4 ,
    parameter MIN_LENGTH_FRAME_BITS  = 512,
    parameter MAX_LENGTH_FRAME_BYTES = 1982,
    parameter NB_CRC32               = 32,
    parameter NB_TIMER               = 10, // Nr of bits from frame
    parameter PREAMBLE               = 56'hAAAAAAAAAAAAAA,
    parameter NB_PREAMBLE            = 56,
    parameter SFD                    = 8'hAB,
    parameter NB_SFD                 = 8,
    parameter CRC32_POLYNOMIAL       = 33'h104C11DB7
    )
   (
    // Outputs.
    //output wire [MAX_LENGTH_FRAME_BYTES*8-1:0] o_assembled_frame , //Full assembled frame, not to be used
    //output wire                                o_frame_ready , //Full frame ready
    output wire [NB_DATA-1:0]      o_data , //NB DATA bits of  data to be TX'd
    output wire                    o_data_ready , //NB DATA bits are ready
    output reg [LOG2_NB_DATA-1:0 ] o_tail_size ,
    // Inputs.
    input wire                     i_data_valid ,
    input wire [NB_ADDRESS-1:0]    i_destination_address,
    input wire [NB_ADDRESS-1:0]    i_source_address,
    input wire [NB_LENTYP-1:0]     i_lentyp,
    input wire [NB_DATA-1:0]       i_client_data,
    input wire [NB_CRC32-1:0]      i_fcs,
    input wire                     i_fcs_present,
  //input wire                                i_half_duplex ,

    input wire                     i_valid , // Throughput control.
    input wire                     i_reset ,
    input wire                     i_clock
    ) ;

   //==========================================================================
   // LOCAL PARAMETERS.
   //==========================================================================
   localparam SOF = {PREAMBLE,SFD};
   localparam NB_SOF = NB_PREAMBLE + NB_SFD;
   localparam NB_HEADER = NB_ADDRESS*2+NB_LENTYP;
   localparam DELAY_CLOCKS = 6; //Nota: puede adelantarse la latencia cubriendo el tiempo muerto del CRC32 con SOF y HEADER
   localparam NB_DATA_TO_CLIENT = NB_SOF+NB_HEADER+NB_DATA+(DELAY_CLOCKS-1)*NB_DATA;
   localparam MIN_LENTYP = MIN_LENGTH_FRAME_BITS/8 - (NB_HEADER+NB_CRC32)/8;

   // FSM
   localparam                NB_STATE      = 3 ;
   localparam [NB_STATE-1:0] ST_0_IDLE     = 0 ;
   localparam [NB_STATE-1:0] ST_1_WAIT_BUFFER     = 1 ;
   localparam [NB_STATE-1:0] ST_2_ASSEMBLE = 2 ;
   localparam [NB_STATE-1:0] ST_3_FINISH     = 3 ;

   //==========================================================================
   // INTERNAL SIGNALS.
   //==========================================================================
   // FSM
   reg [NB_STATE-1:0]                       state ;
   reg [NB_STATE-1:0]                       next_state ;

   reg                                      fsmo_init_buffer;
   reg                                      fsmo_buffer_waiting;
   reg                                      fsmo_shift_data;
   reg                                      fsmo_timer_reset;
   reg                                      fsmo_done;

   // OTHER
   reg                                      data_valid_d;
   reg                                      data_valid_dd;
   wire                                     data_valid_pos;
   wire                                     data_valid_neg;

   reg                                      data_ready_d;
   wire                                     data_ready_pos;

   reg [NB_ADDRESS-1:0]                     destination_address_reg;
   reg [NB_ADDRESS-1:0]                     source_address_reg;
   reg [NB_LENTYP-1:0]                      lentyp_reg;
   reg [NB_CRC32-1:0]                       crc32_reg;
   reg                                      fcs_present_reg;

   reg [NB_TIMER-1:0]                       timer;
   wire                                     timer_assemble;
   wire                                     timer_buffer;
   wire                                     timer_finish;
   reg [DELAY_CLOCKS-1:0]                   delayed_data_valid;
   //reg [MAX_LENGTH_FRAME_BYTES*8-1:0]       assembled_frame;


   // Signals between CRC and TDE
   wire                                     crc_calc_ready ;
   wire [NB_CRC32-1:0]                      crc_calc;
   reg [NB_HEADER-1:0]                      ext_data_to_crc;
   wire                                     extra_clock;
   wire [NB_DATA-1:0]                       data_to_crc;
   wire                                     datavalid_crc;
   wire [LOG2_NB_DATA-1:0]                  tailsize_crc;

   reg [NB_DATA_TO_CLIENT-1:0]              data_to_client;
   wire                                     extra_clock_for_clientdata;

   //==========================================================================
   // ALGORITHM.
   //==========================================================================

   /*DELETE THIS*/
   wire                                     debug_missmatch;
   wire [NB_DATA-1:0]                       debug_lsb_wire;
   wire [NB_DATA-1:0]                       debug_wire;

   assign debug_missmatch = (i_fcs != crc_calc) & crc_calc_ready;
   assign debug_wire = data_to_client[NB_DATA_TO_CLIENT-1-:NB_DATA];
   assign debug_lsb_wire = data_to_client[NB_DATA-1:0];

// & (tailsize_crc != 6'b111000 && tailsize_crc != 6'b000000);


   /* DELETE THIS END */

   assign o_data = data_to_client[NB_DATA_TO_CLIENT-1-:NB_DATA]; //Take 64MSB
   assign o_data_ready = delayed_data_valid[DELAY_CLOCKS-1];

   //Outputs and other
   always @( posedge i_clock )
     begin
        if (i_reset)begin
           data_valid_d <= 1'b0;
           data_valid_dd <= 1'b0;
           data_ready_d <= 1'b0;
           o_tail_size <= 'b0;
        end else if (i_valid) begin
           data_valid_d <= i_data_valid;
           data_valid_dd <= data_valid_d;
           data_ready_d <= o_data_ready;
           if (o_data_ready)
              o_tail_size <= (NB_SOF+NB_HEADER+lentyp_reg*8+NB_CRC32)%NB_DATA;
        end
     end

   assign data_valid_pos = i_data_valid & ~ data_valid_d;
   assign data_valid_neg = ~i_data_valid & data_valid_d;
   assign data_ready_pos = o_data_ready & ~data_ready_d;

   //Capture headers
   always @( posedge i_clock )
     begin
        if (i_reset) begin
          destination_address_reg <= {NB_ADDRESS{1'b0}};
          source_address_reg <= {NB_ADDRESS{1'b0}};
          lentyp_reg <= {NB_LENTYP{1'b0}};
          crc32_reg <= {NB_CRC32{1'b0}};
          fcs_present_reg <= 1'b0;
        end else if (i_valid && data_valid_pos) begin
           destination_address_reg <= i_destination_address;
           source_address_reg <= i_source_address;
           lentyp_reg <= (i_lentyp < MIN_LENTYP) ? MIN_LENTYP : i_lentyp;
           fcs_present_reg <= i_fcs_present;
           if (i_fcs_present)
             crc32_reg <= i_fcs;
        end
     end

   //Timer logic
   always @( posedge i_clock )
     begin
        if (i_reset || timer_finish || fsmo_timer_reset)
          timer <= {NB_TIMER{1'b0}};
        else if (i_valid)
          timer <= timer + 1'b1;
     end

   assign timer_buffer = timer>(DELAY_CLOCKS-2);
   assign timer_assemble = ((timer+1)*NB_DATA) >= lentyp_reg*8;
   assign timer_finish = ((timer)*NB_DATA >= ((lentyp_reg/8+DELAY_CLOCKS+extra_clock_for_clientdata)*NB_DATA));
   //----------------------------------
   //FSM
   //----------------------------------

   // State update.
   always @( posedge i_clock )
     begin
        if ( i_reset )
          state <= ST_0_IDLE ;
        else if ( i_valid )
          state <= next_state ;
     end

   // Calculate next state and FSM outputs.
   always @( * )
     begin
        fsmo_init_buffer = 1'b0;
        fsmo_buffer_waiting = 1'b0;
        fsmo_shift_data = 1'b0;
        fsmo_timer_reset = 1'b0;
        fsmo_done = 1'b0;
        case ( state )
          ST_0_IDLE :
            begin
               casez ( {data_valid_pos, timer_buffer, timer_assemble, timer_finish } )
                 4'b1???: next_state = ST_1_WAIT_BUFFER;
                 default: next_state = ST_0_IDLE;
               endcase
               fsmo_init_buffer = data_valid_pos;
               fsmo_buffer_waiting = 1'b0;
               fsmo_shift_data = 1'b0;
               fsmo_timer_reset = 1'b1;
               fsmo_done = 1'b0;
            end
          ST_1_WAIT_BUFFER:
            begin
               casez ( {data_valid_pos, timer_buffer, timer_assemble, timer_finish } )
                 4'b?1??: next_state = ST_2_ASSEMBLE;
                 default: next_state = ST_1_WAIT_BUFFER;
               endcase // casez ( {data_valid_pos, timer_buffer, timer_done } )
               fsmo_init_buffer = 1'b0;
               fsmo_buffer_waiting = !timer_buffer;
               fsmo_shift_data = timer_buffer;//timer_buffer;
               fsmo_timer_reset = 1'b0;
               fsmo_done = 1'b0;
            end
          ST_2_ASSEMBLE:
            begin
               casez ( {data_valid_pos, timer_buffer, timer_assemble, timer_finish } )
                 4'b??1?: next_state = ST_3_FINISH;
                 default: next_state = ST_2_ASSEMBLE;
               endcase
               fsmo_init_buffer = 1'b0;
               fsmo_buffer_waiting = 1'b0;
               fsmo_shift_data = 1'b1;
               fsmo_timer_reset = 1'b0;
               fsmo_done = 1'b0;
            end
          ST_3_FINISH:
            begin
               casez ( {data_valid_pos, timer_buffer, timer_assemble, timer_finish } )
                 4'b???1: next_state = ST_0_IDLE;
                 default: next_state = ST_3_FINISH;
               endcase
               fsmo_init_buffer = 1'b0;
               fsmo_buffer_waiting = 1'b0;
               fsmo_shift_data = !timer_finish;
               fsmo_timer_reset = timer_finish;
               fsmo_done = timer_finish;
            end
        endcase
     end


   //----------------------------------
   //OTHER LOGIC
   //----------------------------------

   always @ (posedge i_clock)
     begin
        if (i_reset )
           data_to_client <= 'b0;
        else if (i_valid && fsmo_init_buffer)
          data_to_client <= {SOF, i_destination_address, i_source_address, i_lentyp, i_client_data, data_to_client[NB_DATA_TO_CLIENT-NB_SOF-NB_HEADER-NB_DATA-1:0]};
        else if (i_valid && fsmo_buffer_waiting)
          data_to_client[NB_DATA_TO_CLIENT-NB_SOF-NB_HEADER-NB_DATA-((timer)*NB_DATA)-1-:NB_DATA] <= i_client_data;
        else if (i_valid && fsmo_shift_data)
          data_to_client <= {data_to_client[NB_DATA_TO_CLIENT-NB_DATA-1:0], i_client_data};
        else if (i_valid && fsmo_done)
          if (i_fcs_present) begin
             data_to_client[tailsize_crc-1-:NB_CRC32] <= crc32_reg;
          end else begin
             data_to_client[NB_DATA_TO_CLIENT-tailsize_crc-1-:NB_CRC32] <= crc_calc;
          end
     end // always @ (posedge i_clock)

   always @(posedge i_clock)
     begin
       if (i_reset)
         ext_data_to_crc <= 'b0;
       else if (data_valid_pos) begin //First clock, add everything to the register
         ext_data_to_crc <= {i_source_address[NB_ADDRESS-(NB_DATA-NB_ADDRESS)-1:0], i_lentyp, i_client_data};
       end else if (i_valid & (i_data_valid || data_valid_d || data_valid_dd)) //Add new incoming data
         ext_data_to_crc <= {ext_data_to_crc[NB_HEADER-NB_DATA-1:0], i_client_data};
     end

   assign extra_clock = ((lentyp_reg*8)%NB_DATA > 16) || ((lentyp_reg*8)%NB_DATA == 0); //Add an extra clock for i_data tailsizes > 16 (including 48) because of included HEADER
   assign data_to_crc = (data_valid_pos) ? {i_destination_address, i_source_address[NB_ADDRESS-1-:(NB_DATA-NB_ADDRESS)]} : ext_data_to_crc[NB_HEADER-1-:NB_DATA]; // Take the 64 MSB
   assign datavalid_crc = i_data_valid || data_valid_d || (data_valid_dd & extra_clock); //|| data_valid_dd; //Add 2 clocks to allow CRC to finish
   assign tailsize_crc = (NB_HEADER + lentyp_reg*8)%NB_DATA;


   //For client data
   assign extra_clock_for_clientdata = tailsize_crc > (NB_DATA-NB_CRC32);

   always @(posedge i_clock)
     begin
       if (i_reset)
         delayed_data_valid <= 'b0;
       else if (i_valid && data_valid_neg)
         delayed_data_valid <= {{(DELAY_CLOCKS-1){1'b1}},{1'b0}};
       else if (i_valid)
         delayed_data_valid <= {delayed_data_valid[DELAY_CLOCKS-1-1:0], i_data_valid};
     end

   //==========================================================================
   // CONNECTION TO DUT
   //==========================================================================
   CRC32
     #(
       .NB_DATA              (NB_DATA         ),
       .NB_CRC32             (NB_CRC32        ),
       .CRC32_POLYNOMIAL     (CRC32_POLYNOMIAL)
       )
   u_CRC32
     (
      .o_crc32_valid        (/*NOT CONNECTED   */),
      .o_crc_ready          (crc_calc_ready      ),
      .o_crc32              (crc_calc            ),

      .i_data               (data_to_crc         ),
      .i_data_valid         (datavalid_crc       ),
      .i_crc32              (/*NOT CONNECTED   */),
      .i_crc32_valid        (/*NOT CONNECTED   */),
      .i_tail_size_bits     (tailsize_crc        ),

      .i_valid              (i_valid             ),

      .i_reset              (i_reset             ),
      .i_clock              (i_clock             )
      ) ;

endmodule
