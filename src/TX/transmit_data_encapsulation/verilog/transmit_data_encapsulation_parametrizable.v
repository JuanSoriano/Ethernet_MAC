module transmit_data_encapsulation_parametrizable
  #(
    // Parameters.
    parameter NB_DATA                = 64 ,
    parameter LOG2_NB_DATA           = 6 ,
    parameter NB_ADDRESS             = 48,
    parameter NB_LENTYP              = 16 ,
    parameter LOG2_NB_LENTYP         = 4 ,
    parameter MIN_LENGTH_FRAME_BYTES = 64 ,
    parameter MAX_LENGTH_FRAME_BYTES = 1982,
    parameter NB_CRC32               = 32,
    parameter NB_TIMER               = 10, // Nr of bits from frame
    parameter PREAMBLE               = 56'hAAAAAAAAAAAAAA,
    parameter NB_PREAMBLE            = 56,
    parameter SFD                    = 8'hAB,
    parameter NB_SFD                 = 8,
    parameter CRC32_POLYNOMIAL       = 33'h104C11DB7
    )
   (
    // Outputs.
    output wire [NB_DATA-1:0]     o_data , //NB DATA bits of  data to be TX'd
    output wire                   o_data_ready , //NB DATA bits are ready
    output reg [LOG2_NB_DATA-1:0] o_tail_size ,
    output reg                    o_length_error,
    // Inputs.
    input wire                    i_data_valid ,
    input wire [NB_ADDRESS-1:0]   i_destination_address,
    input wire [NB_ADDRESS-1:0]   i_source_address,
    input wire [NB_LENTYP-1:0]    i_lentyp,
    input wire [NB_DATA-1:0]      i_client_data,
    input wire [NB_CRC32-1:0]     i_fcs,
    input wire                    i_fcs_present,
    input wire [LOG2_NB_DATA-1:0] i_ethernet_ii_tail_size_bits,

    input wire                    i_valid , // Throughput control.
    input wire                    i_reset ,
    input wire                    i_clock
    ) ;

   //==========================================================================
   // LOCAL PARAMETERS.
   //==========================================================================
   localparam SOF                             = {PREAMBLE,SFD};
   localparam NB_SOF                          = NB_PREAMBLE + NB_SFD;
   localparam NB_HEADER                       = NB_ADDRESS*2+NB_LENTYP;
   localparam DELAY_CLOCKS                    = 5;
   localparam NB_DATA_TO_CLIENT               = NB_SOF+NB_HEADER+NB_DATA+DELAY_CLOCKS*NB_DATA;

   localparam MIN_LENTYP_BYTES                = 46;
   localparam MAX_LENTYP_BYTES                = 1500; //IEEE Ethernet max frame size
   localparam ETHERNET_II_FRAME_BYTES         = 1536; //Ethernet II min frame size
   localparam LOG2_MAX_FRAMESIZE_BITS         = 14;

   localparam                NB_STATE          = 2 ;
   localparam [NB_STATE-1:0] ST_0_IDLE         = 0 ;
   localparam [NB_STATE-1:0] ST_1_FILL_BUFFER  = 1 ;
   localparam [NB_STATE-1:0] ST_2_TRANSMIT     = 2 ;
   localparam [NB_STATE-1:0] ST_3_EMPTY_BUFFER = 3 ;

   //==========================================================================
   // INTERNAL SIGNALS.
   //==========================================================================
   // FSM
   reg [NB_STATE-1:0]             state ;
   reg [NB_STATE-1:0]             next_state ;

   reg                            fsmo_init_buffer;
   reg                            fsmo_buffer_filling;
   reg                            fsmo_shift_data;
   reg                            fsmo_done;
   // OTHER

   wire [NB_HEADER+NB_DATA-1:0]    data_in;
   reg [NB_DATA-1:0]               client_data;

   reg                             data_valid_d;
   wire                            data_valid_pos;
   wire                            data_valid_neg;

   reg                             data_ready_d;
   wire                            data_ready_pos;

   reg [NB_ADDRESS-1:0]            destination_address_reg;
   reg [NB_ADDRESS-1:0]            source_address_reg;
   reg [NB_LENTYP-1:0]             lentyp_reg;
   reg [NB_LENTYP-1:0]             lentyp_after_pad_check;
   reg [NB_CRC32-1:0]              crc32_reg;
   reg                             fcs_present_reg;

   reg [NB_TIMER-1:0]              timer;
   reg [NB_TIMER-1:0]              timer_ethii_empty_buffer;

   wire                            timeout_fill_buffer;
   wire                            timeout_transmit;
   wire                            timeout_empty_buffer;

   // Signals between CRC and TDE
   wire                            crc_calc_ready ;
   wire [NB_CRC32-1:0]             crc_calc;
   reg [NB_HEADER-1:0]             ext_data_to_crc;
   wire                            extra_clock;
   wire [NB_DATA-1:0]              data_to_crc;

   wire                            datavalid_crc;
   wire                            ext_valid_to_crc_for_ethii;
   wire [LOG2_NB_DATA-1:0]         tailsize_crc;
   wire                            timer_crc;

   reg [NB_DATA_TO_CLIENT-1:0]     data_to_client;
   wire                            extra_clock_for_clientdata;

   integer                         n;

   reg [NB_DATA-1:0]               data_padding_mask;
   wire                            padding_required;
   wire                            padding_timer_mask_last_data;
   wire                            padding_timer_replace_with_zero;

   wire                            invalid_lentyp;
   wire                            type_field_detected;

   //==========================================================================
   // ALGORITHM.
   //==========================================================================

   assign padding_required = lentyp_reg < MIN_LENTYP_BYTES; //Frame too short
   assign invalid_lentyp = lentyp_reg > MAX_LENTYP_BYTES & lentyp_reg < ETHERNET_II_FRAME_BYTES; //LENTYP not defined
   assign type_field_detected = lentyp_reg > ETHERNET_II_FRAME_BYTES; //Ignore lentyp field and pass frame to lower layers

   //Timer logic
   always @( posedge i_clock )
     begin
        if (i_reset | data_valid_neg)
          timer <= {NB_TIMER{1'b0}};
        else if (i_data_valid)
          timer <= timer + 1'b1;
     end

   always @( posedge i_clock )
     begin
        if (i_reset | data_valid_pos)
          timer_ethii_empty_buffer <= {NB_TIMER{1'b0}};
        else if (data_valid_neg)
          timer_ethii_empty_buffer <= timer;
     end

   assign padding_timer_mask_last_data = padding_required & (timer == (NB_HEADER+lentyp_reg*8)/NB_DATA + DELAY_CLOCKS);
   assign padding_timer_replace_with_zero = padding_required & (timer > (NB_HEADER+lentyp_reg*8)/NB_DATA + DELAY_CLOCKS);

   assign timeout_fill_buffer = timer > DELAY_CLOCKS;
   assign timeout_transmit = (type_field_detected) ? data_valid_neg : (timer+1)*NB_DATA >= lentyp_after_pad_check*8-DELAY_CLOCKS*NB_DATA;
   assign timeout_empty_buffer = (type_field_detected) ? timer+1 >= timer_ethii_empty_buffer+DELAY_CLOCKS  : (timer+1)*NB_DATA >= (NB_SOF+NB_HEADER+lentyp_after_pad_check*8+NB_CRC32)/NB_DATA;

   //Possibly padded data
   always @(*)
     casez ({padding_required, padding_timer_mask_last_data, padding_timer_replace_with_zero})
       3'b110: client_data = i_client_data & data_padding_mask;
       3'b101: client_data = {NB_DATA{1'b0}};
       3'b100: client_data = i_client_data;
       default: client_data = i_client_data;
     endcase // casez ({padding_required, timer_tailsize_padding, timeout_padding})

   //Padding mask
   always @( * )
     begin
        if (i_reset)
          data_padding_mask <= {NB_DATA{1'b1}};
        else if (i_valid && data_valid_pos && (lentyp_reg*8%NB_DATA!=0)) begin
           for (n=0; n<NB_DATA; n=n+1)
             if (n<(NB_DATA - lentyp_reg*8%NB_DATA))
               data_padding_mask[n] = 1'b0;
        end else if (i_valid && data_valid_neg)
          data_padding_mask <= {NB_DATA{1'b1}};
     end

   always @(posedge i_clock)
     begin
        if (i_reset)
          data_valid_d <= 1'b0;
        else if (i_valid)
          data_valid_d <= i_data_valid;
     end
   assign data_valid_pos = i_data_valid & ~ data_valid_d;
   assign data_valid_neg = ~i_data_valid & data_valid_d;

   //Capture fields
   always @( posedge i_clock ) begin
      if (i_reset) begin
         destination_address_reg <= {NB_ADDRESS{1'b0}};
         source_address_reg <= {NB_ADDRESS{1'b0}};
         lentyp_reg <= {NB_LENTYP{1'b0}};
         crc32_reg <= {NB_CRC32{1'b0}};
         fcs_present_reg <= 1'b0;
      end else if (i_valid && data_valid_pos) begin
         destination_address_reg <= i_destination_address;
         source_address_reg <= i_source_address;
         lentyp_reg <= i_lentyp;
         lentyp_after_pad_check <= (i_lentyp < MIN_LENTYP_BYTES) ? MIN_LENTYP_BYTES : i_lentyp;
         fcs_present_reg <= i_fcs_present;
         if (i_fcs_present)
           crc32_reg <= i_fcs;
      end
   end


   always @(posedge i_clock) begin
     if (i_reset)
       o_length_error <= 1'b0;
     else if (i_valid & data_valid_neg)
       if (type_field_detected)
         o_length_error <= ((timer+1)*NB_DATA > MAX_LENTYP_BYTES);
       else
         o_length_error <= ((timer+1) != lentyp_reg/NB_DATA);
   end

   //assign o_length_error = (type_field_detected) ? ((timer+1)*NB_DATA > MAX_LENTYP_BYTES) & data_valid_neg : ((timer+1) != lentyp_reg/NB_DATA) & data_valid_neg;

   always @ (posedge i_clock)
     begin
        if (i_reset )
          data_to_client <= 'b0;
        else if (i_valid && fsmo_init_buffer)
          data_to_client <= {SOF, i_destination_address, i_source_address, i_lentyp, i_client_data, data_to_client[NB_DATA_TO_CLIENT-NB_SOF-NB_HEADER-NB_DATA-1:0]};
        else if (i_valid && fsmo_buffer_filling)
          data_to_client[NB_DATA_TO_CLIENT-NB_SOF-NB_HEADER-NB_DATA-((timer)*NB_DATA)-1-:NB_DATA] <= client_data;
        else if (i_valid && fsmo_shift_data)
            data_to_client <= {data_to_client[NB_DATA_TO_CLIENT-NB_DATA-1:0], client_data};
        if (i_valid && fsmo_done)
          if (i_fcs_present) begin
             data_to_client[NB_DATA_TO_CLIENT-tailsize_crc-1-:NB_CRC32] <= i_fcs;
          end else begin
             data_to_client[NB_DATA_TO_CLIENT-tailsize_crc-1-:NB_CRC32] <= crc_calc;
          end
     end // always @ (posedge i_clock)


   //==================
   // FSM
   //==================
   // State update.
   always @( posedge i_clock )
     begin
        if ( i_reset )
          state <= ST_0_IDLE ;
        else if ( i_valid )
          state <= next_state ;
     end

   // Calculate next state and FSM outputs.
   always @( * )
     begin
        fsmo_init_buffer = 1'b0;
        fsmo_buffer_filling = 1'b0;
        fsmo_shift_data = 1'b0;
        fsmo_done = 1'b0;
        case ( state )
          ST_0_IDLE :
            begin
               casez ( {data_valid_pos, timeout_fill_buffer, timeout_transmit, timeout_empty_buffer} )
                 default: next_state = ST_0_IDLE;
               endcase
            end
          ST_1_FILL_BUFFER:
            begin
               casez ({data_valid_pos, timeout_fill_buffer, timeout_transmit, timeout_empty_buffer})
                 default: next_state = ST_1_FILL_BUFFER;
               endcase // casez ( {data_valid_pos, timer_buffer, timer_done } )
            end
          ST_2_TRANSMIT:
            begin
               casez ({data_valid_pos, timeout_fill_buffer, timeout_transmit, timeout_empty_buffer})
                 default: next_state = ST_2_TRANSMIT;
               endcase
            end
          ST_3_EMPTY_BUFFER:
            begin
               casez ({data_valid_pos, timeout_fill_buffer, timeout_transmit, timeout_empty_buffer})
                 default: next_state = ST_3_EMPTY_BUFFER;
               endcase
            end
        endcase
     end

   //Signal to extend valid to CRC when lentype isn't known
   assign ext_valid_to_crc_for_ethii = 1'b1;
   assign tailsize_crc = (type_field_detected) ? NB_HEADER+i_ethernet_ii_tail_size_bits%NB_DATA : (NB_HEADER+lentyp_after_pad_check*8)%NB_DATA;
   assign datavalid_crc = (type_field_detected) ? i_data_valid | ext_valid_to_crc_for_ethii : (timer+1)*NB_DATA <= NB_HEADER+lentyp_after_pad_check*8;

   assign data_in = {i_destination_address, i_source_address, i_lentyp, i_client_data} ;

   always @(posedge i_clock)
     begin
        if (i_reset)
          ext_data_to_crc <= 'b0;
        else if (data_valid_pos) begin //First clock, add everything to the register
           ext_data_to_crc <= data_in[NB_HEADER+NB_DATA-NB_DATA-1:0];
        end else if (i_valid & datavalid_crc) //Add new incoming data
          ext_data_to_crc <= {ext_data_to_crc[NB_HEADER-NB_DATA-1:0], client_data};
     end
   //==========================================================================
   // CONNECTION TO DUT
   //==========================================================================
   CRC32
     #(
       .NB_DATA              (NB_DATA         ),
       .NB_CRC32             (NB_CRC32        ),
       .CRC32_POLYNOMIAL     (CRC32_POLYNOMIAL)
       )
   u_CRC32
     (
      .o_crc32_valid        (/*NOT CONNECTED   */),
      .o_crc_ready          (crc_calc_ready      ),
      .o_crc32              (crc_calc            ),

      .i_data               (data_to_crc         ),
      .i_data_valid         (datavalid_crc       ),
      .i_crc32              (/*NOT CONNECTED   */),
      .i_crc32_valid        (/*NOT CONNECTED   */),
      .i_tail_size_bits     (tailsize_crc        ),

      .i_valid              (i_valid             ),

      .i_reset              (i_reset             ),
      .i_clock              (i_clock             )
      ) ;

endmodule
