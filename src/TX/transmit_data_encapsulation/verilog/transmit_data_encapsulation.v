module transmit_data_encapsulation
  #(
    // Parameters.
    parameter NB_DATA                = 64 ,
    parameter LOG2_NB_DATA           = 6 ,
    parameter NB_ADDRESS             = 48,
    parameter NB_LENTYP              = 16 ,
    parameter LOG2_NB_LENTYP         = 4 ,
    parameter MIN_LENGTH_FRAME_BYTES = 64 ,
    parameter MAX_LENGTH_FRAME_BYTES = 1982,
    parameter NB_CRC32               = 32,
    parameter NB_TIMER               = 10, // Nr of bits from frame
    parameter PREAMBLE               = 56'h55555555555555,
    parameter NB_PREAMBLE            = 56,
    parameter SFD                    = 8'hD5,
    parameter NB_SFD                 = 8,
    parameter CRC32_POLYNOMIAL       = 33'h104C11DB7
    )
   (
    // Outputs.
    output wire [NB_DATA-1:0]      o_data , //NB DATA bits of  data to be TX'd
    output wire                    o_data_ready , //NB DATA bits are ready
    output reg [LOG2_NB_DATA-1:0 ] o_tail_size ,
    // Inputs.
    input wire                     i_data_valid ,
    input wire [NB_ADDRESS-1:0]    i_destination_address,
    input wire [NB_ADDRESS-1:0]    i_source_address,
    input wire [NB_LENTYP-1:0]     i_lentyp,
    input wire [NB_DATA-1:0]       i_client_data,
    input wire [NB_CRC32-1:0]      i_fcs,
    input wire                     i_fcs_present,
    input wire                     i_fcs_ready,
    //input wire                                i_half_duplex ,

    input wire                     i_valid , // Throughput control.
    input wire                     i_reset ,
    input wire                     i_clock
    ) ;

   //==========================================================================
   // LOCAL PARAMETERS.
   //==========================================================================
   localparam SOF                             = {PREAMBLE,SFD};
   localparam NB_SOF                          = NB_PREAMBLE + NB_SFD;
   localparam NB_HEADER                       = NB_ADDRESS*2+NB_LENTYP;
   localparam DELAY_CLOCKS                    = 5; //Nota: puede adelantarse la latencia cubriendo el tiempo muerto del CRC32 con SOF y HEADER
   localparam NB_DATA_TO_CLIENT               = NB_SOF+NB_HEADER+NB_DATA+DELAY_CLOCKS*NB_DATA;
   localparam MIN_LENTYP_BYTES                = MIN_LENGTH_FRAME_BYTES-NB_HEADER/8-NB_CRC32/8;
   localparam LOG2_MAX_FRAMESIZE_BITS         = 14;
   localparam MIN_DATA_SIZE                   = 46;

   localparam MIN_N_CLOCKS_DATAV_CRC          = ((2*NB_ADDRESS+NB_LENTYP+46*8)%NB_DATA == 0) ? ((2*NB_ADDRESS+NB_LENTYP+46*8)/NB_DATA) : ((2*NB_ADDRESS+NB_LENTYP+46*8)/NB_DATA) + 1; //Min nr of clocks to crc
   localparam NB_TIMER_PADDING                = 3;


   // FSM
   localparam                NB_STATE         = 2 ;
   localparam [NB_STATE-1:0] ST_0_IDLE        = 0 ;
   localparam [NB_STATE-1:0] ST_1_FILL_BUFFER = 1 ;
   localparam [NB_STATE-1:0] ST_2_ASSEMBLE    = 2 ;
   localparam [NB_STATE-1:0] ST_3_FINISH      = 3 ;

   //==========================================================================
   // INTERNAL SIGNALS.
   //==========================================================================
   // FSM
   reg [NB_STATE-1:0]              state ;
   reg [NB_STATE-1:0]              next_state ;

   reg                             fsmo_o_data_ready;
   reg                             fsmo_init_buffer;
   reg                             fsmo_buffer_waiting;
   //reg                             fsmo_check_padding;
   reg                             fsmo_shift_data;
   reg                             fsmo_timer_reset;
   reg                             fsmo_done;

   // OTHER
   reg                             delayed_fsmo_o_data_ready;

   reg [NB_DATA-1:0]               client_data;

   reg                             data_valid_d;
   reg                             data_valid_dd;
   wire                            data_valid_pos;
   //wire                            data_valid_neg;

   reg                             data_ready_d;
   //wire                            data_ready_pos;
   wire                            data_ready_neg;

   reg [NB_ADDRESS-1:0]            destination_address_reg;
   reg [NB_ADDRESS-1:0]            source_address_reg;
   reg [NB_LENTYP-1:0]             captured_i_lentyp;
   reg [NB_LENTYP-1:0]             lentyp_reg;
   reg [NB_CRC32-1:0]              crc32_reg;
   reg                             fcs_present_reg;

   reg [NB_TIMER-1:0]              timer;
   wire                            time_out_fill_buffer;
   wire                            time_out_assemble;
   wire                            time_out_finish;
   reg [DELAY_CLOCKS-1:0]          delayed_data_valid;

   // Signals between CRC and TDE
   wire                            crc_calc_ready ;
   wire [NB_CRC32-1:0]             crc_calc;
   reg [NB_HEADER-1:0]             ext_data_to_crc;
   wire                            extra_clock;
   wire [NB_DATA-1:0]              data_to_crc;

   //For smaller frames than MIN_LEN_TYP
   wire                            min_datavalid_crc;

   wire                            datavalid_crc;
   wire [LOG2_NB_DATA-1:0]         tailsize_crc;
   wire                            timer_crc;

   reg [NB_DATA_TO_CLIENT-1:0]     data_to_client;
   wire                            extra_clock_for_clientdata;

   integer                         i;
   integer                         n;

   reg [NB_TIMER_PADDING-1:0]      timer_padding;
   reg [NB_DATA-1:0]               data_padding_mask;
   wire                            timeout_padding;
   wire                            timer_tailsize_padding;

   wire                            padding_required;

   wire                            datavalid_crc_test;
   wire                            dataready_test;
   reg                             dataready_test_d;
   wire                            dataready_test_neg;
   reg                             processing_reg;
   reg [NB_TIMER-1:0]              timer_valids;

   reg [NB_DATA_TO_CLIENT-1:0]     data_to_client_last_padded;
   //==========================================================================
   // ALGORITHM.
   //==========================================================================

   assign o_data = data_to_client[NB_DATA_TO_CLIENT-1-:NB_DATA]; //Take 64MSB
   assign o_data_ready = fsmo_o_data_ready; //|| delayed_fsmo_o_data_ready;

   assign padding_required = i_lentyp < MIN_LENTYP_BYTES;

   //Possibly padded data
   always @(*)
     casez ({padding_required, timer_tailsize_padding, timeout_padding})
       3'b11?: client_data = i_client_data & data_padding_mask;
       3'b101: client_data = {NB_DATA{1'b0}};
       3'b100: client_data = i_client_data;
       default: client_data = i_client_data;
     endcase // casez ({padding_required, timer_tailsize_padding, timeout_padding})

   //Signals that the block is working
   always @(posedge i_clock)
     begin
        if (i_reset)
          processing_reg <= 1'b0;
        else if (i_valid && data_valid_pos)
          processing_reg <= 1'b1;
        else if (i_valid && dataready_test_neg)
          processing_reg <= 1'b0;
     end

   always @(posedge i_clock)
     if (i_reset | fsmo_done)
       timer_valids <= {NB_TIMER{1'b0}};
      else if (i_valid & (processing_reg | data_valid_pos))
       timer_valids <= timer_valids + 1'b1;

   assign dataready_test = (lentyp_reg > MIN_LENTYP_BYTES) ? (timer_valids*NB_DATA) < (NB_HEADER+lentyp_reg*8+NB_CRC32) : (timer_valids*NB_DATA) < (NB_HEADER+MIN_LENTYP_BYTES*8+NB_CRC32);

     always @(posedge i_clock)
       if (i_reset)
         dataready_test_d <= 1'b0;
       else if (i_valid)
         dataready_test_d <= dataready_test;

   assign dataready_test_neg = ~dataready_test & dataready_test_d;

   always @( posedge i_clock )
     begin
        if (i_reset)begin
           data_valid_d <= 1'b0;
           data_valid_dd <= 1'b0;
           data_ready_d <= 1'b0;
           o_tail_size <= 'b0;
        end else if (i_valid) begin
           data_valid_d <= i_data_valid;
           data_valid_dd <= data_valid_d;
           data_ready_d <= o_data_ready;
           if (o_data_ready)
             o_tail_size <= (NB_SOF+NB_HEADER+lentyp_reg*8+NB_CRC32)%NB_DATA;
        end
     end // always @ ( posedge i_clock )

   //Padding logic
   always @( * )
     begin
	  n = 'b0;
     data_padding_mask = {NB_DATA{1'b1}};
        if (i_lentyp*8%NB_DATA!=0) begin
           for (n=0; n<NB_DATA; n=n+1)
             if (n<(NB_DATA - (i_lentyp*8)%NB_DATA))
               data_padding_mask[n] = 1'b0;
        end
     end

   always @(posedge i_clock)
     begin
        if (i_reset || data_ready_neg)
          timer_padding <= 'b0;
        else if (i_valid & (processing_reg | i_data_valid) & ~timeout_padding)
          timer_padding <= timer_padding + 1'b1;
     end

   assign timeout_padding = (i_lentyp < MIN_LENTYP_BYTES ) ? ((timer_padding+1)*NB_DATA > i_lentyp*8) & ~data_valid_pos : 1'b0;
   assign timer_tailsize_padding = (i_lentyp < MIN_LENTYP_BYTES ) & i_data_valid ? (timer_padding == i_lentyp*8/NB_DATA) : 1'b0;

   assign data_valid_pos = i_data_valid & ~ data_valid_d;
   //assign data_valid_neg = ~i_data_valid & data_valid_d;
   //assign data_ready_pos = o_data_ready & ~data_ready_d;
   assign data_ready_neg = ~o_data_ready & data_ready_d;

   //Capture headers
   always @( posedge i_clock )
     begin
        if (i_reset) begin
        //    destination_address_reg <= {NB_ADDRESS{1'b0}};
        // source_address_reg <= {NB_ADDRESS{1'b0}};
        lentyp_reg <= {NB_LENTYP{1'b0}};
        captured_i_lentyp <= MIN_LENTYP_BYTES;
        // fcs_present_reg <= 1'b0;
     end else if (i_valid && data_valid_pos) begin
        // destination_address_reg <= i_destination_address;
        // source_address_reg <= i_source_address;
        captured_i_lentyp <= i_lentyp;
        lentyp_reg <= (i_lentyp<MIN_LENTYP_BYTES) ? MIN_LENTYP_BYTES : i_lentyp;
        // fcs_present_reg <= i_fcs_present;
     end
     end


   //Timer logic
   always @( posedge i_clock )
     begin
        if (i_reset || fsmo_timer_reset)
          timer <= {NB_TIMER{1'b0}};
        else if (i_valid)
          timer <= timer + 1'b1;
     end

   //Cuidado con NB_DATA!=64: Delay clocks en time_out_fill_buffer no aplica de la misma forma, asi mismo, datavalid_crc hay que revisarlo
   assign time_out_fill_buffer = (captured_i_lentyp < MIN_LENTYP_BYTES) ? (timer+1>(DELAY_CLOCKS-2)): (timer+1>(DELAY_CLOCKS-1)); //Waiting period for buffer to fill and allow time to wait for CRC latency
   //Signals that the data field in the frame has all been received and put into the buffer
   assign time_out_assemble = ((timer+1)*NB_DATA >= lentyp_reg*8-8);
   //Signals that the transmission has ended
   assign time_out_finish = (captured_i_lentyp < MIN_LENTYP_BYTES) ? (timer+1) >= 7 & (state == ST_3_FINISH) : ((timer+1) >= ((NB_DATA_TO_CLIENT/NB_DATA)+extra_clock_for_clientdata)) & (state == ST_3_FINISH);//Prueba
   //Signals that CRC should be inserted
   assign timer_crc = (captured_i_lentyp < MIN_LENTYP_BYTES) ? ((timer+1) >= DELAY_CLOCKS+1) : ((timer+1) >= ((NB_DATA_TO_CLIENT/NB_DATA)-1) );
   //----------------------------------
   //FSM
   //----------------------------------

   // State update.
   always @( posedge i_clock )
     begin
        if ( i_reset )
          state <= ST_0_IDLE ;
        else if ( i_valid )
          state <= next_state ;
     end

   // Calculate next state and FSM outputs.
   always @( * )
     begin
        fsmo_o_data_ready = 1'b0;
        fsmo_init_buffer = 1'b0;
        //fsmo_check_padding = 1'b0;
        fsmo_buffer_waiting = 1'b0;
        fsmo_shift_data = 1'b0;
        fsmo_timer_reset = 1'b0;
        fsmo_done = 1'b0;
        case ( state )
          ST_0_IDLE :
            begin
               casez ( {data_valid_pos, time_out_fill_buffer,  time_out_assemble , time_out_finish } )
                 4'b1???: next_state = ST_1_FILL_BUFFER;
                 default: next_state = ST_0_IDLE;
               endcase
               fsmo_o_data_ready = 1'b0;
               fsmo_init_buffer = data_valid_pos;
               fsmo_buffer_waiting = 1'b0;
               //fsmo_check_padding = 1'b0;
               fsmo_shift_data = 1'b0;
               fsmo_timer_reset = 1'b1;
               fsmo_done = 1'b0;
            end
          ST_1_FILL_BUFFER:
            begin
               casez ( {data_valid_pos, time_out_fill_buffer,  time_out_assemble , time_out_finish } )
                 4'b?1??: next_state = ST_2_ASSEMBLE;
                 default: next_state = ST_1_FILL_BUFFER;
               endcase // casez ( {data_valid_pos, timer_buffer, timer_done } )
               fsmo_o_data_ready = 1'b0;
               fsmo_init_buffer = 1'b0;
               fsmo_buffer_waiting = 1'b1;
               //fsmo_check_padding = time_out_fill_buffer;
               fsmo_shift_data = 1'b0;
               fsmo_timer_reset = 1'b0;
               fsmo_done = 1'b0;
            end
          ST_2_ASSEMBLE:
            begin
               casez ( {data_valid_pos, time_out_fill_buffer,  time_out_assemble , time_out_finish } )
                 4'b??1?: next_state = ST_3_FINISH;
                 default: next_state = ST_2_ASSEMBLE;
               endcase
               fsmo_o_data_ready = 1'b1;
               fsmo_init_buffer = 1'b0;
               fsmo_buffer_waiting = 1'b0;
               //fsmo_check_padding = 1'b0;
               fsmo_shift_data = 1'b1;
               fsmo_timer_reset = time_out_assemble;
               fsmo_done = 1'b0;
            end
          ST_3_FINISH:
            begin
               casez ( {data_valid_pos, time_out_fill_buffer,  time_out_assemble , time_out_finish } )
                 4'b???1: next_state = ST_0_IDLE;
                 default: next_state = ST_3_FINISH;
               endcase
               fsmo_o_data_ready = 1'b1;
               fsmo_init_buffer = 1'b0;
               fsmo_buffer_waiting = 1'b0;
               //fsmo_check_padding = 1'b0;
               fsmo_shift_data = 1'b1;
               fsmo_timer_reset = time_out_finish;
               if (timer_crc)
                 fsmo_done = 1'b1;
            end
        endcase
     end


   //----------------------------------
   //OTHER LOGIC
   //----------------------------------

   //Save specific padded data frames from 41-46 lentyp which have certain characteristics
   always @(*)
     begin
        data_to_client_last_padded = {data_to_client[NB_DATA_TO_CLIENT-NB_DATA-1:0], client_data};
        data_to_client_last_padded[NB_DATA_TO_CLIENT-NB_SOF-NB_HEADER-(DELAY_CLOCKS-1)*NB_DATA-1-:NB_DATA] = client_data;
     end

   always @ (posedge i_clock)
     begin
        if (i_reset | data_ready_neg)
          data_to_client <= 'b0;
        else if (i_valid && fsmo_init_buffer)
          data_to_client <= {SOF, i_destination_address, i_source_address, i_lentyp, client_data, data_to_client[NB_DATA_TO_CLIENT-NB_SOF-NB_HEADER-NB_DATA-1:0]};
        else if (i_valid && fsmo_buffer_waiting)
            data_to_client[NB_DATA_TO_CLIENT-NB_SOF-NB_HEADER-NB_DATA-((timer)*NB_DATA)-1-:NB_DATA] <= client_data;
        else if (i_valid && fsmo_shift_data) begin
           if (padding_required & timer_tailsize_padding)
             data_to_client <= data_to_client_last_padded;
           else
             data_to_client <= {data_to_client[NB_DATA_TO_CLIENT-NB_DATA-1:0], client_data};
        end if (i_valid && fsmo_done) begin
           if (i_fcs_present) begin
              data_to_client[NB_DATA_TO_CLIENT-tailsize_crc-1-:NB_CRC32] <= i_fcs;
           end else begin
              data_to_client[NB_DATA_TO_CLIENT-tailsize_crc-1-:NB_CRC32] <= crc_calc;
           end
        end
     end // always @ (posedge i_clock)

   always @(posedge i_clock)
     begin
        if (i_reset)
          ext_data_to_crc <= 'b0;
        else if (data_valid_pos) begin //First clock, add everything to the register
           ext_data_to_crc <= {i_source_address[NB_ADDRESS-(NB_DATA-NB_ADDRESS)-1:0], i_lentyp, client_data};
        end else if (i_valid & datavalid_crc) //Add new incoming data
          ext_data_to_crc <= {ext_data_to_crc[NB_HEADER-NB_DATA-1:0], client_data};
     end

   assign extra_clock = ((lentyp_reg*8)%NB_DATA > 16) || ((lentyp_reg*8)%NB_DATA == 0); //Add an extra clock for i_data tailsizes > 16 (including 48) because of included HEADER
   assign data_to_crc = (data_valid_pos) ? {i_destination_address, i_source_address[NB_ADDRESS-1-:(NB_DATA-NB_ADDRESS)]} : ext_data_to_crc[NB_HEADER-1-:NB_DATA]; // Take the 64 MSB
   assign min_datavalid_crc = data_valid_pos | (timer_valids+1 <= MIN_N_CLOCKS_DATAV_CRC) & processing_reg;
   assign datavalid_crc = (captured_i_lentyp < MIN_LENTYP_BYTES) ? min_datavalid_crc : i_data_valid || data_valid_d || (data_valid_dd & extra_clock); //|| data_valid_dd; //Add 2 clocks to allow CRC to finish
   assign tailsize_crc = (NB_HEADER + lentyp_reg*8)%NB_DATA;


   //For client data
   assign extra_clock_for_clientdata = tailsize_crc > (NB_DATA-NB_CRC32);

   //==========================================================================
   // CONNECTION TO DUT
   //==========================================================================
   CRC32
     #(
       .NB_DATA              (NB_DATA         ),
       .NB_CRC32             (NB_CRC32        ),
       .CRC32_POLYNOMIAL     (CRC32_POLYNOMIAL),
       .REF_IN               (1               ),
       .REF_OUT              (1               )
       )
   u_CRC32
     (
      .o_crc32_valid        (/*NOT CONNECTED   */),
      .o_crc_ready          (crc_calc_ready      ),
      .o_crc32              (crc_calc            ),

      .i_data               (data_to_crc         ),
      .i_data_valid         (datavalid_crc       ),
      .i_crc32              (/*NOT CONNECTED   */),
      .i_crc32_valid        (/*NOT CONNECTED   */),
      .i_tail_size_bits     (tailsize_crc        ),

      .i_valid              (i_valid             ),

      .i_reset              (i_reset             ),
      .i_clock              (i_clock             )
      ) ;

endmodule
