module transmit_data_encapsulation_wbuffer
  #(
    // Parameters.
    parameter NB_DATA                = 64 ,
    parameter LOG2_NB_DATA           = 6 ,
    parameter NB_ADDRESS             = 48,
    parameter NB_LENTYP              = 16 ,
    parameter LOG2_NB_LENTYP         = 4 ,
    parameter MIN_LENGTH_FRAME_BYTES = 518,
    parameter MAX_LENGTH_FRAME_BYTES = 1982,
    parameter NB_CRC32               = 32,
    parameter NB_TIMER               = 10, // Nr of bits from frame
    parameter PREAMBLE               = 56'hAAAAAAAAAAAAAA,
    parameter NB_PREAMBLE            = 56,
    parameter SFD                    = 8'hAB,
    parameter NB_SFD                 = 8,
    parameter CRC32_POLYNOMIAL       = 33'h104C11DB7
    )
   (
    // Outputs.
    output wire [MAX_LENGTH_FRAME_BYTES*8-1:0] o_assembled_frame ,
    output wire                                o_frame_ready ,
    output reg [LOG2_NB_DATA-1:0 ]             o_tail_size ,
    // Inputs.
    input wire                                 i_data_valid ,
    input wire [NB_ADDRESS-1:0]                i_destination_address,
    input wire [NB_ADDRESS-1:0]                i_source_address,
    input wire [NB_LENTYP-1:0]                 i_lentyp,
    input wire [NB_DATA-1:0]                   i_client_data,
    input wire [NB_CRC32-1:0]                  i_fcs,
    input wire                                 i_fcs_present,
  //input wire                                i_half_duplex ,

    input wire                                 i_valid , // Throughput control.
    input wire                                 i_reset ,
    input wire                                 i_clock
    ) ;

   //==========================================================================
   // LOCAL PARAMETERS.
   //==========================================================================
   localparam SOF = {PREAMBLE,SFD};
   localparam NB_SOF = NB_PREAMBLE + NB_SFD;
   localparam NB_HEADER = NB_ADDRESS*2+NB_LENTYP;

   // FSM
   localparam                NB_STATE      = 3 ;
   localparam [NB_STATE-1:0] ST_0_IDLE     = 0 ;
   localparam [NB_STATE-1:0] ST_1_SFD      = 1 ;
   localparam [NB_STATE-1:0] ST_2_ASSEMBLE = 2 ;
   localparam [NB_STATE-1:0] ST_3_CRC      = 3 ;
   localparam [NB_STATE-1:0] ST_4_DONE     = 4 ;

   //==========================================================================
   // INTERNAL SIGNALS.
   //==========================================================================
   // FSM
   reg [NB_STATE-1:0]                       state ;
   reg [NB_STATE-1:0]                       next_state ;

   reg                                      fsmo_appendsof;
   reg                                      fsmo_appenddata;
   reg                                      fsmo_appendcrc;
   reg                                      fsmo_frameready;
   reg                                      fsmo_timer_reset;

   // OTHER
   reg                                      data_valid_d;
   reg                                      data_valid_dd;
   wire                                     data_valid_pos;

   reg [NB_ADDRESS-1:0]                     destination_address_reg;
   reg [NB_ADDRESS-1:0]                     source_address_reg;
   reg [NB_LENTYP-1:0]                      lentyp_reg;
   reg [NB_CRC32-1:0]                       crc32_reg;
   reg                                      fcs_present_reg;

   reg [NB_TIMER-1:0]                       timer;
   wire                                     timer_done;

   reg [MAX_LENGTH_FRAME_BYTES*8-1:0]       assembled_frame;


   // Signals between CRC and TDE
   wire                                     crc_calc_ready ;
   wire [NB_CRC32-1:0]                      crc_calc;
   reg [NB_HEADER-1:0]                      ext_data_to_crc;
   wire                                     extra_clock;
   wire [NB_DATA-1:0]                       data_to_crc;
   wire                                     datavalid_crc;
   wire [LOG2_NB_DATA-1:0]                  tailsize_crc;

   //==========================================================================
   // ALGORITHM.
   //==========================================================================

   /*DELETE THIS*/
   wire                                     debug_missmatch;

   assign debug_missmatch = (i_fcs != crc_calc) & crc_calc_ready;
// & (tailsize_crc != 6'b111000 && tailsize_crc != 6'b000000);


   /* DELETE THIS END */






   assign o_frame_ready = fsmo_frameready;
   assign o_assembled_frame = assembled_frame;

   //Outputs and other
   always @( posedge i_clock )
     begin
        if (i_reset)begin
           data_valid_d <= 1'b0;
           data_valid_d <= 1'b0;
           o_tail_size <= 'b0;
        end else if (i_valid) begin
           data_valid_d <= i_data_valid;
           data_valid_dd <= data_valid_d;
           if (o_frame_ready)
              o_tail_size <= (NB_SOF+NB_HEADER+lentyp_reg*8+NB_CRC32)%NB_DATA;
        end
     end

   assign data_valid_pos = i_data_valid & ~ data_valid_d;

   //Capture headers
   always @( posedge i_clock )
     begin
        if (i_reset) begin
          destination_address_reg <= {NB_ADDRESS{1'b0}};
          source_address_reg <= {NB_ADDRESS{1'b0}};
          lentyp_reg <= {NB_LENTYP{1'b0}};
          crc32_reg <= {NB_CRC32{1'b0}};
          fcs_present_reg <= 1'b0;
        end else if (i_valid && data_valid_pos) begin
           destination_address_reg <= i_destination_address;
           source_address_reg <= i_source_address;
           lentyp_reg <= i_lentyp;
           fcs_present_reg <= i_fcs_present;
           if (i_fcs_present)
             crc32_reg <= i_fcs;
        end
     end

   //Timer logic
   always @( posedge i_clock )
     begin
        if (i_reset || timer_done || fsmo_timer_reset)
          timer <= {NB_TIMER{1'b0}};
        else if (i_valid && i_data_valid)
          timer <= timer + 1'b1;
     end

   assign timer_done = ((timer+2)*NB_DATA >= lentyp_reg*8) ;//& i_data_valid;
   //----------------------------------
   //FSM
   //----------------------------------

   // State update.
   always @( posedge i_clock )
     begin
        if ( i_reset )
          state <= ST_0_IDLE ;
        else if ( i_valid )
          state <= next_state ;
     end

   // Calculate next state and FSM outputs.
   always @( * )
     begin
        fsmo_appendsof = 1'b0;
        fsmo_appenddata = 1'b0;
        fsmo_appendcrc = 1'b0;
        fsmo_frameready = 1'b0;
        fsmo_timer_reset = 1'b0;
        case ( state )
          ST_0_IDLE :
            begin
               casez ( {data_valid_pos, timer_done, fcs_present_reg||crc_calc_ready } )
                 3'b1??: next_state = ST_1_SFD;
                 default: next_state = ST_0_IDLE;
               endcase
               fsmo_appendsof = data_valid_pos;
               fsmo_appenddata = 1'b0;
               fsmo_appendcrc = 1'b0;
               fsmo_frameready = 1'b0;
               fsmo_timer_reset = data_valid_pos;
            end

          ST_1_SFD :
            begin
               casez ( {data_valid_pos, timer_done, fcs_present_reg||crc_calc_ready } )
                 default: next_state = ST_2_ASSEMBLE;
               endcase
               fsmo_appendsof = 1'b0;
               fsmo_appenddata = 1'b1;
               fsmo_appendcrc = 1'b0;
               fsmo_frameready = 1'b0;
               fsmo_timer_reset = 1'b0;
            end

          ST_2_ASSEMBLE :
            begin
               casez ( {data_valid_pos, timer_done, fcs_present_reg||crc_calc_ready } )
                 3'b?1?: next_state = ST_3_CRC;
                 default: next_state = ST_2_ASSEMBLE;
               endcase
               fsmo_appendsof = 1'b0;
               fsmo_appenddata = 1'b1;
               fsmo_appendcrc = 1'b0;
               fsmo_frameready = 1'b0;
               fsmo_timer_reset = 1'b0;
            end

          ST_3_CRC :
            begin
               casez ( {data_valid_pos, timer_done, fcs_present_reg||crc_calc_ready } )
                 3'b??1: next_state = ST_4_DONE;
                 default: next_state = ST_3_CRC;
               endcase
               fsmo_appendsof = 1'b0;
               fsmo_appenddata = 1'b0;
               fsmo_appendcrc = fcs_present_reg||crc_calc_ready;
               fsmo_frameready = 1'b0;
               fsmo_timer_reset = 1'b0;
            end

          ST_4_DONE :
            begin
               casez ( {data_valid_pos, timer_done, fcs_present_reg||crc_calc_ready } )
                 default: next_state = ST_0_IDLE;
               endcase
               fsmo_appendsof = 1'b0;
               fsmo_appenddata = 1'b0;
               fsmo_appendcrc = 1'b0;
               fsmo_frameready = 1'b1;
               fsmo_timer_reset = 1'b0;
            end

        endcase
     end


   //----------------------------------
   //OTHER LOGIC
   //----------------------------------

   always @ (posedge i_clock)
     begin
        if ((i_reset /*|| state==ST_0_IDLE*/))
          assembled_frame <= 'b0;
        else if (fsmo_appendsof)
          assembled_frame <= {SOF, i_destination_address, i_source_address, i_lentyp, i_client_data, assembled_frame[MAX_LENGTH_FRAME_BYTES*8-NB_SOF-NB_HEADER-NB_DATA-1:0]};
        else if (fsmo_appenddata)
          //assembled_frame[] <= {{assembled_frame[MAX_LENGTH_FRAME_BYTES-NB_SOF-timer*NB_DATA-1-:NB_DATA]}, i_client_data, assembled_frame[MAX_LENGTH_FRAME_BYTES-NB_SOF-((timer+1)*NB_DATA)-1-:]};
          assembled_frame[MAX_LENGTH_FRAME_BYTES*8-NB_SOF-NB_HEADER-NB_DATA-(timer*NB_DATA)-1-:NB_DATA] <= i_client_data;
        else if (fsmo_appendcrc )
          if (i_fcs_present)
            assembled_frame[MAX_LENGTH_FRAME_BYTES*8-NB_SOF-NB_HEADER-(lentyp_reg*(NB_DATA/8))-1-:NB_CRC32] <= crc32_reg;
          else
            assembled_frame[MAX_LENGTH_FRAME_BYTES*8-NB_SOF-NB_HEADER-(lentyp_reg*(NB_DATA/8))-1-:NB_CRC32] <= crc_calc;
     end // always @ (posedge i_clock)

   //==========================================================================
   // CONNECTION TO DUT
   //==========================================================================
   always @(posedge i_clock)
     begin
       if (i_reset)
         ext_data_to_crc <= 'b0;
       else if (data_valid_pos) //First clock, add everything to the register
         ext_data_to_crc <= {i_source_address[NB_ADDRESS-(NB_DATA-NB_ADDRESS)-1:0], i_lentyp, i_client_data};
       else if (i_valid & (i_data_valid || data_valid_d || data_valid_dd)) //Add new incoming data
         ext_data_to_crc <= {ext_data_to_crc[NB_HEADER-NB_DATA-1:0], i_client_data};
     end

   assign extra_clock = ((lentyp_reg*8)%NB_DATA > 16) || ((lentyp_reg*8)%NB_DATA == 0); //Add an extra clock for i_data tailsizes > 16 (including 48) because of included HEADER
   //assign ext_data_to_crc = (data_valid_pos) ? {i_destination_address, i_source_address, i_lentyp, i_client_data} : {ext_data_to_crc[NB_HEADER+NB_DATA-NB_DATA-1:0], i_client_data};
   assign data_to_crc = (data_valid_pos) ? {i_destination_address, i_source_address[NB_ADDRESS-1-:(NB_DATA-NB_ADDRESS)]} : ext_data_to_crc[NB_HEADER-1-:NB_DATA]; // Take the 64 MSB
   assign datavalid_crc = i_data_valid || data_valid_d || (data_valid_dd & extra_clock); //|| data_valid_dd; //Add 2 clocks to allow CRC to finish
   assign tailsize_crc = (NB_HEADER + lentyp_reg*8)%NB_DATA;



   CRC32
     #(
       .NB_DATA              (NB_DATA         ),
       .NB_CRC32             (NB_CRC32        ),
       .CRC32_POLYNOMIAL     (CRC32_POLYNOMIAL)
       )
   u_CRC32
     (
      .o_crc32_valid        (/*NOT CONNECTED   */),
      .o_crc_ready          (crc_calc_ready      ),
      .o_crc32              (crc_calc            ),

      .i_data               (data_to_crc         ),
      .i_data_valid         (datavalid_crc       ),
      .i_crc32              (/*NOT CONNECTED   */),
      .i_crc32_valid        (/*NOT CONNECTED   */),
      .i_tail_size_bits     (tailsize_crc        ),

      .i_valid              (i_valid             ),

      .i_reset              (i_reset             ),
      .i_clock              (i_clock             )
      ) ;

endmodule
