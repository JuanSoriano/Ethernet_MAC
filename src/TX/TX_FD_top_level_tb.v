module TX_FD_top_level_tb ();

   localparam DATAREQ_PAYLOADS_FILE                                      = "data_request_payload.mem";
   localparam DATAREQ_FIELDS_FILE                                        = "data_request_fields.mem";
   localparam DATAREQ_DATAVALID_FILE                                     = "data_request_valid.mem";

   localparam NB_DATA                = 64;
   localparam LOG2_NB_DATA           = 6;
   localparam NB_ADDRESS             = 48;
   localparam NB_LENTYP              = 16 ;
   localparam LOG2_NB_LENTYP         = 4 ;
   localparam MIN_LENGTH_FRAME_BYTES = 64;
   localparam MAX_LENGTH_FRAME_BYTES = 1982;
   localparam NB_CRC32               = 32;
   localparam NB_TIMER               = 10; // Nr of bits from frame
   localparam PREAMBLE               = 56'hAAAAAAAAAAAAAA;
   localparam NB_PREAMBLE            = 56;
   localparam SFD                    = 8'hAB;
   localparam NB_SFD                 = 8;
   localparam CRC32_POLYNOMIAL       = 33'h104C11DB7;
   localparam MIN_FRAME_SIZE_BITS    = 512;
   localparam IPG_BITS               = 96;
   localparam IPG_STRETCH_BITS       = 0;

   wire [NB_DATA-1:0]      data_o;
   wire                    rx_status_o;
   wire [LOG2_NB_DATA-1:0] tail_size_o;

   wire                  MA_DATA_request_i;
   wire [NB_ADDRESS-1:0] destination_address_i;
   wire [NB_ADDRESS-1:0] source_address_i;
   wire [NB_LENTYP-1:0]  lentyp_i;
   wire [NB_DATA-1:0]    client_data_i;
   wire [NB_CRC32-1:0]   fcs_i;
   wire                  fcs_present_i;

   wire                  valid;
   wire                  reset;

   reg                   clock = 1'b0;
   integer               timer = 0;

   //For vector matching
   integer                                         fd_datavalid_input = 0;
   integer                                         fd_payload_input = 0;
   integer                                         fd_fields_input = 0;
   integer                                         scan_file ;
   integer                                         last_pos = 0;
   integer                                         new_pos = 0;

   //Registers to parse data
   reg                                             datavalid;
   reg [NB_DATA-1:0]                               payload;
   reg [NB_ADDRESS-1:0]                            dest_addr;
   reg [NB_ADDRESS-1:0]                            source_addr;
   reg [NB_LENTYP-1:0]                             lentyp;
   reg [NB_CRC32-1:0]                              fcs;


   reg                                             datavalid_reg;
   reg [NB_DATA-1:0]                               payload_reg;
   reg [NB_ADDRESS-1:0]                            dest_addr_reg;
   reg [NB_ADDRESS-1:0]                            source_addr_reg;
   reg [NB_LENTYP-1:0]                             lentyp_reg;
   reg [NB_CRC32-1:0]                              fcs_reg;

   reg                                             datavalid_d;
   wire                                            datavalid_pos;

   initial
     begin
        fd_payload_input = $fopen(DATAREQ_PAYLOADS_FILE,"r");
        if (fd_payload_input==0)
          begin
             $display("Error de lectura de archivo DATAREQ_PAYLOADS_FILE");
             $stop;
          end
        fd_fields_input = $fopen(DATAREQ_FIELDS_FILE,"r");
        if (fd_fields_input==0)
          begin
             $display("Error de lectura de archivo DATAREQ_FIELDS_FILE");
             $stop;
          end
        fd_datavalid_input = $fopen(DATAREQ_DATAVALID_FILE, "r");
        if (fd_datavalid_input==0)
          begin
             $display("Error de lectura de archivo DATAREQ_DATAVALID_FILE");
             $stop;
          end
     end // initial begin

   //File read
   always @ (posedge clock)
     begin
        if (valid /*&& !endreg*/) begin
           scan_file = $fscanf(fd_datavalid_input, "%1b", datavalid);
           if($feof(fd_payload_input)) begin
              $display("EOF datavalid");
              $fclose(fd_payload_input);
           end
           if (datavalid_reg)begin
              //Capture payload
              scan_file = $fscanf(fd_payload_input, "%64b", payload);
              new_pos = $ftell(fd_payload_input);
              if (new_pos - last_pos <64)
                payload = payload << (NB_DATA-(new_pos-last_pos));
              last_pos = new_pos;
              if($feof(fd_payload_input)) begin
                 $display("EOF datavalid");
                 $fclose(fd_payload_input);
              end
           end
           if (datavalid_pos) begin
              if (datavalid_pos)
                //Capture fields
                scan_file = $fscanf(fd_fields_input, "%b", dest_addr) ;
              if ($feof(fd_fields_input))begin
                 $display ("EOF data");
                 $fclose(fd_fields_input);
              end
              scan_file = $fscanf(fd_fields_input, "%b", source_addr) ;
              scan_file = $fscanf(fd_fields_input, "%b", lentyp) ;
              scan_file = $fscanf(fd_fields_input, "%b", fcs) ;
           end // if (datavalid_pos)
           end
     end // always @ (posedge clock)

   always @ (posedge clock)
     begin
        if (reset)begin
           datavalid_reg <= 'b0;
           payload_reg <= 'b0;
           dest_addr_reg <= 'b0;
           source_addr_reg <= 'b0;
           lentyp_reg <= 'b0;
           fcs_reg <= 'b0;
        end else if (valid) begin
           datavalid_reg <= datavalid;
           payload_reg <= payload;
           dest_addr_reg <= dest_addr;
           source_addr_reg <= source_addr;
           lentyp_reg <= lentyp;
           fcs_reg <= fcs;
        end
     end

   always @ (posedge clock)
     begin
        if (reset)
          datavalid_d <= 1'b0;
        else
          datavalid_d <= datavalid_reg;
     end

   assign datavalid_pos = datavalid_reg & ~datavalid_d;


   always
     begin
        #(50) clock = ~clock ;
     end

   always @ ( posedge clock )
     begin
        timer   <= timer + 1;
     end

   assign reset = (timer == 2) ; // Reset at time 2
   assign valid = timer > 5;

   TX_FD_top_level
     #(
       .NB_DATA                (NB_DATA                ),
       .LOG2_NB_DATA           (LOG2_NB_DATA           ),
       .NB_ADDRESS             (NB_ADDRESS             ),
       .NB_LENTYP              (NB_LENTYP              ),
       .LOG2_NB_LENTYP         (LOG2_NB_LENTYP         ),
       .MIN_LENGTH_FRAME_BYTES (MIN_LENGTH_FRAME_BYTES ),
       .MAX_LENGTH_FRAME_BYTES (MAX_LENGTH_FRAME_BYTES ),
       .NB_CRC32               (NB_CRC32               ),
       .NB_TIMER               (NB_TIMER               ),
       .PREAMBLE               (PREAMBLE               ),
       .NB_PREAMBLE            (NB_PREAMBLE            ),
       .SFD                    (SFD                    ),
       .NB_SFD                 (NB_SFD                 ),
       .CRC32_POLYNOMIAL       (CRC32_POLYNOMIAL       ),
       .MIN_FRAME_SIZE_BITS    (MIN_FRAME_SIZE_BITS    ),
       .IPG_BITS               (IPG_BITS               ),
       .IPG_STRETCH_BITS       (IPG_STRETCH_BITS       )
       )
   u_TX_FD_top_level
     (
      .o_data                  (data_o                 ),
      .o_rx_status             (rx_status_o            ),
      .o_tail_size             (tail_size_o            ),
      .i_MA_DATA_request       (MA_DATA_request_i      ),
      .i_destination_address   (destination_address_i  ),
      .i_source_address        (source_address_i       ),
      .i_lentyp                (lentyp_i               ),
      .i_client_data           (client_data_i          ),
      .i_fcs                   (fcs_i                  ),
      .i_fcs_present           (fcs_present_i          ),
      .i_enable_deferring      (enable_deferring_i     ),
      .i_valid                 (valid                  ),
      .i_reset                 (reset                  ),
      .i_clock                 (clock                  )
      ) ;


   assign MA_DATA_request_i = datavalid_d;
   assign destination_address_i = dest_addr_reg;
   assign source_address_i = source_addr_reg;
   assign lentyp_i = lentyp_reg;
   assign client_data_i = payload_reg;
   assign fcs_i = fcs_reg;
   assign fcs_present_i = 1'b0;
   assign enable_deferring_i = 1'b0;

endmodule
