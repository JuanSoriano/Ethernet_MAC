module backoff
#(
    // Parameters.
    parameter                                   NB_DATA         = 64 ,
    parameter                                   LOG2_NB_DATA    = 6 ,
    parameter                                   NB_TIMER        = 10 , // Nr of bits from frame
    parameter                                   NB_ATTEMPTS     = 4 ,
    parameter                                   NB_BACKOFF      = 4                           
)
(
    // Outputs.
    output  wire                                o_backoff ,                          
    // Inputs.
    input   wire                                i_start ,
    input   wire    [NB_BACKOFF-1:0]            i_backoff_limit ,
    input   wire    [NB_ATTEMPTS-1:0]           i_attempts ,
    input   wire                                i_valid ,   // Throughput control.

    input   wire                                i_reset ,
    input   wire                                i_clock
) ;

    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================

    // FSM
    localparam                                  NB_STATE        = 1 ;
    localparam      [NB_STATE-1:0]              ST_0_IDLE       = 0 ;
    localparam      [NB_STATE-1:0]              ST_1_WAIT       = 1 ;

    // Other        

    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
    // FSM
    reg             [NB_STATE-1:0]                  state ;
    reg             [NB_STATE-1:0]                  next_state ;

    reg                                             fsmo_reset_timer ;
    reg                                             fsmo_update_maxbackoff ;
    reg                                             fsmo_update_randomnum ;

    //Other

    reg             [NB_BACKOFF-1:0]                max_backoff ;
    reg             [NB_BACKOFF-1:0]                max_backoff_d ;

    reg             [NB_TIMER-1:0]                  timer ;
    reg             [NB_TIMER-1:0]                  max_timer ; // Max timer to signal a time_act
    wire                                            time_out ;

    //Random Gem
    wire                                            random_length ; // Output from random gen

    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    //----------------------------------
    //FSM
    //----------------------------------

    // State update.
    always @( posedge i_clock )
    begin
        if ( i_reset )
            state <= ST_0_IDLE ;
        else if ( i_valid )
            state <= next_state ;
    end

    // Calculate next state and FSM outputs.
    always @( * )
    begin

        next_state                  = ST_0_IDLE ;
        fsmo_reset_timer            = 0 ;
        fsmo_update_maxbackoff      = 0 ;
        fsmo_update_randomnum       = 0 ;

        case ( state )
            ST_0_IDLE :
            begin
                casez ( {i_start, time_out} )
                    2'b1?   : next_state   = ST_1_WAIT ;
                    default : next_state   = ST_0_IDLE ;
                endcase
                fsmo_reset_timer            = i_start ;
                fsmo_update_maxbackoff      = i_start ;
                fsmo_update_randomnum       = i_start ;
            end

            ST_1_WAIT :
            begin
                casez ( {i_start, time_out} )
                    2'b?1   : next_state   = ST_0_IDLE ;
                    default : next_state   = ST_1_WAIT ;
                endcase
                fsmo_reset_timer            = time_out ;
                fsmo_update_maxbackoff      = time_out ;
                fsmo_update_randomnum       = time_out ;
            end
        endcase
    end


    //----------------------------------
    //OTHER LOGIC
    //----------------------------------

    // Timer logic
    always @( posedge i_clock )
    begin
        if ( i_reset || i_valid && fsmo_reset_timer || time_out )
            timer <= {NB_TIMER{1'b0}} ;
        else if ( i_valid && !time_out )
            timer <= timer + 1'b1 ;
    end

    always @( posedge i_clock )
    begin
        if ( i_reset )
            max_timer <= {NB_TIMER{1'b0}} ;
        else if ( i_valid )
            max_timer <= random_length ;
    end
    assign time_out = ( timer >= max_timer ) ;

    // Max backoff delayed
    always @( posedge i_clock )
    begin
        if ( i_reset )
            max_backoff_d <= 1'b0 ;
        else if ( i_valid && fsmo_update_maxbackoff )
            max_backoff_d <= max_backoff ;
    end 

    // Max backoff update
    always @( * )
    begin
        if (i_attempts==1'b1 && i_valid)
            max_backoff = 2 ;
        else if ( (i_backoff_limit <= i_attempts) && fsmo_update_maxbackoff && i_valid) begin
            max_backoff = max_backoff_d * 2;
        end
        else 
            max_backoff = 2 ;
    end 

endmodule