module transmit_link_mgmt
#(
    // Parameters.
    parameter                                   NB_DATA         = 64 ,
    parameter                                   LOG2_NB_DATA    = 6 ,
    parameter                                   NB_TIMER        = 10 // Nr of bits from frame           
)
(
    // Outputs.
    output  reg                                 o_transmit_code ,                         
    // Inputs.
    input   wire                                i_transmit_frame ,
    input   wire                                i_attempts ,
    input   wire                                i_attempt_limit ,
    input   wire                                i_transmitting ,
    input   wire                                i_bursting ,
    input   wire                                i_half_duplex ,
    input   wire                                i_deferring ,
    input   wire                                i_valid ,   // Throughput control.

    input   wire                                i_reset ,
    input   wire                                i_clock
) ;

    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================

    // FSM
    localparam                                  NB_STATE                     = 3  ;
    localparam      [NB_STATE-1:0]              ST_0_INIT                    = 0  ;
    localparam      [NB_STATE-1:0]              ST_1_CHECK                   = 1  ;
    localparam      [NB_STATE-1:0]              ST_2_WAIT_TRANSMISSION_END   = 2  ;
    localparam      [NB_STATE-1:0]              ST_3_WAIT_DEFERRING          = 3  ;
    localparam      [NB_STATE-1:0]              ST_4_ENDING                  = 4  ;

    // Other        
    localparam      [NB_TIMER-1:0]              LIMIT_T_WAIT_A               = 10 ;
    localparam      [NB_TIMER-1:0]              LIMIT_T_WAIT_B               = 20 ;
    localparam      [NB_TIMER-1:0]              LIMIT_T_WAIT_C               = 30 ;
    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
    // FSM
    reg             [NB_STATE-1:0]                  state ;
    reg             [NB_STATE-1:0]                  next_state ;

    reg                                             fsmo_set_deferring ;
    reg                                             fsmo_set_wastransmitting ;
    reg                                             fsmo_reset_timer ;
    reg                                             fsmo_clear_deferring ;
    reg                                             fsmo_timer_limit ;

    //Other

    wire                                            att_biggerthan_attlim ;
    wire                                            att_biggerthan_zero ;
    wire                                            att_equals_zero ;

    reg                                             attempts ;                                            

    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    //----------------------------------
    //FSM
    //----------------------------------

    // State update.
    always @( posedge i_clock )
    begin
        if ( i_reset )
            state <= ST_0_INIT ;
        else if ( i_valid )
            state <= next_state ;
    end

    // Calculate next state and FSM outputs.
    always @( * )
    begin

        case ( state )
            ST_0_INIT :
            begin
                casez ( {i_transmit_frame, i_transmitting} )

                endcase

            end

            ST_1_CHECK :
            begin
                casez ( {} )

                endcase

            end

            ST_2_WAIT_TRANSMISSION_END :
            begin
                casez ( {} )

                endcase

            end

            ST_3_WAIT_DEFERRING :
            begin
                casez ( {} )

                endcase

            end

            ST_4_ENDING :
            begin
                casez ( {} )

                endcase

            end

        endcase
    end


    //----------------------------------
    //OTHER LOGIC
    //----------------------------------
    //revisar attempts
    always @ ( posedge i_clock )
    begin
        if ( i_reset || )
            attempts <= 1'b0 ;
        else if ( i_valid )
    end

    assign att_biggerthan_attlim = (i_attempts > i_attempt_limit );

    assign att_biggerthan_zero = (i_attempts > 0) ;

    assign att_equals_zero = (i_attempts == 0) ;

endmodule