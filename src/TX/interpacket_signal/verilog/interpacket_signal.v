module interpacket_signal
#(
    // Parameters.
    parameter                                   NB_DATA         = 64 ,
    parameter                                   LOG2_NB_DATA    = 6 ,
    parameter                                   NB_TIMER        = 10 // Nr of bits from frame           
)
(
    // Outputs.
    //output  reg                                 o_deferring ,                         
    // Inputs.
    input   wire                                i_start ,
    input   wire                                i_collision_detected ,
    input   wire                                i_valid ,   // Throughput control.

    input   wire                                i_reset ,
    input   wire                                i_clock
) ;

    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================

    // FSM
    localparam                                  NB_STATE                     = 2  ;
    localparam      [NB_STATE-1:0]              ST_0_IDLE                    = 0  ;
    localparam      [NB_STATE-1:0]              ST_1_WAIT_COUNT              = 1  ;
    localparam      [NB_STATE-1:0]              ST_2_COLLISION_DETECTED      = 2  ;

    // Other        

    localparam      [NB_TIMER-1:0]              LIMIT_T_IPG                  = 10 ;
    localparam      [NB_TIMER-1:0]              LIMIT_T_JAM_SIGNAL           = 20 ;
    localparam      [NB_TIMER-1:0]              LIMIT_T_IPS                  = 30 ;

    localparam                                  BIT_EXT                      = 0 ;
    localparam                                  BIT_EXT_ERROR                = 1 ;

    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
    // FSM
    reg             [NB_STATE-1:0]                  state ;
    reg             [NB_STATE-1:0]                  next_state ;

    reg                                             fsmo_transmit_type ;
    reg                                             fsmo_reset_timer ;
    reg                                             fsmo_timer_limit ;

    //Other

    reg                                             was_transmitting ;
    reg             [NB_TIMER-1:0]                  timer ;

    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    //----------------------------------
    //FSM
    //----------------------------------

    // State update.
    always @( posedge i_clock )
    begin
        if ( i_reset )
            state <= ST_0_WAIT_CHANNEL_BUSY ;
        else if ( i_valid )
            state <= next_state ;
    end

    // Calculate next state and FSM outputs.
    always @( * )
    begin
        fsmo_transmit_type = 1'b0;
        fsmo_reset_timer   = 1'b0 ;
        fsmo_timer_limit   = LIMIT_T_IPS;

        case ( state )
            ST_0_IDLE :
            begin
                casez ( {i_start, time_out, i_collision_detected} )
                    3'b1??  : state_next  = ST_1_WAIT_COUNT ;
                    default : state_next  = ST_0_IDLE ;
                endcase
                fsmo_transmit_type = BIT_EXT;
                fsmo_reset_timer   = i_start ;
                fsmo_timer_limit   = LIMIT_T_IPS;
            end

            ST_1_WAIT_COUNT :
            begin
                casez ( {i_start, time_out, i_collision_detected} )
                    3'b?10  : state_next  = ST_2_COLLISION_DETECTED ;
                    3'b??1  : state_next  = ST_0_IDLE ;
                    default : state_next  = ST_1_WAIT_COUNT ;
                endcase
                fsmo_transmit_type = BIT_EXT ;
                fsmo_reset_timer   = i_collision_detected ;
                fsmo_timer_limit   = LIMIT_T_IPG;
            end

            ST_2_COLLISION_DETECTED :
            begin
                casez ( {i_start, time_out, i_collision_detected} )
                    3'b?1?  : state_next  = ST_0_IDLE ;
                    default : state_next  = ST_2_COLLISION_DETECTED ;
                endcase
                fsmo_transmit_type = BIT_EXT_ERROR ;
                fsmo_reset_timer   = 1'b0 ;
                fsmo_timer_limit   = LIMIT_T_JAM_SIGNAL ;
            end
        endcase
    end


    //----------------------------------
    //OTHER LOGIC
    //----------------------------------

    // Timer logic
    always @( posedge i_clock )
    begin
        if ( i_reset || i_valid && fsmo_reset_timer || time_out )
            timer <= {NB_TIMER{1'b0}} ;
        else if ( i_valid && !time_out )
            timer <= timer + 1'b1 ;
    end

    assign time_out = (timer >= (fsmo_timer_limit-1'b1));

endmodule