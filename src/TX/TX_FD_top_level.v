module TX_FD_top_level
  #(
    parameter NB_DATA                = 64,
    parameter LOG2_NB_DATA           = 6,
    parameter NB_ADDRESS             = 48,
    parameter NB_LENTYP              = 16 ,
    parameter LOG2_NB_LENTYP         = 4 ,
    parameter MIN_LENGTH_FRAME_BYTES = 64,
    parameter MAX_LENGTH_FRAME_BYTES = 1982,
    parameter NB_CRC32               = 32,
    parameter NB_TIMER               = 10,
    parameter PREAMBLE               = 56'h55555555555555,
    parameter NB_PREAMBLE            = 56,
    parameter SFD                    = 8'hD5,
    parameter NB_SFD                 = 8,
    parameter CRC32_POLYNOMIAL       = 33'h104C11DB7,
    parameter MIN_FRAME_SIZE_BITS    = 512,
    parameter IPG_BITS               = 96,
    parameter IPG_STRETCH_BITS       = 0
    )
   (
    output wire [NB_DATA-1:0]      o_data,
    output wire                    o_data_valid,
    output wire [2-1:0]            o_tx_status,
    output wire [LOG2_NB_DATA-1:0] o_tail_size,

    input wire                     i_MA_DATA_request,
    input wire [NB_ADDRESS-1:0]    i_destination_address,
    input wire [NB_ADDRESS-1:0]    i_source_address,
    input wire [NB_LENTYP-1:0]     i_lentyp,
    input wire [NB_DATA-1:0]       i_client_data,
    input wire [NB_CRC32-1:0]      i_fcs,
    input wire                     i_fcs_present,
    input wire                     i_fcs_ready,
    input wire                     i_enable_deferring,

    input wire                     i_valid,
    input wire                     i_reset,
    input wire                     i_clock
    ) ;

   wire                         deferring_def_transmitter;
   wire                         transmitter_transmitting_deferring;
   wire [NB_DATA-1:0]           encap_data_transmitter;
   wire                         encap_dataready_transmitter;

   wire [LOG2_NB_DATA-1:0]      tailsize_to_lengthcalc;
   reg                          datavalid_d;
   wire                         datavalid_pos;

   assign o_data_valid = transmitter_transmitting_deferring;
   assign tailsize_to_lengthcalc = i_lentyp*8%NB_DATA;

   always @(posedge i_clock)
     if (i_reset)
       datavalid_d <= 1'b0;
     else if (i_valid)
       datavalid_d <= i_MA_DATA_request;

   assign datavalid_pos = i_MA_DATA_request & ~datavalid_d;

   frame_transmitter
     #(
       .NB_DATA                (NB_DATA                            ),
       .LOG2_NB_DATA           (LOG2_NB_DATA                       )
       )
   u_frame_transmitter
      (
      .o_data                  (o_data                             ),
      .o_transmitting          (transmitter_transmitting_deferring ),
      .o_transmission_done     (o_tx_status[0]                     ),
      .i_data                  (encap_data_transmitter             ),
      .i_frame_ready           (encap_dataready_transmitter        ),
      .i_deferring             (deferring_def_transmitter          ),
      .i_enable_deferring      (i_enable_deferring                 ),
      .i_clock                 (i_clock                            ),
      .i_valid                 (i_valid                            ),
      .i_reset                 (i_reset                            )
       ) ;

   transmit_data_encapsulation
     #(
       .NB_DATA                (NB_DATA                            ),
       .LOG2_NB_DATA           (LOG2_NB_DATA                       ),
       .NB_ADDRESS             (NB_ADDRESS                         ),
       .NB_LENTYP              (NB_LENTYP                          ),
       .LOG2_NB_LENTYP         (LOG2_NB_LENTYP                     ),
       .MIN_LENGTH_FRAME_BYTES (MIN_LENGTH_FRAME_BYTES             ),
       .MAX_LENGTH_FRAME_BYTES (MAX_LENGTH_FRAME_BYTES             ),
       .NB_CRC32               (NB_CRC32                           ),
       .NB_TIMER               (NB_TIMER                           ),
       .PREAMBLE               (PREAMBLE                           ),
       .NB_PREAMBLE            (NB_PREAMBLE                        ),
       .SFD                    (SFD                                ),
       .NB_SFD                 (NB_SFD                             ),
       .CRC32_POLYNOMIAL       (CRC32_POLYNOMIAL                   )
       )
   u_transmit_data_encapsulation
     (
      .o_data                  (encap_data_transmitter             ),
      .o_data_ready            (encap_dataready_transmitter        ),
      .o_tail_size             (o_tail_size                        ),
      .i_data_valid            (i_MA_DATA_request                  ),
      .i_destination_address   (i_destination_address              ),
      .i_source_address        (i_source_address                   ),
      .i_lentyp                (i_lentyp                           ),
      .i_client_data           (i_client_data                      ),
      .i_fcs                   (i_fcs                              ),
      .i_fcs_present           (i_fcs_present                      ),
      .i_fcs_ready             (i_fcs_ready                        ),
      .i_valid                 (i_valid                            ),
      .i_reset                 (i_reset                            ),
      .i_clock                 (i_clock                            )
      );

   deferring_full_duplex
     #(
       .NB_DATA                (NB_DATA                            ),
       .LOG2_NB_DATA           (LOG2_NB_DATA                       ),
       .NB_TIMER               (NB_TIMER                           ),
       .IPG_BITS               (IPG_BITS                           ),
       .IPG_STRETCH_BITS       (IPG_STRETCH_BITS                   )
       )
   u_deferring_full_duplex
     (
      .o_deferring             (deferring_def_transmitter          ),
      .i_transmitting          (transmitter_transmitting_deferring ),
      .i_valid                 (i_valid                            ),
      .i_reset                 (i_reset                            ),
      .i_clock                 (i_clock                            )
      );

   length_calculator
     #(
       .NB_DATA               (NB_DATA              ),
       .LOG2_NB_DATA          (LOG2_NB_DATA         ),
       .NB_TIMER              (NB_TIMER             ),
       .NBYTES_MAX_FRAME_SIZE (MAX_LENGTH_FRAME_BYTES),
       .NB_LENTYP             (NB_LENTYP            ),
       .LOG2_MAX_FRAME_SIZE   (16                   ),
       .FULL_FRAME_COMPARISON (0                    )
       )

   u_length_calculator
     (
       .o_frame_size_found    (/*NOT CONNECTED*/      ),
       .o_frame_size          (/*NOT CONNECTED*/      ),
       .o_frame_too_long      (/*NOT CONNECTED*/      ),
       .o_dv_missmatch        (o_tx_status[1]         ),
       .i_rdv                 (i_MA_DATA_request      ),
       .i_shift               ('b0                    ),
       .i_tail_size           (tailsize_to_lengthcalc ),
       .i_lentyp              (i_lentyp               ),
       .i_lentyp_valid        (datavalid_pos          ),
       .i_reset               (i_reset                ),
       .i_valid               (i_valid                ),
       .i_clock               (i_clock                )
       );


endmodule
