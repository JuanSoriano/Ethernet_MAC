module frame_transmitter
  #(
    parameter NB_DATA = 64,
    parameter LOG2_NB_DATA = 6
    )
   (
    output wire [NB_DATA-1:0] o_data ,
    output wire               o_transmitting ,
    output wire               o_transmission_done,

    input wire [NB_DATA-1:0]  i_data,
    input wire                i_frame_ready,
    input wire                i_deferring,
    input wire                i_enable_deferring,

    input wire                i_clock,
    input wire                i_valid,
    input wire                i_reset
    ) ;

   reg                        transmitting_reg ;

   assign o_transmitting = (i_enable_deferring) ? (i_frame_ready & ~i_deferring) : (i_frame_ready);

   assign o_data = (o_transmitting) ? i_data : {NB_DATA{1'b0}};

   always @ ( posedge i_clock )
     begin
        if (i_reset)
          transmitting_reg <= 1'b0;
        else if (i_valid)
          transmitting_reg <= o_transmitting;
     end

   assign o_transmission_done = ~o_transmitting & transmitting_reg ;


/*
   assign o_transmitting = 1'b1;

   assign o_data = i_data;

   always @ ( posedge i_clock )
     begin
        if (i_reset)
          transmitting_reg <= 1'b0;
        else if (i_valid)
          transmitting_reg <= o_transmitting;
     end

   assign o_transmission_done = 1'b1;
*/
endmodule
