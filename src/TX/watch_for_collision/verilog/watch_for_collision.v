module watch_for_collision
#(
    // Parameters.
    parameter                                   NB_DATA         = 64 ,
    parameter                                   LOG2_NB_DATA    = 6 ,        
)
(
    // Outputs.
    output  wire                                o_clear_transmitsucceeding ,
    output  wire                                o_set_newcollision ,
    output  wire                                o_set_latecollisionerror ,
    output  wire                                o_clear_bursting ,
    // Inputs.
    input   wire                                i_transmit_succeeding ,
    input   wire                                i_collision_detect ,
    input   wire                                i_currenttransmitbit ,
    input   wire                                i_slottime_minus_headersize ,
    input   wire                                i_burst_mode ,
    input   wire                                i_burst_start ,
    input   wire                                i_valid ,   // Throughput control.

    input   wire                                i_reset ,
    input   wire                                i_clock
) ;

    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================

    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
                                       
    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    assign o_clear_transmitsucceeding = (i_transmit_succeeding && i_collision_detect) ;

    assign o_set_newcollision = o_clear_transmitsucceeding ;

    assign o_set_latecollisionerror = ((i_transmit_succeeding && i_collision_detect) && (i_currenttransmitbit >= i_slottime_minus_headersize)) || (i_burst_mode && (i_burst_start==0)) ;

    assign o_clear_bursting = (i_transmit_succeeding && i_collision_detect) && i_burst_mode ;


endmodule