module deferring_half_duplex
#(
    // Parameters.
    parameter                                   NB_DATA         = 64 ,
    parameter                                   LOG2_NB_DATA    = 6 ,
    parameter                                   NB_TIMER        = 10 // Nr of bits from frame           
)
(
    // Outputs.
    output  reg                                 o_deferring ,                         
    // Inputs.
    input   wire                                i_carrier_sense ,
    input   wire                                i_transmitting ,
    input   wire                                i_frame_waiting ,
    input   wire                                i_valid ,   // Throughput control.

    input   wire                                i_reset ,
    input   wire                                i_clock
) ;

    //==========================================================================
    // LOCAL PARAMETERS.
    //==========================================================================

    // FSM
    localparam                                  NB_STATE                     = 3  ;
    localparam      [NB_STATE-1:0]              ST_0_WAIT_CHANNEL_BUSY       = 0  ;
    localparam      [NB_STATE-1:0]              ST_1_WAIT_CHANNEL_READY      = 1  ;
    localparam      [NB_STATE-1:0]              ST_2_WAIT_A                  = 2  ;
    localparam      [NB_STATE-1:0]              ST_3_WAIT_B                  = 3  ;
    localparam      [NB_STATE-1:0]              ST_4_WAIT_C                  = 4  ;
    localparam      [NB_STATE-1:0]              ST_5_WAIT_FRAME_TRANSMITTED  = 5  ;

    // Other        
    localparam      [NB_TIMER-1:0]              LIMIT_T_WAIT_A               = 10 ;
    localparam      [NB_TIMER-1:0]              LIMIT_T_WAIT_B               = 20 ;
    localparam      [NB_TIMER-1:0]              LIMIT_T_WAIT_C               = 30 ;
    //==========================================================================
    // INTERNAL SIGNALS.
    //==========================================================================
    // FSM
    reg             [NB_STATE-1:0]                  state ;
    reg             [NB_STATE-1:0]                  next_state ;

    reg                                             fsmo_set_deferring ;
    reg                                             fsmo_set_wastransmitting ;
    reg                                             fsmo_reset_timer ;
    reg                                             fsmo_clear_deferring ;
    reg                                             fsmo_timer_limit ;

    //Other

    reg                                             was_transmitting ;
    reg             [NB_TIMER-1:0]                  timer ;

    //==========================================================================
    // ALGORITHM.
    //==========================================================================

    //----------------------------------
    //FSM
    //----------------------------------

    // State update.
    always @( posedge i_clock )
    begin
        if ( i_reset )
            state <= ST_0_WAIT_CHANNEL_BUSY ;
        else if ( i_valid )
            state <= next_state ;
    end

    // Calculate next state and FSM outputs.
    always @( * )
    begin
        fsmo_set_deferring          = 1'b0 ;
        fsmo_set_wastransmitting    = 1'b0 ;
        fsmo_reset_timer            = 1'b0 ;
        fsmo_clear_deferring        = 1'b0 ;
        fsmo_timer_limit            = LIMIT_T_WAIT_A ;

        case ( state )
            ST_0_WAIT_CHANNEL_BUSY :
            begin
                casez ( {i_carrier_sense, i_transmitting, was_transmitting, time_out, i_frame_waiting} )
                    5'b1????: state_next  = ST_1_WAIT_CHANNEL_READY ;
                    default : state_next  = ST_0_WAIT_CHANNEL_BUSY ;
                endcase
                fsmo_set_deferring          = i_carrier_sense ;
                fsmo_set_wastransmitting    = 1'b0 ;
                fsmo_reset_timer            = 1'b0 ;
                fsmo_clear_deferring        = 1'b0 ;
            end

            ST_1_WAIT_CHANNEL_READY :
            begin
                casez ( {i_carrier_sense, i_transmitting, was_transmitting, time_out, i_frame_waiting} )
                    5'b001??: state_next  = ST_2_WAIT_A ;
                    5'b000??: state_next  = ST_3_WAIT_B ;
                    default : state_next  = ST_1_WAIT_CHANNEL_READY ;
                endcase
                fsmo_set_deferring          = 1'b1 ;
                fsmo_set_wastransmitting    = 1'b1 ;
                fsmo_reset_timer            = 1'b0 ;
            end

            ST_2_WAIT_A :
            begin
                casez ( {i_carrier_sense, i_transmitting, was_transmitting, time_out, i_frame_waiting} )
                    5'b???1?: state_next  = ST_4_WAIT_C ;
                    default : state_next  = ST_2_WAIT_A ;
                endcase
                fsmo_reset_timer            = 1'b0 ;
                fsmo_timer_limit            = LIMIT_T_WAIT_A ;
            end

            ST_3_WAIT_B :
            begin
                casez ( {i_carrier_sense, i_transmitting, was_transmitting, time_out, i_frame_waiting} )
                    5'b???1?: state_next  = ST_4_WAIT_C ;
                    default : state_next  = ST_3_WAIT_B ;
                endcase
                fsmo_reset_timer            = i_carrier_sense ;
                fsmo_timer_limit            = LIMIT_T_WAIT_B ;
            end

            ST_4_WAIT_C :
            begin
                casez ( {i_carrier_sense, i_transmitting, was_transmitting, time_out, i_frame_waiting} )
                    5'b???1?: state_next  = ST_5_WAIT_FRAME_TRANSMITTED ;
                    default : state_next  = ST_4_WAIT_C ;
                endcase
                fsmo_timer_limit            = LIMIT_T_WAIT_C ;
            end

            ST_5_WAIT_FRAME_TRANSMITTED :
            begin
                casez ( {i_carrier_sense, i_transmitting, was_transmitting, time_out, i_frame_waiting} )
                    5'b????0: state_next  = ST_0_WAIT_CHANNEL_BUSY ;
                    default : state_next  = ST_5_WAIT_FRAME_TRANSMITTED ;
                endcase
                fsmo_clear_deferring        = 1'b1 ;
            end
        endcase
    end


    //----------------------------------
    //OTHER LOGIC
    //----------------------------------

    // Timer logic
    always @( posedge i_clock )
    begin
        if ( i_reset || i_valid && fsmo_reset_timer || time_out )
            timer <= {NB_TIMER{1'b0}} ;
        else if ( i_valid && !time_out )
            timer <= timer + 1'b1 ;
    end

    assign time_out = (timer >= (fsmo_timer_limit-1'b1));

    // O_deferring logic
    always @( posedge i_clock )
    begin
        if ( i_reset || i_valid && fsmo_clear_deferring )
            o_deferring <= 1'b0 ;
        else if ( i_valid && fsmo_set_deferring )
            o_deferring <= 1'b1 ;
    end

    // Set was_transmitting
    always @( posedge i_clock )
    begin
        if ( i_reset )
            was_transmitting <= 1'b0 ;
        else if ( i_valid )
            was_transmitting <= fsmo_set_wastransmitting ;

    end

endmodule