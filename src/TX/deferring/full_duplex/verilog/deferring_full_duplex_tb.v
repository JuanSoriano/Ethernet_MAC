module deferring_full_duplex_tb ();

   localparam                                   NB_DATA         = 64 ;
   localparam                                   LOG2_NB_DATA    = 6 ;
   localparam                                   NB_TIMER        = 2 ;

   wire tb_deferring_o ;

   wire tb_transmitting_i ;

   wire valid ;
   wire reset ;
   reg  clock = 1'b0;

   integer timer = 0;

   always
     begin
        #(50) clock = ~clock ;
     end

   always @ ( posedge clock )
     begin
        timer   <= timer + 1;
     end

   assign reset = (timer == 2) ; // Reset at time 2
   assign valid = 1'b1;

   assign tb_transmitting_i = (timer == 3) && (timer <= 10);

   deferring_full_duplex
     #(
       .NB_DATA       (NB_DATA           ),
       .LOG2_NB_DATA  (LOG2_NB_DATA      ),
       .NB_TIMER      (NB_TIMER          )
       )
   u_deferring_full_duplex
      (
      .o_deferring    (tb_deferring_o    ),
      .i_transmitting (tb_transmitting_i ),
      .i_valid        (valid             ),
      .i_reset        (reset             ),
      .i_clock        (clock             )
      );

endmodule
