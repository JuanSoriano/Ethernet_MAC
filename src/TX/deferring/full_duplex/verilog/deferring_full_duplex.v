module deferring_full_duplex
  #(
    // Parameters.
    parameter                                   NB_DATA          = 64 ,
    parameter                                   LOG2_NB_DATA     = 6 ,
    parameter                                   NB_TIMER         = 2 ,
    parameter                                   IPG_BITS         = 96,
    parameter                                   IPG_STRETCH_BITS = 0
    )
   (
    // Outputs.
    output wire o_deferring ,
    // Inputs.
    input wire i_transmitting ,
    input wire i_valid , // Throughput control.

    input wire i_reset ,
    input wire i_clock
    ) ;

   //==========================================================================
   // LOCAL PARAMETERS.
   //==========================================================================

   // FSM
   localparam                                  NB_STATE                     = 2  ;
   localparam      [NB_STATE-1:0]              ST_0_WAIT_TRANSMITTING       = 0  ;
   localparam      [NB_STATE-1:0]              ST_1_WAIT_END_TRANSMISION    = 1  ;
   localparam      [NB_STATE-1:0]              ST_2_WAIT_IPG_EXT            = 2  ;

   localparam      [NB_TIMER-1:0]              LIMIT_T_IPG_EXT              = ((IPG_BITS+IPG_STRETCH_BITS)/NB_DATA) + ((IPG_BITS+IPG_STRETCH_BITS)%NB_DATA != 0);

   //==========================================================================
   // INTERNAL SIGNALS.
   //==========================================================================
   // FSM
   reg [NB_STATE-1:0] state ;
   reg [NB_STATE-1:0] next_state ;

   reg                fsmo_set_deferring ;
   //reg               fsmo_clear_deferring ;
   reg                fsmo_reset_timer ;
   reg [NB_TIMER-1:0] fsmo_timer_limit ;

   //Other
   reg [NB_TIMER-1:0] timer ;
   wire               time_out;
   reg                transmitting_reg;
   wire               transmitting_pos;
   wire               transmitting_neg;

   //==========================================================================
   // ALGORITHM.
   //==========================================================================

   //----------------------------------
   //FSM
   //----------------------------------

   // State update.
   always @( posedge i_clock )
     begin
        if ( i_reset )
          state <= ST_0_WAIT_TRANSMITTING ;
        else if ( i_valid )
          state <= next_state ;
     end

   // Calculate next state and FSM outputs.
   always @( * )
     begin
        fsmo_set_deferring          = 1'b0 ;
        //fsmo_clear_deferring        = 1'b0 ;
		    fsmo_reset_timer            = 1'b0 ;
		    fsmo_timer_limit            = LIMIT_T_IPG_EXT ;

        case ( state )
          ST_0_WAIT_TRANSMITTING :
            begin
               casez ( {transmitting_pos, transmitting_neg, time_out} )
                 3'b1??  : next_state  = ST_1_WAIT_END_TRANSMISION ;
                 default : next_state  = ST_0_WAIT_TRANSMITTING ;
               endcase
        		   fsmo_set_deferring          = transmitting_pos ;
        		   //fsmo_clear_deferring        = 1'b1 ;
				       fsmo_reset_timer            = 1'b0 ;
				       fsmo_timer_limit            = LIMIT_T_IPG_EXT ;
            end

          ST_1_WAIT_END_TRANSMISION :
            begin
               casez ( {transmitting_pos, transmitting_neg, time_out} )
                 3'b?1?  : next_state      = ST_2_WAIT_IPG_EXT ;
                 default : next_state      = ST_1_WAIT_END_TRANSMISION ;
               endcase
        		   fsmo_set_deferring          = 1'b1 ;
        		   //fsmo_clear_deferring        = 1'b0 ;
				       fsmo_reset_timer            = transmitting_neg ;
				       fsmo_timer_limit            = LIMIT_T_IPG_EXT ;
            end

          ST_2_WAIT_IPG_EXT :
            begin
               casez ( {transmitting_pos, transmitting_neg, time_out} )
                 3'b??1  : next_state      = ST_0_WAIT_TRANSMITTING ;
                 default : next_state      = ST_2_WAIT_IPG_EXT ;
               endcase
        		   fsmo_set_deferring          = !time_out ;
        		   //fsmo_clear_deferring        = time_out ;
				       fsmo_reset_timer            = 1'b0 ;
				       fsmo_timer_limit            = LIMIT_T_IPG_EXT ;
            end
        endcase
     end

   //----------------------------------
   //OTHER LOGIC
   //----------------------------------

   // Timer logic
   always @( posedge i_clock )
     begin
        if ( i_reset || i_valid && fsmo_reset_timer || i_valid && time_out )
          timer <= {NB_TIMER{1'b0}} ;
        else if ( i_valid && !time_out )
          timer <= timer + 1'b1 ;
     end

   assign time_out = (timer >= (fsmo_timer_limit-1'b1));

   always @(posedge i_clock)
     begin
        if (i_reset)
          transmitting_reg <= 1'b0;
        else if (i_valid)
          transmitting_reg <= i_transmitting;
     end

   assign transmitting_pos = i_transmitting & ~transmitting_reg;
   assign transmitting_neg = ~i_transmitting & transmitting_reg;

   assign o_deferring = (fsmo_set_deferring) ? 1'b1 : 1'b0;

endmodule
