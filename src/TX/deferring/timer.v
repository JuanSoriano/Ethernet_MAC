

    always @(*)
	begin
		fsmo_load_dst_addr	= 1'b0 ;
		fsmo_load_
		case ( state )
			ST_0_IDLE:
			begin
				casez( {i_valid, timer_done} )
				   2'b1?	: state_next	= ST_1_GET_DST_ADDR ;
				   default	: state_next	= ST_0_IDLE ;
			    endcase
			    fsmo_load_dst_addr	= i_valid ;
			    fsmo_timer_limit    = LIMIT_T_DST_ADDR ;
			    fsmo_reset_timer	= 1'b1 ; // o ~i_valid?
			    ...
			end
			ST_1_GET_DST_ADDR:
			begin
				casez( {i_valid, timer_done} )
				   2'b?1	: state_next	= ST_2_CHECK_DST_ADDR ;
				   default	: state_next	= ST_0_IDLE ;
			    endcase
			    fsmo_load_dst_addr	= i_valid ;
			    fsmo_timer_limit    = LIMIT_T_DST_ADDR ;
			    fsmo_reset_timer	= timer_done ; // o ~i_valid?
			    ...
			end
			ST_2_GET_SRC_ADDR:
			begin
				casez( {i_valid, timer_done, dst_addr_ok} )
				   3'b??1   : state_next	= ST_0_IDLE ;
				   2'b?1	: state_next	= ST_2_CHECK_DST_ADDR ;
				   default	: state_next	= ST_0_IDLE ;
			    endcase
			    fsmo_load_dst_addr	= i_valid ;
			    fsmo_timer_limit    = LIMIT_T_SRC_ADDR ;
			    fsmo_reset_timer	= 1'b1 ; // o ~i_valid?
			    ...
			end
		endcase
   	end



   	always @( posedge i_clock )
   	begin
   		if ( i_reset || fsmo_reset_timer )
   			timer
   				<= 0 ;
		else if ( i_valid )
			timer
				<= ( timer_done )? 0 : timer+1'b1 ;
   	end
   	assign timer_done = timer >= (fsmo_timer_limit-1'b1);