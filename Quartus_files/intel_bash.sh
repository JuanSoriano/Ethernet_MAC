#export INTEL_PATH=/opt/intelFPGA/lite_17.1/
#export INTEL_PATH=/opt/intelFPGA/lite_16.1
#export INTEL_PATH=/opt/intelFPGA/lite_16.0

export INTEL_PATH=/opt/QuartusPrimeLite

#export QUARTUS_ROOTDIR=$INTEL_PATH/lite_16.1/quartus
export QUARTUS_ROOTDIR=$INTEL_PATH/quartus


export QUARTUS_ROOTDIR_OVERRIDE=$QUARTUS_ROOTDIR

export QSYS_ROOTDIR=$QUARTUS_ROOTDIR/sopc_builder/bin

export DSP_BUILDER_ROOT=$QUARTUS_ROOTDIR/dsp_builder

export QUARTUS_SOFT=$QUARTUS_ROOTDIR/bin:$DSP_BUILDER_ROOT:$QSYS_ROOTDIR:$INTEL_PATH/modelsim_ase/bin

#env_var_prepend "${QUARTUS_ROOTDIR}/bin64"

#
# Setup Nios2eds (if you can find it)
#

export SOPC_KIT_NIOS2=$INTEL_PATH/nios2eds

export NIOS2_SOFT=$SOPC_KIT_NIOS2/bin:$SOPC_KIT_NIOS2/sdk2/bin:$SOPC_KIT_NIOS2/bin/gnu/H-x86_64-pc-linux-gnu/bin		  


#
# SoCEDS Environment setup
#
 
export SOCEDS_DEST_ROOT="/opt/intelFPGA/eds_16.1/embedded"
export SOCEDS_SOFT=$SOCEDS_DEST_ROOT/host_tools/gnu/dtc:$SOCEDS_DEST_ROOT/host_tools/altera/secureboot:$SOCEDS_DEST_ROOT/host_tools/altera/imagecat:$SOCEDS_DEST_ROOT/host_tools/altera/diskutils:$SOCEDS_DEST_ROOT/host_tools/altera/device_tree:$SOCEDS_DEST_ROOT/host_tools/altera/mkpimage:$SOCEDS_DEST_ROOT/host_tools/altera/mkimage:$SOCEDS_DEST_ROOT/host_tools/altera/preloadergen:$SOCEDS_DEST_ROOT/host_tools/mentor/gnu/arm/baremetal/bin



#export DS5_ROOT=$INTEL_PATH/DS-5_v5.25.0
#export D5_SOFT=$DS5_ROOT/bin:$DS5_ROOT/sw/ARMCompiler5.06u3/bin:$DS5_ROOT/sw/gcc/bin



export LM_LICENSE_FILE=1800@localhost
export QUARTUS_64BIT=1
export MWARCH=x86_64
export MWOS=linux



#################################################################################

export PATH=$PATH:$QUARTUS_SOFT:$NIOS2_SOFT:$SOCEDS_SOFT:$D5_SOFT:$QSYS_ROOTDIR
#################################################################################
